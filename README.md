# Dnes uvařím

Dnes uvařím is an application for managing recipes.

This web application allows users to add their recipes, browse recipes from the database, add to favorites, plan meals and create shopping lists.

Running version is available here: https://www.dnesuvarim.cz/

## Requirements

To be able to fully run this application, several ENV variables are needed. In \server is provided an example file with the variables.

You will need to create a variables.env and fill in the values.

You can keep *_URL values from the example file if you want to run this application only locally.

EMAIL_* variables are needed for option to sent emails to reset passwords. Otherwise, are not necessary, but have to be defined.

NODE_ENV can have value development or production.

### PSQL Database

Run the SQL scripts in \server\populateDB to create the database with sample data. Then fill in
- *PG_USER (your username for the database)
- *PG_PASSWORD (your password for the database)
- *PG_DATABASE (database name)
- *PG_HOST (database host name)
- *PG_PORT (port for connecting to the database)

The app will not work correctly without a connected database!

## How to run

In both \server and \client folder run
```
npm install
```

### How to run in development mode

This app is split into client and server. In dev mode, they have to be run separately.

The server can be started on port 3001 from /server folder with the command:
```
npm run dev-start
```

Client on the port 3000 from /client folder:
```
npm start
```

You can then access the frontend on http://localhost:3000/

### How to run the build

Create a build by running this command in \client folder:
```
npm run build
```

Move the build into the /server folder and run the following command from there:
```
npm run dev-start
```
or
```
npm start
```

You can then access the frontend on http://localhost:3001/

## Language

Default language for this application is Czech, but it can be changed to English in /client/src/localization/config.tsx by setting:
```
lng: "en"
```

Or a button for switching languages can be added to UI by changing DEBUG_LANGUAGES to true in /client/src/utils/utils.tsx.