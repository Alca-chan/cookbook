import React from 'react';
import './App.css';
import {ThemeProvider} from '@mui/material/styles';
import {UserContext} from "./contexts/UserContext";
import {useLoggedInUserInfo} from "./contexts/UserInfo";
import {useCartInfo} from "./contexts/CartInfo";
import {CartContext} from "./contexts/CartContext";
import {FavoritesContext} from "./contexts/FavoritesContext";
import {useFavoritesInfo} from "./contexts/FavoritesInfo";
import {CalendarContext} from "./contexts/CalendarContext";
import {useCalendarInfo} from "./contexts/CalendarInfo";
import {StaticDataContext} from "./contexts/StaticDataContext";
import {useStaticData} from "./contexts/StaticData";
import {AppContent} from "./AppContent";
import {muiTheme} from "./utils/makeStyles";
import createCache from "@emotion/cache";
import {CacheProvider} from "@emotion/react";

export const muiCache = createCache({
  "key": "mui",
  "prepend": true,
})

export const APP_NAME = "Dnes uvařím"

const App = () => {
  const {user, updateUser, logoutUser} = useLoggedInUserInfo()
  const cartInfo = useCartInfo(user)
  const favoritesInfo = useFavoritesInfo(user)
  const calendarInfo = useCalendarInfo(user)
  const staticData = useStaticData()

  return (
    <StaticDataContext.Provider value={staticData}>
      <UserContext.Provider value={{user, updateUser, logoutUser}}>
        <FavoritesContext.Provider value={favoritesInfo}>
          <CalendarContext.Provider value={calendarInfo}>
            <CartContext.Provider value={cartInfo}>
              <CacheProvider value={muiCache}>
                <ThemeProvider theme={muiTheme}>
                  <AppContent/>
                </ThemeProvider>
              </CacheProvider>
            </CartContext.Provider>
          </CalendarContext.Provider>
        </FavoritesContext.Provider>
      </UserContext.Provider>
    </StaticDataContext.Provider>
  )
}

export default App
