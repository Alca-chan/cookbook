import React, {useContext} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch,} from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import {CircularProgress, CssBaseline, darken} from '@mui/material';
import {ScrollToTop} from "./components/ScrollToTop";
import {NavigationBar} from "./components/NavigationBar/NavigationBar";
import {useTranslation} from "react-i18next";
import {getRoute, getRoutes} from "./utils/routes";
import {RedirectingRoute} from "./components/RedirectingRoute";
import {Toast} from "./components/Toast";
import {UserContext} from "./contexts/UserContext";
import {makeStyles} from "./utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  background: {
    [theme.breakpoints.up('md')]: {
      marginRight: theme.spacing(4),
      marginLeft: theme.spacing(4),
    },
    [theme.breakpoints.up('lg')]: {
      marginRight: theme.spacing(12),
      marginLeft: theme.spacing(12),
    },
    [theme.breakpoints.up('xl')]: {
      marginRight: theme.spacing(14),
      marginLeft: theme.spacing(14),
    },
    background: theme.palette.appBackground.inner,
    borderBottomLeftRadius: '10px',
    borderBottomRightRadius: '10px'
  },
  content: {
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      marginRight: theme.spacing(2),
      marginLeft: theme.spacing(2),
    },
    marginBottom: theme.spacing(2),
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(6)
  },
  scrollbar: {
    '@global': {
      '*::-webkit-scrollbar': {
        width: '12px',
        height: '12px',
        [theme.breakpoints.down('xs')]: {
          width: '16px',
          height: '16px'
        }
      },
      '*::-webkit-scrollbar-track': {
        backgroundColor: darken(theme.palette.background.paper, 0.1),
        boxShadow: 'inset 0 0 2px grey',
        webkitBoxShadow: 'inset 0 0 2px grey',
        borderRadius: '10px'
      },
      '*::-webkit-scrollbar-thumb': {
        backgroundColor: darken(theme.palette.background.paper, 0.2),
        boxShadow: 'inset 0 0 2px grey',
        webkitBoxShadow: 'inset 0 0 2px grey',
        borderRadius: '10px',
      }
    }
  }
}))

export const AppContent = () => {
  const {classes} = useStyles()
  const {user} = useContext(UserContext)
  const {t, i18n} = useTranslation()
  const routes = getRoutes(t, user)

  return (
    <Router>
      <Toast/>
      <AppBar color={'secondary'} position={'static'} elevation={0}>
        <NavigationBar/>
      </AppBar>
      <main className='App'>
        <CssBaseline/>
        <div className={classes.background + ' ' + classes.scrollbar}>
          <div className={classes.content}>
            {user === undefined ? (
              <CircularProgress/>
            ) : (
              <Switch>
                {
                  routes.map((route, index) =>
                    route.authenticated ?
                      <Route path={route.path} exact component={route.component} key={index}/>
                      :
                      <RedirectingRoute path={route.path} redirectPath={getRoute(i18n.language, "/login")}
                                        key={index}/>
                  )
                }
              </Switch>
            )}
          </div>
        </div>
        <ScrollToTop/>
      </main>
    </Router>
  )
}
