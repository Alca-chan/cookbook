import axios, {CancelToken} from "axios";
import {axiosArray, calendarDBToCalendar, SERVER_URL} from "./utils";
import {Calendar, CalendarDB} from "./types";

export const getUsersCalendar = async (token: CancelToken): Promise<Calendar[]> => {
  return await axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/user/calendar"
  }).then((data) => data.map((c: CalendarDB) => calendarDBToCalendar(c)))
}

export const getCalendarByUser = async (token: CancelToken, userId: string): Promise<Calendar[]> => {
  return await axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/calendar/" + userId
  }).then((data) => data.map((c: CalendarDB) => calendarDBToCalendar(c)))
}

export const editRecipeInCalendar = async (id: string, recipeId: string, date: string, portions: number, order: number, title: string) => {
  await axios({
    method: "PUT",
    data: {
      id: id,
      recipeId: recipeId,
      date: date,
      portions: portions,
      order: order,
      title: title
    },
    withCredentials: true,
    url: SERVER_URL + "/calendar"
  })
}

export const moveDayInCalendar = async (ids: string[], date: string) => {
  await axios({
    method: "PUT",
    data: {
      ids: ids,
      date: date
    },
    withCredentials: true,
    url: SERVER_URL + "/calendar/move"
  })
}

export const updateCalendarOrder = async (changed: { id: string, order: number }[]) => {
  await axios({
    method: "PUT",
    data: {
      changed: changed
    },
    withCredentials: true,
    url: SERVER_URL + "/calendar/order"
  })
}

export const addRecipeToCalendar = async (recipeId: string, date: string, portions: number, order: number, title: string) => {
  await axios({
    method: "POST",
    data: {
      recipeId: recipeId,
      date: date,
      portions: portions,
      order: order,
      title: title
    },
    withCredentials: true,
    url: SERVER_URL + "/calendar"
  })
}

export const removeRecipeFromCalendar = async (ids: string[]) => {
  await axios({
    method: "DELETE",
    data: {
      ids: ids
    },
    withCredentials: true,
    url: SERVER_URL + "/calendar"
  })
}