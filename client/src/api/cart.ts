import axios, {CancelToken} from "axios";
import {axiosArray, cartItemDBToCartItemShort, SERVER_URL} from "./utils";
import {CartItem, CartItemDB, CartItemToAdd} from "./types";

export const getUsersCart = (token: CancelToken): Promise<CartItem[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/user/cart"
  }).then((data) => data.map((item: CartItemDB) => cartItemDBToCartItemShort(item)))
}

export const getCartByUser = (token: CancelToken, userId: string): Promise<CartItem[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/cart/" + userId
  }).then((data) => data.map((item: CartItemDB) => cartItemDBToCartItemShort(item)))
}

export const addAllItemsToCart = async (cartItems: CartItemToAdd[], date?: string) => {
  if (cartItems.length < 1) return
  await axios({
    method: "POST",
    data: {
      items: cartItems,
      date: date ?? null
    },
    withCredentials: true,
    url: SERVER_URL + "/cart/all"
  })
}

export const updateItemsInCart = async (cartItems: CartItem[]) => {
  if (cartItems.length < 1) return
  await axios({
    method: "PUT",
    data: {
      items: cartItems
    },
    withCredentials: true,
    url: SERVER_URL + "/cart"
  })
}

export const updateCartDone = async (cartItems: CartItem[], done: boolean) => {
  if (cartItems.length < 1) return
  await axios({
    method: "PUT",
    data: {
      done: done,
      ids: cartItems.map(i => i.id)
    },
    withCredentials: true,
    url: SERVER_URL + "/cart/done"
  })
}

export const updateCartOrder = async (cartItems: { id: string, recipeId: string | null, order: number, date: string, title: string | null }[]) => {
  if (cartItems.length < 1) return
  await axios({
    method: "PUT",
    data: {
      items: cartItems
    },
    withCredentials: true,
    url: SERVER_URL + "/cart/order"
  })
}

export const removeItemsFromCart = async (cartItemIds: string[]) => {
  if (cartItemIds.length < 1) return
  await axios({
    method: "DELETE",
    data: {
      ids: cartItemIds
    },
    withCredentials: true,
    url: SERVER_URL + "/cart"
  })
}

export const removeAllItemsForRecipeFromCart = async (recipeId: string, date: string | null) => {
  if (!recipeId) return
  await axios({
    method: "DELETE",
    data: {
      recipeId: recipeId,
      date: date
    },
    withCredentials: true,
    url: SERVER_URL + "/cart/all"
  })
}