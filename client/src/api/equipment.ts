import {CancelToken} from "axios";
import {Equipment, EquipmentDB, RecipeEquipmentDB} from "./types";
import {axiosArray, axiosObject, equipmentDBToEquipment, SERVER_URL} from "./utils";

export const getEquipment = (token: CancelToken): Promise<Equipment[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/equipment"
  }).then((data) => data.map((equipment: EquipmentDB) => equipmentDBToEquipment(equipment)))
}

export const getEquipmentById = (token: CancelToken, id: string): Promise<Equipment | undefined> => {
  return axiosObject({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/equipment/" + id
  }).then((data) => {
    if (!data || data.length < 1) return undefined
    return equipmentDBToEquipment(data[0])
  })
}

export const getEquipmentForRecipe = (token: CancelToken, recipeId: string, equipments: Equipment[]): Promise<Equipment[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipe/" + recipeId + "/equipment"
  }).then(async (data: RecipeEquipmentDB[]) => {
    const recipeEquipments: Equipment[] = []
    data.forEach((recipeEquipment) => {
      const equipment = equipments.find(e => e.id === recipeEquipment.equipment_id)
      if (equipment) recipeEquipments.push(equipment)
    })
    return recipeEquipments
  })
}