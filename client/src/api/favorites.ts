import axios, {CancelToken} from "axios";
import {axiosArray, favoritesItemDBToFavoriteItem, SERVER_URL} from "./utils";
import {FavoritesItem, FavoritesItemDB} from "./types";

export const getUsersFavorites = async (token: CancelToken): Promise<FavoritesItem[]> => {
  return await axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/user/favorites"
  }).then((data) => data.map((f: FavoritesItemDB) => favoritesItemDBToFavoriteItem(f)))
}

export const getFavoritesByUser = async (token: CancelToken, userId: string): Promise<FavoritesItem[]> => {
  return await axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/favorites/" + userId
  }).then((data) => data.map((f: FavoritesItemDB) => favoritesItemDBToFavoriteItem(f)))
}

export const addRecipeToFavorites = async (recipeId: string, collection?: string): Promise<number | undefined> => {
  return await axios({
    method: "POST",
    data: {
      recipeId: recipeId,
      collection: collection ?? null
    },
    withCredentials: true,
    url: SERVER_URL + "/favorites"
  }).then(({data}) => !data.votes || data.votes < 0 ? 0 : data.votes)
    .catch(() => undefined)
}

export const editRecipeInFavorites = async (recipeId: string, collection?: string) => {
  await axios({
    method: "PUT",
    data: {
      recipeId: recipeId,
      collection: collection ?? null
    },
    withCredentials: true,
    url: SERVER_URL + "/favorites"
  })
}

export const removeRecipeFromFavorites = async (recipeId: string): Promise<number | undefined> => {
  return await axios({
    method: "DELETE",
    data: {
      recipeId: recipeId
    },
    withCredentials: true,
    url: SERVER_URL + "/favorites"
  }).then(({data}) => !data.votes || data.votes < 0 ? 0 : data.votes)
    .catch(() => undefined)
}