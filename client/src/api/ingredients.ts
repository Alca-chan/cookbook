import {CancelToken} from "axios";
import {Ingredient, IngredientDB, IngredientMeasure, IngredientsSection, RecipeIngredientDB, Unit} from "./types";
import {axiosArray, axiosObject, ingredientDBToIngredient, SERVER_URL} from "./utils";

export const getIngredients = (token: CancelToken): Promise<Ingredient[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/ingredients"
  }).then((data) => data.map((ingredient: IngredientDB) => ingredientDBToIngredient(ingredient)))
}

export const getIngredientById = (token: CancelToken, id: string): Promise<Ingredient | undefined> => {
  return axiosObject({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/ingredients/" + id
  }).then((data) => {
    if (!data || data.length < 1) return undefined
    return ingredientDBToIngredient(data[0])
  })
}

const getIngredientFromRecipeIngredient = (recipeIngredientDB: RecipeIngredientDB, ingredients: Ingredient[], units: Unit[]): (IngredientMeasure | undefined) => {
  const ingredient = ingredients.find(i => i.id === recipeIngredientDB.ingredient_id)
  if (!ingredient) return undefined
  const unit = units.find(u => u.id === recipeIngredientDB.unit_id)
  if (!unit) return undefined

  return {
    order: recipeIngredientDB.order,
    recipeIngredientId: recipeIngredientDB.id,
    ingredient: ingredient,
    measure: recipeIngredientDB.measure ?? undefined,
    unit: unit,
    note: recipeIngredientDB.note ?? "",
    section: {title: recipeIngredientDB.section ?? "", order: recipeIngredientDB.section_order}
  }
}

export const getIngredientsForRecipe = (token: CancelToken, recipeId: string, ingredients: Ingredient[], units: Unit[]): Promise<IngredientsSection[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipe/" + recipeId + "/ingredients"
  }).then(async (data: RecipeIngredientDB[]) => {
    const ingredientMeasures: IngredientMeasure[] = []
    data.forEach((recipeIngredientDB) => {
      const ingredient = getIngredientFromRecipeIngredient(recipeIngredientDB, ingredients, units)
      if (ingredient) ingredientMeasures.push(ingredient)
    })
    ingredientMeasures.sort((a, b) => a.order - b.order)

    const ingredientsSections: IngredientsSection[] = []
    ingredientMeasures.forEach((ingredient) => {
      const found = ingredientsSections.find(section => section.title === ingredient.section.title)
      if (found) {
        found.ingredients.push(ingredient)
      } else {
        const newSection: IngredientsSection = {
          order: ingredient.section.order,
          title: ingredient.section.title,
          ingredients: [ingredient]
        }
        ingredientsSections.push(newSection)
      }
    })
    ingredientsSections.sort((a, b) => a.order - b.order)
    return ingredientsSections
  })
}