import axios, {CancelToken} from "axios";
import {
  Equipment,
  Ingredient,
  IngredientsSection,
  Recipe,
  RecipeDB,
  RecipeEquipmentDB,
  RecipeIngredientDB,
  RecipeShort,
  RecipeTagDB,
  Tag,
  Unit
} from "../types";
import {axiosArray, axiosObject, recipeDBToRecipeShort, SERVER_URL} from "../utils";
import {getIngredientsForRecipe} from "../ingredients";
import {getUserById} from "../user";
import {getTagsForRecipe} from "../tags";
import {getEquipmentForRecipe} from "../equipment";

export const getRecipesByUser = (token: CancelToken, id: string): Promise<RecipeShort[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipes/user/" + id
  }).then((data) => data.map((r: RecipeDB) => recipeDBToRecipeShort(r)))
}

export const getRandomRecipes = (token: CancelToken, n: number): Promise<RecipeShort[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipes/random/" + n
  }).then((data) => data.map((r: RecipeDB) => recipeDBToRecipeShort(r)))
}

export const getRecipeShortById = (token: CancelToken, id: string): Promise<RecipeShort | undefined> => {
  return axiosObject({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipes/" + id
  }).then((data) => {
    if (!data || data.length < 1) return undefined
    return recipeDBToRecipeShort(data[0])
  })
}

const getUniqueRecipes = async <T extends (RecipeEquipmentDB | RecipeTagDB | RecipeIngredientDB)>(token: CancelToken, data: T[]) => {
  let result: RecipeShort[] = []
  try {
    for (let item of data) {
      if (result.some(recipe => recipe.id === item.recipe_id)) continue
      const recipe = await getRecipeShortById(token, item.recipe_id)
      if (recipe) result.push(recipe)
    }
    return result
  } catch (e) {
    if (axios.isCancel(e)) throw e
    return []
  }
}

export const getRecipesByEquipmentAndTitle = (token: CancelToken, equipment: string[], string: string, orderBy: string, all?: boolean): Promise<RecipeShort[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/equipment/" + (all ? "all" : "any") + "/" + equipment.join(",") + "/recipes/?string=" + string + "&order=" + orderBy
  }).then((data) => getUniqueRecipes(token, data as RecipeEquipmentDB[]))
}

export const getRecipesByTagsAndTitle = (token: CancelToken, tags: string[], string: string, orderBy: string, all?: boolean): Promise<RecipeShort[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/tag/" + (all ? "all" : "any") + "/" + tags.join(",") + "/recipes/?string=" + string + "&order=" + orderBy
  }).then((data) => getUniqueRecipes(token, data as RecipeTagDB[]))
}

export const getRecipesByIngredientsAndTitle = (token: CancelToken, ingredients: string[], string: string, orderBy: string, all?: boolean): Promise<RecipeShort[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/ingredient/" + (all ? "all" : "any") + "/" + ingredients.join(",") + "/recipes/?string=" + string + "&order=" + orderBy
  }).then((data) => getUniqueRecipes(token, data as RecipeIngredientDB[]))
}

export const getRecipeByTitle = (token: CancelToken, string: string, orderBy: string): Promise<RecipeShort[]> => {
  if (!string) {
    return Promise.resolve([])
  }
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipes/title/?string=" + string + "&order=" + orderBy
  }).then((data) => data.map((r: RecipeDB) => recipeDBToRecipeShort(r)))
}

export const getRecipeById = async (token: CancelToken, id: string, ingredients: Ingredient[], units: Unit[], equipment: Equipment[], tags: Tag[]): Promise<Recipe | undefined> => {
  return await axiosObject({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipes/" + id
  }).then(async (data) => {
    if (!data || data.length < 1) return undefined
    const recipeDB: RecipeDB = data[0]
    const recipeIngredients: IngredientsSection[] = await getIngredientsForRecipe(token, recipeDB.id, ingredients, units)
    const recipeTags: Tag[] = await getTagsForRecipe(token, id, tags)
    const recipeEquipment: Equipment[] = await getEquipmentForRecipe(token, id, equipment)
    const user = recipeDB.user_id ? await getUserById(token, recipeDB.user_id) : undefined

    const recipe: Recipe = {
      id: recipeDB.id,
      user: user,
      title: recipeDB.title,
      description: recipeDB.description ?? "",
      ingredients: recipeIngredients,
      method: (JSON.parse(recipeDB.method)).map((string: string, index: number) => {
        return {
          order: index,
          description: string
        }
      }),
      time: recipeDB.time,
      timePrep: recipeDB.time_prep,
      tags: recipeTags,
      votes: recipeDB.votes ?? 0,
      requiredEquipment: recipeEquipment,
      image: recipeDB.image ?? undefined,
      date: recipeDB.date,
      portions: recipeDB.portions,
      portionsNote: recipeDB.portions_note ?? ""
    }
    return recipe
  })
}