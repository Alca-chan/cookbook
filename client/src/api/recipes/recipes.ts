import axios from "axios";
import {Equipment, FilledIngredientMeasure, IngredientMeasure, NewRecipe, Tag} from "../types";
import {SERVER_URL} from "../utils";

export const addRecipe = async (newRecipe: NewRecipe): Promise<string> => {
  return await axios({
    method: "POST",
    data: {
      recipe: newRecipe
    },
    withCredentials: true,
    url: SERVER_URL + "/recipes"
  }).then((result) => result.data)
    .catch(() => null)
}

export type IngredientsStatus = {
  toAdd: FilledIngredientMeasure[],
  toUpdate: IngredientMeasure[]
  toDelete: IngredientMeasure[]
}

export type TagsStatus = {
  toAdd: Tag[],
  toDelete: Tag[]
}

export type EquipmentStatus = {
  toAdd: Equipment[],
  toDelete: Equipment[]
}

export const editRecipe = async (recipe: NewRecipe, recipeId: string, ingredients: IngredientsStatus, tags: TagsStatus, equipment: EquipmentStatus, date: string): Promise<boolean> => {
  return await axios({
    method: "PUT",
    data: {
      recipe: recipe,
      ingredients: ingredients,
      tags: tags,
      equipment: equipment,
      date: date
    },
    withCredentials: true,
    url: SERVER_URL + "/recipes/" + recipeId
  }).then(() => true)
    .catch(() => false)
}

export const removeRecipe = async (recipeId: string): Promise<boolean> => {
  return await axios({
    method: "DELETE",
    withCredentials: true,
    url: SERVER_URL + "/recipes/" + recipeId
  }).then(() => true)
    .catch(() => false)
}