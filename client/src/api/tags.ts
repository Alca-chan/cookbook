import {CancelToken} from "axios";
import {RecipeTagDB, Tag, TagDB} from "./types";
import {axiosArray, axiosObject, SERVER_URL, tagDBToTag} from "./utils";

export const getTags = (token: CancelToken): Promise<Tag[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/tag"
  }).then((data) => data.map((tag: TagDB) => tagDBToTag(tag)))
}

export const getTagById = (token: CancelToken, id: string): Promise<Tag | undefined> => {
  return axiosObject({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/tag/" + id
  }).then((data) => {
    if (!data || data.length < 1) return undefined
    return tagDBToTag(data[0])
  })
}

export const getTagsForRecipe = (token: CancelToken, recipeId: string, tags: Tag[]): Promise<Tag[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/recipe/" + recipeId + "/tag"
  }).then(async (data: RecipeTagDB[]) => {
    const recipeTags: Tag[] = []
    data.forEach((recipeTag) => {
      const tag = tags.find(t => t.id === recipeTag.tag_id)
      if (tag) recipeTags.push(tag)
    })
    return recipeTags
  })
}