import {Moment} from "moment";

export type Ingredient = {
  id: string,
  nameOne: string,
  nameFew: string,
  nameMany: string,
  nameUnit: string,
  namePart: string,
  nameBase: string,
  count: number
}
export type IngredientDB = {
  id: string,
  name_one: string,
  name_few: string | null,
  name_many: string | null,
  name_unit: string | null,
  name_part: string | null,
  name_base: string | null,
  energy_kj: number | null,
  energy_kcal: number | null,
  fat: number | null,
  saturated_fatty_acid: number | null,
  monounsaturated_fatty_acid: number | null,
  polyunsaturated_fatty_acid: number | null,
  carbohydrate: number | null,
  sugar: number | null,
  polyol: number | null,
  starch: number | null,
  fiber: number | null,
  protein: number | null,
  salt: number | null,
  count: string | null
}

export type Recipe = {
  id: string,
  user: User | undefined,
  title: string,
  description: string,
  ingredients: IngredientsSection[],
  method: MethodStep[],
  time: number,
  timePrep: number,
  tags: Tag[],
  votes: number,
  requiredEquipment: Equipment[],
  image: string | undefined,
  date: string,
  portions: number,
  portionsNote: string
}

export type NewRecipe = {
  title: string,
  userId: string,
  description: string,
  ingredients: FilledIngredientsSection[],
  method: NewMethodStep[],
  time: number,
  timePrep: number,
  tags: Tag[],
  votes: number,
  requiredEquipment: Equipment[],
  image: string,
  date: string,
  portions: number,
  portionsNote: string
}

export type MethodStep = {
  order: number,
  description: string
}

export type NewMethodStep = {
  id: number,
  order: number,
  description: string
}

export type RecipeDB = {
  id: string,
  user_id: string,
  title: string,
  description: string | null,
  method: string,
  time: number,
  time_prep: number,
  votes: number | null,
  image: string | null,
  date: string,
  portions: number,
  portions_note: string | null
}

export type RecipeShort = {
  id: string,
  userId: string | undefined,
  title: string,
  description: string,
  time: number,
  timePrep: number,
  votes: number,
  image: string | undefined,
  date: string,
  portions: number,
  portionsNote: string
}

export type RecipeEquipmentDB = {
  id: string,
  recipe_id: string,
  equipment_id: string
}

export type RecipeIngredientDB = {
  id: string,
  ingredient_id: string,
  measure: number | null,
  note: string | null,
  recipe_id: string,
  unit_id: string,
  section: string | null,
  section_order: number,
  order: number
}

export type RecipeIngredient = {
  id: string,
  recipeId: string,
  ingredient: IngredientMeasure
}

export type RecipeTagDB = {
  id: string,
  recipe_id: string,
  tag_id: string
}

export type Tag = {
  id: string,
  name: string,
  count: number
}

export type TagDB = {
  id: string,
  name: string,
  count: string | null
}

export type Equipment = {
  id: string,
  name: string,
  count: number
}

export type EquipmentDB = {
  id: string,
  name: string,
  count: string | null
}

export type IngredientsSection = {
  order: number,
  title?: string,
  ingredients: IngredientMeasure[]
}

export type NewIngredientsSection = {
  key: number,
  order: number,
  title?: string,
  ingredients: NewIngredientMeasure[]
}

export type FilledIngredientsSection = {
  order: number,
  title: string,
  ingredients: FilledIngredientMeasure[]
}

export type IngredientMeasure = {
  order: number,
  recipeIngredientId: string,
  ingredient: Ingredient,
  measure: number | undefined,
  unit: Unit,
  note: string,
  section: { title: string, order: number }
}

export type NewIngredientMeasure = {
  key: number,
  order: number,
  recipeIngredientId: string | undefined,
  ingredient?: Ingredient,
  measure: number | undefined,
  unit?: Unit,
  note: string,
  section: { title: string, order: number }
}

export type FilledIngredientMeasure = {
  order: number,
  recipeIngredientId: string | undefined,
  ingredient: Ingredient,
  measure: number | undefined,
  unit: Unit,
  note: string,
  section: { title: string, order: number }
}

export type Unit = {
  id?: string,
  symbol: string,
  nameOne: string,
  nameFew: string,
  nameMany: string,
  namePart: string,
  measurable: boolean
}

export type UnitDB = {
  id?: string,
  symbol: string,
  name_one: string | null,
  name_few: string | null,
  name_many: string | null,
  name_part: string | null,
  measurable: boolean
}

export type CartItemDB = {
  id: string,
  recipe_id: string | null,
  user_id: string,
  measure: string,
  ingredient: string,
  recipe_ingredient_id: string | null,
  done: boolean,
  order: number,
  date: string | null,
  title: string | null
}

export type CartItem = {
  id: string,
  recipeId: string | null,
  measure: string,
  ingredient: string,
  recipeIngredientId: string | null,
  done: boolean,
  order: number,
  date: Moment | undefined,
  title: string | null
}

export type CartItemToAdd = {
  recipeId: string | null,
  measure: string,
  ingredient: string,
  recipeIngredientId: string | null,
  order: number,
  title: string | null
}

export type CartList = {
  recipeId: string | null,
  title: string,
  allChecked: boolean,
  items: CartItem[],
  date: Moment | undefined
}

export type FavoritesItemDB = {
  user_id: string,
  recipe_id: string,
  collection: string | null
}

export type FavoritesItem = {
  userId: string,
  recipeId: string,
  collection: string | undefined
}

export type Calendar = {
  id: string,
  recipeId: string | undefined,
  userId: string,
  date: string,
  portions: number,
  order: number,
  title: string | undefined
}

export type CalendarDB = {
  id: string,
  recipe_id: string | null,
  user_id: string,
  date: string,
  portions: number | null,
  order: number,
  title: string | null
}

export type User = {
  id: string,
  username: string,
  email: string
}