import {CancelToken} from "axios";
import {Unit, UnitDB} from "./types";
import {axiosArray, axiosObject, SERVER_URL, unitDBToUnit} from "./utils";

export const getUnits = (token: CancelToken): Promise<Unit[]> => {
  return axiosArray({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/unit"
  }).then((data) => data.map((unit: UnitDB) => unitDBToUnit(unit)))
}

export const getUnitById = (token: CancelToken, id: string): Promise<Unit | undefined> => {
  return axiosObject({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/unit/" + id
  }).then((data) => {
    if (!data || data.length < 1) return undefined
    return unitDBToUnit(data[0])
  })
}