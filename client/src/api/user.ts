import axios, {CancelToken} from "axios";
import {User} from "./types";
import {SERVER_URL, ServerResponse} from "./utils";

export const loginUser = async (username: string, password: string): Promise<ServerResponse> => {
  return await axios({
    method: "POST",
    data: {
      username: username,
      password: password
    },
    withCredentials: true,
    url: SERVER_URL + "/login"
  }).then((result) => {
    if (result.data === ServerResponse.AuthOK || result.data === ServerResponse.AuthError) {
      return result.data
    }
    return ServerResponse.ServerError
  }).catch(() => ServerResponse.ServerError)
}

export const registerUser = async (username: string, email: string, password: string): Promise<ServerResponse> => {
  return await axios({
    method: "POST",
    data: {
      username: username,
      password: password,
      email: email
    },
    withCredentials: true,
    url: SERVER_URL + "/register"
  }).then((result) => {
    if (result.data === ServerResponse.RegOK || result.data === ServerResponse.RegError) {
      return result.data
    }
    return ServerResponse.ServerError
  }).catch(() => ServerResponse.ServerError)
}

export const checkTakenUsername = async (username: string): Promise<boolean> => {
  return await axios({
    method: "GET",
    withCredentials: true,
    url: SERVER_URL + "/users/username/" + username
  }).then(({data}) => {
    return data.length > 0
  })
}

export const checkTakenEmail = async (email: string): Promise<boolean> => {
  return await axios({
    method: "GET",
    withCredentials: true,
    url: SERVER_URL + "/users/email/" + email
  }).then(({data}) => {
    return data.length > 0
  })
}

export const getUserById = async (token: CancelToken, id: string): Promise<User | undefined> => {
  return await axios({
    method: "GET",
    cancelToken: token,
    url: SERVER_URL + "/users/" + id
  }).then(({data}) => {
    if (data.length < 1) return undefined
    const user: User = {
      id: data[0].id,
      username: data[0].username,
      email: data[0].email
    }
    return user
  }).catch((e) => {
    if (axios.isCancel(e)) throw e
    return undefined
  })
}

export const getUser = async (token: CancelToken): Promise<User> => {
  return await axios({
    method: "GET",
    withCredentials: true,
    cancelToken: token,
    url: SERVER_URL + "/user"
  }).then(({data}) => {
    const user: User = {
      id: data.id,
      username: data.username,
      email: data.email
    }
    return user
  })
}

export const logoutUser = (): Promise<User> => {
  return axios({
    method: "GET",
    withCredentials: true,
    url: SERVER_URL + "/logout"
  }).then((result) => {
    const user: User = {
      id: result.data.id,
      username: result.data.username,
      email: result.data.email
    }
    return user
  })
}

export const resetPassword = async (email: string) => {
  await axios({
    method: "POST",
    data: {
      email: email
    },
    withCredentials: true,
    url: SERVER_URL + "/password-reset"
  })
}

export const changeResetPassword = async (token: string, newPassword: string) => {
  await axios({
    method: "POST",
    data: {
      password: newPassword
    },
    withCredentials: true,
    url: SERVER_URL + "/password-reset/" + token
  })
}

export const changePassword = async (password: string, newPassword: string): Promise<ServerResponse> => {
  return await axios({
    method: "PUT",
    data: {
      password: password,
      newPassword: newPassword
    },
    withCredentials: true,
    url: SERVER_URL + "/users/password"
  }).then(({data}) => {
    if (data === ServerResponse.PswdChangeOK || data === ServerResponse.PswdError) {
      return data
    }
    return ServerResponse.ServerError
  }).catch(() => ServerResponse.ServerError)
}

export const changeEmail = async (password: string, newEmail: string): Promise<ServerResponse> => {
  return await axios({
    method: "PUT",
    data: {
      password: password,
      newEmail: newEmail
    },
    withCredentials: true,
    url: SERVER_URL + "/users/email"
  }).then(({data}) => {
    if (data === ServerResponse.EmailChangeOK || data === ServerResponse.PswdError) {
      return data
    }
    return ServerResponse.ServerError
  }).catch(() => ServerResponse.ServerError)
}