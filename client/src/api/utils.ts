import {
  Calendar,
  CalendarDB,
  CartItem,
  CartItemDB, Equipment, EquipmentDB,
  FavoritesItem,
  FavoritesItemDB,
  Ingredient,
  IngredientDB,
  Recipe,
  RecipeDB,
  RecipeShort, Tag, TagDB,
  Unit,
  UnitDB
} from "./types";
import axios, {AxiosRequestConfig, CancelToken, CancelTokenSource} from "axios";
import {getRecipeShortById} from "./recipes/recipeSearch";

const moment = require('moment')

export const SERVER_URL = (process.env.NODE_ENV === "production" ? "" : "http://localhost:3001") + "/api"

export enum ServerResponse {
  AuthOK = "Authentication successful",
  RegOK = "Registration successful",
  AuthError = "Wrong username or password",
  RegError = "User already exists",
  TokenError = "Invalid or expired token",
  PswdChangeOK = "Password was successfully changed",
  EmailChangeOK = "Email was successfully changed",
  PswdError = "Wrong password",
  ServerError = "",
}

export const getCancelToken = (): CancelTokenSource => {
  return axios.CancelToken.source()
}

export const axiosArray = (config: AxiosRequestConfig) => {
  return axios(config)
    .then(({data}) => data)
    .catch((e) => {
      if (axios.isCancel(e)) throw e
      return []
    })
}

export const axiosObject = (config: AxiosRequestConfig) => {
  return axios(config)
    .then(({data}) => data)
    .catch((e) => {
      if (axios.isCancel(e)) throw e
      return undefined
    })
}

export const getUniqueRecipeShortPromises = <T extends (CartItem | Calendar)>(token: CancelToken, items: T[]) => {
  const uniquePromises: { recipeId: string, promise: Promise<RecipeShort | undefined> }[] = []
  items.forEach(i => {
    if (!i.recipeId) return
    if (!uniquePromises.some(u => u.recipeId === i.recipeId))
      uniquePromises.push({recipeId: i.recipeId, promise: getRecipeShortById(token, i.recipeId)})
  })
  return uniquePromises
}

export const recipeDBToRecipeShort = (recipeDB: RecipeDB): RecipeShort => ({
  id: recipeDB.id,
  userId: recipeDB.user_id,
  title: recipeDB.title,
  description: recipeDB.description ?? "",
  time: recipeDB.time,
  timePrep: recipeDB.time_prep,
  votes: recipeDB.votes ?? 0,
  image: recipeDB.image ?? undefined,
  date: recipeDB.date,
  portions: recipeDB.portions,
  portionsNote: recipeDB.portions_note ?? ""
})

export const recipeToRecipeShort = (recipe: Recipe): RecipeShort => ({
  id: recipe.id,
  userId: recipe.user?.id,
  title: recipe.title,
  description: recipe.description,
  time: recipe.time,
  timePrep: recipe.timePrep,
  votes: recipe.votes,
  image: recipe.image,
  date: recipe.date,
  portions: recipe.portions,
  portionsNote: recipe.portionsNote
})

export const calendarDBToCalendar = (calendarDB: CalendarDB): Calendar => ({
  id: calendarDB.id,
  recipeId: calendarDB.recipe_id ?? undefined,
  userId: calendarDB.user_id,
  date: calendarDB.date,
  portions: calendarDB.portions ?? 0,
  order: calendarDB.order,
  title: calendarDB.title ?? undefined
})

export const cartItemDBToCartItemShort = (cartItemDB: CartItemDB): CartItem => ({
  id: cartItemDB.id,
  recipeId: cartItemDB.recipe_id,
  measure: cartItemDB.measure,
  ingredient: cartItemDB.ingredient,
  recipeIngredientId: cartItemDB.recipe_ingredient_id,
  done: cartItemDB.done,
  order: cartItemDB.order,
  date: cartItemDB.date ? moment(cartItemDB.date) : undefined,
  title: cartItemDB.title
})

export const ingredientDBToIngredient = (ingredientDB: IngredientDB): Ingredient => ({
  id: ingredientDB.id.toString(),
  nameOne: ingredientDB.name_one,
  nameFew: ingredientDB.name_few ?? ingredientDB.name_one,
  nameMany: ingredientDB.name_many ?? ingredientDB.name_one,
  nameUnit: ingredientDB.name_unit ?? ingredientDB.name_one,
  namePart: ingredientDB.name_part ?? ingredientDB.name_one,
  nameBase: ingredientDB.name_base ?? ingredientDB.name_one,
  count: ingredientDB.count ? parseInt(ingredientDB.count) : 0
})

export const unitDBToUnit = (unitDB: UnitDB): Unit => ({
  id: unitDB.id,
  symbol: unitDB.symbol,
  nameOne: unitDB.name_one ?? unitDB.symbol,
  nameFew: unitDB.name_few ?? unitDB.symbol,
  nameMany: unitDB.name_many ?? unitDB.symbol,
  namePart: unitDB.name_part ?? unitDB.symbol,
  measurable: unitDB.measurable
})

export const tagDBToTag = (tagDB: TagDB): Tag => ({
  id: tagDB.id,
  name: tagDB.name,
  count: tagDB.count ? parseInt(tagDB.count) : 0
})

export const equipmentDBToEquipment = (equipmentDB: EquipmentDB): Equipment => ({
  id: equipmentDB.id,
  name: equipmentDB.name,
  count: equipmentDB.count ? parseInt(equipmentDB.count) : 0
})

export const cartItemToUpdateCartItem = (cartItem: CartItem): { id: string, recipeId: string | null, order: number, date: string, title: string | null } => {
  return {
    id: cartItem.id,
    recipeId: cartItem.recipeId,
    order: cartItem.order,
    date: cartItem.date ? moment(cartItem.date).format("YYYY-MM-DD") : undefined,
    title: cartItem.title
  }
}

export const favoritesItemDBToFavoriteItem = (favoritesItemDB: FavoritesItemDB): FavoritesItem => {
  return {
    userId: favoritesItemDB.user_id,
    recipeId: favoritesItemDB.recipe_id,
    collection: favoritesItemDB.collection ?? undefined
  }
}