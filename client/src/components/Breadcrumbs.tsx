import {Breadcrumbs as DisplayBreadcrumbs, Link, Typography} from "@mui/material";
import {getHomePagePaths} from "../utils/routes";
import {useTranslation} from "react-i18next";

export const Breadcrumbs = (props: { current: string, previous?: { path: string, name: string }[] }) => {
  const {t} = useTranslation()
  const homePaths = getHomePagePaths()

  const previous = props.previous ?
    props.previous.filter((p) => !homePaths.includes(p.path)).map((p, index) =>
      <Link underline={"hover"} key={index + 2} href={p.path} color={"textSecondary"}>
        {p.name}
      </Link>
    )
    : []

  const breadcrumbs = [
    <Link underline={"hover"} key={1} href={"/"} color={"textSecondary"}>
      {t("common:recipes")}
    </Link>,
    ...previous,
    <Typography key={previous.length + 3} color={"textPrimary"}>
      {props.current}
    </Typography>
  ]

  return (
    <DisplayBreadcrumbs
      separator={'›'}
    >
      {breadcrumbs}
    </DisplayBreadcrumbs>
  )
}