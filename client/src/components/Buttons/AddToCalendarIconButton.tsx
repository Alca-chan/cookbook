import {Add, Event} from "@mui/icons-material";
import {Badge, Icon, IconButton} from "@mui/material";
import React, {useContext, useMemo, useState} from "react";
import {UserContext} from "../../contexts/UserContext";
import {CalendarContext} from "../../contexts/CalendarContext";
import {getRoute} from "../../utils/routes";
import {useTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import {AddToCalendarDialog} from "../Dialogs/AddToCalendarDialog";
import {RecipeShort} from "../../api/types";
import {makeStyles, withStyles} from "../../utils/makeStyles";

const StyledBadge = withStyles(Badge, () => ({
  badge: {
    right: 4,
    top: 20
  }
}))

const useStyles = makeStyles()(theme => ({
  calendarButton: {
    '&:hover': {
      backgroundColor: theme.palette.icon.background
    }
  },
  background: {
    backgroundColor: theme.palette.icon.background
  },
  backgroundSelected: {
    backgroundColor: theme.palette.icon.backgroundSelectedHighlight,
    '&:hover': {
      backgroundColor: theme.palette.icon.backgroundSelectedHighlight
    }
  },
  color: {
    '&:hover': {
      color: theme.palette.icon.colorSelectedHighlight
    }
  },
  selectedColor: {
    color: theme.palette.icon.colorSelectedHighlight,
    '&:hover': {
      color: theme.palette.icon.color
    }
  }
}))

export const AddToCalendarIconButton = (props: { recipe: RecipeShort, changeOnAdd?: boolean, background?: boolean }) => {
  const {classes} = useStyles()
  const {i18n} = useTranslation()
  const history = useHistory()
  const {user} = useContext(UserContext)
  const {calendar} = useContext(CalendarContext)
  const [openCalendarDialog, setOpenCalendarDialog] = useState(false)

  const id = props.recipe.id

  const inCalendar = useMemo(() => {
    if (!user || !calendar) return false
    return calendar.some(event => event.recipeId === id)
  }, [user, id, calendar])

  const onCalendarClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation()
    e.preventDefault()
    if (!inCalendar || !props.changeOnAdd) {
      setOpenCalendarDialog(true)
    } else {
      history.push(getRoute(i18n.language, "/calendar"))
    }
  }

  const selectedClass = classes.selectedColor + (props.background ? (' ' + classes.backgroundSelected) : '')
  const regularClass = classes.color + (props.background ? (' ' + classes.background) : '')

  return (
    <>
      <AddToCalendarDialog open={openCalendarDialog} setOpen={setOpenCalendarDialog}
                           recipe={{...props.recipe, userId: props.recipe.userId}}/>
      <div>
        <IconButton size={"large"} onClick={(e) => onCalendarClick(e)}
                    className={classes.calendarButton + ' ' + ((inCalendar && props.changeOnAdd) ? selectedClass : regularClass)}>
          {
            (!inCalendar || !props.changeOnAdd) ?
              <StyledBadge badgeContent={<Add fontSize={"small"}/>}>
                <Icon fontSize={"medium"}>
                  <Event fontSize={"small"} style={{position: 'absolute', right: 4}}/>
                </Icon>
              </StyledBadge>
              :
              <Event/>
          }
        </IconButton>
      </div>
    </>
  )
}