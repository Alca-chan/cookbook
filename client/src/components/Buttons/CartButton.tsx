import React, {useState} from "react";
import {AddShoppingCart, RemoveShoppingCart, ShoppingCart} from "@mui/icons-material";
import {IconButton} from "@mui/material";

export const CartButton = (props: { recipeId: string, inCart: boolean, onClick: () => void }) => {
  const [isHovered, setIsHovered] = useState(false)

  const AddToCartIcon = () => {
    return <AddShoppingCart fontSize='small'/>
  }

  const InCartIcon = () => {
    if (!isHovered)
      return <ShoppingCart fontSize='small'/>
    else
      return <RemoveShoppingCart fontSize='small'/>
  }

  return (
    <IconButton onClick={props.onClick}
                onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
      {
        props.inCart
          ? <InCartIcon/>
          : <AddToCartIcon/>
      }
    </IconButton>
  )
}
