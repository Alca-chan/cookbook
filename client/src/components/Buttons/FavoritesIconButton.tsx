import {Typography} from "@mui/material";
import React, {useContext, useEffect, useMemo, useRef, useState} from "react";
import {FavoritesContext} from "../../contexts/FavoritesContext";
import {UserContext} from "../../contexts/UserContext";
import {useHistory} from "react-router-dom";
import {getRoute} from "../../utils/routes";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";
import {FavoritesIcon} from "../Icons/FavoritesIcon";
import {FavoritesDialog} from "../Dialogs/FavoritesDialog";

const useStyles = makeStyles()(theme => ({
  favoritesButton: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: theme.spacing(1.5),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    borderRadius: '25px',
    '&:hover': {
      cursor: "pointer",
      backgroundColor: theme.palette.icon.background
    }
  },
  favoritesIcon: {
    marginRight: theme.spacing(1)
  },
  color: {
    color: theme.palette.icon.color,
    '&:hover': {
      color: theme.palette.icon.colorSelectedHighlight
    }
  },
  selectedColor: {
    color: theme.palette.icon.colorSelectedHighlight,
    '&:hover': {
      color: theme.palette.icon.color
    }
  },
  background: {
    backgroundColor: theme.palette.icon.background
  },
  backgroundSelected: {
    backgroundColor: theme.palette.icon.backgroundSelectedHighlight,
    '&:hover': {
      backgroundColor: theme.palette.icon.backgroundSelectedHighlight
    }
  }
}))

export const FavoritesIconButton = (props: { recipeId: string, votes?: number, background?: boolean }) => {
  const {classes} = useStyles()
  const {i18n} = useTranslation()
  const mounted = useRef(true)
  const history = useHistory()
  const {user} = useContext(UserContext)
  const {favorites, addFavoriteRecipe, editFavoriteRecipe, removeFavoriteRecipe} = useContext(FavoritesContext)
  const [votes, setVotes] = useState(props.votes !== undefined ? props.votes : 0)
  const [openFavoritesDialog, setOpenFavoritesDialog] = useState(false)

  const id = props.recipeId

  useEffect(() => {
    mounted.current = true
    return () => {
      mounted.current = false
    }
  }, [])

  const favoritesItem = useMemo(() => {
    if (!user || !favorites) return undefined
    return favorites.find(like => like.recipeId === id)
  }, [user, id, favorites])

  const onLikeClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation()
    e.preventDefault()
    if (!user) history.push(getRoute(i18n.language, "/login"))
    setOpenFavoritesDialog(true)
  }

  const editOrAddToFavorites = async (selectedCollection: string | undefined) => {
    if (favoritesItem) {
      if (selectedCollection !== favoritesItem.collection) editFavoriteRecipe(id, selectedCollection)
    } else {
      addFavoriteRecipe(id, selectedCollection).then((value) => {
        if (mounted.current && value !== undefined) setVotes(value)
      })
    }
  }

  const removeFromFavorites = () => {
    removeFavoriteRecipe(id).then((value) => {
      if (mounted.current && value !== undefined) setVotes(value)
    })
  }

  const selectedClass = classes.selectedColor + (props.background ? (' ' + classes.backgroundSelected) : '')
  const regularClass = classes.color + (props.background ? (' ' + classes.background) : '')

  return (
    <>
      {
        openFavoritesDialog && (
          <FavoritesDialog open={openFavoritesDialog} setOpen={setOpenFavoritesDialog}
                           recipeId={props.recipeId} inFavorites={Boolean(favoritesItem)}
                           collection={favoritesItem && favoritesItem.collection}
                           onSubmit={editOrAddToFavorites} onDelete={removeFromFavorites}/>
        )
      }
      <div
        className={(favoritesItem ? selectedClass : regularClass) + ' ' + classes.favoritesButton}
        onClick={(e) => onLikeClick(e)}>
        <FavoritesIcon className={classes.favoritesIcon}/>
        <Typography display={"inline"}>{votes}</Typography>
      </div>
    </>
  )
}