import {Event} from "@mui/icons-material";
import {Typography} from "@mui/material";
import React from "react";
import {getRoute} from "../../utils/routes";
import {useTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import {RecipeShort} from "../../api/types";
import {Moment} from "moment";
import {displayShortDateFormat} from "../../utils/utils";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  calendarButton: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: theme.spacing(1.5),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    borderRadius: '25px',
    '&:hover': {
      cursor: "pointer",
      backgroundColor: theme.palette.icon.background
    }
  },
  calendarIcon: {
    marginRight: theme.spacing(1)
  },
  background: {
    backgroundColor: theme.palette.icon.background
  },
  backgroundSelected: {
    backgroundColor: theme.palette.icon.backgroundSelectedHighlight,
    '&:hover': {
      backgroundColor: theme.palette.icon.backgroundSelectedHighlight
    }
  },
  color: {
    '&:hover': {
      color: theme.palette.icon.colorSelectedHighlight
    }
  },
  selectedColor: {
    color: theme.palette.icon.colorSelectedHighlight,
    '&:hover': {
      color: theme.palette.icon.color
    }
  }
}))

export const InCalendarIconButton = (props: { recipe: RecipeShort, date: Moment, background?: boolean }) => {
  const {classes} = useStyles()
  const {i18n} = useTranslation()
  const history = useHistory()

  const onCalendarClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation()
    e.preventDefault()
    history.push(getRoute(i18n.language, "/calendar"))
  }

  const selectedClass = classes.selectedColor + (props.background ? (' ' + classes.backgroundSelected) : '')

  return (
    <div onClick={(e) => onCalendarClick(e)}
         className={classes.calendarButton + ' ' + selectedClass}>
      <Event className={classes.calendarIcon}/>
      <Typography>{props.date.format(displayShortDateFormat)}</Typography>
    </div>
  )
}