import React from "react";
import {alpha, IconButton} from "@mui/material";
import {Add} from "@mui/icons-material";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  plusIcon: {
    backgroundColor: alpha(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.black, 0.10),
    }
  }
}))

export const PlusButton = (props: { onClick: () => void }) => {
  const {classes} = useStyles()

  return (
    <IconButton size={"small"} onClick={props.onClick} className={classes.plusIcon}>
      <Add/>
    </IconButton>
  )
}