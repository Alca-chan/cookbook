import React, {useState} from 'react';
import AdapterDateFns from '@mui/lab/AdapterMoment';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {DatePicker} from '@mui/lab';
import {Moment} from "moment";
import {useTranslation} from "react-i18next";
import {TextField} from "@mui/material";
import {makeStyles} from "../../utils/makeStyles";
import {defaultDateFormat} from "../../utils/utils";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  datePicker: {
    marginTop: theme.spacing(1)
  }
}))

export const CalendarDatePicker = (props: { selectedDate: Moment | null, onChange: (newDate: Moment | null) => void, minDate?: Moment | null }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [open, setOpen] = useState(false)

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        readOnly
        open={open}
        onClose={() => setOpen(false)}
        inputFormat={"D. M. yyyy"}
        label={t("common:date")}
        value={props.selectedDate ? moment(props.selectedDate).format(defaultDateFormat) : null}
        disableMaskedInput
        onChange={(date) => props.onChange(date)}
        minDate={props.minDate !== undefined ? props.minDate : moment()}
        renderInput={(params) =>
          <TextField {...params} onClick={() => setOpen(true)} fullWidth className={classes.datePicker}/>
        }
      />
    </LocalizationProvider>
  )
}