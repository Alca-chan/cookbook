import React, {useState} from 'react';
import AdapterDateFns from '@mui/lab/AdapterMoment';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {DateRangePicker} from '@mui/lab';
import {Moment} from "moment";
import {useTranslation} from "react-i18next";
import {Stack, TextField} from "@mui/material";
import {defaultDateFormat} from "../../utils/utils";

const moment = require('moment')
require('moment/locale/cs')
require('moment/locale/en-gb')

export const CalendarDateRangePicker = (props: { startDate: Moment | null, endDate: Moment | null, onChange: (newDate: [Moment | null, Moment | null]) => void }) => {
  const {t, i18n} = useTranslation()
  const [open, setOpen] = useState(false)

  moment.locale(i18n.language === 'cs' ? 'cs' : 'en-gb')

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} locale={moment.Locale}>
      <DateRangePicker
        readOnly
        startText={t("userCalendar:startDate")}
        endText={t("userCalendar:endDate")}
        open={open}
        onClose={() => setOpen(false)}
        inputFormat={"D. M. yyyy"}
        label={t("common:date")}
        value={[props.startDate ? moment(props.startDate).format(defaultDateFormat) : null, props.endDate ? moment(props.endDate).format(defaultDateFormat) : null]}
        disableMaskedInput
        onChange={(date) => props.onChange(date)}
        toolbarTitle={null}
        cancelText={t("common:cancel")}
        renderInput={(startProps, endProps) => (
          <Stack direction={"row"} spacing={2}>
            <TextField {...startProps} onClick={() => setOpen(true)} fullWidth/>
            <TextField {...endProps} onClick={() => setOpen(true)} fullWidth/>
          </Stack>
        )}
      />
    </LocalizationProvider>
  )
}