import React, {useContext, useState} from 'react';
import {Moment} from "moment";
import {UserContext} from "../../contexts/UserContext";
import {CalendarContext} from "../../contexts/CalendarContext";
import {ConfirmDialog} from "../Dialogs/ConfirmDialog";
import {CalendarFormDialog} from "../Dialogs/CalendarFormDialog";
import {RecipeCard} from "../RecipesGrid/RecipeCard";
import {Delete, DragIndicator, Edit} from "@mui/icons-material";
import {CalendarEvent} from "../../pages/UserCalendar";
import {alpha, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  menuOptionIcon: {
    marginRight: theme.spacing(1)
  },
  recipeCard: {
    position: 'relative',
    "& .appearIndicator": {
      display: "none"
    },
    "&:hover .appearIndicator": {
      display: 'flex'
    }
  },
  dragIndicator: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    width: '32px',
    height: '48px',
    backgroundColor: alpha(theme.palette.table.header, 0.5),
    color: theme.palette.text.secondary,
    borderBottomRightRadius: '10px',
    justifyContent: 'center',
    alignItems: 'center'
  }
}))

export const CalendarRecipeCard = (props: { event: CalendarEvent, date: Moment }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {user} = useContext(UserContext)
  const {calendar, moveInCalendar, removeFromCalendar} = useContext(CalendarContext)
  const [openCalendarRecipeDialog, setOpenCalendarRecipeDialog] = useState(false)
  const [openDeleteRecipeDialog, setOpenDeleteRecipeDialog] = useState(false)

  const onChangeRecipeDate = () => {
    if (!user || !calendar) return
    setOpenCalendarRecipeDialog(true)
  }

  const saveRecipeDate = (selectedDate: Moment) => {
    if (!user || !calendar) return
    const order = calendar.filter(c => moment(selectedDate).isSame(c.date, 'day')).length
    props.event.order = order
    moveInCalendar([props.event.calendarId], selectedDate, [{id: props.event.calendarId, order: order}])
  }

  const onRemoveRecipe = () => {
    if (!user) return
    setOpenDeleteRecipeDialog(true)
  }

  const removeRecipe = () => {
    if (!user) return
    removeFromCalendar([props.event.calendarId])
  }

  return (
    <>
      {
        openDeleteRecipeDialog && (
          <ConfirmDialog open={openDeleteRecipeDialog} setOpen={setOpenDeleteRecipeDialog}
                         onConfirm={removeRecipe}
                         title={t("userCalendar:removeRecipeTitle")}>
            <Typography variant={"body1"}>
              {t("userCalendar:removeRecipeText", {
                title: props.event.recipe.title,
                date: moment(props.date).format("DD. MM.")
              })}
            </Typography>
          </ConfirmDialog>
        )
      }
      {
        openCalendarRecipeDialog && (
          <CalendarFormDialog open={openCalendarRecipeDialog} setOpen={setOpenCalendarRecipeDialog}
                              onSave={saveRecipeDate}
                              title={t("userCalendar:moveRecipeTitle")}>
            <Typography variant={"body1"}>
              {t("userCalendar:moveRecipeText", {
                title: props.event.recipe.title,
                date: moment(props.date).format("DD. MM.")
              })}
            </Typography>
          </CalendarFormDialog>
        )
      }
      <div className={classes.recipeCard}>
        <RecipeCard recipe={props.event.recipe}
                    displayFavorites={true}
                    displayCalendar={false}
                    menuOptions={[{
                      action: onChangeRecipeDate,
                      menuItem:
                        <>
                          <Edit fontSize={"small"}
                                className={classes.menuOptionIcon}/>{t("userCalendar:changeDate")}
                        </>
                    }, {
                      action: onRemoveRecipe,
                      menuItem:
                        <>
                          <Delete fontSize={"small"}
                                  className={classes.menuOptionIcon}/>{t("userCalendar:removeFromCalendar")}
                        </>
                    }]}
        />
        <div className={classes.dragIndicator + " appearIndicator"}>
          <DragIndicator/>
        </div>
      </div>
    </>
  )
}