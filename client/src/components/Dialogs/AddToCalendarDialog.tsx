import {CalendarFormDialog} from "./CalendarFormDialog";
import React, {useContext, useState} from "react";
import {CalendarContext} from "../../contexts/CalendarContext";
import {RecipeShort} from "../../api/types";
import {Moment} from "moment";
import {UserContext} from "../../contexts/UserContext";
import {useTranslation} from "react-i18next";
import {Checkbox, FormControlLabel} from "@mui/material";
import {AddToShoppingListDialog} from "./AddToShoppingListDialog";

export const AddToCalendarDialog = (props: { open: boolean, setOpen: (open: boolean) => void, recipe: RecipeShort }) => {
  const {t} = useTranslation()
  const {user} = useContext(UserContext)
  const {addToCalendar} = useContext(CalendarContext)
  const [addToShoppingList, setAddToShoppingList] = useState(true)
  const [openShoppingListDialog, setOpenShoppingListDialog] = useState(false)
  const [selectedDate, setSelectedDate] = useState<Moment>()

  const recipe = props.recipe

  const onChangeShoppingList = (e: React.MouseEvent<HTMLLabelElement, MouseEvent>) => {
    e.preventDefault()
    e.stopPropagation()
    setAddToShoppingList(!addToShoppingList)
  }

  const saveToCalendar = (date: Moment) => {
    if (!user) return
    addToCalendar(recipe.id, date, 1, 0, "")
    if (addToShoppingList) {
      setSelectedDate(date)
      setOpenShoppingListDialog(true)
    }
  }

  return (
    <>
      {
        openShoppingListDialog &&
        <AddToShoppingListDialog open={openShoppingListDialog} setOpen={setOpenShoppingListDialog} recipe={recipe}
                                 date={selectedDate}/>
      }
      {
        props.open && (
          <CalendarFormDialog
            open={props.open} setOpen={props.setOpen}
            onSave={saveToCalendar}
            title={t("userCalendar:addToCalendarTitle", {recipe: recipe.title})}
            childrenAfter={
              <FormControlLabel
                label={t("cart:createShoppingList") as string}
                onClick={(e) => onChangeShoppingList(e)}
                control={
                  <Checkbox color={"primary"}
                            checked={addToShoppingList}
                  />
                }
              />
            }
          />
        )
      }
    </>
  )
}