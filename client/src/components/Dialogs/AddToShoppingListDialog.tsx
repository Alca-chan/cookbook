import {Typography} from "@mui/material";
import React, {useCallback, useContext, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {CartItem, CartItemToAdd, RecipeShort} from "../../api/types";
import {getCancelToken} from "../../api/utils";
import {Loading} from "../Loading";
import {getIngredientsForRecipe} from "../../api/ingredients";
import {StaticDataContext} from "../../contexts/StaticDataContext";
import {createCartItemToAdd} from "../../utils/utils";
import {CartContext} from "../../contexts/CartContext";
import {Moment} from "moment";
import {ShoppingListDialog} from "./ShoppingListDialog";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  addToShoppingListText: {
    marginBottom: theme.spacing(1)
  }
}))

type CartItemSection = {
  title?: string,
  items: ((CartItemToAdd | CartItem) & { checked: boolean, inCart: boolean, changed: boolean })[]
}

export const AddToShoppingListDialog = (props: { open: boolean, setOpen: (open: boolean) => void, recipe: RecipeShort, date?: Moment, children?: React.ReactNode }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {addItemsToCart} = useContext(CartContext)
  const {ingredients, units} = useContext(StaticDataContext)
  const [cartItemSections, setCartItemSections] = useState<CartItemSection[]>()

  const recipeId = props.recipe.id

  const onSubmit = (data: CartItemSection[]) => {
    props.setOpen(false)
    addItemsToCart(data.map(s => s.items.filter(i => i.checked)).flat(), props.date)
  }

  const loadRecipeIngredients = useCallback(async (token) => {
    if (!ingredients || !units) return
    const result = await getIngredientsForRecipe(token, recipeId, ingredients, units)
    const sections: CartItemSection[] = result.map(section => {
      const items = section.ingredients.map(i => {
        return {
          ...createCartItemToAdd(i.recipeIngredientId, recipeId, i.measure, i.unit, i.ingredient, i.order, i.note),
          checked: true,
          inCart: false,
          changed: false
        }
      })
      return {
        title: section.title,
        items: items
      }
    })
    setCartItemSections(sections)
  }, [ingredients, units, recipeId])

  useEffect(() => {
    const source = getCancelToken()
    loadRecipeIngredients(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadRecipeIngredients])

  if (!cartItemSections) {
    return <Loading/>
  }

  return (
    <ShoppingListDialog
      open={props.open} setOpen={props.setOpen} title={t("cart:addToShoppingListTitle")}
      data={cartItemSections} onSubmit={onSubmit}
    >
      <Typography variant={"body1"} className={classes.addToShoppingListText}>
        {t("cart:addToShoppingListText", {recipe: props.recipe.title})}
      </Typography>
    </ShoppingListDialog>
  )
}