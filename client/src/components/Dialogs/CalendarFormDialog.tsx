import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from "@mui/material";
import React, {useState} from "react";
import {CalendarDatePicker} from "../Calendar/CalendarDatePicker";
import {Moment} from "moment";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const moment = require('moment')

const useStyles = makeStyles<{ childrenBefore: boolean, childrenAfter: boolean }>()((theme, childrenBefore) => ({
  childrenBefore: {
    marginBottom: childrenBefore ? theme.spacing(1) : 0
  },
  childrenAfter: {
    marginTop: childrenBefore ? theme.spacing(1) : 0
  }
}))

export const CalendarFormDialog = (props: { open: boolean, setOpen: (open: boolean) => void, onSave: (selectedDate: Moment) => void, title: string, defaultDate?: Moment, children?: React.ReactNode, childrenAfter?: React.ReactNode }) => {
  const {classes} = useStyles({childrenBefore: Boolean(props.children), childrenAfter: Boolean(props.childrenAfter)})
  const {t} = useTranslation()
  const [selectedDate, setSelectedDate] = useState<Moment | null>(props.defaultDate ?? moment())

  const onSubmit = () => {
    if (!selectedDate) return
    props.setOpen(false)
    props.onSave(selectedDate)
  }

  return (
    <Dialog
      fullWidth
      open={props.open}
      onClose={() => props.setOpen(false)}
    >
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        <div className={classes.childrenBefore}>
          {props.children}
        </div>
        <CalendarDatePicker selectedDate={selectedDate} onChange={setSelectedDate}/>
        <div className={classes.childrenAfter}>
          {props.childrenAfter}
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          variant={"contained"} color={"grey"}
          onClick={() => props.setOpen(false)}
        >
          {t("common:cancel")}
        </Button>
        <Button
          variant={"contained"} color={"primary"}
          onClick={onSubmit}
        >
          {t("common:save")}
        </Button>
      </DialogActions>
    </Dialog>
  )
}