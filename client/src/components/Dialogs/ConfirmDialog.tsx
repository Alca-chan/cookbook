import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from "@mui/material";
import React from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  actions: {
    marginTop: theme.spacing(2)
  }
}))

export const ConfirmDialog = (props: { open: boolean, setOpen: (open: boolean) => void, onConfirm: () => void, title: string, children?: React.ReactNode }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()

  return (
    <Dialog
      fullWidth
      open={props.open}
      onClose={() => props.setOpen(false)}
    >
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>{props.children}</DialogContent>
      <DialogActions className={classes.actions}>
        <Button
          variant={"contained"} color={"primary"}
          onClick={() => props.setOpen(false)}
        >
          {t("common:no")}
        </Button>
        <Button
          variant={"contained"} color={"grey"}
          onClick={() => {
            props.setOpen(false)
            props.onConfirm()
          }}
        >
          {t("common:yes")}
        </Button>
      </DialogActions>
    </Dialog>
  )
}