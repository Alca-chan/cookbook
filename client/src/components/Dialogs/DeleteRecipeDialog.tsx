import React, {useContext} from "react";
import {removeRecipe} from "../../api/recipes/recipes";
import {useHistory} from "react-router-dom";
import {ConfirmDialog} from "./ConfirmDialog";
import {Recipe, RecipeShort} from "../../api/types";
import {UserContext} from "../../contexts/UserContext";
import {useTranslation} from "react-i18next";

export const DeleteRecipeDialog = (props: { open: boolean, setOpen: (open: boolean) => void, recipe: Recipe | RecipeShort }) => {
  const {t} = useTranslation()
  const history = useHistory()
  const {user} = useContext(UserContext)

  const deleteRecipe = async () => {
    if (props.recipe && user) {
      await removeRecipe(props.recipe.id)
      history.push("/")
    }
  }

  return (
    <ConfirmDialog open={props.open} setOpen={props.setOpen} onConfirm={() => deleteRecipe()}
                   title={t("common:deleteRecipeText", {title: props.recipe.title})}/>
  )
}