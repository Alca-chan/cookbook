import {MenuItem, TextField, Typography} from "@mui/material";
import React, {useContext, useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import {CartItem, CartItemToAdd, IngredientsSection, RecipeShort} from "../../api/types";
import {Loading} from "../Loading";
import {StaticDataContext} from "../../contexts/StaticDataContext";
import {compareDates, createCartItemToAdd, defaultDateFormat, displayDateFormat} from "../../utils/utils";
import {CartContext} from "../../contexts/CartContext";
import moment, {Moment} from "moment";
import {CalendarContext} from "../../contexts/CalendarContext";
import {ShoppingListDialog} from "./ShoppingListDialog";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  addToShoppingListText: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1)
  },
  selectDate: {
    width: "100%",
    marginTop: theme.spacing(1)
  }
}))

type CartItemSection = {
  title?: string,
  items: ((CartItemToAdd | CartItem) & { checked: boolean, inCart: boolean, changed: boolean })[]
}

const splitItemsToRemoveUpdateAdd = (sections: CartItemSection[]) => sections.reduce((previous: { remove: CartItem[], update: CartItem[], add: CartItemToAdd[] }, section: CartItemSection) => {
  const newRemove = [...previous.remove]
  const newUpdate = [...previous.update]
  const newAdd = [...previous.add]
  section.items.forEach(item => {
    if (item.checked) {
      if (!item.inCart) {
        newAdd.push(item)
      } else if (item.changed) {
        newUpdate.push(item as CartItem)
      }
    } else if (item.inCart) {
      newRemove.push(item as CartItem)
    }
  })
  return {remove: newRemove, update: newUpdate, add: newAdd}
}, {remove: [], update: [], add: []})

export const EditShoppingListDialog = (props: { open: boolean, setOpen: (open: boolean) => void, recipe: RecipeShort, ingredientSections: IngredientsSection[], children?: React.ReactNode }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {cart, editCartItems} = useContext(CartContext)
  const {calendar} = useContext(CalendarContext)
  const {ingredients, units} = useContext(StaticDataContext)
  const [date, setDate] = useState<Moment | undefined>(undefined)

  const recipeId = props.recipe.id

  const onSubmit = (data: CartItemSection[]) => {
    props.setOpen(false)
    const splitItems = splitItemsToRemoveUpdateAdd(data)
    editCartItems(splitItems.remove as CartItem[], splitItems.update, splitItems.add, date)
  }

  const onChangeDate = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const newSelectedValue = e.target.value
    if (newSelectedValue) setDate(newSelectedValue !== "noDate" ? moment(newSelectedValue) : undefined)
  }

  const ingredientSections = props.ingredientSections
  const cartItemSections = useMemo(() => {
    if (!ingredients || !units) return
    const sections: CartItemSection[] = ingredientSections.map(section => {
      const items = section.ingredients.map(ingredient => {
        if (cart) {
          const found = cart.find(i => i.recipeIngredientId === ingredient.recipeIngredientId && i.recipeId === recipeId && compareDates(i.date, date))
          if (found) return {
            ...found,
            checked: true,
            inCart: true,
            changed: false
          }
        }
        return {
          ...createCartItemToAdd(ingredient.recipeIngredientId, recipeId, ingredient.measure, ingredient.unit, ingredient.ingredient, ingredient.order, ingredient.note),
          checked: false,
          inCart: false,
          changed: false
        }
      })
      return {
        title: section.title,
        items: items
      }
    })
    return sections
  }, [ingredients, units, recipeId, ingredientSections, date, cart])

  const dateOptions = useMemo(() => {
    if (!calendar) return undefined
    const options: Moment[] = []
    calendar.forEach(e => {
      if (e.recipeId === recipeId)
        options.push(moment(e.date))
    })
    return options
  }, [calendar, recipeId])

  if (!cartItemSections) {
    return <Loading/>
  }

  return (
    <ShoppingListDialog
      open={props.open} setOpen={props.setOpen} title={t("cart:editShoppingListTitle")}
      data={cartItemSections} onSubmit={onSubmit}
    >
      <>
        <TextField
          className={classes.selectDate}
          variant={"outlined"}
          value={date ? date.format(defaultDateFormat) : "noDate"}
          label={t("common:date")}
          type={"text"}
          select
          onChange={onChangeDate}
        >
          <MenuItem value={"noDate"}>{t("cart:noDate")}</MenuItem>
          {
            dateOptions &&
            dateOptions.map((o, index) =>
              <MenuItem value={o.format(defaultDateFormat)} key={index}>{o.format(displayDateFormat)}</MenuItem>
            )
          }
        </TextField>
        <Typography variant={"body1"} className={classes.addToShoppingListText}>
          {t("cart:editShoppingListText", {recipe: props.recipe.title + (date ? ` (${date.format(displayDateFormat)})` : '')})}
        </Typography>
      </>
    </ShoppingListDialog>
  )
}