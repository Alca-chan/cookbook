import React, {useContext, useState} from "react";
import {UserContext} from "../../contexts/UserContext";
import {useTranslation} from "react-i18next";
import {
  alpha,
  Button,
  darken,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  MenuItem,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import {FavoritesContext} from "../../contexts/FavoritesContext";
import {Loading} from "../Loading";
import {makeStyles} from "../../utils/makeStyles";
import red from "@mui/material/colors/red";

const useStyles = makeStyles()(() => ({
  fullWidth: {
    width: "100%"
  },
  deleteButton: {
    backgroundColor: alpha(red[400], 0.9),
    '&:hover': {
      backgroundColor: darken(red[400], 0.15)
    }
  }
}))

export const FavoritesDialog = (props: { open: boolean, setOpen: (open: boolean) => void, recipeId: string, inFavorites: boolean, collection?: string | undefined, onSubmit: (selectedCollection: string | undefined) => void, onDelete: () => void }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {user} = useContext(UserContext)
  const {collections} = useContext(FavoritesContext)
  const [selectedCollection, setSelectedCollection] = useState<string | undefined>(props.collection)
  const [addCollection, setAddCollection] = useState(false)
  const [newCollectionName, setNewCollectionName] = useState<string>(t("favorites:newCollection"))

  const onChangeCollection = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    e.preventDefault()
    e.stopPropagation()
    const newSelectedValue = e.target.value
    setSelectedCollection(newSelectedValue === "noCollection" ? undefined : (newSelectedValue as string))
    setAddCollection(newSelectedValue === "addCollection")
  }

  const onChangeCollectionName = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    e.preventDefault()
    e.stopPropagation()
    const newValue = e.target.value
    setNewCollectionName(newValue ?? "")
  }

  const onSubmit = () => {
    if (!user) return
    const collection = addCollection ? newCollectionName : selectedCollection
    props.onSubmit(collection)
    props.setOpen(false)
  }

  const onDelete = () => {
    if (!user) return
    props.onDelete()
    props.setOpen(false)
  }

  if (!collections) {
    return <Loading/>
  }

  return (
    <Dialog
      fullWidth
      open={props.open}
      onClose={() => props.setOpen(false)}
    >
      <DialogTitle>{props.inFavorites ? t("favorites:editRecipeInFavorites") : t("favorites:addToFavorites")}</DialogTitle>
      <DialogContent>
        <Stack direction={"column"}>
          <Typography>{t("favorites:selectCollection")}:</Typography>
          <TextField
            variant={"outlined"}
            value={selectedCollection ?? "noCollection"}
            type={"text"}
            margin={"normal"}
            select
            onChange={onChangeCollection}
          >
            <MenuItem value={"noCollection"} key={"noCollection"}>{t("favorites:noCollection")}</MenuItem>
            {
              collections.length > 0 && <Divider/>
            }
            {
              collections.map(c =>
                <MenuItem value={c} key={c}>{c}</MenuItem>
              )
            }
            <Divider/>
            <MenuItem value={"addCollection"} key={"addCollection"}>{t("favorites:addCollection")}</MenuItem>
          </TextField>
          {
            addCollection &&
            <TextField
                variant={"outlined"}
                value={newCollectionName}
                type={"text"}
                margin={"normal"}
                label={t("favorites:newCollectionTitle")}
                onChange={(e) => onChangeCollectionName(e)}
            />
          }
        </Stack>
      </DialogContent>
      <DialogActions>
        <Stack direction={"row"} spacing={1} justifyContent={props.inFavorites ? "space-between" : "flex-end"}
               className={classes.fullWidth}>
          {
            props.inFavorites && (
              <Button
                className={classes.deleteButton}
                variant={"contained"} color={"grey"}
                onClick={onDelete}
              >
                {t("common:delete")}
              </Button>
            )
          }
          <Stack direction={"row"} spacing={1}>
            <Button
              variant={"contained"} color={"grey"}
              onClick={() => props.setOpen(false)}
            >
              {t("common:cancel")}
            </Button>
            <Button
              variant={"contained"} color={"primary"}
              onClick={onSubmit}
            >
              {t("common:save")}
            </Button>
          </Stack>
        </Stack>
      </DialogActions>
    </Dialog>
  )
}