import {
  Button,
  Card,
  CardContent,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField
} from "@mui/material";
import React, {useContext, useState} from "react";
import {useTranslation} from "react-i18next";
import {CartContext} from "../../contexts/CartContext";
import {makeStyles} from "../../utils/makeStyles";
import {ShoppingListItemRow} from "../ShoppingList/ShoppingListItemRow";
import {createNewCartItem} from "../../utils/utils";
import {CartItemToAdd, CartList} from "../../api/types";
import {PlusButton} from "../Buttons/PlusButton";

const useStyles = makeStyles()(theme => ({
  actions: {
    marginTop: theme.spacing(2)
  },
  sectionCard: {
    marginTop: theme.spacing(1)
  },
  sectionTitle: {
    width: "100%",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor: theme.palette.table.subHeader
  },
  sectionItems: {
    paddingTop: theme.spacing(1),
    "&:last-child": {
      paddingBottom: theme.spacing(1)
    }
  },
  plusButton: {
    width: "100%",
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
}))

export const NewShoppingListDialog = (props: { open: boolean, setOpen: (open: boolean) => void, list?: CartList }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {addItemsToCart} = useContext(CartContext)
  const list = props.list ?? {
    recipeId: null,
    title: t("cart:newShoppingList"),
    allChecked: false,
    items: [],
    date: undefined
  }
  const newItem = createNewCartItem(undefined, list.recipeId ?? undefined, t("cart:measure"), t("cart:ingredient"), list.items.length, list.title ?? undefined)
  const [items, setItems] = useState<CartItemToAdd[]>([newItem])
  const [title, setTitle] = useState<string>(list.title ?? t("cart:newShoppingList"))

  const onSubmit = () => {
    const itemsToAdd = [...items]
    if (!props.list) {
      itemsToAdd.forEach(i => i.title = title)
    }
    addItemsToCart(itemsToAdd, list.date)
    props.setOpen(false)
  }

  const updateItem = (item: CartItemToAdd, changedItem: CartItemToAdd) => {
    if (item.ingredient !== changedItem.ingredient) {
      item.ingredient = changedItem.ingredient
    }
    if (item.measure !== changedItem.measure) {
      item.measure = changedItem.measure
    }
    setItems([...items])
  }

  const addNewItem = () => {
    const item = {...newItem, order: list.items.length + items.length}
    setItems([...items, item])
  }

  return (
    <Dialog
      fullWidth
      open={props.open}
      onClose={() => props.setOpen(false)}
    >
      <DialogTitle>{props.list ? t("cart:addItemsToShoppingList", {list: props.list.title}) : t("cart:createShoppingList")}</DialogTitle>
      <DialogContent>
        {
          !props.list && (
            <TextField
              variant={"outlined"}
              value={title}
              type={"text"}
              margin={"normal"}
              label={t("cart:newShoppingListTitle")}
              onChange={(e) => setTitle(e.target.value)}
            />
          )
        }
        <Card variant={"outlined"} className={classes.sectionCard}>
          <CardContent className={classes.sectionItems}>
            <Grid container>
              {
                items.map((item, itemIndex) =>
                  <Grid item xs={12} key={itemIndex}>
                    <ShoppingListItemRow
                      item={item} editMode={true}
                      updateItem={updateItem}
                    />
                  </Grid>
                )
              }
              <div className={classes.plusButton}>
                <PlusButton onClick={() => addNewItem()}/>
              </div>
            </Grid>
          </CardContent>
        </Card>
      </DialogContent>
      <DialogActions className={classes.actions}>
        <Button
          variant={"contained"} color={"grey"}
          onClick={() => props.setOpen(false)}
        >
          {t("common:cancel")}
        </Button>
        <Button
          variant={"contained"} color={"primary"}
          onClick={onSubmit}
        >
          {t("common:save")}
        </Button>
      </DialogActions>
    </Dialog>
  )
}