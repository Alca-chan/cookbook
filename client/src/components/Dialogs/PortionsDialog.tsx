import {Button, Dialog, DialogActions, DialogContent, DialogTitle, Radio, Stack, TextField} from "@mui/material";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";

export const PortionsDialog = (props: { open: boolean, setOpen: (open: boolean) => void, onSave: (selectedPortions: number, value: number) => void, title: string, defaultPortions?: number, defaultMultiplier?: number, defaultOption?: number, children?: React.ReactNode }) => {
  const {t} = useTranslation()
  const [selectedPortions, setSelectedPortions] = useState<number>(props.defaultPortions ?? 4)
  const [selectedMultiplier, setSelectedMultiplier] = useState<number>(props.defaultMultiplier ?? 1)
  const [selectedOption, setSelectedOption] = useState<number>(props.defaultOption ?? 0)

  const onChangePortions = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const portions = parseInt(e.target.value)
    if (portions)
      setSelectedPortions(portions)
  }

  const onChangeMultiplier = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const multiplier = parseFloat(e.target.value)
    if (multiplier)
      setSelectedMultiplier(multiplier)
  }

  const onChangeOption = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedOption(parseInt(e.target.value) ?? 0)
  }

  const onSubmit = () => {
    if (!selectedPortions) return
    props.setOpen(false)
    props.onSave(selectedOption, selectedOption === 0 ? selectedPortions : selectedMultiplier)
  }

  return (
    <Dialog
      fullWidth
      open={props.open}
      onClose={() => props.setOpen(false)}
    >
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        {props.children}
        <Stack direction={"column"}>
          <Stack direction={"row"} alignItems={"center"}>
            <Radio
              color={"primary"}
              value={0}
              checked={selectedOption === 0}
              onChange={(e) => onChangeOption(e)}
            />
            <TextField
              fullWidth
              id="portions"
              type='number'
              inputProps={{
                min: "1", step: "1"
              }}
              label={t("recipePage:portions")}
              variant={"outlined"}
              margin={"normal"}
              value={selectedPortions.toString()}
              onChange={(e) => onChangePortions(e)}
              onFocus={() => setSelectedOption(0)}
            />
          </Stack>
          <Stack direction={"row"} alignItems={"center"}>
            <Radio
              color={"primary"}
              value={1}
              checked={selectedOption === 1}
              onChange={(e) => onChangeOption(e)}
            />
            <TextField
              fullWidth
              id="multiply"
              type='number'
              inputProps={{
                min: "0", step: "0.5"
              }}
              label={t("recipePage:multiplyBy")}
              variant={"outlined"}
              margin={"normal"}
              value={selectedMultiplier.toString()}
              onChange={(e) => onChangeMultiplier(e)}
              onFocus={() => setSelectedOption(1)}
            />
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button
          variant={"contained"} color={"grey"}
          onClick={() => props.setOpen(false)}
        >
          {t("recipePage:cancel")}
        </Button>
        <Button
          variant={"contained"} color={"primary"}
          onClick={onSubmit}
        >
          {t("recipePage:save")}
        </Button>
      </DialogActions>
    </Dialog>
  )
}