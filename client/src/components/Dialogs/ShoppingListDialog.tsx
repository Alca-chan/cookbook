import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Typography
} from "@mui/material";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {CartItem, CartItemToAdd} from "../../api/types";
import {Loading} from "../Loading";
import {ShoppingListItemRow} from "../ShoppingList/ShoppingListItemRow";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  actions: {
    marginTop: theme.spacing(2)
  },
  sectionCard: {
    marginTop: theme.spacing(1)
  },
  sectionTitle: {
    width: "100%",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor: theme.palette.table.subHeader
  },
  sectionItems: {
    padding: 0,
    paddingTop: theme.spacing(1)
  }
}))

type Item = (CartItemToAdd | CartItem) & { checked: boolean, inCart: boolean, changed: boolean }

type CartItemSection = {
  title?: string,
  items: Item[]
}

export const ShoppingListDialog = (props: { open: boolean, setOpen: (open: boolean) => void, data: CartItemSection[], onSubmit: (data: CartItemSection[]) => void, title: string, children?: React.ReactNode }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [cartItemSections, setCartItemSections] = useState<CartItemSection[]>()

  const data = props.data
  useEffect(() => {
    setCartItemSections(data)
  }, [data])

  const onSubmit = () => {
    if (cartItemSections)
      props.onSubmit(cartItemSections)
  }

  const updateItem = (item: Item, changedItem: Item) => {
    if (item.ingredient !== changedItem.ingredient) {
      item.ingredient = changedItem.ingredient
      item.changed = true
    }
    if (item.measure !== changedItem.measure) {
      item.measure = changedItem.measure
      item.changed = true
    }
    if (cartItemSections) setCartItemSections([...cartItemSections])
  }

  const changeChecked = (item: (CartItemToAdd & { checked: boolean })) => {
    item.checked = !item.checked
    if (cartItemSections) setCartItemSections([...cartItemSections])
  }

  if (!cartItemSections) {
    return <Loading/>
  }

  return (
    <Dialog
      fullWidth
      open={props.open}
      onClose={() => props.setOpen(false)}
    >
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        {props.children}
        <Grid container direction={"column"}>
          {
            cartItemSections.map((section, sectionIndex) =>
              <Card variant={"outlined"} key={sectionIndex} className={classes.sectionCard}>
                {
                  section.title &&
                  <CardHeader className={classes.sectionTitle} title={<Typography variant={"h6"}>
                    {section.title}
                  </Typography>}/>
                }
                <CardContent className={classes.sectionItems}>
                  <Grid container>
                    {
                      section.items.map((item, itemIndex) =>
                        <Grid item xs={12} key={itemIndex}>
                          <ShoppingListItemRow
                            item={item} editMode={true}
                            checked={item.checked} onCheckboxClick={() => changeChecked(item)}
                            updateItem={updateItem}
                          />
                        </Grid>
                      )
                    }
                  </Grid>
                </CardContent>
              </Card>
            )
          }
        </Grid>
      </DialogContent>
      <DialogActions className={classes.actions}>
        <Button
          variant={"contained"} color={"grey"}
          onClick={() => props.setOpen(false)}
        >
          {t("common:cancel")}
        </Button>
        <Button
          variant={"contained"} color={"primary"}
          onClick={onSubmit}
        >
          {t("common:save")}
        </Button>
      </DialogActions>
    </Dialog>
  )
}