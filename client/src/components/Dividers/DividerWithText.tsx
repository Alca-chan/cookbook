import React from "react";
import {Typography} from "@mui/material";
import {HorizontalDivider} from "./HorizontalDivider";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  content: {
    paddingTop: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    color: 'gray'
  }
}))

export const DividerWithText = (props: { text: string }) => {
  const {classes} = useStyles()

  return (
    <HorizontalDivider>
      {
        props.text !== "" &&
        <>
            <Typography variant='h5' align='center' component={'span'} className={classes.content}>
              {props.text}
            </Typography>
        </>
      }
    </HorizontalDivider>
  )
}