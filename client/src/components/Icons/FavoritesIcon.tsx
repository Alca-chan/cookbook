import {SvgIcon, SvgIconProps} from "@mui/material";
import React from "react";
import {ReactComponent as ChefsHat} from "../Icons/chefs-hat.svg";

export const FavoritesIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon {...props}>
      <ChefsHat/>
    </SvgIcon>
  )
}