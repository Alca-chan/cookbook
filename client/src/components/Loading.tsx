import React from "react";
import {CircularProgress} from "@mui/material";
import {makeStyles} from "../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  loading: {
    marginTop: theme.spacing(5)
  }
}))

export const Loading = () => {
  const {classes} = useStyles()

  return (
    <CircularProgress className={classes.loading}/>
  )
}