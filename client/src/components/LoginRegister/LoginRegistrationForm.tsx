import React, {useContext, useEffect, useState} from "react";
import {Button, Card, CardActions, CardContent, TextField} from "@mui/material";
import Typography from "@mui/material/Typography";
import {Action} from "../../pages/Login";
import {checkTakenUsername, loginUser, registerUser} from "../../api/user";
import {UserContext} from "../../contexts/UserContext";
import {Link} from "react-router-dom";
import {Trans, useTranslation} from "react-i18next";
import {ServerResponse} from "../../api/utils";
import {getRoute} from "../../utils/routes";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  card: {
    padding: theme.spacing(1)
  },
  errorText: {
    display: "flex",
    width: "100%"
  },
  forgotPasswordText: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: theme.spacing(1)
  }
}))

const PASSWORD_MIN_LENGTH = 6

export const LoginRegistrationForm = (props: { actionName: Action }) => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const {updateUser} = useContext(UserContext)
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [error, setError] = useState('')
  const [userNameTaken, setUserNameTaken] = useState(false)
  const [userNameError, setUserNameError] = useState(false)
  const [emailError, setEmailError] = useState(false)
  const [passwordError, setPasswordError] = useState(false)
  const [confirmPasswordError, setConfirmPasswordError] = useState(false)

  useEffect(() => {
    let mounted = false
    if (!mounted) resetErrors()
    return () => {
      mounted = true
    }
  }, [props.actionName])

  const resetErrors = () => {
    setError("")
    setUserNameTaken(false)
    setUserNameError(false)
    setEmailError(false)
    setPasswordError(false)
    setConfirmPasswordError(false)
  }

  const checkEmailAddress = (): boolean => {
    const emailRegEx: RegExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (email && emailRegEx.test(email.toLowerCase())) {
      return true
    }
    setEmailError(true)
    return false
  }

  const checkUsername = (): boolean => {
    if (/^[0-9a-zA-Z_.-]+$/.test(username)) {
      return true
    }
    setUserNameError(true)
    return false
  }

  const checkPassword = (): boolean => {
    if (password.length >= PASSWORD_MIN_LENGTH) {
      return true
    }
    setPasswordError(true)
    return false
  }

  const checkConfirmPassword = (): boolean => {
    if (password === confirmPassword) {
      return true
    }
    setConfirmPasswordError(true)
    return false
  }

  const onSubmitLogin = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const result: ServerResponse = await loginUser(username, password)
    if (result === ServerResponse.AuthOK) {
      updateUser()
    } else if (result === ServerResponse.AuthError) {
      setError(t("login:wrongCredentials"))
    } else {
      setError(t("login:serverError"))
    }
  }

  const onSubmitRegister = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (!checkUsername()) return
    const takenUsername = await checkTakenUsername(username)
    const validEmail = checkEmailAddress()
    const validPassword = checkPassword() && checkConfirmPassword()
    if (takenUsername) setUserNameTaken(true)
    if (!validEmail || !validPassword || takenUsername) return

    const result: ServerResponse = await registerUser(username, email, password)
    if (result === ServerResponse.RegOK) {
      updateUser()
    } else if (result === ServerResponse.RegError) {
      setUserNameTaken(true)
    } else {
      setError(t("login:serverError"))
    }
  }

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    resetErrors()
    if (props.actionName === "Login") {
      await onSubmitLogin(e)
    } else {
      await onSubmitRegister(e)
    }
  }

  return (
    <form noValidate autoComplete='off' onSubmit={e => onSubmit(e)}>
      <Card className={classes.card}>
        <CardContent>
          <TextField
            fullWidth
            id="username"
            type='username'
            label={t("login:username")}
            margin='normal'
            variant='outlined'
            value={username}
            onChange={e => setUsername(e.target.value)}
            error={userNameTaken || userNameError}
            helperText={userNameError ? t("login:usernameError") : (userNameTaken ? t("login:usernameTaken") : "")}
          />
          {
            props.actionName === 'Register' && (
              <TextField
                fullWidth
                id="email"
                type='email'
                label={t("login:email")}
                margin='normal'
                variant='outlined'
                value={email}
                onChange={e => setEmail(e.target.value)}
                error={emailError}
                helperText={emailError ? t("login:emailError") : ""}
              />
            )
          }
          <TextField
            fullWidth
            id="password"
            type='password'
            label={t("login:password")}
            margin='normal'
            variant='outlined'
            onChange={e => setPassword(e.target.value)}
            error={passwordError}
            helperText={passwordError ? t("login:passwordError", {length: PASSWORD_MIN_LENGTH}) : ""}
          />
          {
            props.actionName === 'Register' && (
              <TextField
                fullWidth
                id="confirmPassword"
                type='password'
                label={t("login:confirmPassword")}
                margin='normal'
                variant='outlined'
                onChange={e => setConfirmPassword(e.target.value)}
                error={confirmPasswordError}
                helperText={confirmPasswordError ? t("login:confirmPasswordError") : ""}
              />
            )
          }
          {
            error && (
              <div className={classes.errorText}>
                <Typography variant='subtitle2' align='left' color='error' component={'span'}>{error}</Typography>
              </div>
            )
          }
          {
            props.actionName === 'Login' && (
              <div className={classes.forgotPasswordText}>
                <Typography variant={"body2"}>
                  <Trans i18nKey="login:resetPasswordText">
                    <Link to={getRoute(i18n.language, "/password-reset/:token?").replace(":token?", "")}/>
                  </Trans>
                </Typography>
              </div>
            )
          }
        </CardContent>
        <CardActions>
          <Button
            className={classes.button}
            variant={"contained"} color={"primary"}
            size='large'
            type='submit'>
            {props.actionName === 'Login' ? t("login:loginButton") : t("login:registerButton")}
          </Button>
        </CardActions>
      </Card>
    </form>
  )
}