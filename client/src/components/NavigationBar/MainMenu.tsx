import {IconButton, Menu, MenuItem} from "@mui/material";
import React, {useContext} from "react";
import {Add, Event, ExitToApp as Logout, List, Menu as MenuIcon, Settings, ShoppingCart} from "@mui/icons-material";
import {useTranslation} from "react-i18next";
import {UserContext} from "../../contexts/UserContext";
import {useHistory} from "react-router-dom";
import {getRoute} from "../../utils/routes";
import {makeStyles} from "../../utils/makeStyles";
import {FavoritesIcon} from "../Icons/FavoritesIcon";

const useStyles = makeStyles()(theme => ({
  icon: {
    marginRight: theme.spacing(1)
  }
}))

export const MainMenu = () => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const history = useHistory()
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null)
  const {logoutUser} = useContext(UserContext)
  const lang = i18n.language

  const onMenuClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchor(e.currentTarget)
  }

  const onMenuClose = () => {
    setAnchor(null)
  }

  const onOptionSelect = (e: React.MouseEvent<HTMLLIElement, MouseEvent>, action: () => void) => {
    e.stopPropagation()
    e.preventDefault()
    setAnchor(null) // close menu
    action()
  }

  return (
    <div>
      <IconButton size={"large"} onClick={(e) => {
        e.stopPropagation()
        e.preventDefault()
        onMenuClick(e)
      }}>
        <MenuIcon/>
      </IconButton>
      <Menu open={anchor !== null} anchorEl={anchor}
            anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
            PaperProps={{
              style: {
                transform: 'translateX(-10px) translateY(10px)',
              }
            }}
            onClose={onMenuClose}
            onClick={(e) => {
              e.stopPropagation()
              e.preventDefault()
            }}>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => history.push(getRoute(lang, "/my-recipes")))
          }}>
          <List className={classes.icon}/>{t("userRecipes:userRecipes")}
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => history.push(getRoute(lang, "/new-recipe")))
          }}>
          <Add className={classes.icon}/>{t("userRecipes:createRecipe")}
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => history.push(getRoute(lang, "/calendar")))
          }}>
          <Event className={classes.icon}/>{t("userCalendar:userCalendar")}
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => history.push(getRoute(lang, "/my-cookbooks")))
          }}>
          <FavoritesIcon className={classes.icon}/>{t("favorites:favorites")}
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => history.push(getRoute(lang, "/shopping-list")))
          }}>
          <ShoppingCart className={classes.icon}/>{t("cart:cart")}
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => history.push(getRoute(lang, "/settings")))
          }}>
          <Settings className={classes.icon}/>{t("common:settings")}
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            onOptionSelect(e, () => logoutUser())
          }}>
          <Logout className={classes.icon}/>{t("login:logout")}
        </MenuItem>
      </Menu>
    </div>
  )
}