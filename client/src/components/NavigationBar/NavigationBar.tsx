import {alpha, Badge, IconButton, Stack} from "@mui/material";
import {Link} from "react-router-dom";
import HomeRoundedIcon from "@mui/icons-material/HomeRounded";
import Button from "@mui/material/Button";
import Toolbar from "@mui/material/Toolbar";
import React, {useContext, useMemo} from "react";
import {UserContext} from "../../contexts/UserContext";
import {FavoritesContext} from "../../contexts/FavoritesContext";
import red from "@mui/material/colors/red";
import {Event, ShoppingCart, ShoppingCartOutlined} from "@mui/icons-material";
import {CartContext} from "../../contexts/CartContext";
import {useTranslation} from "react-i18next";
import {MainMenu} from "./MainMenu";
import {getRoute} from "../../utils/routes";
import {makeStyles, withStyles} from "../../utils/makeStyles";
import {FavoritesIcon} from "../Icons/FavoritesIcon";
import {DEBUG_LANGUAGES} from "../../utils/utils";

const StyledBadge = withStyles(Badge, theme => ({
  badge: {
    right: -5,
    top: 22,
    backgroundColor: theme.palette.secondary.light
  }
}))

const useStyles = makeStyles()(theme => ({
  buttons: {
    width: "100%"
  },
  menuButton: {
    margin: theme.spacing(1, 0.5, 1, 0.5),
    [theme.breakpoints.up('sm')]: {
      marginBottom: theme.spacing(0),
      marginTop: theme.spacing(0)
    }
  },
  favoriteButton: {
    marginRight: theme.spacing(1),
    '&:hover': {
      color: red[400]
    }
  },
  calendarButton: {
    '&:hover': {
      color: red[400]
    }
  },
  cartButton: {
    marginRight: theme.spacing(1),
    '&:hover': {
      color: red[400]
    }
  },
  homeButton: {
    width: "100%"
  },
  homeButtonIcon: {
    marginRight: theme.spacing(1),
    color: alpha(theme.palette.common.black, 0.545)
  },
  link: {
    textDecoration: 'none'
  },
  languageButton: {
    borderRadius: '25px'
  }
}))

export const NavigationBar = () => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const lang = i18n.language
  const {user} = useContext(UserContext)
  const {cart} = useContext(CartContext)
  const {favorites} = useContext(FavoritesContext)

  const nextLanguage = useMemo(() => {
    return i18n.language === "en" ? "cs" : "en"
  }, [i18n.language])

  const changeLanguage = () => {
    i18n.changeLanguage(nextLanguage).then()
  }

  return (
    <Toolbar>
      <Stack className={classes.buttons} direction={{xs: "column", sm: "row"}}
             justifyContent={{sx: "center", sm: "space-between"}} alignItems={{xs: "flex-start", sm: "center"}}>
        <Link className={classes.link} to='/'>
          <Button className={classes.homeButton} size={"large"} color={"grey"}>
            <HomeRoundedIcon className={classes.homeButtonIcon}/>
            {t("common:recipes")}
          </Button>
        </Link>
        <Stack className={classes.buttons} direction={"row"} spacing={1}
               alignItems={"center"} justifyContent={"flex-end"}>
          {
            DEBUG_LANGUAGES && (
              <Button color={"grey"} onClick={() => changeLanguage()} className={classes.languageButton}>
                {nextLanguage.toUpperCase()}
              </Button>
            )
          }
          {
            user && (
              <>
                <Link className={classes.link} to={getRoute(lang, '/calendar')}>
                  <IconButton size={"large"} className={classes.calendarButton}>
                    <Event/>
                  </IconButton>
                </Link>
                <Link className={classes.link} to={getRoute(lang, '/my-cookbooks')}>
                  <IconButton size={"large"} className={classes.favoriteButton}>
                    {
                      favorites ?
                        <StyledBadge badgeContent={favorites.length} max={99} showZero>
                          <FavoritesIcon/>
                        </StyledBadge>
                        : <FavoritesIcon/>
                    }
                  </IconButton>
                </Link>
                <Link className={classes.link} to={getRoute(lang, '/shopping-list')}>
                  <IconButton size={"large"} className={classes.cartButton}>
                    {
                      cart ?
                        <StyledBadge badgeContent={cart.length} max={99} showZero>
                          <ShoppingCart/>
                        </StyledBadge>
                        : <ShoppingCartOutlined/>
                    }
                  </IconButton>
                </Link>
              </>
            )
          }
          {
            !user
              ?
              <Link className={classes.link} to={getRoute(lang, '/login')}>
                <Button className={classes.menuButton} variant={"contained"} color={"primary"} size={"large"}>
                  {t("login:login")}
                </Button>
              </Link>
              :
              <MainMenu/>
          }
        </Stack>
      </Stack>
    </Toolbar>
  )
}