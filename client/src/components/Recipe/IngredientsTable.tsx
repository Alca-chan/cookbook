import React, {useCallback, useContext, useEffect, useState} from "react";
import {
  Button,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography
} from "@mui/material";
import {IngredientMeasure, Recipe} from "../../api/types";
import {ShoppingCart} from "@mui/icons-material";
import {UserContext} from "../../contexts/UserContext";
import {PortionsDialog} from "../Dialogs/PortionsDialog";
import {TableCellCheckbox} from "./TableCellCheckbox";
import {useTranslation} from "react-i18next";
import {getMeasureIngredientUnitString} from "../../utils/utils";
import {recipeToRecipeShort} from "../../api/utils";
import {EditShoppingListDialog} from "../Dialogs/EditShoppingListDialog";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    width: '100%'
  },
  tableCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border
  },
  actionCell: {
    padding: 0
  },
  sectionTitleCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border,
    fontSize: 18,
    backgroundColor: theme.palette.table.subHeader
  },
  headerCell: {
    borderWidth: 0,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border,
    fontSize: 18
  },
  headerCellPortions: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border,
    fontSize: 18
  },
  headerCellActions: {
    padding: 0,
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border
  },
  tableHeader: {
    backgroundColor: theme.palette.primary.light
  },
  tableHeaderPortions: {
    backgroundColor: theme.palette.table.subHeader
  },
  shoppingCartIcon: {
    marginLeft: theme.spacing(0.25),
    marginRight: theme.spacing(0.25)
  },
  checkbox: {
    height: '18px',
    width: '18px'
  },
  noteText: {
    marginLeft: theme.spacing(1)
  }
}))

export const IngredientsTable = (props: { recipe: Recipe, portions: number, portionsNote: string }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {user} = useContext(UserContext)
  const [data, setData] = useState<{ order: number, title?: string, ingredients: (IngredientMeasure & { checked: boolean })[] }[]>([])
  const [openPortionsDialog, setOpenPortionsDialog] = useState(false)
  const [selectedPortions, setSelectedPortions] = useState<number>(props.portions)
  const [selectedMultiplier, setSelectedMultiplier] = useState<number>(1)
  const [selectedOption, setSelectedOption] = useState<number>(0)
  const [openShoppingListDialog, setOpenShoppingListDialog] = useState(false)

  const recipeIngredients = props.recipe.ingredients

  const getData = useCallback(() => {
    const ingredients = recipeIngredients.map(s => ({
      ...s,
      ingredients: s.ingredients.map(i => ({
        ...i,
        checked: false
      }))
    }))
    setData(ingredients)
  }, [recipeIngredients])

  useEffect(() => {
    getData()
  }, [getData])

  const addToCart = () => {
    setOpenShoppingListDialog(true)
  }

  const onPortionsChanged = (option: number, value: number) => {
    if (option === 0) {
      setSelectedPortions(value)
      setSelectedMultiplier(value / props.portions)
    } else {
      setSelectedMultiplier(value)
      setSelectedPortions(props.portions * value)
    }
    setSelectedOption(option)
  }

  const onCheck = (row: IngredientMeasure & { checked: boolean }, checked: boolean) => {
    row.checked = checked
    setData([...data])
  }

  return (
    <Paper className={classes.container}>
      {
        openPortionsDialog && (
          <PortionsDialog open={openPortionsDialog} setOpen={setOpenPortionsDialog}
                          onSave={onPortionsChanged}
                          title={t("recipePage:changePortions")}
                          defaultOption={selectedOption}
                          defaultPortions={selectedPortions}
                          defaultMultiplier={selectedMultiplier}
          />
        )
      }
      {
        openShoppingListDialog && (
          <EditShoppingListDialog open={openShoppingListDialog} setOpen={setOpenShoppingListDialog}
                                  recipe={recipeToRecipeShort(props.recipe)} ingredientSections={data}
          />
        )
      }
      <Table className={classes.container}>
        <TableHead className={classes.container}>
          <TableRow className={classes.tableHeader}>
            <TableCell align={"center"} colSpan={4} className={classes.headerCell}>
              {t("recipePage:ingredients")}
            </TableCell>
          </TableRow>
          <TableRow className={classes.tableHeaderPortions}>
            {
              user && (
                <TableCell className={classes.headerCellActions} align='center'>
                  <IconButton onClick={() => addToCart()} className={classes.shoppingCartIcon}>
                    <ShoppingCart fontSize='small'/>
                  </IconButton>
                </TableCell>
              )
            }
            <TableCell align={"left"} colSpan={3} className={classes.headerCellPortions}>
              <Grid container alignItems={"center"} justifyContent={"flex-start"} spacing={3}>
                <Grid item>
                  <Typography variant={"body1"}>
                    {t("recipePage:portions")}: {selectedOption === 0 ? selectedPortions : props.portions * selectedMultiplier}{props.portionsNote.length > 0 ? " (" + props.portionsNote + ")" : ""}
                  </Typography>
                </Grid>
                <Grid item>
                  <Button variant={"contained"} color={"grey"} size={"small"}
                          onClick={() => setOpenPortionsDialog(true)}>{t("recipePage:changeButton")}</Button>
                </Grid>
              </Grid>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            data.map((section, sectionIndex) => {

              const ingredientsRows = (
                section.ingredients.map((row, index) => {
                  const measure = row.measure === undefined ? undefined : (selectedOption === 0 ? (selectedPortions / props.portions) * row.measure : row.measure * selectedMultiplier)
                  const {
                    measureString,
                    unitString,
                    ingredientString
                  } = getMeasureIngredientUnitString(measure, row.unit, row.ingredient)
                  return (
                    <TableRow key={sectionIndex + "_" + index}>
                      <TableCellCheckbox checked={row.checked} onClick={(checked) => onCheck(row, checked)}/>
                      <TableCell width={"20%"} className={classes.tableCell}
                                 align='right'>{(measureString.length > 0 ? (measureString + " ") : "") + unitString}</TableCell>
                      <TableCell className={classes.tableCell}>
                        {ingredientString}
                        {
                          (row.note && row.note.replace(/\s/g, "").length > 0) &&
                          <Typography className={classes.noteText} variant={"body2"}
                                      color={"textSecondary"}>{row.note}</Typography>
                        }
                      </TableCell>
                    </TableRow>
                  )
                })
              )

              if (!section.title) {
                return ingredientsRows
              }

              return (
                [
                  <TableRow key={sectionIndex}>
                    <TableCell className={classes.sectionTitleCell} colSpan={4}
                               align={'center'}>{section.title}</TableCell>
                  </TableRow>
                  ,
                  ingredientsRows
                ]
              )
            })
          }
        </TableBody>
      </Table>
    </Paper>
  )
}
