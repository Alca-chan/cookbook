import React, {useState} from "react";
import {Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@mui/material";
import {MethodStep} from "../../api/types";
import {TableCellCheckbox} from "./TableCellCheckbox";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    width: '100%'
  },
  headerCell: {
    width: '100%',
    fontSize: 18
  },
  tableHeader: {
    backgroundColor: theme.palette.primary.light
  },
  tableRow: {},
  tableRowCurrent: {
    backgroundColor: theme.palette.table.highlight
  },
  tableCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border
  }
}))

export const Instructions = (props: { data: MethodStep[] }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [checked, setChecked] = useState<boolean[]>(new Array(props.data.length).fill(false))
  const [current, setCurrent] = useState<number>(0)

  const onCheckboxClick = (selected: boolean, index: number) => {
    const newChecked = [...checked]
    newChecked[index] = selected
    setChecked(newChecked)
    const newCurrent = newChecked.findIndex(i => !i)
    setCurrent(newCurrent)
  }

  return (
    <Paper className={classes.container}>
      <Table>
        <TableHead>
          <TableRow className={classes.tableHeader}>
            <TableCell colSpan={3} align='center' className={classes.headerCell}>{t("recipePage:method")}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            props.data.map((step, index) =>
              <TableRow key={index} className={index === current ? classes.tableRowCurrent : classes.tableRow}>
                <TableCellCheckbox onClick={(selected) => onCheckboxClick(selected, index)}/>
                <TableCell align='right' width={"10%"} className={classes.tableCell}>{index + 1}.</TableCell>
                <TableCell className={classes.tableCell}>{step.description}</TableCell>
              </TableRow>
            )
          }
        </TableBody>
      </Table>
    </Paper>
  )
}