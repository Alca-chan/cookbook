import {Grid, Paper, Typography} from "@mui/material";
import React from "react";
import {Link} from "react-router-dom";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  labelGridText: {
    marginTop: theme.spacing(2)
  },
  labelGrid: {
    marginTop: theme.spacing(0.5)
  },
  label: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    background: theme.palette.label.light
  },
  link: {
    textDecoration: 'none'
  }
}))

export const Labels = (props: { data: { name: string, id: string }[], title: string, getUrl: (id: string) => string }) => {
  const {classes} = useStyles()

  return (
    <>
      <Typography align={"left"} className={classes.labelGridText}>
        {props.title}:
      </Typography>
      <Grid container spacing={1} className={classes.labelGrid}>
        {
          props.data.map((item) =>
            <Grid item key={item.id}>
              <Link className={classes.link} to={props.getUrl(item.id)}>
                <Paper className={classes.label}>
                  <Typography style={{width: '100%', height: '100%'}}>
                    {item.name}
                  </Typography>
                </Paper>
              </Link>
            </Grid>
          )
        }
      </Grid>
    </>
  )
}