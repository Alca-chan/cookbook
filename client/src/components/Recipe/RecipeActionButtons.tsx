import {IconButton, Stack, useMediaQuery, useTheme} from "@mui/material";
import {FavoritesIconButton} from "../Buttons/FavoritesIconButton";
import {Link} from "@mui/icons-material";
import {RecipeUserMenu} from "./RecipeUserMenu";
import React, {useContext, useMemo} from "react";
import {makeStyles} from "../../utils/makeStyles";
import {RecipeShort} from "../../api/types";
import {UserContext} from "../../contexts/UserContext";
import {copyLinkToast} from "../../utils/toasts";
import {useTranslation} from "react-i18next";
import {AddToCalendarIconButton} from "../Buttons/AddToCalendarIconButton";
import {InCalendarIconButton} from "../Buttons/InCalendarIconButton";
import {CalendarContext} from "../../contexts/CalendarContext";

const moment = require('moment')

const useStyles = makeStyles()(() => ({
  fullWidth: {
    width: "100%"
  },
  linkIcon: {
    transform: "rotate(-45deg)"
  }
}))

export const RecipeActionButtons = (props: { recipe: RecipeShort }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {user} = useContext(UserContext)
  const {calendar} = useContext(CalendarContext)
  const theme = useTheme()
  const smallDisplay = useMediaQuery(theme.breakpoints.down('sm'))
  const recipeId = props.recipe.id

  const closestDate = useMemo(() => {
    if (!calendar || !recipeId) return undefined
    const sorted = calendar
      .filter(e => e.recipeId === recipeId && moment().isSameOrBefore(moment(e.date), 'day'))
      .sort((a, b) => moment(a.date).isBefore(b.date))
    return sorted.length > 0 ? sorted[0].date : undefined
  }, [calendar, recipeId])
  const onLinkClick = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation()
    e.preventDefault()
    await copyLinkToast(t, navigator.clipboard.writeText(window.location.href))
  }

  const calendarButtons = (
    <Stack direction={"row"} spacing={1} alignItems={"center"}>
      {
        user && (
          <AddToCalendarIconButton recipe={props.recipe} changeOnAdd={false} background={true}/>
        )
      }
      {
        closestDate && (
          <InCalendarIconButton recipe={props.recipe} date={moment(closestDate)} background={true}/>
        )
      }
    </Stack>
  )

  const inlineCalendarIcons = !(smallDisplay && closestDate)

  return (
    <Stack direction={"column"} alignItems={"flex-start"} className={classes.fullWidth} spacing={1}>
      <Stack direction={"row"} justifyContent={"space-between"} className={classes.fullWidth}>
        <Stack direction={"row"} spacing={1} alignItems={"center"}>
          <FavoritesIconButton recipeId={props.recipe.id} votes={props.recipe.votes} background={true}/>
          {
            inlineCalendarIcons && calendarButtons
          }
        </Stack>
        <Stack direction={"row"} spacing={1} alignItems={"flex-start"}>
          <IconButton size={"large"} onClick={(e) => onLinkClick(e)}>
            <Link className={classes.linkIcon}/>
          </IconButton>
          {
            (props.recipe.userId && user && user.id === props.recipe.userId) && (
              <RecipeUserMenu recipe={props.recipe}/>
            )
          }
        </Stack>
      </Stack>
      {
        !inlineCalendarIcons && calendarButtons
      }
    </Stack>
  )
}