import {IconButton, Menu, MenuItem} from "@mui/material";
import {Delete, Edit, MoreVert} from "@mui/icons-material";
import React, {useState} from "react";
import {getRoute} from "../../utils/routes";
import {useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {Recipe, RecipeShort} from "../../api/types";
import {DeleteRecipeDialog} from "../Dialogs/DeleteRecipeDialog";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()((theme) => ({
  moreButton: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  menuOptionIcon: {
    marginRight: theme.spacing(1)
  }
}))

export const RecipeUserMenu = (props: { recipe: Recipe | RecipeShort }) => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const history = useHistory()
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)

  const edit = () => {
    history.push(getRoute(i18n.language, "/recipe/:id/edit").replace(":id", props.recipe.id))
  }

  const onRemoveRecipe = () => {
    setAnchor(null)
    setOpenDeleteDialog(true)
  }

  const onMenuClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchor(e.currentTarget)
  }

  const onMenuClose = () => {
    setAnchor(null)
  }

  return (
    <>
      <DeleteRecipeDialog open={openDeleteDialog} setOpen={setOpenDeleteDialog} recipe={props.recipe}/>
      <IconButton size={"large"} className={classes.moreButton} onClick={onMenuClick}>
        <MoreVert/>
      </IconButton>
      <Menu open={anchor !== null} anchorEl={anchor}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            onClose={onMenuClose}>

        <MenuItem onClick={edit}>
          <Edit fontSize={"small"}
                className={classes.menuOptionIcon}/>{t("common:editRecipe")}</MenuItem>

        <MenuItem onClick={onRemoveRecipe}>
          <Delete fontSize={"small"}
                  className={classes.menuOptionIcon}/>{t("common:deleteRecipe")}</MenuItem>
      </Menu>
    </>
  )
}