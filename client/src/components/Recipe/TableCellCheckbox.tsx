import {Checkbox, TableCell} from "@mui/material";
import React, {useState} from "react";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  checkboxCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border,
    '&:hover': {
      cursor: 'pointer'
    },
  },
  checkbox: {
    height: '18px',
    width: '18px'
  }
}))

export const TableCellCheckbox = (props: { checked?: boolean, onClick?: (selected: boolean) => void }) => {
  const {classes} = useStyles()
  const [selected, setSelected] = useState(props.checked === undefined ? false : props.checked)

  const onClick = (e: React.MouseEvent<HTMLTableHeaderCellElement, MouseEvent>) => {
    e.preventDefault()
    e.stopPropagation()
    const newSelected = !selected
    setSelected(newSelected)
    if (props.onClick) props.onClick(newSelected)
  }

  return (
    <TableCell width={"10%"} className={classes.checkboxCell} align='center' onClick={(e) => onClick(e)}>
      <Checkbox className={classes.checkbox} checked={props.checked === undefined ? selected : props.checked}
                color={"primary"}/>
    </TableCell>
  )
}