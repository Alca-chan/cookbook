import React from "react";
import {minutesToTime} from "../../utils/utils";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  background: {
    borderRadius: "25px",
    background: theme.palette.label.main,
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(1.5),
    paddingRight: theme.spacing(1.5)
  }
}))

export const TimeLabel = (props: { minutes: number }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()

  const minutesToTimeString = (minutes: number): string => {
    const time = minutesToTime(minutes)
    const result: string[] = []
    if (time.days > 0) {
      result.push(time.days + " " + t("recipeForm:timeDay", {count: time.days}))
    }
    if (time.hours > 0) {
      result.push(time.hours + " " + t("recipeForm:timeHour", {count: time.hours}))
    }
    if (time.minutes > 0) {
      result.push(time.minutes + " " + t("recipeForm:timeMin", {count: time.minutes}))
    }
    return result.join(" ")
  }

  return (
    <div className={classes.background}>{minutesToTimeString(props.minutes)}</div>
  )
}