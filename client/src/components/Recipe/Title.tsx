import React from "react";
import {Typography} from "@mui/material";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  bigLetters: {
    display: 'inline',
    fontWeight: 500,
    fontSize: 38,
    [theme.breakpoints.up('sm')]: {
      fontSize: 42
    },
    [theme.breakpoints.up('md')]: {
      fontSize: 48
    }
  },
  smallLetters: {
    display: 'inline',
    fontWeight: 500,
    fontSize: 32,
    [theme.breakpoints.up('sm')]: {
      fontSize: 36
    },
    [theme.breakpoints.up('md')]: {
      fontSize: 42
    }
  }
}))

export const Title = (props: { title: string }) => {
  const {classes} = useStyles()

  const onCopy = (e: React.ClipboardEvent<HTMLDivElement>) => {
    e.clipboardData?.setData('text/plain', props.title + ": " + window.location.href)
    e.preventDefault()
  }

  const createTitle = (i: number) => {
    if (i >= props.title.length) return <></>
    return (
      <>
        {
          props.title[i].match(/^[A-Z].*/) ?
            <Typography className={classes.bigLetters} component='div'>
              {props.title[i].toUpperCase()}
              {createTitle(i + 1)}
            </Typography>
            :
            <Typography className={classes.smallLetters} component='div'>
              {props.title[i].toUpperCase()}
              {createTitle(i + 1)}
            </Typography>
        }
      </>
    )
  }

  return (
    <div onCopy={(e) => onCopy(e)}>
      {createTitle(0)}
    </div>
  )
}