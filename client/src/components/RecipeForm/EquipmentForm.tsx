import {Equipment} from "../../api/types";
import React, {useContext, useState} from "react";
import {Loading} from "../Loading";
import {Autocomplete, TextField} from "@mui/material";
import {SelectedEquipmentGrid} from "./SelectedEquipmentGrid";
import {useTranslation} from "react-i18next";
import {StaticDataContext} from "../../contexts/StaticDataContext";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  inputField: {
    margin: 0,
    width: '100%'
  }
}))

export const EquipmentForm = (props: { equipment: Equipment[], onChange: (equipment: Equipment[]) => void }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {equipment} = useContext(StaticDataContext)
  const [data, setData] = useState<Equipment[]>([...props.equipment])
  const [equipmentValue, setEquipmentValue] = useState<Equipment | null>(null)
  const [equipmentInputValue, setEquipmentInputValue] = useState("")

  if (!equipment) {
    return <Loading/>
  }

  const onChangeEquipment = (e: React.ChangeEvent<{}>, newValue: Equipment | null) => {
    setEquipmentValue(null)
    setEquipmentInputValue("")
    if (newValue != null) {
      const newData = [...data, newValue]
      setData(newData)
      props.onChange(newData)
    }
  }

  const onRemoveEquipment = (equipment: Equipment) => {
    const newData = data.filter(e => e.id !== equipment.id)
    setData(newData)
    props.onChange(newData)
  }

  return (
    <div className={classes.container}>
      {
        data.length > 0 && <SelectedEquipmentGrid data={data} onClick={onRemoveEquipment}/>
      }
      <Autocomplete
        className={classes.inputField}
        value={equipmentValue}
        onChange={onChangeEquipment}
        inputValue={equipmentInputValue}
        onInputChange={(event: React.ChangeEvent<{}>, newInputValue: string) => {
          setEquipmentInputValue(newInputValue)
        }}
        id="equipmentName"
        options={equipment.filter(t => !data.some(selected => selected.id === t.id))}
        getOptionLabel={(option) => option.name}
        noOptionsText={t("recipeForm:noEquipment")}
        renderInput={(params) =>
          <TextField {...params} label={t("recipeForm:searchEquipment")} variant="outlined"/>}
        disableCloseOnSelect={true}
      />
    </div>
  )
}