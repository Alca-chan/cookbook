import {MenuItem, TextField} from "@mui/material";
import React from "react";
import {useTranslation} from "react-i18next";
import {Crop} from "react-image-crop";
import {makeStyles} from "../../../utils/makeStyles";

const useStyles = makeStyles()(() => ({
  aspectSelect: {
    minWidth: "100px"
  }
}))

export const CropAspectMenu = (props: { onChangeAspect: (newCrop: Crop) => void, image: HTMLImageElement | undefined, crop: Crop }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()

  const onChangeAspect = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    e.stopPropagation()
    e.preventDefault()
    if (!props.image) return

    const crop = props.crop
    const image = props.image

    const newAspect = parseFloat(e.target.value) ?? undefined
    if (!newAspect) {
      props.onChangeAspect({...crop, aspect: undefined})
      return
    }

    const min = Math.min(crop.width, crop.height)
    let width = min * newAspect
    let height = min

    let offsetX = Math.max(crop.x + (crop.width - width) / 2, 0)
    let offsetY = Math.max(crop.y + (crop.height - height) / 2, 0)

    if (width > image.width) {
      width = image.width
      height = width / newAspect
      offsetX = 0
    }

    if (height > image.height) {
      height = image.height
      width = height / newAspect
      offsetY = 0
    }

    if (width + offsetX > image.width) {
      offsetX = image.width - width
    }

    if (height + offsetY > image.height) {
      offsetY = image.height - height
    }

    const newCrop: Crop = {
      ...crop,
      x: offsetX,
      y: offsetY,
      width: width,
      height: height,
      aspect: newAspect
    }
    props.onChangeAspect(newCrop)
  }

  return (
    <TextField
      className={classes.aspectSelect}
      variant={"outlined"}
      label={t("recipeForm:aspect")}
      value={props.crop.aspect ?? 0}
      type={"number"}
      select
      size='small'
      onChange={(e) => onChangeAspect(e)}
    >
      <MenuItem value={1}>1:1</MenuItem>
      <MenuItem value={16 / 9}>16:9</MenuItem>
      <MenuItem value={4 / 3}>4:3</MenuItem>
      <MenuItem value={0}>{t("recipeForm:noAspect")}</MenuItem>
    </TextField>
  )
}