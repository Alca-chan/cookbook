import {Button, Collapse, Stack, Typography} from "@mui/material";
import React, {useCallback, useEffect, useState} from "react";
import {cropImage, getImageDimensions, toBase64} from "../../../utils/imageUtils";
import {ImageView} from "../../RecipesSearch/ImageView";
import {Crop as CropIcon, Delete, Edit, Restore} from "@mui/icons-material";
import ReactCrop, {Crop} from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../../utils/makeStyles";
import {CropAspectMenu} from "./CropAspectMenu";
import {RotateImageMenu} from "./RotateImageMenu";
import {HorizontalDivider} from "../../Dividers/HorizontalDivider";

const useStyles = makeStyles<{ imageWidth: number | undefined }>()((theme, {imageWidth}) => ({
  cropContainer: {
    maxWidth: "100%",
    width: "100%",
    background: "#333",
    paddingTop: theme.spacing(0.5)
  },
  fullwidth: {
    width: "100%"
  },
  image: {
    maxWidth: imageWidth ? (imageWidth + "px") : "100%"
  }
}))

export const ImageForm = (props: { onImageFileChange: (image: string, imageFileName: string) => void, onImageCropped: (newImage: string) => void, image: string | undefined, imageFileName: string }) => {
  const [imageDimensions, setImageDimensions] = useState<{ width: number, height: number } | undefined>()
  const {classes} = useStyles({imageWidth: imageDimensions?.width})
  const {t} = useTranslation()
  const [image, setImage] = useState<HTMLImageElement>()
  const [crop, setCrop] = useState<Crop>({unit: '%', width: 0, height: 0, x: 0, y: 0, aspect: 4 / 3})
  const [originalImage, setOriginalImage] = useState<string>()
  const [completedCrop, setCompletedCrop] = useState<Crop>()
  const [openCropper, setOpenCropper] = useState<boolean>(false)
  const [openPreview, setOpenPreview] = useState<boolean>(true)

  const loadImageDimensions = async (image: string | undefined) => {
    if (!image) return
    const imageDim = await getImageDimensions(image)
    setImageDimensions(imageDim)
  }

  useEffect(() => {
    loadImageDimensions(props.image).then()
  }, [props.image])

  const onRevertImage = () => {
    if (!originalImage) return
    props.onImageFileChange(originalImage, props.imageFileName)
  }

  const onSelectFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    let loadedImage: string | undefined = undefined
    let imageName: string | undefined = undefined
    if (e.target.files && e.target.files.length > 0) {
      imageName = e.target.files[0].name
      loadedImage = await toBase64(e.target.files[0])
      setOpenPreview(Boolean(loadedImage))
      setOriginalImage(loadedImage)
      setOpenCropper(false)
      props.onImageFileChange(loadedImage ?? "", imageName ?? "")
    }
  }

  const onImageLoaded = (img: HTMLImageElement) => {
    setImage(img)
  }

  const onImageRotated = async (newImage: HTMLImageElement) => {
    setImage(newImage)
    props.onImageFileChange(newImage.src, props.imageFileName)
  }

  const onChangeAspect = (newCrop: Crop) => {
    setCrop(newCrop)
  }

  const onCropChange = (crop: Crop) => {
    setCrop(crop)
  }

  const onRemoveImage = () => {
    setImage(undefined)
    props.onImageFileChange("", "")
  }

  const onCrop = useCallback(async () => {
    if (!completedCrop || !image) return
    if (completedCrop.height === 0 || completedCrop.width === 0) {
      const newCrop = {...crop, width: image.width / 2, height: image.height / 2}
      setCrop(newCrop)
      setCompletedCrop(newCrop)
      return
    }
    const croppedImage = cropImage(image, completedCrop)
    props.onImageCropped(croppedImage)
    setCrop(c => ({unit: '%', width: 0, height: 0, x: 0, y: 0, aspect: c.aspect}))
  }, [props, completedCrop, image, crop])

  return (
    <Stack direction={"column"} spacing={2} justifyContent={"center"} alignItems={"flex-start"}>
      {
        (!openCropper && props.image) && (
          <>
            <Button variant={"outlined"} color={"grey"} onClick={() => setOpenPreview(!openPreview)}>
              {
                openPreview ? t("recipeForm:hideImage") : t("recipeForm:openImage")
              }
            </Button>
            <Collapse in={openPreview} className={classes.fullwidth}>
              <Stack direction={"column"} spacing={1} alignItems={"center"}>
                <div className={classes.image}>
                  <ImageView img={props.image} width={"100%"}/>
                </div>
                <Stack direction={"column"} justifyContent={"center"} alignItems={"flex-end"} spacing={1}
                       className={classes.fullwidth}>
                  <Stack direction={{xs: "column", sm: "row"}} spacing={1}
                         justifyContent={{xs: "center", sm: "flex-end"}} alignItems={{xs: "flex-end", sm: "center"}}>
                    <Button variant={"contained"} color={"secondary"}
                            onClick={() => setOpenCropper(true)} startIcon={<Edit/>}>
                      {t("recipeForm:editImage")}
                    </Button>
                    <Button variant={"contained"} color={"grey"}
                            onClick={onRevertImage} startIcon={<Restore/>}>
                      {t("recipeForm:revertChanges")}
                    </Button>
                  </Stack>
                  <Button variant={"contained"} color={"grey"}
                          onClick={() => onRemoveImage()} startIcon={<Delete/>}>
                    {t("recipeForm:removeImage")}
                  </Button>
                </Stack>
              </Stack>
            </Collapse>
          </>
        )
      }
      {
        (openCropper) && (
          <Stack direction={"column"} spacing={2} className={classes.fullwidth}>
            <div className={classes.cropContainer}>
              {
                props.image && (
                  <ReactCrop
                    src={props.image}
                    crop={crop}
                    onImageLoaded={onImageLoaded}
                    onChange={(crop) => onCropChange(crop)}
                    onComplete={(crop) => setCompletedCrop(crop)}
                  />
                )
              }
            </div>
            <Stack direction={{xs: "column", sm: "row"}} spacing={{xs: 2, sm: 0}}
                   justifyContent={{xs: "center", sm: "space-between"}}>
              <Stack direction={"column"} spacing={1}
                     justifyContent={"center"} alignItems={"flex-start"}>
                <Stack direction={"row"} spacing={1}
                       justifyContent={"flex-start"} alignItems={"center"}>
                  <CropAspectMenu crop={crop} image={image} onChangeAspect={onChangeAspect}/>
                  <Button
                    onClick={onCrop}
                    variant={"contained"}
                    color={"secondary"}
                    startIcon={<CropIcon/>}
                  >
                    {t("recipeForm:crop")}
                  </Button>
                </Stack>
                <div>
                  <RotateImageMenu image={image} onImageRotated={onImageRotated}/>
                </div>
              </Stack>
              <Stack direction={{xs: "row", sm: "column"}} spacing={1}
                     justifyContent={{xs: "flex-end", sm: "center"}} alignItems={{xs: "center", sm: "flex-end"}}>
                <Button variant={"contained"} color={"grey"}
                        onClick={onRevertImage} startIcon={<Restore/>}>
                  {t("recipeForm:revertChanges")}
                </Button>
                <Button
                  onClick={() => setOpenCropper(false)}
                  variant={"contained"}
                  color={"secondary"}
                >
                  {t("recipeForm:done")}
                </Button>
              </Stack>
            </Stack>
          </Stack>
        )
      }
      {
        props.image &&
        <HorizontalDivider/>
      }
      <Stack direction={{xs: "column", sm: "row"}} spacing={{xs: 1, sm: 2}}
             alignItems={{xs: "flex-start", sm: "center"}}>
        <Button
          variant={"contained"}
          color={"secondary"}
          component={"label"}
          id={"image"}
        >
          {t("recipeForm:chooseImage")}
          <input
            type={"file"}
            accept={"image/*"}
            hidden
            onChange={(e) => {
              onSelectFile(e).then(() => e.target.value = "" /* allows to load the same image */)
            }}
          />
        </Button>
        <Typography component={"span"}>
          {props.image ? props.imageFileName : t("recipeForm:noImage")}
        </Typography>
      </Stack>
    </Stack>
  )
}