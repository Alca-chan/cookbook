import {Button, Menu, MenuItem} from "@mui/material";
import {ArrowDropDown} from "@mui/icons-material";
import React from "react";
import {useTranslation} from "react-i18next";
import {rotateImage} from "../../../utils/imageUtils";

enum Rotation {
  left = -90,
  right = 90,
  full = 180
}

export const RotateImageMenu = (props: { image: HTMLImageElement | undefined, onImageRotated: (newImage: HTMLImageElement) => void }) => {
  const {t} = useTranslation()
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null)


  const onMenuClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchor(e.currentTarget)
  }

  const onMenuClose = () => {
    setAnchor(null)
  }

  const rotate = async (rotation: Rotation) => {
    if (props.image) {
      const newImage = await rotateImage(rotation, props.image)
      props.onImageRotated(newImage)
    }
  }

  const onRotationOptionSelect = (e: React.MouseEvent<HTMLLIElement, MouseEvent>, rotation: Rotation) => {
    e.stopPropagation()
    e.preventDefault()
    setAnchor(null) // close menu
    rotate(rotation).then()
  }

  return (
    <>
      <Button
        variant={"contained"}
        color={"grey"}
        endIcon={<ArrowDropDown/>}
        onClick={(e) => {
          e.stopPropagation()
          e.preventDefault()
          onMenuClick(e)
        }}>
        {t("recipeForm:rotate")}
      </Button>
      <Menu
        open={anchor !== null} anchorEl={anchor}
        anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
        onClose={onMenuClose}
        onClick={(e) => {
          e.stopPropagation()
          e.preventDefault()
        }}>
        <MenuItem value={Rotation.right}
                  onClick={(e) => onRotationOptionSelect(e, Rotation.right)}>{t("recipeForm:rotateRight90")}</MenuItem>
        <MenuItem value={Rotation.left}
                  onClick={(e) => onRotationOptionSelect(e, Rotation.left)}>{t("recipeForm:rotateLeft90")}</MenuItem>
        <MenuItem value={Rotation.full}
                  onClick={(e) => onRotationOptionSelect(e, Rotation.full)}>{t("recipeForm:rotate180")}</MenuItem>
      </Menu>
    </>
  )
}