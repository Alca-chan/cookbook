import React, {useContext, useState} from "react";
import {NewIngredientMeasure, NewIngredientsSection} from "../../api/types";
import {Button, Grid, IconButton, Paper, Stack, TextField} from "@mui/material";
import {NoResultsText} from "../RecipesGrid/NoResultsText";
import {Add, Clear, DragIndicator} from "@mui/icons-material";
import {IngredientsFormItem} from "./IngredientsFormItem";
import {Loading} from "../Loading";
import {ConfirmDialog} from "../Dialogs/ConfirmDialog";
import {DragDropContext, Draggable, Droppable, DropResult} from "react-beautiful-dnd";
import {PlusButton} from "../Buttons/PlusButton";
import {wait} from "../../utils/utils";
import {useTranslation} from "react-i18next";
import Typography from "@mui/material/Typography";
import {StaticDataContext} from "../../contexts/StaticDataContext";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  dragDropContext: {
    width: '100%'
  },
  ingredientContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'nowrap',
    paddingLeft: theme.spacing(1),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  paper: {
    width: '100%',
    padding: theme.spacing(0.5),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  sectionItem: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'nowrap',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  sectionInputField: {
    backgroundColor: theme.palette.table.subHeader,
    margin: 0,
    width: '100%'
  },
  deleteIcon: {
    marginLeft: theme.spacing(1)
  },
  addSectionButton: {
    marginTop: theme.spacing(2)
  },
  errorText: {
    width: "100%",
    marginBottom: theme.spacing(0.5)
  }
}))

export const IngredientsForm = (props: { ingredients: NewIngredientsSection[], onChange: (ingredients: NewIngredientsSection[]) => void, error: boolean }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {units, ingredients} = useContext(StaticDataContext)
  const newItem: NewIngredientMeasure = {
    key: props.ingredients.reduce((n, section) => n + section.ingredients.length, 0),
    order: props.ingredients.reduce((n, section) => n + section.ingredients.length, 0),
    recipeIngredientId: undefined,
    ingredient: undefined,
    measure: 0,
    unit: undefined,
    note: "",
    section: {title: "", order: props.ingredients.length}
  }
  const newSection: NewIngredientsSection = {
    key: props.ingredients.length,
    order: props.ingredients.length,
    title: "",
    ingredients: [newItem]
  }
  const [data, setData] = useState<NewIngredientsSection[]>([...props.ingredients, newSection])
  const [openDeleteIngredientDialog, setOpenDeleteIngredientDialog] = useState<boolean>(false)
  const [openDeleteSectionDialog, setOpenDeleteSectionDialog] = useState<boolean>(false)
  const [ingredientToDelete, setIngredientToDelete] = useState<{ sectionIndex: number, index: number }>({
    sectionIndex: -1,
    index: -1
  })
  const [sectionToDelete, setSectionToDelete] = useState<number>(-1)
  const measureRefs = React.useRef<HTMLElement[][]>([...data.map(() => [])])

  if (!ingredients || !units) {
    return <Loading/>
  }

  if (ingredients.length < 1) {
    return (
      <NoResultsText>No ingredients</NoResultsText>
    )
  }

  const update = (newData: NewIngredientsSection[]) => {
    setData(newData)
    newData.forEach((section, sectionIndex) => {
      section.order = sectionIndex
      section.ingredients.forEach((i, index) => i.order = index)
    })
    props.onChange(newData)
  }

  const onChange = (ingredientMeasure: NewIngredientMeasure, sectionIndex: number, index: number) => {
    const section = data[sectionIndex].ingredients
    const newIngredients: NewIngredientMeasure[] = [...section.slice(0, index), ingredientMeasure, ...section.slice(index + 1)]
    const newData: NewIngredientsSection[] = [...data]
    newData[sectionIndex].ingredients = newIngredients
    update(newData)
  }

  const onChangeSectionTitle = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, index: number) => {
    const newData = [...data]
    const title = e.target.value ?? ""
    newData[index].title = title
    newData[index].ingredients.forEach(i => i.section = {title: title, order: index})
    update(newData)
  }

  const getNewItem = (section: NewIngredientsSection): NewIngredientMeasure => {
    const item = {...newItem}
    item.section = {title: section.title ?? "", order: section.order}
    item.order = data.reduce((n, section) => n + section.ingredients.length, 0)
    item.key = data.reduce((n, section) => n + section.ingredients.length, 0)
    return item
  }

  const addIngredient = (section: number) => {
    const item = getNewItem(data[section])
    const newData = [...data]
    newData[section].ingredients = [...newData[section].ingredients, item]
    setData(newData)
  }

  const addIngredientBellow = (section: number, index: number) => {
    const item = getNewItem(data[section])
    const newData = [...data]
    newData[section].ingredients.splice(index + 1, 0, item)
    setData(newData)
  }

  const addSectionBelow = () => {
    const newSection: NewIngredientsSection = {
      key: data.length,
      order: data.length,
      title: "",
      ingredients: []
    }
    const item = getNewItem(newSection)
    newSection.ingredients.push(item)
    setData([...data, newSection])
  }

  const deleteSectionDialog = (sectionIndex: number) => {
    setSectionToDelete(sectionIndex)
    setOpenDeleteSectionDialog(true)
  }

  const deleteIngredientDialog = (sectionIndex: number, index: number) => {
    setIngredientToDelete({sectionIndex: sectionIndex, index: index})
    setOpenDeleteIngredientDialog(true)
  }

  const removeSection = () => {
    const newData = [...data]
    newData.splice(sectionToDelete, 1)
    update(newData)
  }

  const removeIngredient = () => {
    const newData = [...data]
    newData[ingredientToDelete.sectionIndex].ingredients.splice(ingredientToDelete.index, 1)
    update(newData)
  }

  const nextRow = async (sectionIndex: number, ingredientIndex: number) => {
    if (!measureRefs.current || measureRefs.current.length <= sectionIndex) return
    addIngredientBellow(sectionIndex, ingredientIndex)
    await wait(10) // wait for measureRefs
    if (measureRefs.current.length > sectionIndex && measureRefs.current[sectionIndex].length > ingredientIndex + 1) {
      measureRefs.current[sectionIndex][ingredientIndex + 1].focus()
    }
  }

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return

    const sourceIndex = result.source.index
    const destIndex = result.destination.index
    if (result.type === "droppableItem") {
      const newData = [...data]
      const removed = newData.splice(result.source.index, 1)[0]
      newData.splice(result.destination.index, 0, removed)
      update(newData)

    } else if (result.type === "droppableSubItem") {
      const sourceParentId = parseInt(result.source.droppableId)
      const destParentId = parseInt(result.destination.droppableId)

      const sourceSubItems = data.find(s => s.key === sourceParentId)?.ingredients
      const destSubItems = data.find(s => s.key === destParentId)?.ingredients

      if (!sourceSubItems || !destSubItems) return

      let newItems = [...data]

      if (sourceParentId === destParentId) {
        const reorderedSubItems = [...sourceSubItems]
        const removed = reorderedSubItems.splice(result.source.index, 1)[0]
        reorderedSubItems.splice(result.destination.index, 0, removed)

        newItems = newItems.map(item => {
          if (item.key === sourceParentId) {
            item.ingredients = reorderedSubItems
          }
          return item
        })
        update(newItems)
      } else {
        let newSourceSubItems = [...sourceSubItems]
        const [draggedItem] = newSourceSubItems.splice(sourceIndex, 1)

        let newDestSubItems = [...destSubItems]
        newDestSubItems.splice(destIndex, 0, draggedItem)
        newItems = newItems.map(item => {
          if (item.key === sourceParentId) {
            item.ingredients = newSourceSubItems
          } else if (item.key === destParentId) {
            draggedItem.section = {title: item.title ?? "", order: item.order}
            item.ingredients = newDestSubItems
          }
          return item
        })
        update(newItems)
      }
    }
  }

  const noIngredientError = () => {
    return (
      <div className={classes.errorText}>
        <Typography variant='subtitle2' align='left' color='error'
                    component={'span'}>{t("recipeForm:noIngredientError")}</Typography>
      </div>
    )
  }

  return (
    <Grid container justifyContent={"flex-start"} className={classes.container}>
      {
        openDeleteIngredientDialog &&
        <ConfirmDialog open={openDeleteIngredientDialog} setOpen={setOpenDeleteIngredientDialog}
                       title={t("recipeForm:deleteIngredientText", {
                         row: (ingredientToDelete.index + 1),
                         ingredient: (data[ingredientToDelete.sectionIndex].ingredients[ingredientToDelete.index].ingredient?.nameOne ? (" (" + data[ingredientToDelete.sectionIndex].ingredients[ingredientToDelete.index].ingredient?.nameOne + ")") : "")
                       })}
                       onConfirm={removeIngredient}/>
      }
      {
        openDeleteSectionDialog &&
        <ConfirmDialog open={openDeleteSectionDialog} setOpen={setOpenDeleteSectionDialog}
                       title={t("recipeForm:deleteSectionText", {section: (data[sectionToDelete].title && data[sectionToDelete].title !== "" ? (" " + data[sectionToDelete].title) : "")})}
                       onConfirm={removeSection}/>
      }
      <div className={classes.dragDropContext}>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable" type={"droppableItem"}>
            {(provided) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                <Stack spacing={1} direction={"column"}>
                  {
                    data.map((section, sectionIndex) => (
                      <Draggable key={section.key} draggableId={section.key.toString()} index={sectionIndex}>
                        {(provided) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.dragHandleProps}
                            {...provided.draggableProps}
                          >
                            <Paper variant={"outlined"} className={classes.paper}>
                              <Stack direction={"row"} alignItems={"center"}
                                     className={classes.sectionItem}>
                                <DragIndicator/>
                                <TextField
                                  className={classes.sectionInputField}
                                  id="sectionName"
                                  type='text'
                                  label={t("recipeForm:section")}
                                  margin='normal'
                                  variant='outlined'
                                  value={section.title}
                                  onChange={(e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => onChangeSectionTitle(e, sectionIndex)}
                                />
                                <div className={classes.deleteIcon}>
                                  <IconButton size={"small"}
                                              onClick={() => deleteSectionDialog(sectionIndex)}><Clear/></IconButton>
                                </div>
                              </Stack>
                              <Droppable droppableId={section.key.toString()} type={"droppableSubItem"}>
                                {(provided) => (
                                  <div
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                  >
                                    {
                                      section.ingredients.map((ingredient, ingredientIndex) => (
                                        <Draggable key={ingredient.key + section.key * 100}
                                                   draggableId={section.key + "_" + ingredient.key}
                                                   index={ingredientIndex}>
                                          {(provided) => (
                                            <div
                                              ref={provided.innerRef}
                                              {...provided.draggableProps}
                                              {...provided.dragHandleProps}
                                              style={provided.draggableProps.style}
                                            >
                                              <Stack direction={"column"} alignItems={"center"}>
                                                <Stack direction={"row"} alignItems={"center"}
                                                       className={classes.ingredientContainer}>
                                                  <DragIndicator/>
                                                  <IngredientsFormItem ingredient={ingredient}
                                                                       ingredientsSelect={ingredients}
                                                                       unitsSelect={units}
                                                                       onChange={(ingredientMeasure: NewIngredientMeasure) => onChange(ingredientMeasure, sectionIndex, ingredientIndex)}
                                                                       error={props.error && sectionIndex === 0 && ingredientIndex === 0}
                                                                       sectionIndex={sectionIndex}
                                                                       index={ingredientIndex}
                                                                       onEnter={() => nextRow(sectionIndex, ingredientIndex)}
                                                                       measureRefs={measureRefs}/>
                                                  <div className={classes.deleteIcon}>
                                                    <IconButton size={"small"}
                                                                onClick={() => deleteIngredientDialog(sectionIndex, ingredientIndex)}><Clear/></IconButton>
                                                  </div>
                                                </Stack>
                                                {
                                                  (props.error && sectionIndex === 0 && ingredientIndex === 0) && noIngredientError()
                                                }
                                              </Stack>
                                            </div>
                                          )}
                                        </Draggable>
                                      ))
                                    }
                                    {provided.placeholder}
                                    <PlusButton onClick={() => addIngredient(sectionIndex)}/>
                                  </div>
                                )}
                              </Droppable>
                            </Paper>
                          </div>
                        )}
                      </Draggable>
                    ))
                  }
                  {provided.placeholder}
                </Stack>
              </div>
            )}
          </Droppable>
        </DragDropContext>
        {
          (props.error && data.length === 0) && noIngredientError()
        }
      </div>
      <Button
        variant={"contained"}
        color={"grey"}
        size={"small"}
        className={classes.addSectionButton}
        startIcon={<Add/>}
        onClick={addSectionBelow}
      >
        {t("recipeForm:addSection")}
      </Button>
    </Grid>
  )
}