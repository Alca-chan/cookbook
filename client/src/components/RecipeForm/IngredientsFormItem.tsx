import React, {useState} from "react";
import {Ingredient, NewIngredientMeasure, Unit} from "../../api/types";
import {Autocomplete, Grid, TextField} from "@mui/material";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(() => ({
  inputField: {
    margin: 0,
    width: '100%'
  }
}))

export const IngredientsFormItem = (props: { ingredient: NewIngredientMeasure, ingredientsSelect: Ingredient[], unitsSelect: Unit[], onChange: (ingredientMeasure: NewIngredientMeasure) => void, error?: boolean, sectionIndex: number, index: number, measureRefs: React.RefObject<HTMLElement[][]>, onEnter: () => void }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [ingredientInputValue, setIngredientInputValue] = useState("")
  const [unitInputValue, setUnitInputValue] = useState("")
  const unitRef = React.useRef<HTMLElement>(null)
  const ingredientRef = React.useRef<HTMLElement>(null)
  const noteRef = React.useRef<HTMLElement>(null)

  const onChangeMeasure = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const value = e.target.value ? parseFloat(e.target.value) : 0
    const ingredientMeasure: NewIngredientMeasure = {...props.ingredient}
    ingredientMeasure.measure = value
    props.onChange(ingredientMeasure)
  }

  const onChangeUnit = (e: React.ChangeEvent<{}>, newValue: Unit | null) => {
    const ingredientMeasure: NewIngredientMeasure = {...props.ingredient}
    ingredientMeasure.unit = newValue ?? undefined
    if (newValue !== null && !newValue.measurable) {
      ingredientMeasure.measure = undefined
    } else if (ingredientMeasure.measure === undefined) {
      ingredientMeasure.measure = 0
    }
    props.onChange(ingredientMeasure)
  }

  const onChangeIngredient = (e: React.ChangeEvent<{}>, newValue: Ingredient | null) => {
    const ingredientMeasure: NewIngredientMeasure = {...props.ingredient}
    ingredientMeasure.ingredient = newValue ?? undefined
    props.onChange(ingredientMeasure)
  }

  const onChangeNote = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const value = e.target.value
    const ingredientMeasure: NewIngredientMeasure = {...props.ingredient}
    ingredientMeasure.note = value
    props.onChange(ingredientMeasure)
  }

  const moveToNext = (index: number) => {
    switch (index) {
      case 0:
        if (unitRef.current) unitRef.current.focus()
        break
      case 1:
        if (ingredientRef.current) ingredientRef.current.focus()
        break
      case 2:
        if (noteRef.current) noteRef.current.focus()
        break
      case 3:
        props.onEnter()
    }
  }

  const addMeasureRef = (e: HTMLElement) => {
    if (!props.measureRefs.current) return
    if (props.sectionIndex === 0 && props.index === 0) {
      props.measureRefs.current.length = 0
      props.measureRefs.current.push([e])
      return
    }
    if (props.measureRefs.current.length - 1 < props.sectionIndex) {
      props.measureRefs.current.push([e])
    } else {
      if (props.index === 0) props.measureRefs.current[props.sectionIndex].length = 0
      if (props.measureRefs.current[props.sectionIndex].length - 1 < props.index) {
        props.measureRefs.current[props.sectionIndex].push(e)
      }
    }
  }

  return (
    <Grid container justifyContent={"flex-start"} alignItems={"flex-start"} spacing={2}>
      <Grid item xs={6} sm={2}>
        {
          props.ingredient.measure !== undefined &&
          (
            <TextField
              required={props.sectionIndex === 0 && props.index === 0}
              className={classes.inputField}
              id="ingredientMeasure"
              type={"number"}
              inputProps={{
                min: 0, step: 1,
                onKeyUp: event => {
                  const index = 0
                  if (event.key === "Enter") {
                    moveToNext(index)
                  }
                }
              }}
              label={t("recipeForm:measure")}
              margin={"normal"}
              variant={"outlined"}
              value={props.ingredient.measure.toString()}
              onChange={onChangeMeasure}
              error={props.error && !(props.ingredient.measure > 0)}
              inputRef={(ref) => addMeasureRef(ref)}
            />
          )
        }
      </Grid>
      <Grid item xs={6} sm={3}>
        <Autocomplete
          className={classes.inputField}
          value={props.ingredient.unit ?? null}
          onChange={onChangeUnit}
          inputValue={unitInputValue ?? ""}
          onInputChange={(event: React.ChangeEvent<{}>, newInputValue: string) => {
            setUnitInputValue(newInputValue ?? "")
          }}
          id="unitName"
          options={props.unitsSelect}
          getOptionLabel={(option) => {
            if (option.symbol === "ks") return `- (${option.nameOne})`
            if (!props.ingredient.measure) return option.nameOne
            if (props.ingredient.measure % 1 !== 0) return option.namePart
            if (props.ingredient.measure < 2) return option.nameOne
            if (props.ingredient.measure < 5) return option.nameFew
            return option.nameMany
          }}
          filterOptions={((options, state) => options.filter(o => {
            return o.nameOne.includes(state.inputValue)
              || o.nameFew.includes(state.inputValue)
              || o.nameMany.includes(state.inputValue)
              || o.namePart.includes(state.inputValue)
              || o.symbol.includes(state.inputValue)
          }))}
          isOptionEqualToValue={(option) => option && option.id === props.ingredient.unit?.id}
          noOptionsText={t("recipeForm:noUnits")}
          renderInput={(params) =>
            <TextField {...params} label={t("recipeForm:unit")} variant={"outlined"}
                       required={props.sectionIndex === 0 && props.index === 0}
                       error={props.error && !props.ingredient.unit}
                       inputRef={unitRef}
                       inputProps={{
                         onKeyUp: event => {
                           const index = 1
                           if (event.key === "Enter") {
                             moveToNext(index)
                           }
                         }, ...params.inputProps
                       }}/>}
          autoHighlight
          autoSelect
        />
      </Grid>
      <Grid item xs={6} sm={4}>
        <Autocomplete
          className={classes.inputField}
          value={props.ingredient.ingredient ?? null}
          onChange={onChangeIngredient}
          inputValue={ingredientInputValue ?? ""}
          onInputChange={(event: React.ChangeEvent<{}>, newInputValue: string) => {
            setIngredientInputValue(newInputValue ?? "")
          }}
          id="ingredientName"
          options={props.ingredientsSelect}
          getOptionLabel={(option) => {
            if (props.ingredient.unit && props.ingredient.unit.symbol === 'ks' && props.ingredient.measure) {
              if (props.ingredient.measure % 1 !== 0) return option.namePart
              if (props.ingredient.measure < 2) return option.nameOne
              if (props.ingredient.measure < 5) return option.nameFew
              return option.nameMany
            }
            return option.nameUnit
          }}
          filterOptions={((options, state) => options.filter(o => {
            return o.nameOne.includes(state.inputValue)
              || o.nameFew.includes(state.inputValue)
              || o.nameMany.includes(state.inputValue)
              || o.namePart.includes(state.inputValue)
              || o.nameBase.includes(state.inputValue)
              || o.nameUnit.includes(state.inputValue)
          }))}
          isOptionEqualToValue={(option) => option.id === props.ingredient.ingredient?.id}
          noOptionsText={t("recipeForm:noIngredients")}
          renderInput={(params) =>
            <TextField {...params} label={t("recipeForm:ingredient")} variant={"outlined"}
                       required={props.sectionIndex === 0 && props.index === 0}
                       error={props.error && !props.ingredient.ingredient}
                       inputRef={ingredientRef}
                       inputProps={{
                         onKeyUp: event => {
                           const index = 2
                           if (event.key === "Enter") {
                             moveToNext(index)
                           }
                         }, ...params.inputProps
                       }}/>}
          autoHighlight
          autoSelect
        />
      </Grid>
      <Grid item xs={6} sm={3}>
        <TextField
          className={classes.inputField}
          id="note"
          type={"text"}
          label={t("recipeForm:note")}
          margin={"normal"}
          variant={"outlined"}
          value={props.ingredient.note}
          onChange={onChangeNote}
          inputRef={noteRef}
          inputProps={{
            onKeyUp: event => {
              const index = 3
              if (event.key === "Enter") {
                moveToNext(index)
              }
            }
          }}
        />
      </Grid>
    </Grid>
  )
}