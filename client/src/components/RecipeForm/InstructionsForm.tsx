import React, {useState} from "react";
import {Grid, IconButton} from "@mui/material";
import {Clear, DragIndicator} from "@mui/icons-material";
import {InstructionsFormItem} from "./InstructionsFormItem";
import {ConfirmDialog} from "../Dialogs/ConfirmDialog";
import {DragDropContext, Draggable, Droppable, DropResult} from "react-beautiful-dnd";
import {NewMethodStep} from "../../api/types";
import {PlusButton} from "../Buttons/PlusButton";
import {wait} from "../../utils/utils";
import {useTranslation} from "react-i18next";
import Typography from "@mui/material/Typography";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  grid: {
    marginBottom: theme.spacing(1)
  },
  stepGrid: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'nowrap'
  },
  methodStep: {
    width: '100%',
  },
  deleteIcon: {
    marginLeft: theme.spacing(1)
  },
  errorText: {
    width: "100%",
    marginBottom: theme.spacing(0.5)
  }
}))

export const InstructionsForm = (props: { instructions: NewMethodStep[], onChange: (instructions: NewMethodStep[]) => void, error: boolean }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const newItem: NewMethodStep = {
    id: props.instructions.length,
    order: props.instructions.length,
    description: ""
  }
  const [data, setData] = useState<NewMethodStep[]>([...props.instructions, newItem])
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false)
  const [stepToDelete, setStepToDelete] = useState<number>(-1)
  const inputRefs = React.useRef<HTMLElement[]>([])

  const update = (newData: NewMethodStep[]) => {
    setData(newData)
    newData.forEach((s, index) => s.order = index + 1)
    props.onChange(newData)
  }

  const onChange = (step: string, index: number) => {
    const newStep = {...data[index]}
    newStep.description = step
    const newData = [...data.slice(0, index), newStep, ...data.slice(index + 1)]
    update(newData)
  }

  const add = () => {
    const item = {...newItem}
    item.order = data.length
    item.id = data.length
    setData([...data, item])
  }

  const addBelow = (index: number, text?: string[]): number => {
    const n = text ? text.length : 1
    const items: NewMethodStep[] = []
    for (let i = 0; i < n; i++) {
      const item = {...newItem}
      item.id = data.length + items.length
      if (text) item.description = text[i]
      items.push(item)
    }
    const newData = [...data]
    newData.splice(index + 1, 0, ...items)
    newData.forEach((s, index) => s.order = index + 1)
    setData(newData)
    return index + items.length
  }

  const deleteDialog = (index: number) => {
    setStepToDelete(index)
    setOpenDeleteDialog(true)
  }

  const remove = () => {
    const newData = [...data]
    newData.splice(stepToDelete, 1)
    update(newData)
  }

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return

    const newData = [...data]
    const removed = newData.splice(result.source.index, 1)[0]
    newData.splice(result.destination.index, 0, removed)
    update(newData)
  }

  const nextStep = async (currentIndex: number, text: string[]) => {
    if (!inputRefs.current) return
    const lastAddedIndex = addBelow(currentIndex, text)
    await wait(10) // wait for inputRefs
    if (inputRefs.current.length > lastAddedIndex) {
      inputRefs.current[lastAddedIndex].focus()
    }
  }

  const moveDown = async (currentIndex: number) => {
    if (!inputRefs.current) return
    if (inputRefs.current.length > currentIndex + 1) {
      inputRefs.current[currentIndex + 1].focus()
    }
  }

  const moveUp = async (currentIndex: number) => {
    if (!inputRefs.current) return
    if (currentIndex > 0) {
      inputRefs.current[currentIndex - 1].focus()
    }
  }

  const noStepError = () => {
    return (
      <div className={classes.errorText}>
        <Typography variant='subtitle2' align='left' color='error'
                    component={'span'}>{t("recipeForm:noStepError")}</Typography>
      </div>
    )
  }

  return (
    <div className={classes.container}>
      <ConfirmDialog open={openDeleteDialog} setOpen={setOpenDeleteDialog}
                     title={t("recipeForm:deleteStep", {step: (stepToDelete + 1)})} onConfirm={remove}/>

      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided) => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {
                data.map((step, index) =>
                  <Draggable key={step.id} draggableId={step.id.toString()} index={index}>
                    {(provided) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <Grid container direction={"column"} alignItems={"center"}>
                          <Grid container direction={"row"} alignItems={"center"}
                                className={classes.stepGrid}>
                            <DragIndicator/>
                            <Grid item className={classes.methodStep}>
                              <InstructionsFormItem index={index} step={step.description}
                                                    onChange={(step: string) => onChange(step, index)}
                                                    error={props.error && index === 0}
                                                    inputRefs={inputRefs}
                                                    onEnter={(text: string[]) => nextStep(index, text)}
                                                    onArrowUp={() => moveUp(index)}
                                                    onArrowDown={() => moveDown(index)}
                              />
                            </Grid>
                            <Grid item>
                              <IconButton size={"small"} onClick={() => deleteDialog(index)}
                                          className={classes.deleteIcon}><Clear/></IconButton>
                            </Grid>
                          </Grid>
                          <Grid item xs={12}>
                            {
                              (props.error && index === 0) && noStepError()
                            }
                          </Grid>
                        </Grid>
                      </div>
                    )}
                  </Draggable>
                )}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      {
        (props.error && data.length === 0) && noStepError()
      }
      <PlusButton onClick={add}/>
    </div>
  )
}