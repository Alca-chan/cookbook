import React, {useEffect, useState} from "react";
import {TextField} from "@mui/material";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  textField: {
    margin: 0,
    width: '100%'
  },
  item: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}))

export const InstructionsFormItem = (props: { index: number, step: string, onChange: (step: string) => void, error?: boolean, inputRefs: React.RefObject<HTMLElement[]>, onEnter: (text: string[]) => void, onArrowUp: () => void, onArrowDown: () => void }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [step, setStep] = useState(props.step)

  // if we keep the step value only in props, it won't update on enter (eg. abcd\n efg leaves displayed abcd efg and adds efg to the line below
  // if we have the state in the component, it won't be updated after pasting multiline text (components will keep their state)
  // if we set key property on the text field to props.step, rerender causes loss of focus (kez changes with each character)
  // solved by adding useEffect that updates this state when props.step changes, and the component rerenders
  const propsStep = props.step
  useEffect(() => {
    setStep(propsStep)
  }, [propsStep])

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const value = e.target.value
    if (value.includes("\n")) {
      const text = value.split('\n')
      const beforeEnter = text.splice(0, 1)[0]
      setStep(beforeEnter)
      props.onChange(beforeEnter)
      props.onEnter(text)
      return
    }
    setStep(value)
    props.onChange(value)
  }

  const addRef = (e: HTMLElement) => {
    if (!props.inputRefs.current) return
    if (props.index === 0) props.inputRefs.current.length = 0
    if (props.inputRefs.current.length - 1 < props.index) {
      props.inputRefs.current.push(e)
    }
  }

  return (
    <div className={classes.item}>
      <TextField
        required={props.index === 0}
        className={classes.textField}
        error={props.error}
        multiline
        maxRows={4}
        id="step"
        type='text'
        label={(props.index + 1) + ". " + t("recipeForm:step")}
        variant='outlined'
        value={step}
        onChange={onChange}
        onFocus={(e) => e.target.selectionStart = step.length}
        inputRef={ref => addRef(ref)}
        inputProps={{
          onKeyUp: event => {
            if (event.key === "ArrowUp") {
              props.onArrowUp()
            } else if (event.key === "ArrowDown") {
              props.onArrowDown()
            }
          }
        }}
      />
    </div>
  )
}