import {Grid, TextField} from "@mui/material";
import React from "react";
import {useTranslation} from "react-i18next";

export const PortionsForm = (props: { portions: number, portionsNote: string, error?: boolean, onPortionsChanged: (portions: number, portionsNote: string) => void }) => {
  const {t} = useTranslation()

  const onChangePortions = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const value = e.target.value ? parseFloat(e.target.value) : null
    if (value !== null)
      props.onPortionsChanged(value, props.portionsNote)
  }

  return (
    <Grid container justifyContent='flex-start' alignItems='flex-start' spacing={2}>
      <Grid item xs={6}>
        <TextField
          fullWidth
          required
          id="portions"
          type='number'
          inputProps={{
            min: 0, step: 1
          }}
          label={t("recipeForm:portions")}
          variant='outlined'
          value={props.portions.toString()}
          error={props.error}
          helperText={props.error ? t("recipeForm:portionsError") : ""}
          onChange={(e) => onChangePortions(e)}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          fullWidth
          id="portionsNote"
          type='text'
          label={t("recipeForm:portionsNote")}
          variant='outlined'
          value={props.portionsNote}
          helperText={t("recipeForm:portionsNoteExample")}
          onChange={(e) => props.onPortionsChanged(props.portions, e.target.value)}
        />
      </Grid>
    </Grid>
  )
}