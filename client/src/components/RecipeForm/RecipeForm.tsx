import React, {useContext, useEffect, useState} from "react";
import {
  alpha,
  Button,
  Card,
  CardContent,
  CardHeader,
  darken,
  Grid,
  Paper,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import {UserContext} from "../../contexts/UserContext";
import {IngredientsForm} from "./IngredientsForm";
import {InstructionsForm} from "./InstructionsForm";
import {useHistory} from "react-router-dom";
import red from "@mui/material/colors/red";
import {ArrowBack} from "@mui/icons-material";
import {
  Equipment,
  FilledIngredientMeasure,
  FilledIngredientsSection,
  IngredientMeasure,
  NewIngredientMeasure,
  NewIngredientsSection,
  NewMethodStep,
  NewRecipe,
  Recipe,
  Tag
} from "../../api/types";
import {addRecipe, editRecipe} from "../../api/recipes/recipes";
import {TagsForm} from "./TagsForm";
import {EquipmentForm} from "./EquipmentForm";
import {ImageForm} from "./ImageForm/ImageForm";
import {PortionsForm} from "./PortionsForm";
import {DeleteRecipeDialog} from "../Dialogs/DeleteRecipeDialog";
import {useTranslation} from "react-i18next";
import {TimeForm} from "./TimeForm";
import {defaultDateFormat, minutesToOneTimeUnit, timeToMinutes} from "../../utils/utils";
import {getRoute} from "../../utils/routes";
import {APP_NAME} from "../../App";
import {makeStyles} from "../../utils/makeStyles";
import {compressImage} from "../../utils/imageUtils";
import toast from "react-hot-toast";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  paper: {
    padding: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  card: {
    padding: 0,
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  cardContent: {
    padding: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: 0,
    width: '100%',
    "&:last-child": {
      paddingBottom: 0
    }
  },
  header: {
    padding: theme.spacing(1),
    backgroundColor: theme.palette.primary.light
  },
  buttonsGrid: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  cancelButton: {
    marginLeft: theme.spacing(2)
  },
  deleteButton: {
    backgroundColor: alpha(red[400], 0.9),
    '&:hover': {
      backgroundColor: darken(red[400], 0.15),
    }
  },
  icon: {
    marginRight: theme.spacing(1)
  },
  backButton: {
    marginBottom: theme.spacing(2),
    display: 'flex',
    alignItems: 'flex-start'
  },
  sectionTitle: {
    width: "100%",
    paddingLeft: theme.spacing(1.5),
    marginBottom: theme.spacing(1)
  },
  ingredientsForm: {
    marginTop: theme.spacing(2)
  }
}))

export const RecipeForm = (props: { recipe?: Recipe }) => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const lang = i18n.language
  const history = useHistory()
  const {user} = useContext(UserContext)
  const recipe = props.recipe

  const [title, setTitle] = useState<string>(recipe ? recipe.title : "")
  const [titleError, setTitleError] = useState<boolean>(false)
  const [titleSpecialCharactersError, setTitleSpecialCharactersError] = useState<boolean>(false)
  const [titleAppNameError, setTitleAppNameError] = useState<boolean>(false)
  const [description, setDescription] = useState<string>(recipe ? recipe.description : "")
  const [ingredients, setIngredients] = useState<NewIngredientsSection[]>(recipe ? recipe.ingredients.map((section) => {
    const newIngredients: NewIngredientMeasure[] = section.ingredients.map((ingredient) => ({
      ...ingredient,
      key: ingredient.order
    }))
    const newSection: NewIngredientsSection = {
      key: section.order,
      order: section.order,
      title: section.title,
      ingredients: newIngredients
    }
    return newSection
  }) : [])
  const [ingredientError, setIngredientError] = useState<boolean>(false)
  const [method, setMethod] = useState<NewMethodStep[]>(recipe ? recipe.method.map((s) => ({
    id: s.order,
    order: s.order,
    description: s.description
  })) : [])
  const [methodError, setMethodError] = useState<boolean>(false)
  const convertedTime = recipe ? minutesToOneTimeUnit(recipe.time) : undefined
  const convertedTimePrep = recipe ? minutesToOneTimeUnit(recipe.timePrep) : undefined
  const [time, setTime] = useState<number>(convertedTime ? convertedTime.value : 30)
  const [timeUnit, setTimeUnit] = useState<string>(convertedTime ? convertedTime.unit : "minutes")
  const [timeError, setTimeError] = useState<boolean>(false)
  const [timeCompareError, setTimeCompareError] = useState<boolean>(false)
  const [timePrep, setTimePrep] = useState<number>(convertedTimePrep ? convertedTimePrep.value : 30)
  const [timePrepUnit, setTimePrepUnit] = useState<string>(convertedTimePrep ? convertedTimePrep.unit : "minutes")
  const [timePrepError, setTimePrepError] = useState<boolean>(false)
  const [tags, setTags] = useState<Tag[]>(recipe ? recipe.tags : [])
  const [equipment, setEquipment] = useState<Equipment[]>(recipe ? recipe.requiredEquipment : [])
  const [image, setImage] = useState<string>(recipe && recipe.image ? recipe.image : "")
  const [imageFileName, setImageFileName] = useState<string>()
  const [portions, setPortions] = useState<number>(recipe ? recipe.portions : 4)
  const [portionsError, setPortionsError] = useState<boolean>(false)
  const [portionsNote, setPortionsNote] = useState<string>(recipe ? recipe.portionsNote : "")
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false)

  useEffect(() => {
    if (user && recipe && (!recipe.user || user.id !== recipe.user.id)) {
      history.push(getRoute(lang, "/recipe/:id").replace(":id", recipe.id))
    }
  }, [user, history, recipe, lang])

  const resetErrors = () => {
    setTitleError(false)
    setTitleAppNameError(false)
    setIngredientError(false)
    setMethodError(false)
    setTimeError(false)
    setTimePrepError(false)
    setPortionsError(false)
  }

  const onSubmit = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    resetErrors()
    let error = false
    if (!title) {
      setTitleError(true)
      error = true
    }
    const titleMatch = title.match(/[/\\#$~*<>{}[\]]/g)
    if (titleMatch && titleMatch.length > 0) {
      setTitleSpecialCharactersError(true)
      error = true
    }
    if (title.toLowerCase().includes(APP_NAME.toLowerCase())) {
      setTitleAppNameError(true)
      error = true
    }
    if (!(time > 0)) {
      setTimeError(true)
      error = true
    }
    if (timeToMinutes(time, timeUnit) < timeToMinutes(timePrep, timePrepUnit)) {
      setTimeCompareError(true)
      error = true
    }
    if (!(timePrep > 0)) {
      setTimePrepError(true)
      error = true
    }
    if (!ingredients.some((section: NewIngredientsSection) => {
      return section.ingredients.some(i => i && i.ingredient && i.unit && (!i.unit.measurable || (i.measure && i.measure > 0)))
    })) {
      setIngredientError(true)
      error = true
    }
    if (method.length < 1) {
      setMethodError(true)
      error = true
    }
    if (!(portions > 0)) {
      setPortionsError(true)
      error = true
    }
    if (error) {
      toast.error(<b>{t("recipeForm:notSufficientlyFilled")}</b>)
      return
    }

    let filledIngredientsSections: FilledIngredientsSection[] = ingredients.filter((section) => {
      if (!(section.ingredients.length > 0)) return false
      const validIngredients: NewIngredientMeasure[] = section.ingredients
        .filter(i => i.ingredient && i.unit && (!i.unit.measurable || (i.measure && i.measure > 0)) && i.section)
      section.ingredients = validIngredients
      return validIngredients.length > 0
    }).sort((a, b) => {
      if (a.title === "" && b.title !== "") return -1
      if (b.title === "" && a.title !== "") return 1
      return 0
    }).map((section) => {
      const newSection: FilledIngredientsSection = {
        title: section.title!,
        order: section.order,
        ingredients: section.ingredients.map((ingredient, ingredientIndex) => ({
          order: ingredientIndex,
          recipeIngredientId: ingredient.recipeIngredientId,
          ingredient: ingredient.ingredient!,
          measure: ingredient.measure!,
          unit: ingredient.unit!,
          note: ingredient.note!,
          section: ingredient.section!
        }))
      }
      return newSection
    })

    if (filledIngredientsSections.length < 1) {
      setIngredientError(true)
      return
    }

    const newIngredients: FilledIngredientsSection[] = []
    filledIngredientsSections.forEach((section) => {
      const found = newIngredients.find(s => s.title === section.title)
      if (found) {
        // merge sections
        section.ingredients.forEach((ingredient, ingredientIndex) => {
          ingredient.order = found.ingredients.length + ingredientIndex
          ingredient.section.order = found.order
        })
        found.ingredients.push(...section.ingredients)
      } else {
        const order = newIngredients.length
        section.order = order
        section.ingredients.forEach((ingredient, ingredientIndex) => {
          ingredient.order = ingredientIndex
          ingredient.section.order = order
        })
        newIngredients.push(section)
      }
    })

    const compressedImage = image && image.length > 0
      ? ((!recipe || image !== recipe.image) ? await compressImage(image) : image)
      : ""

    if (user) {
      const newRecipe: NewRecipe = {
        userId: user.id,
        title: title,
        description: description,
        ingredients: newIngredients,
        method: method.sort((a, b) => a.order - b.order),
        time: timeToMinutes(time, timeUnit),
        timePrep: timeToMinutes(timePrep, timePrepUnit),
        tags: tags,
        votes: 0,
        requiredEquipment: equipment,
        image: compressedImage,
        date: moment().format(defaultDateFormat),
        portions: portions,
        portionsNote: portionsNote
      }

      if (!recipe) { // new recipe
        const id = await addRecipe(newRecipe)
        if (id) {
          toast.success(<b>{t("toast:uploadingToServerSuccessful")}</b>)
          history.push(getRoute(lang, "/recipe/:id").replace(":id", id))
        } else {
          toast.error(<b>{t("toast:uploadingToServerError")}</b>)

        }
      } else { // edit recipe
        const ingredientsToUpdate: IngredientMeasure[] = []
        const ingredientsToAdd: FilledIngredientMeasure[] = []
        let allIngredients: FilledIngredientMeasure[] = newRecipe.ingredients.map(s => s.ingredients).flat()
        let allOldIngredients: IngredientMeasure[] = recipe.ingredients.map(s => s.ingredients).flat()
        allIngredients.forEach(ingredient => {
          if (ingredient.recipeIngredientId !== undefined) {
            allOldIngredients = allOldIngredients.filter(i => ingredient.recipeIngredientId !== i.recipeIngredientId)
            ingredientsToUpdate.push(ingredient as IngredientMeasure)
          } else {
            ingredientsToAdd.push(ingredient)
          }
        })

        const tagsToAdd: Tag[] = []
        let oldTags = [...recipe.tags]
        newRecipe.tags.forEach(tag => {
          const found = oldTags.find(t => t.id === tag.id)
          if (!found) {
            tagsToAdd.push(tag)
          }
          oldTags = oldTags.filter(t => t.id !== tag.id)
        })

        const equipmentToAdd: Equipment[] = []
        let oldEquipment = [...recipe.requiredEquipment]
        newRecipe.requiredEquipment.forEach(equipment => {
          const found = oldEquipment.find(e => e.id === equipment.id)
          if (!found) {
            equipmentToAdd.push(equipment)
          }
          oldEquipment = oldEquipment.filter(e => e.id !== equipment.id)
        })

        if (await editRecipe(newRecipe, recipe.id, {
          toAdd: ingredientsToAdd,
          toUpdate: ingredientsToUpdate,
          toDelete: allOldIngredients
        }, {
          toAdd: tagsToAdd,
          toDelete: oldTags
        }, {
          toAdd: equipmentToAdd,
          toDelete: oldEquipment
        }, recipe.date ?? moment().format(defaultDateFormat))) {
          toast.success(<b>{t("toast:uploadingToServerSuccessful")}</b>)
          history.push(getRoute(lang, "/recipe/:id").replace(":id", recipe.id))
        } else {
          toast.error(<b>{t("toast:uploadingToServerError")}</b>)
        }
      }
    }
  }

  const cancel = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    if (recipe) {
      history.push(getRoute(lang, "/recipe/:id").replace(":id", recipe.id))
    } else {
      history.push("/")
    }
  }

  const deleteRecipeDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    if (recipe && user) {
      setOpenDeleteDialog(true)
    }
  }

  const onIngredientsChange = (newIngredients: NewIngredientsSection[]) => {
    setIngredients(newIngredients)
  }

  const onInstructionsChange = (instructions: NewMethodStep[]) => {
    const newInstructions = instructions.filter(i => i.description.length > 0)
    setMethod(newInstructions)
  }

  const onTagsChange = (tags: Tag[]) => {
    setTags(tags)
  }

  const onEquipmentChange = (equipment: Equipment[]) => {
    setEquipment(equipment)
  }

  const onImageFileChange = (image: string, imageFileName: string) => {
    setImage(image)
    setImageFileName(imageFileName)
  }

  const onImageCropped = (newImage: string) => {
    setImage(newImage)
  }

  const onPortionsChanged = (portions: number, portionsNote: string) => {
    setPortions(portions)
    setPortionsNote(portionsNote)
  }

  return (
    <>
      {
        recipe && (
          <DeleteRecipeDialog open={openDeleteDialog} setOpen={setOpenDeleteDialog} recipe={recipe}/>
        )
      }
      {
        recipe && (
          <div className={classes.backButton}>
            <Button onClick={(e) => cancel(e)}
                    variant={"contained"} color={"grey"}>
              <ArrowBack className={classes.icon}/>{t("recipeForm:backToRecipe")}
            </Button>
          </div>
        )
      }
      <form noValidate autoComplete='off'>
        <Stack direction={{xs: "column", xl: "row"}} spacing={{xs: 0, xl: 2}}>
          <Grid item xs={12} xl={6}>
            <Paper className={classes.paper}>
              <Stack spacing={2}>
                <TextField
                  fullWidth
                  required
                  error={titleError || titleSpecialCharactersError || titleAppNameError}
                  id="title"
                  type='text'
                  label={t("recipeForm:title")}
                  variant='outlined'
                  value={title}
                  helperText={titleError ?
                    t("recipeForm:titleError")
                    : (titleAppNameError
                      ? t("recipeForm:titleAppNameError", {appName: APP_NAME})
                      : (titleSpecialCharactersError
                        ? t("recipeForm:titleSpecialCharactersError", {characters: "/, \\, #, $, ~, *, <, >, {, }, [, ]"})
                        : ""))}
                  onChange={e => setTitle(e.target.value)}
                />
                <TextField
                  fullWidth
                  id="description"
                  type='text'
                  label={t("recipeForm:description")}
                  variant='outlined'
                  value={description}
                  inputProps={{
                    min: 1, step: timeUnit === "minutes" ? 5 : 1
                  }}
                  onChange={e => setDescription(e.target.value)}
                />
              </Stack>
            </Paper>
            <Paper className={classes.paper}>
              <Typography variant={"h6"} className={classes.sectionTitle} align={"left"}>
                {t("recipeForm:image")}
              </Typography>
              <ImageForm onImageFileChange={onImageFileChange} onImageCropped={onImageCropped} image={image}
                         imageFileName={imageFileName ?? ""}/>
            </Paper>
          </Grid>
          <Grid item xs={12} xl={6}>
            <Paper className={classes.paper}>
              <Stack spacing={2}>
                <Stack direction={{xs: 'column', md: 'row'}} spacing={2} style={{width: "100%"}}>
                  <TimeForm value={time} selectedUnit={timeUnit} onChange={setTime} onSelect={setTimeUnit}
                            error={timeError ? t("recipeForm:timeError") : (timeCompareError ? t("recipeForm:timeCompareError") : undefined)}
                            label={t("recipeForm:time")} hint={t("recipeForm:timeHint")}/>
                  <TimeForm value={timePrep} selectedUnit={timePrepUnit} onChange={setTimePrep}
                            onSelect={setTimePrepUnit}
                            error={timePrepError ? t("recipeForm:timePrepError") : undefined}
                            label={t("recipeForm:timePrep")} hint={t("recipeForm:timePrepHint")}/>
                </Stack>
                <Typography variant={"h6"} className={classes.sectionTitle}
                            align={"left"}>{t("recipeForm:equipment")}</Typography>
                <EquipmentForm equipment={equipment} onChange={onEquipmentChange}/>
                <Typography variant={"h6"} className={classes.sectionTitle}
                            align={"left"}>{t("recipeForm:tags")}</Typography>
                <TagsForm tags={tags} onChange={onTagsChange}/>
              </Stack>
            </Paper>
          </Grid>
        </Stack>
        <Stack direction={{xs: "column", xl: "row"}} spacing={{xs: 0, xl: 2}}>
          <Grid item xs={12} xl={6}>
            <Card className={classes.card}>
              <CardHeader title={t("recipeForm:ingredients")} className={classes.header}
                          titleTypographyProps={{variant: "h6"}}/>
              <CardContent className={classes.cardContent}>
                <Stack>
                  <PortionsForm portions={portions} portionsNote={portionsNote} error={portionsError}
                                onPortionsChanged={onPortionsChanged}/>
                  <div className={classes.ingredientsForm}>
                    <IngredientsForm ingredients={ingredients} onChange={onIngredientsChange}
                                     error={ingredientError}/>
                  </div>
                </Stack>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12} xl={6}>
            <Card className={classes.card}>
              <CardHeader title={t("recipeForm:method")} className={classes.header}
                          titleTypographyProps={{variant: "h6"}}/>
              <CardContent className={classes.cardContent}>
                <InstructionsForm instructions={method} onChange={onInstructionsChange}
                                  error={methodError}/>
              </CardContent>
            </Card>
          </Grid>
        </Stack>
        <Stack justifyContent='space-between' direction={"row"} className={classes.buttonsGrid}>
          <div>
            <Button
              variant={"contained"}
              color={"primary"}
              size={"large"}
              onClick={(e) => onSubmit(e)}
            >
              {t("recipeForm:save")}
            </Button>
            <Button
              className={classes.cancelButton}
              variant={"contained"}
              color={"grey"}
              size={"large"}
              onClick={(e) => cancel(e)}
            >
              {t("recipeForm:cancel")}
            </Button>
          </div>
          {
            recipe && (
              <Button
                className={classes.deleteButton}
                variant={"contained"}
                color={"grey"}
                size={"large"}
                onClick={(e) => deleteRecipeDialog(e)}
              >
                {t("recipeForm:delete")}
              </Button>
            )
          }
        </Stack>
      </form>
    </>
  )
}