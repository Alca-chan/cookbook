import React from "react";
import {alpha, Card, CardActionArea, CardContent, Grid, lighten, Typography} from "@mui/material";
import {Clear} from "@mui/icons-material";
import {Tag} from "../../api/types";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  cardGrid: {
    marginBottom: theme.spacing(2)
  },
  cardContent: {
    display: 'flex'
  },
  card: {
    background: theme.palette.primary.light,
    height: '100%'
  },
  clear: {
    marginRight: theme.spacing(1),
    color: alpha(theme.palette.text.primary, 0.3)
  },
  text: {
    fontSize: 18,
    color: lighten(theme.palette.text.primary, 0.1)
  }
}))

export const SelectedTagsGrid = (props: { data: Tag[], onClick: (tag: Tag) => void }) => {
  const {classes} = useStyles()
  const selected = props.data

  return (
    <>
      <Grid container spacing={1} justifyContent='flex-start' alignItems='stretch' className={classes.cardGrid}>
        {
          selected.map((tag) =>
            <Grid item key={tag.id}>
              <Card className={classes.card}>
                <CardActionArea onClick={() => {
                  props.onClick(tag)
                }}>
                  <CardContent className={classes.cardContent}>
                    <Clear className={classes.clear}/>
                    <Typography className={classes.text}>{tag.name}</Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          )
        }
      </Grid>
    </>
  )
}