import {Tag} from "../../api/types";
import React, {useContext, useState} from "react";
import {Loading} from "../Loading";
import {Autocomplete, TextField} from "@mui/material";
import {SelectedTagsGrid} from "./SelectedTagsGrid";
import {useTranslation} from "react-i18next";
import {StaticDataContext} from "../../contexts/StaticDataContext";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  inputField: {
    margin: 0,
    width: '100%'
  }
}))

export const TagsForm = (props: { tags: Tag[], onChange: (tags: Tag[]) => void }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {tags} = useContext(StaticDataContext)
  const [data, setData] = useState<Tag[]>([...props.tags])
  const [tagValue, setTagValue] = useState<Tag | null>(null)
  const [tagInputValue, setTagInputValue] = useState("")

  if (!tags) {
    return <Loading/>
  }

  const onChangeTag = (e: React.ChangeEvent<{}>, newValue: Tag | null) => {
    setTagValue(null)
    setTagInputValue("")
    if (newValue != null) {
      const newData = [...data, newValue]
      setData(newData)
      props.onChange(newData)
    }
  }

  const onRemoveTag = (tag: Tag) => {
    const newData = data.filter(t => t.id !== tag.id)
    setData(newData)
    props.onChange(newData)
  }

  return (
    <div className={classes.container}>
      {
        data.length > 0 && <SelectedTagsGrid data={data} onClick={onRemoveTag}/>
      }
      <Autocomplete
        className={classes.inputField}
        value={tagValue}
        onChange={onChangeTag}
        inputValue={tagInputValue}
        onInputChange={(event: React.ChangeEvent<{}>, newInputValue: string) => {
          setTagInputValue(newInputValue)
        }}
        id="tagName"
        options={tags.filter(t => !data.some(selected => selected.id === t.id))}
        getOptionLabel={(option) => option.name}
        noOptionsText={t("recipeForm:noTags")}
        renderInput={(params) =>
          <TextField {...params} label={t("recipeForm:searchTag")} variant="outlined"/>}
        disableCloseOnSelect={true}
      />
    </div>
  )
}