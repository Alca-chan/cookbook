import {Grid, TextField, Typography} from "@mui/material";
import {TimeUnitSelect} from "./TimeUnitSelect";
import React from "react";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  time: {
    paddingRight: theme.spacing(1)
  },
  hint: {
    padding: theme.spacing(0.4),
    paddingLeft: theme.spacing(1.6),
    display: "flex"
  }
}))

export const TimeForm = (props: { value: number, selectedUnit: string, onChange: (newValue: number) => void, onSelect: (selectedValue: string) => void, error?: string, label: string, hint: string }) => {
  const {classes} = useStyles()

  const onChangeTime = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const value = e.target.value ? parseInt(e.target.value) : null
    if (value !== null)
      props.onChange(value)
  }

  return (
    <Grid container direction={"row"}>
      <Grid item xs={7} className={classes.time}>
        <TextField
          fullWidth
          required
          id="timePrep"
          type='number'
          label={props.label}
          error={Boolean(props.error)}
          helperText={Boolean(props.error) ? props.error : undefined}
          variant='outlined'
          value={props.value.toString()}
          inputProps={{
            min: 0, step: props.selectedUnit === "minutes" ? 5 : 1
          }}
          onChange={(e) => onChangeTime(e)}
        />
      </Grid>
      <Grid item xs={5}>
        <TimeUnitSelect selectedValue={props.selectedUnit} onSelect={props.onSelect} count={props.value}/>
      </Grid>
      <Grid item xs={12}>
        <Typography variant={"caption"} color={"textSecondary"} className={classes.hint}>{props.hint}</Typography>
      </Grid>
    </Grid>
  )
}