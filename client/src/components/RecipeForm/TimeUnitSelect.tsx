import {MenuItem, TextField} from "@mui/material";
import React from "react";
import {useTranslation} from "react-i18next";

export const TimeUnitSelect = (props: { selectedValue: string, onSelect: (selectedValue: string) => void, count: number }) => {
  const {t} = useTranslation()

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const newSelectedValue = e.target.value
    if (newSelectedValue) props.onSelect(newSelectedValue as string)
  }

  return (
    <TextField
      style={{width: "100%"}}
      variant={"outlined"}
      value={props.selectedValue}
      type={"number"}
      select
      onChange={onChange}
    >
      <MenuItem value={"minutes"}>{t("recipeForm:timeMin", {count: props.count})}</MenuItem>
      <MenuItem value={"hours"}>{t("recipeForm:timeHour", {count: props.count})}</MenuItem>
      <MenuItem value={"days"}>{t("recipeForm:timeDay", {count: props.count})}</MenuItem>
    </TextField>
  )
}