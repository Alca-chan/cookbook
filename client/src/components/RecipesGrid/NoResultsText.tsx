import {Typography} from "@mui/material";
import React from "react";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  text: {
    marginTop: theme.spacing(5),
    color: theme.palette.text.secondary
  }
}))

export const NoResultsText = (props: { children: React.ReactNode }) => {
  const {classes} = useStyles()

  return (
    <Typography className={classes.text} variant='subtitle1'>{props.children}</Typography>
  )
}