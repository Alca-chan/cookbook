import React from "react";
import {Link} from "react-router-dom";
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  IconButton,
  Menu,
  MenuItem,
  Typography
} from "@mui/material";
import {ImageView} from "../RecipesSearch/ImageView";
import {RecipeShort} from "../../api/types";
import red from "@mui/material/colors/red";
import {MoreVert} from "@mui/icons-material";
import {getRoute, parsePageTitle} from "../../utils/routes";
import {useTranslation} from "react-i18next";
import {AddToCalendarIconButton} from "../Buttons/AddToCalendarIconButton";
import {FavoritesIconButton} from "../Buttons/FavoritesIconButton";
import {TimeLabel} from "../Recipe/TimeLabel";
import {makeStyles} from "../../utils/makeStyles";

export const MAX_CARD_WIDTH = 300
const MAX_IMAGE_WIDTH = MAX_CARD_WIDTH + "px"
const MAX_IMAGE_HEIGHT = "200px"

const useStyles = makeStyles()(theme => ({
  card: {
    width: MAX_IMAGE_WIDTH,
    height: '100%'
  },
  cardActionArea: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  cardMedia: {
    display: 'flex',
    flexDirection: 'column',
    height: MAX_IMAGE_HEIGHT,
    width: MAX_IMAGE_WIDTH,
    alignItems: 'center',
    justifyContent: 'center',
    background: theme.palette.appBackground.image
  },
  image: {
    maxWidth: MAX_IMAGE_WIDTH,
    maxHeight: MAX_IMAGE_HEIGHT
  },
  cardHeader: {
    flex: 1,
    width: '100%'
  },
  description: {
    whiteSpace: 'normal',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 4,
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  buttonArea: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    '&:hover': {
      cursor: "default"
    }
  },
  buttonGroup: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  like: {
    '&:hover': {
      color: red[400]
    }
  },
  likeSelected: {
    color: red[400],
    '&:hover': {
      color: theme.palette.text.secondary,
    }
  },
  calendar: {
    '&:hover': {
      color: red[400]
    }
  },
  calendarSelected: {
    color: red[400],
    '&:hover': {
      color: theme.palette.text.secondary,
    }
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },
  timeLabel: {
    marginBottom: theme.spacing(1),
    marginLeft: theme.spacing(1)
  }
}))

export type MenuOption = {
  menuItem: React.ReactNode,
  action: () => void,
}

export const RecipeCard = (props: { recipe: RecipeShort, displayFavorites: boolean, displayCalendar: boolean, menuOptions?: MenuOption[] }) => {
  const {classes} = useStyles()
  const {i18n} = useTranslation()
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null)

  const onMenuClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchor(e.currentTarget)
  }

  const onMenuClose = () => {
    setAnchor(null)
  }

  const onOptionSelect = (action: () => void) => {
    setAnchor(null) // close menu
    action()
  }

  return (
    <Card className={classes.card}>
      <Link to={(location) => ({
        pathname: getRoute(i18n.language, "/recipe/:id").replace(":id", props.recipe.id),
        state: {path: location.pathname + location.search, name: parsePageTitle(document.title)}
      })} className={classes.link}>
        <div className={classes.cardActionArea}>
          <div>
            <CardHeader title={props.recipe.title} className={classes.cardHeader} action={
              (props.menuOptions && props.menuOptions.length > 0) && (
                <div>
                  <IconButton size={"medium"} onClick={(e) => {
                    e.stopPropagation()
                    e.preventDefault()
                    onMenuClick(e)
                  }}>
                    <MoreVert/>
                  </IconButton>
                  <Menu open={anchor !== null} anchorEl={anchor}
                        anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                        onClose={onMenuClose}
                        onClick={(e) => {
                          e.stopPropagation()
                          e.preventDefault()
                        }}>
                    {
                      props.menuOptions.map((option, index) =>
                        <MenuItem
                          key={index}
                          onClick={(e) => {
                            e.stopPropagation()
                            e.preventDefault()
                            onOptionSelect(option.action)
                          }}>
                          {option.menuItem}
                        </MenuItem>
                      )
                    }
                  </Menu>
                </div>
              )
            }/>
            <CardMedia className={classes.cardMedia}>
              <div className={classes.image}>
                <ImageView img={props.recipe.image} width={'100%'} height={'100%'}/>
              </div>
            </CardMedia>
            <CardContent>
              <Typography variant={"body2"} color={"textPrimary"} component={"p"} className={classes.description}>
                {props.recipe.description}
              </Typography>
            </CardContent>
          </div>
          {
            (props.displayFavorites || props.displayCalendar) && (
              <CardActions className={classes.buttonArea} onClick={(e) => {
                e.stopPropagation()
                e.preventDefault()
              }}>
                <div className={classes.timeLabel}>
                  <TimeLabel minutes={props.recipe.time}/>
                </div>
                <div className={classes.buttonGroup}>
                  {
                    props.displayCalendar &&
                    <AddToCalendarIconButton recipe={props.recipe} changeOnAdd={true} background={false}/>
                  }
                  {
                    props.displayFavorites &&
                    <FavoritesIconButton recipeId={props.recipe.id} votes={props.recipe.votes} background={false}/>
                  }
                </div>
              </CardActions>
            )
          }
        </div>
      </Link>
    </Card>
  )
}