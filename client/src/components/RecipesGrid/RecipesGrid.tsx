import React, {useContext} from 'react';
import {Grid} from '@mui/material';
import {RecipeShort} from '../../api/types';
import {UserContext} from "../../contexts/UserContext";
import {RecipeCard} from "./RecipeCard";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(() => ({
  cardGrid: {
    alignItems: 'stretch',
    justifyContent: 'center'
  }
}))

export const RecipesGrid = (props: { data: RecipeShort[] }) => {
  const {classes} = useStyles()
  const {user} = useContext(UserContext)

  return (
    <Grid container spacing={2} className={classes.cardGrid}>
      {
        props.data.map((recipe) =>
          <Grid item key={recipe.id}>
            <RecipeCard recipe={recipe}
                        displayFavorites={true}
                        displayCalendar={Boolean(user)}
            />
          </Grid>
        )
      }
    </Grid>
  )
}