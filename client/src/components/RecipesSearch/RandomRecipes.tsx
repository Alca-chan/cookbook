import React, {useCallback, useEffect, useState} from "react";
import {RecipeShort} from "../../api/types";
import {RecipesGrid} from "../RecipesGrid/RecipesGrid";
import {Loading} from "../Loading";
import {getRandomRecipes} from "../../api/recipes/recipeSearch";
import {getCancelToken} from "../../api/utils";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  recipesGrid: {
    marginTop: theme.spacing(5)
  }
}))

export const RandomRecipes = () => {
  const {classes} = useStyles()
  const [data, setData] = useState<RecipeShort[]>()

  const loadData = useCallback(async (token) => {
    try {
      const recipes: RecipeShort[] = await getRandomRecipes(token, 12)
      setData(recipes)
    } catch {
      /* canceled request */
    }
  }, [])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  if (!data) {
    return <Loading/>
  }

  return (
    <div className={classes.recipesGrid}>
      <RecipesGrid data={data}/>
    </div>
  )
}