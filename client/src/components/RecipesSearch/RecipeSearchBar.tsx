import React, {useState} from "react";
import {SearchBar} from "./SearchBar";

const MIN_SEARCH_LENGTH = 3
const SEARCH_DELAY = 1400 // in ms
const SEARCH_DELAY_SHORT = 700

export const RecipeSearchBar = (props: { onSearch: (searchValue: string) => void, onClear?: () => void }) => {
  const [searchValue, setSearchValue] = useState("")
  const [timeoutId, setTimeoutId] = useState<ReturnType<typeof setTimeout>>()

  const search = (value: string) => {
    props.onSearch(value)
  }

  const searchWithoutDelay = (value: string) => {
    if (timeoutId) clearTimeout(timeoutId)
    search(value)
  }

  const searchWithDelay = (value: string, delay: number) => {
    if (timeoutId) clearTimeout(timeoutId)
    setTimeoutId(setTimeout(() => search(value), delay))
  }

  const onChange = async (newSearchValue: string) => {
    setSearchValue(newSearchValue)
    if (newSearchValue.length === 0) {
      if (props.onClear) {
        props.onClear()
      } else {
        searchWithoutDelay(newSearchValue)
      }
    } else if (newSearchValue.length >= MIN_SEARCH_LENGTH) {
      searchWithDelay(newSearchValue, SEARCH_DELAY_SHORT)
    } else {
      searchWithDelay(newSearchValue, SEARCH_DELAY)
    }
  }

  return (
    <SearchBar onChange={onChange} onSearchRequest={() => searchWithoutDelay(searchValue)} initialValue={""}/>
  )
}