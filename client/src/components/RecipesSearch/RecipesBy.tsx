import React, {useEffect, useMemo, useState} from "react";
import {Equipment, Ingredient, RecipeShort, Tag} from "../../api/types";
import {Button, Collapse, FormControlLabel, Grid, Radio, RadioGroup, Typography} from "@mui/material";
import {LabelsGrid} from "./RecipesByLabels/LabelsGrid";
import {ArrowBack, Clear, Search} from "@mui/icons-material";
import {LabelSearchBar} from "./RecipesByLabels/LabelSearchBar";
import {SelectedLabelsGrid} from "./RecipesByLabels/SelectedLabelsGrid";
import {RecipesWithLabels} from "./RecipesByLabels/RecipesWithLabels";
import {NoResultsText} from "../RecipesGrid/NoResultsText";
import {Loading} from "../Loading";
import {useTranslation} from "react-i18next";
import {useHistory, useLocation} from "react-router-dom";
import {getRoute, parsePageTitle} from "../../utils/routes";
import {Breadcrumbs} from "../Breadcrumbs";
import {CancelToken} from "axios";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      paddingRight: theme.spacing(4),
      paddingLeft: theme.spacing(4)
    }
  },
  selectedLabels: {
    width: '100%',
    marginTop: theme.spacing(4)
  },
  icon: {
    marginRight: theme.spacing(1)
  },
  buttonsGrid: {
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  searchbar: {
    maxWidth: '500px'
  },
  switchText: {
    marginRight: theme.spacing(1)
  },
  switch: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    }
  },
  clearAndBackButton: {},
  searchButton: {}
}))

export type Label = Ingredient | Tag | Equipment

export const RecipesBy = <T extends Label>(props: { searchComparison: (search: string, label: T) => boolean, getName: (label: T) => string, by: string, data: T[], getRecipesBy: (token: CancelToken, ids: string[], string: string, order: string, all: boolean) => Promise<RecipeShort[]>, noResultsText?: string, backToText?: string, searchPlaceHolderText?: string, noRecipesText?: string }) => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const location = useLocation()
  const history = useHistory()
  const labels = props.data
  const [selected, setSelected] = useState<T[]>()
  const [search, setSearch] = useState<boolean>(false)
  const [searched, setSearched] = useState('')
  const [searchAllAnySwitch, setSearchAllAnySwitch] = useState("any")

  const foundLabels = useMemo(() => {
    if (!labels) {
      return []
    } else if (!searched) {
      return labels
    } else {
      return labels.filter(label => props.searchComparison(searched.toLowerCase(), label))
    }
    // 'props' dependency is not needed because searchComparison function will not change while the component is mounted
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [labels, searched])

  useEffect(() => {
    let mounted = true
    // get params from url
    const params = new URLSearchParams(location.search)
    const l = (params.get('l') ?? "").split(",")
    const s = params.get('s') === "true"
    if (mounted) setSearch(s)
    if (labels && l.length > 0 && l[0]) {
      if (mounted) setSelected(labels.filter(label => l.some(id => label.id === id)))
    } else {
      if (mounted) setSelected([])
    }
    return () => {
      mounted = false
    }
    // 'location.search' dependency is not needed because after mount, url is changed only by either
    // code -> 'selected' state is updated there
    // or user -> page refresh,
    // needs to be called only on mount and when 'labels' change
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [labels])

  const createLink = (s: boolean, l: T[]) => {
    history.replace(getRoute(i18n.language, "/search/" + props.by) + "/?s=" + s + "&l=" + l.map(i => i.id))
  }

  if (!labels || !selected) {
    return <Loading/>
  }

  if (labels.length < 1) {
    return (
      <NoResultsText>{props.noResultsText ?? t("recipeSearch:noResults")}</NoResultsText>
    )
  }

  const onSearch = () => {
    setSearch(true)
    createLink(true, selected)
  }

  const back = () => {
    setSearch(false)
    createLink(false, selected)
  }

  const clearAll = () => {
    setSelected([])
    createLink(search, [])
  }

  const select = (label: T) => {
    const newSelected: T[] = selected.filter(l => l.id !== label.id)
    if (selected.length === newSelected.length) {
      newSelected.push(label)
    }
    setSelected(newSelected)
    createLink(search, newSelected)
  }

  const onInputChange = (value: string) => {
    setSearched(value)
  }

  const onAllAnySwitchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchAllAnySwitch((e.target as HTMLInputElement).value)
  }

  return (
    <div className={classes.container}>
      <Breadcrumbs current={parsePageTitle(document.title)}/>
      <div className={classes.selectedLabels}>
        <Collapse in={selected.length > 0}>
          <SelectedLabelsGrid data={selected} getName={props.getName} onClick={select}/>
        </Collapse>
      </div>
      <Grid container justifyContent='space-between' alignItems='center' className={classes.buttonsGrid}>
        {
          search
            ?
            <>
              <Button className={classes.clearAndBackButton} onClick={back} variant={"contained"} color={"grey"}>
                <ArrowBack className={classes.icon}/>
                {props.backToText ?? t("recipeSearch:back")}
              </Button>
            </>
            :
            <>
              <Button className={classes.clearAndBackButton} onClick={clearAll} variant={"contained"} color={"grey"}>
                <Clear className={classes.icon}/>
                {t("recipeSearch:clearAll")}
              </Button>
              <Button className={classes.searchButton} onClick={onSearch} variant={"contained"} color={"primary"}
                      disabled={selected.length < 1}>
                <Search className={classes.icon}/>
                {t("recipeSearch:search")}
              </Button>
            </>
        }
      </Grid>
      <div className={classes.switch}>
        <Typography variant='subtitle1' className={classes.switchText}>{t("recipeSearch:switchLabel")}</Typography>
        <RadioGroup row onChange={onAllAnySwitchChange} value={searchAllAnySwitch} color={"primary"}>
          <FormControlLabel value="any" control={<Radio color={"primary"}/>} label={t("recipeSearch:any") as string}/>
          <FormControlLabel value="all" control={<Radio color={"primary"}/>} label={t("recipeSearch:all") as string}/>
        </RadioGroup>
        <Typography variant='subtitle1'>{t("recipeSearch:ofSelected")}</Typography>
      </div>
      {
        !search
          ?
          <>
            <div className={classes.searchbar}>
              <LabelSearchBar onChange={onInputChange} initialValue={searched}
                              placeHolderText={props.searchPlaceHolderText}/>
            </div>
            <LabelsGrid
              data={foundLabels} getName={props.getName}
              selected={selected} onSelect={select}/>
          </>
          :
          <RecipesWithLabels labelIds={selected.map(l => l.id)}
                             getRecipesBy={(token, ids, string, orderBy) => props.getRecipesBy(token, ids, string, orderBy, searchAllAnySwitch === "all")}
                             noRecipesText={props.noRecipesText}/>
      }
    </div>
  )
}