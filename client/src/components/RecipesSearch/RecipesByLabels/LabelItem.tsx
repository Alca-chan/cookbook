import React from "react";
import {Card, CardActionArea, CardContent, Stack, Typography} from "@mui/material";
import {CheckBox, CheckBoxOutlineBlank} from "@mui/icons-material";
import {makeStyles} from "../../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  card: {
    height: '100%'
  },
  cardSelected: {
    background: theme.palette.primary.light,
    height: '100%'
  },
  actionArea: {
    height: '100%'
  },
  label: {
    width: '100%'
  },
  text: {
    fontSize: 18
  },
  cardContent: {
    display: 'flex',
    alignItems: 'center'
  },
  checkboxBlank: {
    marginRight: theme.spacing(1),
  },
  checkboxSelected: {
    marginRight: theme.spacing(1),
    color: theme.palette.primary.dark
  }
}))

export const LabelItem = (props: { labelName: string, count?: number, isSelected: boolean, onClick: () => void }) => {
  const {classes} = useStyles()

  return (
    <Card className={props.isSelected ? classes.cardSelected : classes.card}>
      <CardActionArea className={classes.actionArea} onClick={props.onClick}>
        <CardContent className={classes.cardContent}>
          {
            props.isSelected
              ? <CheckBox className={classes.checkboxSelected}/>
              : <CheckBoxOutlineBlank className={classes.checkboxBlank}/>
          }
          <Stack direction={"row"} justifyContent={"space-between"} className={classes.label}>
            <Typography align='left' className={classes.text}>{props.labelName}</Typography>
            {
              props.count !== undefined && (
                <Typography align='right' color={"textSecondary"} className={classes.text}>{props.count}</Typography>
              )
            }
          </Stack>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}