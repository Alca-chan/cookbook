import React, {useState} from 'react';
import TextField from '@mui/material/TextField';
import {IconButton, Zoom} from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import {Clear} from "@mui/icons-material";
import {useTranslation} from "react-i18next";

export const LabelSearchBar = (props: { onChange: (value: string) => void, initialValue: string, placeHolderText?: string }) => {
  const {t} = useTranslation()
  const [value, setValue] = useState(props.initialValue)

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setValue(e.target.value)
    props.onChange(e.target.value)
  }

  const onClear = () => {
    setValue("")
    props.onChange("")
  }

  return (
    <TextField
      fullWidth
      id="label-search"
      label={props.placeHolderText ?? t("recipeSearch:searchPlaceHolder")}
      type='text'
      variant='outlined'
      margin='normal'
      value={value}
      onChange={(e) => onChange(e)}
      InputProps={{
        endAdornment: (
          <InputAdornment position={"end"}>
            <Zoom in={value !== ""}>
              <IconButton size={"medium"} onClick={onClear}>
                <Clear fontSize={"small"}/>
              </IconButton>
            </Zoom>
          </InputAdornment>
        )
      }}
    />
  )
}