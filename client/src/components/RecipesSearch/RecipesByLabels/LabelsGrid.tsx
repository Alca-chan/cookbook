import React, {useState} from "react";
import {Grid} from "@mui/material";
import {ExpandMore} from "@mui/icons-material";
import {DividerWithIconButton} from "../../Dividers/DividerWithIconButton";
import {LabelItem} from "./LabelItem";
import {Label} from "../RecipesBy";
import {makeStyles} from "../../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    width: '100%'
  },
  cardGrid: {
    marginTop: theme.spacing(2)
  },
  divider: {
    marginTop: theme.spacing(2)
  }
}))

export const LabelsGrid = <T extends Label>(props: { data: T[], getName: (label: T) => string, selected: T[], onSelect: (name: T) => void }) => {
  const {classes} = useStyles()
  const initSize = 48
  const [numberOfItems, setNumberOfItems] = useState<number>(initSize)

  const more = () => {
    setNumberOfItems(numberOfItems + initSize)
  }

  return (
    <div className={classes.container}>
      <Grid container spacing={2} className={classes.cardGrid}>
        {
          props.data.slice(0, numberOfItems).map((label) =>
            <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={label.id}>
              <LabelItem key={label.id} labelName={props.getName(label)} count={label.count}
                         isSelected={props.selected.some(n => n.id === label.id)}
                         onClick={() => props.onSelect(label)}/>
            </Grid>
          )
        }
      </Grid>
      <div className={classes.divider}>
        <DividerWithIconButton displayIcon={numberOfItems < props.data.length} onClick={more}>
          <ExpandMore fontSize='large'/>
        </DividerWithIconButton>
      </div>
    </div>
  )
}