import React, {useCallback, useEffect, useRef, useState} from "react";
import {RecipeShort} from "../../../api/types";
import {Loading} from "../../Loading";
import {RecipesSearchGrid} from "../RecipesSearchGrid";
import {getCancelToken} from "../../../api/utils";
import {CancelToken, CancelTokenSource} from "axios";
import {makeStyles} from "../../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  recipesGrid: {
    marginTop: theme.spacing(2)
  }
}))

export const RecipesWithLabels = (props: { labelIds: string[], getRecipesBy: (token: CancelToken, ids: string[], string: string, orderBy: string) => Promise<RecipeShort[]>, noRecipesText?: string }) => {
  const {classes} = useStyles()
  const [data, setData] = useState<RecipeShort[]>()
  const [searchValue, setSearchValue] = useState<string>("")
  const [orderBy, setOrderBy] = useState<string>("id")
  const source = useRef<CancelTokenSource | undefined>()

  const {labelIds} = props

  const loadData = useCallback(async (token) => {
    try {
      if (labelIds && labelIds.length > 0) {
        const recipes: RecipeShort[] = await props.getRecipesBy(token, labelIds, searchValue, orderBy)
        setData(recipes)
      }
    } catch {
      /* canceled request */
    }
    // 'props' dependency is not needed because getRecipesBy function will not change while the component is mounted
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [labelIds, searchValue, orderBy])

  useEffect(() => {
    if (source.current) source.current.cancel()
    source.current = getCancelToken()
    loadData(source.current.token).then()
    return () => {
      if (source.current) source.current.cancel()
    }
  }, [loadData])

  if (!data) {
    return <Loading/>
  }

  return (
    <div className={classes.recipesGrid}>
      <RecipesSearchGrid data={data} onSearch={(searchValue) => setSearchValue(searchValue)}
                         onSort={setOrderBy} noRecipesText={props.noRecipesText}/>
    </div>
  )
}