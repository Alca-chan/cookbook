import React, {useState} from "react";
import {alpha, Card, CardActionArea, CardContent, Collapse, Grid, lighten, Typography} from "@mui/material";
import {Clear, ExpandLess, ExpandMore} from "@mui/icons-material";
import {DividerWithIconButton} from "../../Dividers/DividerWithIconButton";
import {Label} from "../RecipesBy";
import {makeStyles} from "../../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  cardGrid: {
    width: '100%',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(0.5)
  },
  cardContent: {
    display: 'flex'
  },
  gradient: {
    background: 'linear-gradient(transparent, ' + theme.palette.appBackground.inner + ')',
    marginTop: "-8px",
    height: "10px"
  },
  card: {
    background: theme.palette.label.main,
    height: '100%'
  },
  clear: {
    marginRight: theme.spacing(1),
    color: alpha(theme.palette.text.primary, 0.3)
  },
  text: {
    fontSize: 18,
    color: lighten(theme.palette.text.primary, 0.1)
  }
}))

export const SelectedLabelsGrid = <T extends Label>(props: { data: T[], getName: (label: T) => string, onClick: (label: T) => void }) => {
  const {classes} = useStyles()
  const [isCollapsed, setIsCollapsed] = useState<boolean>(true)
  const selected = props.data

  const collapse = () => setIsCollapsed(!isCollapsed)

  return (
    <>
      <Collapse in={!isCollapsed} collapsedSize={70} style={{width: '100%'}}>

        <Grid container spacing={1} justifyContent='center' alignItems='stretch' className={classes.cardGrid}>
          {
            selected.map((label) =>
              <Grid item key={label.id}>
                <Card className={classes.card}>
                  <CardActionArea onClick={() => {
                    props.onClick(label)
                  }}>
                    <CardContent className={classes.cardContent}>
                      <Clear className={classes.clear}/>
                      <Typography className={classes.text}>{props.getName(label)}</Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )
          }
        </Grid>

      </Collapse>
      <Grid container>
        <Grid item xs={12} className={classes.gradient}>
        </Grid>
      </Grid>
      <DividerWithIconButton displayIcon={true} onClick={collapse}>
        {
          isCollapsed
            ? <ExpandMore fontSize='small'/>
            : <ExpandLess fontSize='small'/>
        }
      </DividerWithIconButton>
    </>
  )
}