import React, {useCallback, useEffect, useState} from "react";
import {RecipeShort} from "../../../api/types";
import {Loading} from "../../Loading";
import {getRecipeByTitle} from "../../../api/recipes/recipeSearch";
import {getCancelToken} from "../../../api/utils";
import {makeStyles} from "../../../utils/makeStyles";
import {RecipesSearchGrid} from "../RecipesSearchGrid";

const useStyles = makeStyles()(theme => ({
  recipesGrid: {
    marginTop: theme.spacing(5)
  }
}))

export const RecipesContainingString = (props: { string: string }) => {
  const {classes} = useStyles()
  const [data, setData] = useState<RecipeShort[]>()
  const [orderBy, setOrderBy] = useState<string>("id")

  const loadData = useCallback(async (token) => {
    try {
      const recipes = await getRecipeByTitle(token, props.string, orderBy)
      setData(recipes)
    } catch {
      /* canceled request */
    }
  }, [props.string, orderBy])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  if (!data) {
    return <Loading/>
  }

  return (
    <div className={classes.recipesGrid}>
      {
        data.length > 0 &&
        <RecipesSearchGrid data={data} onSort={setOrderBy}/>
      }
    </div>
  )
}