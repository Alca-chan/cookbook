import React from 'react';
import {Grid} from '@mui/material';
import {RecipeShort} from '../../api/types';
import {RecipesGrid} from "../RecipesGrid/RecipesGrid";
import {SortMenu} from "./SortMenu";
import {NoResultsText} from "../RecipesGrid/NoResultsText";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";
import {RecipeSearchBar} from "./RecipeSearchBar";

const useStyles = makeStyles()(theme => ({
  container: {
    width: "100%",
  },
  options: {
    marginBottom: theme.spacing(4)
  }
}))

export const RecipesSearchGrid = (props: { data: RecipeShort[], onSearch?: (searchValue: string) => void, onSort: (orderBy: string) => void, noRecipesText?: string }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()

  return (
    <div className={classes.container}>
      <Grid container spacing={1} className={classes.options} direction={"row"}
            justifyContent={"space-between"} alignItems={"center"}>
        <Grid item>
          <SortMenu onSelect={props.onSort}/>
        </Grid>
        {
          props.onSearch && (
            <Grid item>
              <RecipeSearchBar onSearch={props.onSearch}/>
            </Grid>
          )
        }
      </Grid>
      {
        props.data.length > 0 ?
          <RecipesGrid data={props.data}/>
          :
          <NoResultsText>{props.noRecipesText ?? t("recipeSearch:noResults")}</NoResultsText>
      }
    </div>
  )
}