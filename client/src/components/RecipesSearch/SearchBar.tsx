import React, {useState} from "react";
import {alpha, Button, darken, IconButton, InputBase, Paper, Zoom} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import {Clear} from "@mui/icons-material";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  searchGrid: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%'
  },
  searchGridColor: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      width: '480px'
    },
    padding: theme.spacing(1),
    background: theme.palette.primary.light
  },
  search: {
    width: '100%',
    backgroundColor: alpha(theme.palette.common.white, 0.55),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.65),
    },
    display: 'flex',
    alignItems: 'center'
  },
  searchButton: {
    marginLeft: theme.spacing(1),
    height: '100%',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: darken(theme.palette.primary.main, 0.035),
    }
  },
  inputRoot: {
    width: '100%'
  },
  inputInput: {
    margin: theme.spacing(1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    }
  },
  clearButton: {
    paddingRight: theme.spacing(1)
  }
}))

export const SearchBar = (props: { onChange: (string: string) => void, onSearchRequest: () => void, initialValue: string }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [value, setValue] = useState(props.initialValue)

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setValue(e.target.value)
    props.onChange(e.target.value)
  }

  const onClear = () => {
    setValue("")
    props.onChange("")
  }

  return (
    <div className={classes.searchGrid}>
      <Paper className={classes.searchGridColor}>
        <div className={classes.search}>
          <InputBase
            placeholder={t("recipeSearch:searchRecipes")}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            value={value}
            onChange={(e) => onChange(e)}
            onKeyDown={(e) => {
              if (e.key === "Enter") props.onSearchRequest()
            }}
          />
          <Zoom in={value.length > 0}>
            <div className={classes.clearButton}>
              <IconButton size={"medium"} onClick={onClear}>
                <Clear fontSize={"small"}/>
              </IconButton>
            </div>
          </Zoom>
        </div>
        <Button className={classes.searchButton} onClick={props.onSearchRequest} color={"grey"}>
          <SearchIcon/>
        </Button>
      </Paper>
    </div>
  )
}