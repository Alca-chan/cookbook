import {Button, Divider, Menu, MenuItem, Typography} from "@mui/material";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {ArrowDropDown, Clear} from "@mui/icons-material";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    width: "100%"
  },
  options: {
    marginBottom: theme.spacing(4)
  },
  by: {
    marginLeft: theme.spacing(2),
    fontStyle: 'italic'
  },
  byOptions: {
    marginLeft: theme.spacing(1)
  },
  sortByLabel: {
    marginLeft: theme.spacing(1)
  }
}))

enum SortBy {
  Default = "id",
  Title = "alphabetic",
  Date = "date",
  Votes = "votes",
  Time = "time",
  TimePrep = "timePrep"
}

export const SortMenu = (props: { onSelect: (option: string) => void }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [anchor, setAnchor] = useState<null | HTMLElement>(null)
  const [sortBy, setSortBy] = useState<SortBy>(SortBy.Default)

  const onMenuClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchor(e.currentTarget)
  }

  const onMenuClose = () => {
    setAnchor(null)
  }

  const onOptionSelect = (e: React.MouseEvent<HTMLLIElement, MouseEvent>, sortBy: SortBy) => {
    e.stopPropagation()
    e.preventDefault()
    setAnchor(null) // close menu
    if (!sortBy) return
    setSortBy(sortBy)
    props.onSelect(sortBy)
  }

  const getSortByString = (sortBy: SortBy): string => {
    switch (sortBy) {
      case SortBy.Title:
        return t("recipeSearch:byTitle")
      case SortBy.Date:
        return t("recipeSearch:byDate")
      case SortBy.Votes:
        return t("recipeSearch:byVotes")
      case SortBy.Time:
        return t("recipeSearch:byTime")
      case SortBy.TimePrep:
        return t("recipeSearch:byTimePrep")
      case SortBy.Default:
        return t("recipeSearch:default")
    }
  }

  return (
    <>
      <Button variant={"contained"} color={"primary"} size={"large"} onClick={onMenuClick} endIcon={<ArrowDropDown/>}>
        {t("recipeSearch:sortBy")}
      </Button>
      {
        sortBy !== SortBy.Default && (
          <Button startIcon={<Clear/>} onClick={() => setSortBy(SortBy.Default)} variant={"outlined"} color={"grey"}
                  size={"large"} className={classes.sortByLabel}>
            {t("recipeSearch:by") + " " + getSortByString(sortBy)}
          </Button>
        )
      }
      <Menu open={anchor !== null} anchorEl={anchor}
            anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
            onClose={onMenuClose}
            onClick={(e) => {
              e.stopPropagation()
              e.preventDefault()
            }}>
        <Typography variant={"body2"} className={classes.by}>{t("recipeSearch:by")}</Typography>
        {
          Object.keys(SortBy).map((sortBy, index) =>
            (
              index > 0 && (
                <MenuItem key={index} className={classes.byOptions} onClick={(e) => {
                  onOptionSelect(e, (SortBy as any)[sortBy])
                }}>
                  {getSortByString((SortBy as any)[sortBy])}
                </MenuItem>
              )
            )
          )
        }
        <Divider/>
        <MenuItem onClick={(e) => {
          onOptionSelect(e, SortBy.Default)
        }}>
          {t("recipeSearch:default")}
        </MenuItem>
      </Menu>
    </>
  )
}