import {Redirect, Route} from "react-router-dom";
import React from "react";

export const RedirectingRoute = (props: { path: string[] | string, redirectPath: string }) => {
  const {path, redirectPath} = props

  return (
    <Route
      path={path} exact
      render={(routeProps) =>
        <Redirect to={{pathname: redirectPath, state: {from: routeProps.location}}}/>
      }
    />
  )
}