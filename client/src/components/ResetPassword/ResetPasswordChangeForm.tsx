import React, {useState} from "react";
import {Button, Card, CardActions, CardContent, CardHeader, TextField} from "@mui/material";
import {changeResetPassword} from "../../api/user";
import {useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {changePasswordToast} from "../../utils/toasts";
import {makeStyles} from "../../utils/makeStyles";
import {PASSWORD_MIN_LENGTH} from "../../utils/utils";

const useStyles = makeStyles()(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  header: {
    background: theme.palette.primary.main
  }
}))

export const ResetPasswordChangeForm = (props: { token: string }) => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [passwordError, setPasswordError] = useState(false)
  const [confirmPasswordError, setConfirmPasswordError] = useState(false)
  const history = useHistory()

  const resetErrors = () => {
    setPasswordError(false)
    setConfirmPasswordError(false)
  }

  const checkPassword = (): boolean => {
    if (password.length >= PASSWORD_MIN_LENGTH) {
      return true
    }
    setPasswordError(true)
    return false
  }

  const checkConfirmPassword = (): boolean => {
    if (password === confirmPassword) {
      return true
    }
    setConfirmPasswordError(true)
    return false
  }

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    resetErrors()

    const validPassword = checkPassword() && checkConfirmPassword()
    if (!validPassword) return

    await changePasswordToast(t,
      changeResetPassword(props.token, password)
    )
  }

  const cancel = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    history.push("/")
  }

  return (
    <form noValidate autoComplete='off' onSubmit={e => onSubmit(e)}>
      <Card>
        <CardHeader className={classes.header} title={t("login:changePassword").toUpperCase()}
                    titleTypographyProps={{variant: 'body1'}}/>
        <CardContent>
          <TextField
            fullWidth
            id="password"
            type='password'
            label={t("login:password")}
            margin='normal'
            variant='outlined'
            onChange={e => setPassword(e.target.value)}
            error={passwordError}
            helperText={passwordError ? t("login:passwordError", {length: PASSWORD_MIN_LENGTH}) : ""}
          />
          <TextField
            fullWidth
            id="confirmPassword"
            type='password'
            label={t("login:confirmPassword")}
            margin='normal'
            variant='outlined'
            onChange={e => setConfirmPassword(e.target.value)}
            error={confirmPasswordError}
            helperText={confirmPasswordError ? t("login:confirmPasswordError") : ""}
          />
        </CardContent>
        <CardActions>
          <Button
            className={classes.button}
            variant={"contained"}
            color={"primary"}
            size={"large"}
            type={"submit"}>
            {t("login:submit")}
          </Button>
          <Button
            className={classes.button}
            variant={"contained"}
            color={"grey"}
            size={"large"}
            onClick={(e) => cancel(e)}>
            {t("login:cancel")}
          </Button>
        </CardActions>
      </Card>
    </form>
  )
}