import React, {useState} from "react";
import {Button, Card, CardActions, CardContent, CardHeader, TextField} from "@mui/material";
import {resetPassword} from "../../api/user";
import {useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {sentPasswordResetToast} from "../../utils/toasts";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  header: {
    background: theme.palette.primary.main
  }
}))

export const ResetPasswordRequestForm = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const [email, setEmail] = useState('')
  const [emailError, setEmailError] = useState(false)
  const history = useHistory()

  const resetErrors = () => {
    setEmailError(false)
  }

  const checkEmailAddress = (): boolean => {
    const emailRegEx: RegExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (email && emailRegEx.test(email.toLowerCase())) {
      return true
    }
    setEmailError(true)
    return false
  }

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    resetErrors()

    const validEmail = checkEmailAddress()
    if (!validEmail) return

    await sentPasswordResetToast(t,
      resetPassword(email)
    )
  }

  const cancel = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    history.push("/", {shallow: true})
  }

  return (
    <form noValidate autoComplete='off' onSubmit={e => onSubmit(e)}>
      <Card>
        <CardHeader className={classes.header} title={t("login:resetPassword").toUpperCase()}
                    titleTypographyProps={{variant: 'body1'}}/>
        <CardContent>
          <TextField
            fullWidth
            id="email"
            type='email'
            label={t("login:email")}
            margin='normal'
            variant='outlined'
            value={email}
            onChange={e => setEmail(e.target.value)}
            error={emailError}
            helperText={emailError ? t("login:emailError") : ""}
          />
        </CardContent>
        <CardActions>
          <Button
            className={classes.button}
            variant={"contained"}
            color={"primary"}
            size={"large"}
            type={"submit"}>
            {t("login:sentRequest")}
          </Button>
          <Button
            className={classes.button}
            variant={"contained"}
            color={"grey"}
            size={"large"}
            onClick={(e) => cancel(e)}>
            {t("login:cancel")}
          </Button>
        </CardActions>
      </Card>
    </form>
  )
}