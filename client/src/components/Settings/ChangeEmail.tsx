import {Button, Collapse, Stack, TextField, Typography} from "@mui/material";
import React, {useContext, useState} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";
import {UserContext} from "../../contexts/UserContext";
import {checkValidEmailAddress} from "../../utils/utils";
import {changeEmail, checkTakenEmail} from "../../api/user";
import {ServerResponse} from "../../api/utils";
import {changeEmailToast} from "../../utils/toasts";
import toast from "react-hot-toast";

const useStyles = makeStyles()(theme => ({
  buttons: {
    marginTop: theme.spacing(2)
  }
}))

export const ChangeEmail = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {user, updateUser} = useContext(UserContext)
  const [email, setEmail] = useState(user ? user.email : '')
  const [emailError, setEmailError] = useState(false)
  const [takenEmailError, setTakenEmailError] = useState(false)
  const [confirmEmailPassword, setConfirmEmailPassword] = useState('')
  const [confirmEmailPasswordError, setConfirmEmailPasswordError] = useState(false)
  const [editEmailMode, setEditEmailMode] = useState(false)
  const [submitEmailError, setSubmitEmailError] = useState(false)

  const resetEmailErrors = () => {
    setEmailError(false)
    setConfirmEmailPasswordError(false)
  }

  const checkEmail = (): boolean => {
    if (checkValidEmailAddress(email)) {
      return true
    }
    setEmailError(true)
    return false
  }

  const submitEmailChange = async () => {
    resetEmailErrors()
    if (!checkEmail()) return
    const takenEmail = await checkTakenEmail(email)
    if (user && user.email === email) {
      toast.success(<b>{t("toast:sendingEmailChangeRequestSuccessful")}</b>)
      setEditEmailMode(false)
      setConfirmEmailPassword('')
      return
    }
    if (takenEmail) {
      setTakenEmailError(true)
      return
    }
    await changeEmailToast(t,
      changeEmail(confirmEmailPassword, email).then((result) => {
        if (result === ServerResponse.EmailChangeOK) {
          updateUser()
          setEditEmailMode(false)
          setConfirmEmailPassword('')
        } else if (result === ServerResponse.PswdError) {
          setConfirmEmailPasswordError(true)
        } else {
          setSubmitEmailError(true)
        }
      })
    )
  }

  const cancelEmailChange = () => {
    if (!user) return
    setEditEmailMode(false)
    setEmail(user.email)
    setConfirmEmailPassword('')
    resetEmailErrors()
  }

  return (
    <>
      <TextField
        fullWidth
        id="email"
        type='email'
        label={editEmailMode ? t("settings:newEmail") : t("settings:email")}
        margin='dense'
        variant='outlined'
        value={email}
        onChange={e => {
          if (!editEmailMode) setEditEmailMode(true)
          setEmail(e.target.value)
        }}
        error={emailError || takenEmailError}
        helperText={emailError ? t("settings:emailError") : (takenEmailError ? t("settings:takenEmailError") : "")}
      />
      <Collapse in={editEmailMode}>
        <>
          <TextField
            fullWidth
            id="confirmEmailPassword"
            type='password'
            label={t("settings:confirmEmailPassword")}
            margin='dense'
            variant='outlined'
            value={confirmEmailPassword}
            onChange={e => setConfirmEmailPassword(e.target.value)}
            error={confirmEmailPasswordError}
            helperText={confirmEmailPasswordError ? t("settings:wrongPassword") : ""}
          />
          {
            submitEmailError && (
              <div>
                <Typography variant='subtitle2' align='left' color='error'
                            component={'span'}>{t("settings:serverError")}</Typography>
              </div>
            )
          }
          <Stack direction={"row"} spacing={2} className={classes.buttons}>
            <Button
              variant={"contained"}
              color={"primary"}
              size={"large"}
              onClick={submitEmailChange}
            >
              {t("settings:save")}
            </Button>
            <Button
              variant={"contained"}
              color={"grey"}
              size={"large"}
              onClick={cancelEmailChange}
            >
              {t("settings:cancel")}
            </Button>
          </Stack>
        </>
      </Collapse>
    </>
  )
}