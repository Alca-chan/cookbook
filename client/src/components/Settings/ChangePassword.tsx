import {Button, Collapse, Stack, TextField, Typography} from "@mui/material";
import React, {useState} from "react";
import {getPageTitle} from "../../utils/routes";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../../utils/makeStyles";
import {PASSWORD_MIN_LENGTH} from "../../utils/utils";
import {changePassword} from "../../api/user";
import {ServerResponse} from "../../api/utils";
import {changePasswordToast} from "../../utils/toasts";

const useStyles = makeStyles()(theme => ({
  buttons: {
    marginTop: theme.spacing(2)
  }
}))

export const ChangePassword = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:settings"))
  const [password, setPassword] = useState('')
  const [passwordError, setPasswordError] = useState(false)
  const [newPassword, setNewPassword] = useState('')
  const [newPasswordError, setNewPasswordError] = useState(false)
  const [confirmPassword, setConfirmPassword] = useState('')
  const [confirmPasswordError, setConfirmPasswordError] = useState(false)
  const [submitPasswordChangeError, setSubmitPasswordError] = useState(false)
  const [editPasswordMode, setEditPasswordMode] = useState(false)

  const resetPasswordErrors = () => {
    setPasswordError(false)
    setNewPasswordError(false)
    setConfirmPasswordError(false)
  }

  const resetChanges = () => {
    setEditPasswordMode(false)
    setPassword('')
    setNewPassword('')
    setConfirmPassword('')
  }

  const checkNewPassword = (): boolean => {
    if (newPassword.length >= PASSWORD_MIN_LENGTH) {
      return true
    }
    setNewPasswordError(true)
    return false
  }

  const checkConfirmPassword = (): boolean => {
    if (newPassword === confirmPassword) {
      return true
    }
    setConfirmPasswordError(true)
    return false
  }

  const submitPasswordChange = async () => {
    resetPasswordErrors()
    if (!checkNewPassword() || !checkConfirmPassword()) return
    await changePasswordToast(t,
      changePassword(password, newPassword).then((result) => {
        if (result === ServerResponse.PswdChangeOK) {
          resetChanges()
        } else if (result === ServerResponse.PswdError) {
          setPasswordError(true)
        } else {
          setSubmitPasswordError(true)
        }
      })
    )
  }

  const cancelPasswordChange = () => {
    resetChanges()
    resetPasswordErrors()
  }

  return (
    <>
      <TextField
        fullWidth
        id="password"
        type='password'
        label={t("settings:password")}
        margin='dense'
        variant='outlined'
        value={password}
        onChange={e => setPassword(e.target.value)}
        onFocus={() => {
          if (!editPasswordMode) setEditPasswordMode(true)
        }}
        error={passwordError}
        helperText={passwordError ? t("settings:wrongPassword") : ""}
      />
      <TextField
        fullWidth
        id="newPassword"
        type='password'
        label={t("settings:newPassword")}
        margin='dense'
        variant='outlined'
        value={newPassword}
        onChange={e => setNewPassword(e.target.value)}
        onFocus={() => {
          if (!editPasswordMode) setEditPasswordMode(true)
        }}
        error={newPasswordError}
        helperText={newPasswordError ? t("settings:newPasswordError", {length: PASSWORD_MIN_LENGTH}) : ""}
      />
      <TextField
        fullWidth
        id="confirmPassword"
        type='password'
        label={t("settings:confirmPassword")}
        margin='dense'
        variant='outlined'
        value={confirmPassword}
        onChange={e => setConfirmPassword(e.target.value)}
        onFocus={() => {
          if (!editPasswordMode) setEditPasswordMode(true)
        }}
        error={confirmPasswordError}
        helperText={confirmPasswordError ? t("settings:confirmPasswordError") : ""}
      />

      {
        submitPasswordChangeError && (
          <div>
            <Typography variant='subtitle2' align='left' color='error'
                        component={'span'}>{t("settings:serverError")}</Typography>
          </div>
        )
      }
      <Collapse in={editPasswordMode}>
        <Stack direction={"row"} spacing={2} className={classes.buttons}>
          <Button
            variant={"contained"}
            color={"primary"}
            size={"large"}
            onClick={submitPasswordChange}
          >
            {t("settings:save")}
          </Button>
          <Button
            variant={"contained"}
            color={"grey"}
            size={"large"}
            onClick={cancelPasswordChange}
          >
            {t("settings:cancel")}
          </Button>
        </Stack>
      </Collapse>
    </>
  )
}