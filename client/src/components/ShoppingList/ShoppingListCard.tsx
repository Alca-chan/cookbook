import React, {useContext, useState} from "react";
import {Link} from "react-router-dom";
import {Card, CardContent, CardHeader, Grid, IconButton, Typography} from "@mui/material";
import {CartItem, CartList} from "../../api/types";
import {FileCopy} from "@mui/icons-material";
import {CartContext} from "../../contexts/CartContext";
import {useTranslation} from "react-i18next";
import {getRoute, parsePageTitle} from "../../utils/routes";
import {Draggable, Droppable} from "react-beautiful-dnd";
import {ShoppingListItemRow} from "./ShoppingListItemRow";
import grey from "@mui/material/colors/grey";
import {copyShoppingListToast} from "../../utils/toasts";
import {defaultDateFormat, displayDateFormat, recipeString} from "../../utils/utils";
import {makeStyles} from "../../utils/makeStyles";
import {PlusButton} from "../Buttons/PlusButton";
import {NewShoppingListDialog} from "../Dialogs/NewShoppingListDialog";
import {Moment} from "moment";
import {CalendarDatePicker} from "../Calendar/CalendarDatePicker";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  card: {
    width: "100%",
    padding: 0
  },
  actions: {
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: theme.palette.table.subHeader
  },
  actionsDisabled: {
    backgroundColor: grey[200]
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },
  cardHeader: {
    backgroundColor: theme.palette.primary.light
  },
  cardHeaderDisabled: {
    backgroundColor: "#d2d2d2"
  },
  cardContent: {
    "&:last-child": {
      paddingBottom: theme.spacing(1)
    }
  },
  plusButton: {
    width: "100%",
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
}))

export const ShoppingListCard = (props: { list: CartList, listIndex: number, editMode: boolean, draggedItem: CartItem | undefined, updateItem: (oldItem: CartItem, newItem: CartItem) => void, markAsDone: (item: CartItem) => void, markAllAsDone: (recipe: CartList) => void }) => {
  const {t, i18n} = useTranslation()
  const {classes} = useStyles()
  const {updateCartItems, removeCartItems, removeAllForRecipeFromCart} = useContext(CartContext)
  const draggedItem = props.draggedItem
  const recipe = props.list
  const editMode = props.editMode
  const date = recipe.date ? moment(recipe.date).format(defaultDateFormat) : undefined
  const [openNewItemDialog, setOpenNewItemDialog] = useState(false)
  const [addItemToList, setAddItemToList] = useState<CartList>()

  const removeAll = (recipe: CartList) => {
    if (!recipe.recipeId) {
      removeCartItems(recipe.items.map(i => i.id))
    } else {
      removeAllForRecipeFromCart(recipe.recipeId, recipe.date ?? null)
    }
  }

  const removeItem = (cartItem: CartItem) => {
    removeCartItems([cartItem.id])
  }

  const addNewItem = (list: CartList) => {
    setOpenNewItemDialog(true)
    setAddItemToList(list)
  }

  const copyRecipeToClipboard = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, recipe: CartList) => {
    e.stopPropagation()
    e.preventDefault()
    await copyShoppingListToast(t, navigator.clipboard.writeText(recipeString(recipe)))
  }

  const changeDate = (selectedDate: Moment | null) => {
    if (!selectedDate) return
    recipe.items.forEach(i => i.date = moment(selectedDate).format(defaultDateFormat))
    updateCartItems(recipe.items)
  }

  return (
    <>
      {
        (openNewItemDialog && addItemToList) && (
          <NewShoppingListDialog open={openNewItemDialog} setOpen={setOpenNewItemDialog} list={addItemToList}/>
        )
      }
      <Card className={classes.card}>
        <CardHeader
          className={(draggedItem && draggedItem.recipeId !== recipe.recipeId) ? classes.cardHeaderDisabled : classes.cardHeader}
          title={
            <>
              {
                recipe.recipeId ?
                  <Link className={classes.link}
                        to={(location) => ({
                          pathname: getRoute(i18n.language, "/recipe/:id").replace(":id", recipe.recipeId as string),
                          state: {path: location.pathname + location.search, name: parsePageTitle(document.title)}
                        })}>
                    {recipe.title}
                  </Link>
                  :
                  recipe.title
              }
              {
                editMode ?
                  <CalendarDatePicker selectedDate={recipe.date ?? null} onChange={changeDate} minDate={null}/>
                  :
                  date && <Typography>{'(' + moment(date).format(displayDateFormat) + ')'}</Typography>
              }
            </>
          }
          action={
            !editMode && (
              <IconButton size={"large"} onClick={(e) => copyRecipeToClipboard(e, recipe)}>
                <FileCopy fontSize={"small"}/>
              </IconButton>
            )
          }
        />
        <CardHeader
          className={classes.actions + ((draggedItem && draggedItem.recipeId !== recipe.recipeId) ? (' ' + classes.actionsDisabled) : '')}
          title={
            <ShoppingListItemRow editMode={editMode}
                                 removeItem={() => removeAll(recipe)}
                                 checked={recipe.allChecked}
                                 onCheckboxClick={() => props.markAllAsDone(recipe)}
            />
          }/>
        <CardContent className={classes.cardContent}>
          <Droppable droppableId={(recipe.recipeId ?? recipe.title) + "_" + date}
                     isDropDisabled={!(draggedItem && draggedItem.recipeId === recipe.recipeId)}>
            {(provided) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                <Grid container key={props.listIndex}>
                  {
                    recipe.items.map((row, index) => {
                      return (
                        <Grid item xs={12} key={row.id}>
                          <Draggable key={row.id} draggableId={row.id} index={index}
                                     isDragDisabled={!editMode}>
                            {(provided) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}>
                                <ShoppingListItemRow item={row} editMode={editMode}
                                                     border={true}
                                                     dragged={draggedItem && row.id === draggedItem.id}
                                                     draggable={true}
                                                     updateItem={props.updateItem}
                                                     removeItem={() => removeItem(row)}
                                                     checked={row.done}
                                                     lineThroughChecked={true}
                                                     onCheckboxClick={() => props.markAsDone(row)}
                                />
                              </div>
                            )}
                          </Draggable>
                        </Grid>
                      )
                    })
                  }
                  {provided.placeholder}
                  {
                    editMode && (
                      <div className={classes.plusButton}>
                        <PlusButton onClick={() => addNewItem(recipe)}/>
                      </div>
                    )
                  }
                </Grid>
              </div>
            )}
          </Droppable>
        </CardContent>
      </Card>
    </>
  )
}