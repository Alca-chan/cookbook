import {Checkbox, Grid, IconButton, Typography} from "@mui/material";
import React from "react";
import {ShoppingListTextField} from "./ShoppingListTextField";
import {CheckBox, CheckBoxOutlineBlank, Delete, DragIndicator} from "@mui/icons-material";
import {makeStyles} from "../../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  row: {
    width: '100%'
  },
  dragged: {
    backgroundColor: theme.palette.background.paper
  },
  rowBorder: {
    borderWidth: 0,
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.table.border
  },
  item: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: theme.spacing(6)
  },
  measure: {
    justifyContent: 'flex-end',
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  ingredient: {
    justifyContent: 'flex-start',
    alignContent: 'center',
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  checkbox: {
    cursor: 'pointer',
    justifyContent: 'center'
  },
  checkboxIcon: {
    height: '18px',
    width: '18px'
  },
  done: {
    textDecorationLine: 'line-through',
    color: theme.palette.text.secondary
  }
}))

export const ShoppingListItemRow = <T extends { measure: string, ingredient: string }>(props: { item?: T, border?: boolean, dragged?: boolean, draggable?: boolean, editMode: boolean, updateItem?: (oldItem: T, newItem: T) => void, removeItem?: () => void, checked?: boolean, onCheckboxClick?: () => void, lineThroughChecked?: boolean, checkboxIcon?: React.ReactNode, checkboxIconChecked?: React.ReactNode }) => {
  const {classes} = useStyles()

  const onClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.preventDefault()
    e.stopPropagation()
    if (props.onCheckboxClick) props.onCheckboxClick()
  }

  return (
    <Grid container direction={"row"}
          className={classes.row + (props.border ? (' ' + classes.rowBorder) : '') + (props.dragged ? (' ' + classes.dragged) : '')}>
      {
        props.checked !== undefined && (
          <Grid item xs={1} className={classes.item + ' ' + classes.checkbox} onClick={(e) => onClick(e)}>
            {
              props.draggable && props.editMode ?
                <DragIndicator/>
                :
                <Checkbox className={classes.checkboxIcon} checked={props.checked} color={"primary"}
                          icon={props.checkboxIcon ?? <CheckBoxOutlineBlank/>}
                          checkedIcon={props.checkboxIconChecked ?? <CheckBox/>}
                />
            }
          </Grid>
        )
      }
      <Grid item xs={props.checked === undefined ? 5 : 4}
            className={classes.item + ' ' + classes.measure + ((props.lineThroughChecked && props.checked) ? (' ' + classes.done) : '')}>
        {
          props.item && (
            <>
              {
                props.editMode ?
                  <ShoppingListTextField
                    defaultValue={props.item.measure}
                    onSubmit={(newMeasure) => {
                      if (props.updateItem && props.item) props.updateItem(props.item, {
                        ...props.item,
                        measure: newMeasure
                      })
                    }}
                  />
                  :
                  <Typography align={"right"} style={{width: '100%'}}>
                    {props.item.measure}
                  </Typography>
              }
            </>
          )
        }
      </Grid>
      <Grid item xs={props.removeItem ? 6 : 7}
            className={classes.item + ' ' + classes.ingredient + ((props.lineThroughChecked && props.checked) ? (' ' + classes.done) : '')}>
        {
          props.item && (
            <>
              {
                props.editMode ?
                  <ShoppingListTextField
                    defaultValue={props.item.ingredient}
                    onSubmit={(newIngredient) => {
                      if (props.updateItem && props.item) props.updateItem(props.item, {
                        ...props.item,
                        ingredient: newIngredient
                      })
                    }}
                  />
                  :
                  <Typography align={"left"}>
                    {props.item.ingredient}
                  </Typography>
              }
            </>
          )
        }
      </Grid>
      {
        (props.editMode && props.removeItem) && (
          <Grid item xs={1}>
            <IconButton size={"large"} onClick={() => {
              if (props.removeItem) props.removeItem()
            }}>
              <Delete fontSize={"small"}/>
            </IconButton>
          </Grid>
        )
      }
    </Grid>
  )
}