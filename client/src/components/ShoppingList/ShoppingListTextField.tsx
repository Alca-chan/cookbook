import React, {useState} from "react";
import {TextField} from "@mui/material";

export const ShoppingListTextField = (props: { defaultValue: string, onSubmit: (value: string) => void }) => {
  const [edit, setEdit] = useState<boolean>(false)
  const [value, setValue] = useState(props.defaultValue)

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setValue(e.target.value ?? "")
  }

  const onSubmit = () => {
    if (!edit || value === props.defaultValue) return
    setEdit(false)
    props.onSubmit(value)
  }

  return (
    <TextField
      fullWidth
      type={"text"}
      margin={"dense"}
      variant={"standard"}
      size={"small"}
      color={"primary"}
      value={edit ? value : props.defaultValue}
      onChange={(e) => onChange(e)}
      onFocus={() => setEdit(true)}
      onBlur={() => onSubmit()}
      onSubmit={() => onSubmit()}
    />
  )
}