import React, {useContext, useState} from "react";
import {Grid} from "@mui/material";
import {CartItem, CartList} from "../../api/types";
import {NoResultsText} from "../RecipesGrid/NoResultsText";
import {Loading} from "../Loading";
import {CartContext} from "../../contexts/CartContext";
import {useTranslation} from "react-i18next";
import {DragDropContext, DragStart, DropResult} from "react-beautiful-dnd";
import {Moment} from "moment";
import {compareDates, defaultDateFormat} from "../../utils/utils";
import {makeStyles} from "../../utils/makeStyles";
import {ShoppingListCard} from "./ShoppingListCard";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  container: {
    justifyContent: 'center',
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1)
  },
  plusButton: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
}))

const findList = (lists: CartList[], recipeId: string | undefined, date: Moment | undefined, title: string | undefined): CartList | undefined => {
  return lists.find(l => l.recipeId ? l.recipeId === recipeId && compareDates(date, l.date) : l.title === title)
}

export const ShoppingListsGrid = (props: { lists: CartList[], editMode: boolean, updateLists: (newLists: CartList[]) => void, changeEditMode: (edit: boolean) => void }) => {
  const {t} = useTranslation()
  const {classes} = useStyles()
  const {
    cart,
    updateCartItems,
    updateItemDone,
    updateItemOrder
  } = useContext(CartContext)
  const [draggedItem, setDraggedItem] = useState<CartItem | undefined>(undefined)

  const lists = props.lists
  const editMode = props.editMode

  if (!cart || !lists) {
    return <Loading/>
  }

  if (cart.length < 1) {
    return <NoResultsText>{t("cart:nothingInCart")}</NoResultsText>
  }

  const updateItem = (oldItem: CartItem, newItem: CartItem) => {
    oldItem.ingredient = newItem.ingredient
    oldItem.measure = newItem.measure
    props.updateLists([...lists])
    updateCartItems([newItem])
  }

  const markAllAsDone = (list: CartList) => {
    const newDone = !list.allChecked
    list.items.forEach(i => i.done = newDone)
    list.allChecked = newDone
    props.updateLists([...lists])
    updateItemDone(list.items, newDone)
  }

  const markAsDone = (item: CartItem) => {
    const newDone = !item.done
    item.done = newDone
    const newLists = [...lists]
    const foundList = findList(newLists, item.recipeId ?? undefined, item.date, item.title ?? undefined)
    if (!foundList) return
    foundList.allChecked = foundList.allChecked && newDone
    props.updateLists(newLists)
    updateItemDone([item], newDone)
  }

  const onDragStart = (start: DragStart) => {
    const id = start.source.droppableId.split("_")[0]
    const sourceList = lists.find(r => {
      if (r.recipeId) return r.recipeId === id
      return r.title === id
    })
    if (!sourceList) {
      setDraggedItem(undefined)
    } else {
      setDraggedItem(sourceList.items.find(i => i.order === start.source.index))
    }
  }

  const onDragEnd = (result: DropResult) => {
    setDraggedItem(undefined)
    if (!result.destination) return

    const sourceDroppableId = result.source.droppableId.split("_")
    const destinationDroppableId = result.destination.droppableId.split("_")
    const sourceListId = sourceDroppableId[0]
    const destinationListId = destinationDroppableId[0]

    const sourceIndex = result.source.index
    const destinationIndex = result.destination.index

    const sourceDate = sourceDroppableId[1] === "undefined" ? undefined : moment(sourceDroppableId[1])
    const destinationDate = destinationDroppableId[1] === "undefined" ? undefined : moment(destinationDroppableId[1])

    if (sourceIndex === destinationIndex && sourceListId === destinationListId && compareDates(sourceDate, destinationDate)) return

    const sourceList = findList(lists, sourceListId, sourceDate, sourceListId)
    if (!sourceList || sourceList.items.length < sourceIndex) return
    if (sourceListId === destinationListId && compareDates(sourceDate, destinationDate)) {
      const sourceItem = sourceList.items.splice(sourceIndex, 1)[0]
      sourceList.items.splice(destinationIndex, 0, sourceItem)
      sourceList.items.forEach((i, index) => i.order = index)

      updateItemOrder(sourceList.items)
      return
    }

    const destinationList = findList(lists, destinationListId, destinationDate, destinationListId)
    if (!destinationList || destinationList.items.length < destinationIndex) return

    const sourceItem = sourceList.items.splice(sourceIndex, 1)[0]
    destinationList.items.splice(destinationIndex, 0, sourceItem)
    if (!sourceItem.recipeId) {
      sourceItem.title = destinationListId
    } else {
      sourceItem.recipeId = destinationListId
    }
    sourceItem.date = destinationDate

    sourceList.items.forEach((i, index) => i.order = index)
    destinationList.items.forEach((i, index) => i.order = index)

    if (sourceList.items.length < 1) {
      props.updateLists([...lists.filter(r => r.recipeId ? r.recipeId !== sourceListId : r.title !== sourceListId)])
    }

    updateItemOrder(sourceList.items.concat(destinationList.items))
  }

  return (
    <>
      <DragDropContext onDragStart={onDragStart} onDragEnd={onDragEnd}>
        <Grid container spacing={2} justifyContent='center' className={classes.container}>
          {
            lists.map((list, listIndex) => {
              return (
                <Grid item xs={12} sm={9} md={6} lg={4} xl={3}
                      key={(list.recipeId ?? list.title) + "_" + (list.date ? moment(list.date).format(defaultDateFormat) : undefined)}>
                  <ShoppingListCard list={list} listIndex={listIndex}
                                    draggedItem={draggedItem} editMode={editMode}
                                    updateItem={updateItem} markAsDone={markAsDone} markAllAsDone={markAllAsDone}
                  />
                </Grid>
              )
            })}
        </Grid>
      </DragDropContext>
    </>
  )
}