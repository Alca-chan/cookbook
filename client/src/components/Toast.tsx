import React from "react";
import {Toaster} from "react-hot-toast";
import {makeStyles} from "../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  container: {
    padding: theme.spacing(2)
  }
}))

export const Toast = () => {
  const {classes} = useStyles()

  return (
    <Toaster
      position="top-right"
      reverseOrder={false}
      containerStyle={{
        top: 80
      }}
      toastOptions={{
        className: classes.container
      }}
    />
  )
}