import React, {useMemo} from "react";
import {useMediaQuery, useTheme} from "@mui/material";
import WordCloud from "react-d3-cloud";
import {Label} from "../RecipesSearch/RecipesBy";

export const LabelCloud = <T extends Label>(props: { data: T[], getName: (label: T) => string, onSearch: (label: string) => void }) => {
  const theme = useTheme()
  const largeDisplay = useMediaQuery(theme.breakpoints.up('xl'))
  const smallDisplay = useMediaQuery(theme.breakpoints.down('md'))

  const data = useMemo(() => {
    return props.data.map(i => ({text: props.getName(i), value: i.count}))
  }, [props])

  const maxCount = useMemo(() => {
    let max = 0
    data.forEach(i => {
      if (i.value > max) max = i.value
    })
    return max
  }, [data])

  return (
    <WordCloud
      data={data}
      width={largeDisplay ? 400 : (smallDisplay ? 300 : 350)}
      height={largeDisplay ? 200 : 100}
      fontSize={(word) => word.value * 30 / maxCount}
      spiral={"archimedean"}
      rotate={0}
      padding={2}
      onWordClick={(event, d) => props.onSearch(d.text)}
    />
  )
}