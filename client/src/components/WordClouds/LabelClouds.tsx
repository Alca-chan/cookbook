import React, {useCallback, useContext} from "react";
import {Grid, Stack} from "@mui/material";
import {useTranslation} from "react-i18next";
import {StaticDataContext} from "../../contexts/StaticDataContext";
import {useHistory} from "react-router-dom";
import {getRoute} from "../../utils/routes";
import {makeStyles} from "../../utils/makeStyles";
import {LabelCloud} from "./LabelCloud";

const useStyles = makeStyles()(theme => ({
  cloud: {
    backgroundColor: theme.palette.appBackground.image,
    cursor: "pointer"
  }
}))

export const LabelClouds = () => {
  const {classes} = useStyles()
  const {i18n} = useTranslation()
  const lang = i18n.language
  const {ingredients, tags} = useContext(StaticDataContext)
  const history = useHistory()

  const searchByIngredient = useCallback((name: string) => {
    if (!ingredients) return
    const found = ingredients.find(i => i.nameBase === name)
    if (found) {
      history.push(getRoute(lang, "/search/ingredients") + "/?s=true&l=" + found.id)
    }
  }, [history, ingredients, lang])

  const searchByTag = useCallback((name: string) => {
    if (!tags) return
    const found = tags.find(i => i.name === name)
    if (found) {
      history.push(getRoute(lang, "/search/tags") + "/?s=true&l=" + found.id)
    }
  }, [history, tags, lang])

  return (
    <Stack direction={{xs: "column", xl: "row"}} spacing={2}>
      {
        ingredients && (
          <Grid item xs={12} xl={6} className={classes.cloud}>
            <LabelCloud data={ingredients} getName={(ingredient) => ingredient.nameBase} onSearch={searchByIngredient}/>
          </Grid>
        )
      }
      {
        tags && (
          <Grid item xs={12} xl={6} className={classes.cloud}>
            <LabelCloud data={tags} getName={(tag) => tag.name} onSearch={searchByTag}/>
          </Grid>
        )
      }
    </Stack>
  )
}