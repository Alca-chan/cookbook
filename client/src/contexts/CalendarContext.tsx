import {createContext} from "react";
import {CalendarInfo} from "./CalendarInfo";

export const CalendarContext = createContext<CalendarInfo>({
  calendar: [], addToCalendar: () => {}, editCalendar: () => {}, moveInCalendar: () => {}, updateOrder: () => {}, removeFromCalendar: () => {}
})