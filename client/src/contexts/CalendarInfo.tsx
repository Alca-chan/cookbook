import {useCallback, useEffect, useState} from "react";
import {Calendar, User} from "../api/types";
import {Moment} from "moment";
import {
  addRecipeToCalendar,
  editRecipeInCalendar,
  getUsersCalendar,
  moveDayInCalendar,
  removeRecipeFromCalendar,
  updateCalendarOrder
} from "../api/calendar";
import {uploadToServerToast} from "../utils/toasts";
import {useTranslation} from "react-i18next";
import {getCancelToken} from "../api/utils";
import {defaultDateFormat} from "../utils/utils";

const moment = require('moment')

export type CalendarInfo = {
  calendar: Calendar[] | undefined,
  addToCalendar: (recipeId: string, date: Moment, portions: number, order: number, title: string) => void,
  editCalendar: (id: string, recipeId: string, date: Moment, portions: number, order: number, title: string) => void,
  moveInCalendar: (ids: string[], date: Moment, changed?: { id: string, order: number }[]) => void,
  updateOrder: (changed: { id: string, order: number }[]) => void,
  removeFromCalendar: (ids: string[]) => void
}

export const useCalendarInfo = (user: User | null | undefined) => {
  const {t} = useTranslation()
  const [calendar, setCalendar] = useState<Calendar[]>()

  const loadCalendarInfo = useCallback(async (token) => {
    try {
      let c: Calendar[] | undefined
      if (user) {
        c = await getUsersCalendar(token)
      } else {
        c = undefined
      }
      setCalendar(c)
    } catch {
      /* canceled request */
    }
  }, [user])

  useEffect(() => {
    const source = getCancelToken()
    loadCalendarInfo(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadCalendarInfo])

  const updateCalendar = async () => {
    await loadCalendarInfo(getCancelToken().token)
  }

  const addToCalendar = async (recipeId: string, date: Moment, portions: number, order: number, title: string) => {
    await uploadToServerToast(t,
      addRecipeToCalendar(recipeId, moment(date).format(defaultDateFormat), portions, order, title)
        .then(() => updateCalendar())
    )
  }

  const editCalendar = async (id: string, recipeId: string, date: Moment, portions: number, order: number, title: string) => {
    await uploadToServerToast(t,
      editRecipeInCalendar(id, recipeId, moment(date).format(defaultDateFormat), portions, order, title)
        .then(() => updateCalendar())
    )
  }

  const moveInCalendar = async (ids: string[], date: Moment, changed: { id: string, order: number }[] | undefined) => {
    await uploadToServerToast(t,
      moveDayInCalendar(ids, moment(date).format(defaultDateFormat))
        .then(() => {
          if (changed) updateCalendarOrder(changed)
          updateCalendar()
        })
    )
  }

  const updateOrder = async (changed: { id: string, order: number }[]) => {
    await uploadToServerToast(t,
      updateCalendarOrder(changed)
        .then(() => updateCalendar())
    )
  }

  const removeFromCalendar = async (ids: string[]) => {
    await uploadToServerToast(t,
      removeRecipeFromCalendar(ids)
        .then(() => updateCalendar())
    )
  }

  const calendarInfo: CalendarInfo = {
    calendar: calendar,
    addToCalendar: async (recipe, date, portions, order, title) => addToCalendar(recipe, date, portions, order, title),
    editCalendar: async (id, recipe, date, portions, order, title) => editCalendar(id, recipe, date, portions, order, title),
    moveInCalendar: async (ids, date, changed) => moveInCalendar(ids, date, changed),
    updateOrder: async (changed) => updateOrder(changed),
    removeFromCalendar: async (ids) => removeFromCalendar(ids)
  }
  return calendarInfo
}