import {createContext} from "react";
import {CartInfo} from "./CartInfo";

export const CartContext = createContext<CartInfo>({
  cart: [], removeCartItems: () => {}, addItemsToCart: () => {}, removeAllForRecipeFromCart: () => {}, updateCartItems: () => {}, updateItemDone: () => {}, updateItemOrder: () => {}, editCartItems: () => {}
})