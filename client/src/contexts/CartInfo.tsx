import {useCallback, useEffect, useState} from "react";
import {CartItem, CartItemToAdd, User} from "../api/types";
import {
  addAllItemsToCart,
  getUsersCart,
  removeAllItemsForRecipeFromCart,
  removeItemsFromCart,
  updateCartDone, updateCartOrder,
  updateItemsInCart
} from "../api/cart";
import {cartItemToUpdateCartItem, getCancelToken} from "../api/utils";
import {Moment} from "moment";
import {compareDates, defaultDateFormat} from "../utils/utils";
import {uploadToServerToast} from "../utils/toasts";
import {useTranslation} from "react-i18next";

const moment = require('moment')

export type CartInfo = {
  cart: CartItem[] | undefined,
  addItemsToCart: (cartItems: CartItemToAdd[], date?: Moment) => void,
  removeCartItems: (cartItemIds: string[]) => void,
  removeAllForRecipeFromCart: (recipeId: string, date: Moment | null) => void,
  updateCartItems: (cartItems: CartItem[]) => void,
  updateItemDone: (cartItems: CartItem[], done: boolean) => void,
  updateItemOrder: (cartItems: CartItem[]) => void,
  editCartItems: (cartItemsToRemove: CartItem[], cartItemsToUpdate: CartItem[], cartItemsToAdd: CartItemToAdd[], date?: Moment) => void
}

export const useCartInfo = (user: User | null | undefined) => {
  const {t} = useTranslation()
  const [cart, setCart] = useState<CartItem[]>()

  const loadCartInfo = useCallback(async (token) => {
    try {
      let c: CartItem[] | undefined
      if (user) {
        c = await getUsersCart(token)
      } else {
        c = undefined
      }
      setCart(c)
    } catch {
      /* canceled request */
    }
  }, [user])

  useEffect(() => {
    const source = getCancelToken()
    loadCartInfo(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadCartInfo])

  const updateCart = async () => {
    await loadCartInfo(getCancelToken().token)
  }

  const removeItems = async (cartItemIds: string[]) => {
    if (!cart) return
    await uploadToServerToast(t,
      removeItemsFromCart(cartItemIds)
        .then(() => updateCart())
    )
  }

  const addAll = async (cartItems: CartItemToAdd[], date?: Moment) => {
    if (!cart) return
    await uploadToServerToast(t,
      addAllItemsToCart(cartItems.filter(i => i.recipeIngredientId === null || !cart.some(c => i.recipeIngredientId === c.recipeIngredientId && compareDates(c.date, date))), date ? moment(date).format(defaultDateFormat) : undefined)
        .then(() => updateCart())
    )
  }

  const removeAll = async (recipeId: string, date: Moment | null) => {
    if (!cart) return
    await uploadToServerToast(t,
      removeAllItemsForRecipeFromCart(recipeId, date ? moment(date).format(defaultDateFormat) : null)
        .then(() => updateCart())
    )
  }

  const updateItem = async (cartItems: CartItem[]) => {
    if (!cart) return
    await uploadToServerToast(t,
      updateItemsInCart(cartItems)
        .then(() => updateCart())
    )
  }

  const updateDone = async (cartItems: CartItem[], done: boolean) => {
    if (!cart) return
    await uploadToServerToast(t,
      updateCartDone(cartItems, done)
        .then(() => updateCart())
    )
  }

  const updateOrder = async (cartItems: CartItem[]) => {
    if (!cart) return
    await uploadToServerToast(t,
      updateCartOrder(cartItems.map(i => cartItemToUpdateCartItem(i)))
        .then(() => updateCart())
    )
  }

  const editItems = async (cartItemsToRemove: CartItem[], cartItemsToUpdate: CartItem[], cartItemsToAdd: CartItemToAdd[], date?: Moment) => {
    if (!cart) return
    await uploadToServerToast(t,
      Promise.all([
        updateItemsInCart(cartItemsToUpdate),
        removeItemsFromCart(cartItemsToRemove.map(i => i.id)),
        addAllItemsToCart(cartItemsToAdd, date ? moment(date).format(defaultDateFormat) : undefined)
      ])
        .then(() => updateCart())
    )
  }

  const cartInfo: CartInfo = {
    cart: cart,
    addItemsToCart: async (cartItems, date) => addAll(cartItems, date),
    removeCartItems: async (cartItemIds) => removeItems(cartItemIds),
    removeAllForRecipeFromCart: async (recipeId, date) => removeAll(recipeId, date),
    updateCartItems: async (cartItems) => updateItem(cartItems),
    updateItemDone: async (cartItems, done) => updateDone(cartItems, done),
    updateItemOrder: async (cartItems) => updateOrder(cartItems),
    editCartItems: async (cartItemsToRemove, cartItemsToUpdate, cartItemsToAdd, date) => editItems(cartItemsToRemove, cartItemsToUpdate, cartItemsToAdd, date)
  }
  return cartInfo
}