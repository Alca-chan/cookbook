import {createContext} from "react";
import {FavoritesInfo} from "./FavoritesInfo";

export const FavoritesContext = createContext<FavoritesInfo>({
  favorites: [], collections: [], addFavoriteRecipe: () => Promise.resolve(0), editFavoriteRecipe: () => {}, removeFavoriteRecipe: () => Promise.resolve(0)
})