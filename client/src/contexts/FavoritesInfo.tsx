import {useCallback, useEffect, useState} from "react";
import {FavoritesItem, User} from "../api/types";
import {
  addRecipeToFavorites,
  editRecipeInFavorites,
  getUsersFavorites,
  removeRecipeFromFavorites
} from "../api/favorites";
import {getCancelToken} from "../api/utils";

export type FavoritesInfo = {
  favorites: FavoritesItem[] | undefined,
  collections: string[] | undefined,
  addFavoriteRecipe: (recipeId: string, collection?: string) => Promise<number | undefined>
  editFavoriteRecipe: (recipeId: string, collection?: string) => void
  removeFavoriteRecipe: (recipeId: string) => Promise<number | undefined>
}

export const useFavoritesInfo = (user: User | null | undefined) => {
  const [favorites, setFavorites] = useState<FavoritesItem[]>()
  const [collections, setCollections] = useState<string[]>()

  const loadFavoritesInfo = useCallback(async (token) => {
    try {
      let items: FavoritesItem[] | undefined
      const cols: string[] = []
      if (user) {
        items = await getUsersFavorites(token)
        items.forEach(item => {
          if (item.collection && !cols.includes(item.collection)) {
            cols.push(item.collection)
          }
        })
        setCollections(cols)
      } else {
        items = undefined
      }
      setFavorites(items)
    } catch {
      /* canceled request */
    }
  }, [user])

  useEffect(() => {
    const source = getCancelToken()
    loadFavoritesInfo(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadFavoritesInfo])

  const updateFavorites = async () => {
    await loadFavoritesInfo(getCancelToken().token)
  }

  const addFavorite = async (recipeId: string, collection?: string): Promise<number | undefined> => {
    return addRecipeToFavorites(recipeId, collection).then((value) => {
      updateFavorites()
      return value
    })
  }

  const editFavorite = async (recipeId: string, collection?: string) => {
    await editRecipeInFavorites(recipeId, collection).then(() =>
      updateFavorites()
    )
  }

  const removeFavorite = async (recipeId: string): Promise<number | undefined> => {
    return removeRecipeFromFavorites(recipeId).then((value) => {
      updateFavorites()
      return value
    })
  }

  const favoritesInfo: FavoritesInfo = {
    favorites: favorites,
    collections: collections,
    addFavoriteRecipe: async (recipe, collection) => addFavorite(recipe, collection),
    editFavoriteRecipe: async (recipe, collection) => editFavorite(recipe, collection),
    removeFavoriteRecipe: async (recipe) => removeFavorite(recipe)
  }
  return favoritesInfo
}