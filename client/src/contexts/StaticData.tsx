import {useCallback, useEffect, useState} from "react";
import {Equipment, Ingredient, Tag, Unit} from "../api/types";
import {getCancelToken} from "../api/utils";
import {getIngredients} from "../api/ingredients";
import {getEquipment} from "../api/equipment";
import {getTags} from "../api/tags";
import {getUnits} from "../api/units";

export type StaticData = {
  ingredients: Ingredient[] | undefined,
  units: Unit[] | undefined,
  equipment: Equipment[] | undefined,
  tags: Tag[] | undefined
}

export const useStaticData = () => {
  const [ingredients, setIngredients] = useState<Ingredient[]>()
  const [units, setUnits] = useState<Unit[]>()
  const [equipment, setEquipment] = useState<Equipment[]>()
  const [tags, setTags] = useState<Tag[]>()

  const loadData = useCallback(async (token) => {
    try {
      const [i, u, e, t] = await Promise.all([getIngredients(token), getUnits(token), getEquipment(token), getTags(token)])
      setIngredients(i)
      setUnits(u)
      setEquipment(e)
      setTags(t)
    } catch {
      /* canceled request */
    }
  }, [])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  const staticData: StaticData = {
    ingredients: ingredients,
    units: units,
    equipment: equipment,
    tags: tags
  }
  return staticData
}