import {createContext} from "react";
import {StaticData} from "./StaticData";

export const StaticDataContext = createContext<StaticData>({
  ingredients: undefined,
  units: undefined,
  equipment: undefined,
  tags: undefined
})