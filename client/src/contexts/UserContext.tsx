import {createContext} from "react";
import {UserInfo} from "./UserInfo";

export const UserContext = createContext<UserInfo>({
  user: null,
  updateUser: () => {},
  logoutUser: () => {}
})