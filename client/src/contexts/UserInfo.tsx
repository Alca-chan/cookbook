import {useCallback, useEffect, useState} from "react";
import {User} from "../api/types";
import {getUser, logoutUser} from "../api/user";
import {getCancelToken} from "../api/utils";

export type UserInfo = {
  user: User | null | undefined,
  updateUser: () => void,
  logoutUser: () => void
}

export const useLoggedInUserInfo = () => {
  const [user, setUser] = useState<User | undefined | null>()

  const loadUserInfo = useCallback(async (token) => {
    try {
      const u: User = await getUser(token)
      setUser((!u || !u.username) ? null : u)
    } catch {
      /* canceled request */
    }
  }, [])

  useEffect(() => {
    const source = getCancelToken()
    loadUserInfo(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadUserInfo])

  const updateUser = async () => {
    await loadUserInfo(getCancelToken().token)
  }

  const userLogout = async () => {
    const u: User = await logoutUser()
    setUser((!u || !u.username) ? null : u)
  }

  // user (and related user info) is updated when window is back in focus
  useEffect(() => {
    window.addEventListener('focus', onFocus)
    return () => {
      window.removeEventListener('focus', onFocus)
    }
  })

  const onFocus = () => {
    updateUser().then()
  }

  const userInfo: UserInfo = {
    user: user,
    updateUser: updateUser,
    logoutUser: userLogout
  }
  return userInfo
}