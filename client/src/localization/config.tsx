import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import common_en from "./en/common.json"
import common_cs from "./cs/common.json"
import cart_en from "./en/cart.json"
import cart_cs from "./cs/cart.json"
import favorites_cs from "./cs/favorites.json"
import favorites_en from "./en/favorites.json"
import login_cs from "./cs/login.json"
import login_en from "./en/login.json"
import recipeForm_cs from "./cs/recipeForm.json"
import recipeForm_en from "./en/recipeForm.json"
import recipePage_cs from "./cs/recipePage.json"
import recipePage_en from "./en/recipePage.json"
import recipeSearch_cs from "./cs/recipeSearch.json"
import recipeSearch_en from "./en/recipeSearch.json"
import userCalendar_cs from "./cs/userCalendar.json"
import userCalendar_en from "./en/userCalendar.json"
import userRecipes_cs from "./cs/userRecipes.json"
import userRecipes_en from "./en/userRecipes.json"
import toast_cs from "./cs/toast.json"
import toast_en from "./en/toast.json"
import settings_cs from "./cs/settings.json"
import settings_en from "./en/settings.json"

// the translations
const resources = {
  en: {
    common: common_en,
    cart: cart_en,
    favorites: favorites_en,
    login: login_en,
    recipeForm: recipeForm_en,
    recipePage: recipePage_en,
    recipeSearch: recipeSearch_en,
    userCalendar: userCalendar_en,
    userRecipes: userRecipes_en,
    toast: toast_en,
    settings: settings_en
  },
  cs: {
    common: common_cs,
    cart: cart_cs,
    favorites: favorites_cs,
    login: login_cs,
    recipeForm: recipeForm_cs,
    recipePage: recipePage_cs,
    recipeSearch: recipeSearch_cs,
    userCalendar: userCalendar_cs,
    userRecipes: userRecipes_cs,
    toast: toast_cs,
    settings: settings_cs
  }
}

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "cs", // default language
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  }).then()

export default i18n