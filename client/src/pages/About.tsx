import React from 'react';
import {Card, CardContent, CardHeader, Container, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {getPageTitle} from "../utils/routes";
import {makeStyles} from "../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  card: {
    marginTop: theme.spacing(3)
  },
  header: {
    backgroundColor: theme.palette.primary.main
  },
  content: {
    backgroundColor: theme.palette.primary.light
  },
  divider: {
    marginTop: theme.spacing(2)
  },
  text: {
    fontSize: 18
  },
  link: {
    display: 'inline',
    textDecoration: 'none',
    color: theme.palette.text.primary
  }
}))

export const About = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:about"))

  return (
    <Container maxWidth='sm'>
      <Card>
        <CardHeader className={classes.header} title="Temporary name: Cookbook"/>
        <CardContent className={classes.content}>
          <Typography className={classes.text}>
            This application is for exploring new and managing your recipes.
          </Typography>
          <Typography className={classes.text}>
            You can add recipes to favourites and create shopping lists.
          </Typography>
        </CardContent>
      </Card>
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <Typography className={classes.text}>
            Created by Alena Zahradníčková in React with TypeScript using Material UI.
          </Typography>
        </CardContent>
      </Card>
    </Container>
  )
}