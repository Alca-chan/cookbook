import React, {useCallback, useContext, useEffect, useMemo, useState} from "react";
import {Button, Stack} from "@mui/material";
import {CartList, RecipeShort} from "../api/types";
import {NoResultsText} from "../components/RecipesGrid/NoResultsText";
import {Add, ArrowBack, Edit, FileCopy} from "@mui/icons-material";
import {Loading} from "../components/Loading";
import {CartContext} from "../contexts/CartContext";
import {useTranslation} from "react-i18next";
import {getPageTitle} from "../utils/routes";
import {copyShoppingListToast} from "../utils/toasts";
import {getCancelToken, getUniqueRecipeShortPromises} from "../api/utils";
import {compareDates, recipeString} from "../utils/utils";
import {makeStyles} from "../utils/makeStyles";
import {ShoppingListsGrid} from "../components/ShoppingList/ShoppingListsGrid";
import {NewShoppingListDialog} from "../components/Dialogs/NewShoppingListDialog";

const useStyles = makeStyles()(theme => ({
  buttonContainer: {
    margin: theme.spacing(0, 1, 3, 1)
  }
}))

export const Cart = () => {
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:shoppingList"))
  const {classes} = useStyles()
  const {cart} = useContext(CartContext)
  const [lists, setLists] = useState<CartList[] | null>(null)
  const [editMode, setEditMode] = useState<boolean>(false)
  const [openNewListDialog, setOpenNewListDialog] = useState<boolean>(false)

  const loadData = useCallback(async (token) => {
    try {
      if (cart) {
        const recipes: CartList[] = []
        const uniquePromises = getUniqueRecipeShortPromises(token, cart.filter(i => i.recipeId !== null))

        const result: (RecipeShort | undefined)[] = await Promise.all(uniquePromises.map(u => u.promise))
        for (let item of cart) {
          const date = item.date
          const found = !item.recipeId
            ? recipes.filter(r => r.title === item.title)
            : recipes.filter(r => r.recipeId === item.recipeId).filter(r => compareDates(date, r.items[0].date))
          if (found.length > 0) {
            found[0].allChecked = found[0].allChecked && item.done
            found[0].items.push(item)
          } else {
            if (!item.recipeId) {
              recipes.push({
                recipeId: null,
                title: item.title ?? "",
                allChecked: item.done,
                items: [item],
                date: item.date
              })
            } else {
              const recipe = result.find(r => r && r.id === item.recipeId)
              if (recipe) recipes.push({
                recipeId: recipe.id,
                title: recipe.title,
                allChecked: item.done,
                items: [item],
                date: date
              })
            }
          }
        }

        recipes
          .sort((a, b) => {
            if (!a.recipeId && !b.recipeId) return a.title.localeCompare(b.title)
            if (!a.recipeId) return 1
            if (!b.recipeId) return -1
            if (a.recipeId === b.recipeId) {
              if (!a.date) return 1
              if (!b.date) return -1
              return a.date.valueOf() - b.date.valueOf()
            }
            return a.recipeId.localeCompare(b.recipeId)
          })
          .map(r => r.items.sort((a, b) => a.order - b.order))
        setLists(recipes)
      }
    } catch {
      /* canceled request */
    }
  }, [cart])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  const shoppingListString = useMemo(() => {
    if (!lists) return ''
    return lists.map(recipe => recipeString(recipe)).join('\n')
  }, [lists])

  const onAddList = () => {
    setOpenNewListDialog(true)
  }

  if (!cart || !lists) {
    return <Loading/>
  }

  if (cart.length < 1) {
    return <NoResultsText>{t("cart:nothingInCart")}</NoResultsText>
  }

  const copyToClipboard = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation()
    e.preventDefault()
    await copyShoppingListToast(t, navigator.clipboard.writeText(shoppingListString))
  }

  return (
    <>
      {
        openNewListDialog && (
          <NewShoppingListDialog open={openNewListDialog} setOpen={setOpenNewListDialog}/>
        )
      }
      <Stack direction={{xs: "column", md: "row"}} spacing={1} justifyContent={"space-between"} alignItems={"flex-end"}
             className={classes.buttonContainer}>
        <Button onClick={(e) => copyToClipboard(e)}
                variant={"contained"} color={"grey"} startIcon={<FileCopy/>}>
          {t("cart:copy")}
        </Button>
        <Stack direction={{xs: "column", sm: "row"}} spacing={1} alignItems={"flex-end"}>
          <Button onClick={() => onAddList()}
                  variant={"contained"} color={"grey"} startIcon={<Add/>}>
            {t("cart:createShoppingList")}
          </Button>
          <Button onClick={() => setEditMode(!editMode)}
                  variant={"contained"} color={"grey"} startIcon={editMode ? <ArrowBack/> : <Edit/>}>
            {
              editMode ? t("cart:exitEditMode") : t("cart:turnOnEditMode")
            }
          </Button>
        </Stack>
      </Stack>
      <ShoppingListsGrid lists={lists} editMode={editMode} updateLists={setLists} changeEditMode={setEditMode}/>
    </>
  )
}