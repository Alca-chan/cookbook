import React, {useCallback, useContext, useEffect, useState} from 'react';
import {Recipe} from "../api/types";
import {NoResultsText} from "../components/RecipesGrid/NoResultsText";
import {RecipeForm} from "../components/RecipeForm/RecipeForm";
import {Loading} from "../components/Loading";
import {useTranslation} from "react-i18next";
import {getRecipeById} from "../api/recipes/recipeSearch";
import {getPageTitle} from "../utils/routes";
import {getCancelToken} from "../api/utils";
import {StaticDataContext} from "../contexts/StaticDataContext";

export const EditRecipe = (props: { match: any }) => {
  const {t} = useTranslation()
  const {ingredients, units, equipment, tags} = useContext(StaticDataContext)
  const [data, setData] = useState<Recipe | undefined>()
  const [error, setError] = useState(false)
  document.title = getPageTitle(t("common:edit") + (data ? (" " + data.title) : ""))

  const id: string = props.match.params.id

  const loadRecipe = useCallback(async (token) => {
    if (!ingredients || !units || !equipment || !tags) return
    try {
      const recipe = await getRecipeById(token, id, ingredients, units, equipment, tags)
      if (!recipe) {
        setData(undefined)
        setError(true)
      } else {
        setData(recipe)
      }
    } catch {
      /* canceled request */
    }
  }, [id, ingredients, units, equipment, tags])

  useEffect(() => {
    const source = getCancelToken()
    loadRecipe(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadRecipe])

  if (error) {
    return (
      <NoResultsText>{t("common:noRecipe", {id: id})}</NoResultsText>
    )
  }

  if (!data) {
    return <Loading/>
  }

  return (
    <RecipeForm recipe={data}/>
  )
}