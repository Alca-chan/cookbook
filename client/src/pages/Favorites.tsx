import React, {useCallback, useContext, useEffect, useMemo, useState} from "react";
import {RecipesGrid} from "../components/RecipesGrid/RecipesGrid";
import {NoResultsText} from "../components/RecipesGrid/NoResultsText";
import {Loading} from "../components/Loading";
import {FavoritesContext} from "../contexts/FavoritesContext";
import {RecipeShort} from "../api/types";
import {useTranslation} from "react-i18next";
import {getRecipeShortById} from "../api/recipes/recipeSearch";
import {getPageTitle} from "../utils/routes";
import {getCancelToken} from "../api/utils";
import {alpha, Button, Paper, Stack, Tab, Tabs, Typography} from "@mui/material";
import {makeStyles} from "../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  buttons: {
    margin: theme.spacing(0, 1, 4, 1)
  },
  header: {
    margin: theme.spacing(0, 1, 2, 1)
  },
  indicator: {
    backgroundColor: theme.palette.text.primary
  },
  tabSelected: {
    color: theme.palette.text.primary,
    background: theme.palette.primary.light,
    borderTopLeftRadius: "8px",
    borderTopRightRadius: "8px"
  },
  tab: {
    color: alpha(theme.palette.text.primary, 0.95)
  },
  paper: {
    paddingBottom: theme.spacing(2),
    background: theme.palette.appBackground.board
  }
}))

type View = "all" | "collections"

export const Favorites = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:favorites"))
  const {favorites, collections} = useContext(FavoritesContext)
  const [data, setData] = useState<RecipeShort[] | null>(null)
  const [view, setView] = useState<View>("collections")
  const [collection, setCollection] = useState<string>("noCollection")

  const noCollection: boolean = useMemo(() => {
    if (!favorites || !collections) return true
    return favorites.some(f => !f.collection || f.collection === "noCollection")
  }, [favorites, collections])

  const loadData = useCallback(async (token) => {
    try {
      if (favorites) {
        const promises: Promise<RecipeShort | undefined>[] = favorites.map(f => getRecipeShortById(token, f.recipeId))
        const result: (RecipeShort | undefined)[] = await Promise.all(promises)
        setData(result.filter(r => r !== undefined) as RecipeShort[])
      }
    } catch {
      /* canceled request */
    }
  }, [favorites])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  const recipes = useMemo(() => {
    if (!data || !favorites) return []
    if (view === "all") return data
    return data.filter(recipe => {
      const found = favorites.find(item => item.recipeId === recipe.id)
      return found && (found.collection === collection || (collection === "noCollection" && !found.collection))
    })
  }, [data, favorites, collection, view])

  const value: string = useMemo(() => {
    if (!collections) return "noCollection"
    if (noCollection && collection === "noCollection") return "noCollection"
    if (!collections.some(c => c === collection)) {
      if (collections.length > 0) {
        setCollection(collections[0])
        return collections[0]
      } else {
        setCollection("noCollection")
        return "noCollection"
      }
    }
    return collection
  }, [collections, collection, noCollection])

  if (!favorites || !collections || !data) {
    return <Loading/>
  }

  if (favorites.length < 1) {
    return <NoResultsText>{t("favorites:nothingInFavorites")}</NoResultsText>
  }

  return (
    <>
      <Stack direction={{xs: "column", sm: "row"}} spacing={{xs: 1, sm: 2}}
             justifyContent={{xs: "center", sm: "flex-end"}} alignItems={{xs: "flex-start", sm: "center"}}
             className={classes.buttons}>
        <Typography>
          {t("favorites:view")}:
        </Typography>
        <Stack direction={"row"} spacing={{xs: 1, sm: 2}} justifyContent={"flex-end"} alignItems={"center"}>
          <Button
            variant={"contained"} color={view === "all" ? "primary" : "grey"}
            onClick={() => setView("all")}
          >
            {t("favorites:allRecipesView")}
          </Button>
          <Button
            variant={"contained"} color={view === "collections" ? "primary" : "grey"}
            onClick={() => setView("collections")}
          >
            {t("favorites:collectionsView")}
          </Button>
        </Stack>
      </Stack>
      {
        view === "collections" && (
          <Tabs className={classes.header} classes={{indicator: classes.indicator}}
                textColor={"inherit"}
                variant={"scrollable"}
                scrollButtons={"auto"}
                value={value}
                onChange={(e, selected) => setCollection(selected)}
          >
            {
              noCollection && (
                <Tab value={"noCollection"} label={t("favorites:noCollection")} {...a11yProps(0)}
                     className={collection === "noCollection" ? classes.tabSelected : classes.tab}/>
              )
            }
            {
              collections.map((c, index) =>
                <Tab key={c} value={c} label={c} {...a11yProps(index + 1)}
                     className={c === collection ? classes.tabSelected : classes.tab}/>
              )
            }
          </Tabs>
        )
      }
      <Paper className={classes.paper}>
        <RecipesGrid data={recipes}/>
      </Paper>
    </>
  )
}

const a11yProps = (index: number) => {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`,
  }
}