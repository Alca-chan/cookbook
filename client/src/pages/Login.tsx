import React, {useContext, useState} from 'react';
import {LoginRegistrationForm} from "../components/LoginRegister/LoginRegistrationForm";
import {Redirect} from "react-router-dom";
import {alpha, Card, Container, Tab, Tabs} from "@mui/material";
import {UserContext} from "../contexts/UserContext";
import {useTranslation} from "react-i18next";
import {getPageTitle} from "../utils/routes";
import {makeStyles} from "../utils/makeStyles";

const useStyles = makeStyles()(theme => ({
  card: {
    marginTop: theme.spacing(2)
  },
  header: {
    background: theme.palette.primary.main
  },
  indicator: {
    backgroundColor: theme.palette.text.primary
  },
  tabSelected: {
    color: theme.palette.text.primary
  },
  tab: {
    color: alpha(theme.palette.text.primary, 0.95)
  }
}))

export type Action = 'Login' | 'Register'

export const Login = () => {
  const [tabIndex, setTabIndex] = useState(1)
  const {classes} = useStyles()
  const {user} = useContext(UserContext)
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:login"))

  if (user) {
    return <Redirect to='/'/>
  }

  const handleTabChange = (event: React.ChangeEvent<{}>, newTabIndex: number) => {
    setTabIndex(newTabIndex)
  }

  return (
    <Container maxWidth='sm'>
      <Card className={classes.card}>
        <Tabs className={classes.header} classes={{indicator: classes.indicator}} value={tabIndex}
              onChange={handleTabChange} centered textColor={"inherit"}>
          <Tab value={1} label={t("login:loginTab")} {...a11yProps(1)}
               className={tabIndex === 1 ? classes.tabSelected : classes.tab}/>
          <Tab value={2} label={t("login:registerTab")} {...a11yProps(2)}
               className={tabIndex === 2 ? classes.tabSelected : classes.tab}/>
        </Tabs>
        <LoginRegistrationForm actionName={tabIndex === 1 ? "Login" : "Register"}/>
      </Card>
    </Container>
  )
}

const a11yProps = (index: number) => {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`,
  }
}