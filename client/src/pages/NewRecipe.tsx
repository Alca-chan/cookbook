import React from "react";
import {RecipeForm} from "../components/RecipeForm/RecipeForm";
import {useTranslation} from "react-i18next";
import {getPageTitle} from "../utils/routes";

export const NewRecipe = () => {
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:newRecipe"))

  return (
    <RecipeForm/>
  )
}