import React from 'react';
import {getPageTitle} from "../utils/routes";
import {useTranslation} from "react-i18next";

export const NotFound = () => {
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:notFound"))

  return (
    <div/>
  )
}