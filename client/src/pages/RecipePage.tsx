import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import {Recipe} from "../api/types";
import {Card, CardActions, CardContent, Grid, Stack, Typography} from "@mui/material";
import {IngredientsTable} from "../components/Recipe/IngredientsTable";
import {ImageView} from "../components/RecipesSearch/ImageView";
import {Instructions} from "../components/Recipe/Instructions";
import {NoResultsText} from "../components/RecipesGrid/NoResultsText";
import {Title} from "../components/Recipe/Title";
import {useHistory, useLocation} from "react-router-dom";
import {TimerOutlined} from "@mui/icons-material";
import {Loading} from "../components/Loading";
import {MAX_IMAGE_SIZE} from "../utils/imageUtils";
import {Labels} from "../components/Recipe/Labels";
import {useTranslation} from "react-i18next";
import {getRecipeById} from "../api/recipes/recipeSearch";
import {getPageTitle, getRoute} from "../utils/routes";
import {Breadcrumbs} from "../components/Breadcrumbs";
import {TimeLabel} from "../components/Recipe/TimeLabel";
import {getCancelToken, recipeToRecipeShort} from "../api/utils";
import {StaticDataContext} from "../contexts/StaticDataContext";
import {displayDateFormat} from "../utils/utils";
import {makeStyles} from "../utils/makeStyles";
import {RecipeActionButtons} from "../components/Recipe/RecipeActionButtons";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  breadcrumbs: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      paddingRight: theme.spacing(4),
      paddingLeft: theme.spacing(4)
    }
  },
  container: {
    marginTop: theme.spacing(2)
  },
  title: {
    width: '100%'
  },
  actions: {
    width: "100%",
    padding: theme.spacing(2),
    paddingBottom: theme.spacing(1)
  },
  generalInfo: {
    marginBottom: theme.spacing(3)
  },
  createdText: {
    marginTop: theme.spacing(0.5)
  },
  time: {
    display: "flex",
    flexDirection: "row",
    marginTop: theme.spacing(2),
    justifyContent: "flex-start",
    alignItems: "center"
  },
  timerIcon: {
    marginRight: theme.spacing(2)
  },
  imageGrid: {
    float: 'left',
    paddingRight: theme.spacing(4),
    paddingBottom: theme.spacing(2)
  },
  imageBackground: {
    float: 'left',
    padding: theme.spacing(1.5),
    paddingBottom: theme.spacing(1),
    width: '100%',
    height: '100%',
    background: theme.palette.appBackground.image,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

  },
  image: {
    maxWidth: MAX_IMAGE_SIZE,
    maxHeight: MAX_IMAGE_SIZE
  }
}))

const MAX_URL_LENGTH = 60

export const RecipePage = (props: { match: any }) => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const lang = i18n.language
  const {ingredients, units, equipment, tags} = useContext(StaticDataContext)
  const [recipe, setRecipe] = useState<Recipe | null>()
  const [error, setError] = useState(false)
  const location = useLocation()
  const history = useHistory()
  document.title = getPageTitle(recipe ? recipe.title : t("common:recipe"))

  const id: string = useMemo(() => {
    return props.match.params.id.match(/(\d+)/)[0]
  }, [props.match.params.id])

  const loadRecipe = useCallback(async (token) => {
    if (!ingredients || !units || !equipment || !tags) return
    try {
      const recipe = await getRecipeById(token, id, ingredients, units, equipment, tags)
      if (!recipe) {
        setRecipe(undefined)
        setError(true)
      } else {
        setRecipe(recipe)
        const title = recipe.title
          .normalize("NFD").replace(/[\u0300-\u036f]/g, "") // replace diacritics
          .replace(/[^a-zA-Z0-9\s]/g, '') // allow only alphabet characters and numbers
          .replace(/\s+/g, '-') // replace spaces with -
          .replace(new RegExp("/^(.{" + (MAX_URL_LENGTH - 10) + "}[^-]*).*/"), "$1") // shorten the title without cutting words
          .substring(0, MAX_URL_LENGTH) // max 50 characters (in case the last word is too long, cut it)
        history.replace(getRoute(lang, '/recipe/:id').replace(":id", id + '-' + title), location.state)
      }
    } catch {
      /* canceled request */
    }
  }, [id, history, lang, ingredients, units, equipment, tags, location.state])

  useEffect(() => {
    const source = getCancelToken()
    loadRecipe(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadRecipe])

  if (error) {
    return (
      <NoResultsText>{t("common:noRecipe", {id: id})}</NoResultsText>
    )
  }

  if (!recipe) {
    return <Loading/>
  }

  return (
    <>
      <div className={classes.breadcrumbs}>
        <Breadcrumbs current={recipe ? recipe.title : t("common:recipe")}
                     previous={location.state ? [location.state as { path: string, name: string }] : []}/>
      </div>
      <Stack direction={"column"} spacing={4} className={classes.container}>
        <div className={classes.title}>
          <Title title={recipe.title}/>
        </div>
        <Stack spacing={4} direction={{xs: "column", xl: "row"}}>
          <Grid item xs={12} xl={6}>
            <div className={classes.imageBackground}>
              <div className={classes.image}>
                <ImageView img={recipe.image} width={'100%'} height={'100%'}/>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} xl={6}>
            <Card className={classes.generalInfo}>
              <CardActions className={classes.actions}>
                <RecipeActionButtons recipe={recipeToRecipeShort(recipe)}/>
              </CardActions>
              <CardContent>
                <Typography align={"left"} className={classes.createdText}>
                  {t("recipePage:createdOn", {date: moment(recipe.date).format(displayDateFormat)}) + (recipe.user ? (" " + t("recipePage:createdBy", {user: recipe.user.username})) : "")}
                </Typography>
                <div className={classes.time}>
                  <TimerOutlined className={classes.timerIcon}/>
                  <Stack direction={{xs: "column", sm: "row"}} spacing={{xs: 1, sm: 3}}>
                    <Stack direction={"row"} spacing={1} alignItems={"center"}>
                      <Typography align={"left"}>
                        {t("recipePage:timePrep")}:
                      </Typography>
                      <TimeLabel minutes={recipe.timePrep}/>
                    </Stack>
                    <Stack direction={"row"} spacing={1} alignItems={"center"}>
                      <Typography align={"left"}>
                        {t("recipePage:timeTotal")}:
                      </Typography>
                      <TimeLabel minutes={recipe.time}/>
                    </Stack>
                  </Stack>
                </div>
                {
                  recipe.requiredEquipment.length > 0 && (
                    <Labels data={recipe.requiredEquipment} title={t("recipePage:equipment")}
                            getUrl={(id) => getRoute(lang, "/search/equipment") + "/?s=true&l=" + id}/>
                  )
                }
                {
                  recipe.tags.length > 0 && (
                    <Labels data={recipe.tags} title={t("recipePage:tags")}
                            getUrl={(id) => getRoute(lang, "/search/tags") + "/?s=true&l=" + id}/>
                  )
                }
              </CardContent>
            </Card>
            <Typography align={"justify"}>
              {recipe.description}
            </Typography>
          </Grid>
        </Stack>
        <Stack spacing={{xs: 2, lg: 4}} direction={{xs: "column", md: "row"}}>
          <Grid item xs={12} md={6}>
            <IngredientsTable recipe={recipe} portions={recipe.portions} portionsNote={recipe.portionsNote}/>
          </Grid>
          <Grid item xs={12} md={6}>
            <Instructions data={recipe.method}/>
          </Grid>
        </Stack>
      </Stack>
    </>
  )
}