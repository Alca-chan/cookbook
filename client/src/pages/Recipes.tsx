import {Button, Stack, Typography, useMediaQuery, useTheme} from "@mui/material";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import {RecipesContainingString} from "../components/RecipesSearch/RecipesByName/RecipesContainingString";
import {RandomRecipes} from "../components/RecipesSearch/RandomRecipes";
import {getRoute} from "../utils/routes";
import {APP_NAME} from "../App";
import {makeStyles} from "../utils/makeStyles";
import {RecipeSearchBar} from "../components/RecipesSearch/RecipeSearchBar";
import {LabelClouds} from "../components/WordClouds/LabelClouds";

const useStyles = makeStyles()(theme => ({
  container: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      paddingRight: theme.spacing(4),
      paddingLeft: theme.spacing(4)
    }
  },
  searchByText: {
    width: "100%",
    color: theme.palette.text.secondary
  },
  orText: {
    color: theme.palette.text.secondary
  },
  clouds: {
    marginTop: theme.spacing(5)
  }
}))

export const Recipes = () => {
  const {classes} = useStyles()
  const {t, i18n} = useTranslation()
  const lang = i18n.language
  document.title = APP_NAME
  const history = useHistory()
  const [searchValue, setSearchValue] = useState("")
  const [displayResults, setDisplayResults] = useState<boolean>(false)
  const theme = useTheme()
  const smallDisplay = useMediaQuery(theme.breakpoints.down('sm'))

  const onSearch = (searchValue: string) => {
    if (!displayResults) setDisplayResults(true)
    setSearchValue(searchValue)
  }

  return (
    <div className={classes.container}>
      <Stack direction={{xs: "column", xl: "row"}} alignItems={"center"} justifyContent={"center"} spacing={2}>
        <Stack direction={{xs: "column", sm: "row"}} alignItems={"center"} justifyContent={"center"} spacing={2}>
          <Typography className={classes.searchByText} variant={'subtitle1'}>{t("recipeSearch:recipesBy")}:</Typography>
          <Stack direction={"row"} alignItems={"center"} justifyContent={"center"} spacing={1}>
            <Button variant={"contained"} color={"primary"}
                    size={smallDisplay ? "medium" : "large"}
                    onClick={() => history.push(getRoute(lang, "/search/tags"))}>
              {t("recipeSearch:byTag")}
            </Button>
            <Button variant={"contained"} color={"primary"}
                    size={smallDisplay ? "medium" : "large"}
                    onClick={() => history.push(getRoute(lang, "/search/ingredients"))}>
              {t("recipeSearch:byIngredients")}
            </Button>
            <Button variant={"contained"} color={"primary"} size={smallDisplay ? "medium" : "large"}
                    onClick={() => history.push(getRoute(lang, "/search/equipment"))}>
              {t("recipeSearch:byEquipment")}
            </Button>
          </Stack>
          <Typography className={classes.orText} variant={'subtitle1'}>{t("recipeSearch:or")}</Typography>
        </Stack>
        <div>
          <RecipeSearchBar onSearch={onSearch} onClear={() => setDisplayResults(false)}/>
        </div>
      </Stack>
      <div>
        {
          displayResults
            ? <RecipesContainingString string={searchValue}/>
            : <RandomRecipes/>
        }
      </div>

      {
        (!smallDisplay && !displayResults) && (
          <div className={classes.clouds}>
            <LabelClouds/>
          </div>
        )
      }
    </div>
  )
}