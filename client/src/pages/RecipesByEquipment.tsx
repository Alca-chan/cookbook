import React, {useContext} from "react";
import {Equipment} from "../api/types";
import {RecipesBy} from "../components/RecipesSearch/RecipesBy";
import {useTranslation} from "react-i18next";
import {getRecipesByEquipmentAndTitle} from "../api/recipes/recipeSearch";
import {getPageTitle} from "../utils/routes";
import {Loading} from "../components/Loading";
import {StaticDataContext} from "../contexts/StaticDataContext";

export const RecipesByEquipment = () => {
  const {t} = useTranslation()
  const {equipment} = useContext(StaticDataContext)
  document.title = getPageTitle(t("common:searchByEquipment"))

  const compareSearchEquipment = (s: string, equipment: Equipment): boolean => {
    return equipment.name.toLowerCase().includes(s)
  }

  const getEquipmentName = (equipment: Equipment): string => {
    return equipment.name
  }

  if (!equipment) {
    return <Loading/>
  }

  return (
    <RecipesBy searchComparison={compareSearchEquipment} getName={getEquipmentName}
               by={"equipment"} data={equipment}
               getRecipesBy={getRecipesByEquipmentAndTitle}
               noResultsText={t("recipeSearch:noEquipment")} backToText={t("recipeSearch:backToEquipment")}
               searchPlaceHolderText={t("recipeSearch:searchEquipment")}
               noRecipesText={t("recipeSearch:noRecipesWithEquipment")}
    />
  )
}