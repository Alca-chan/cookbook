import React, {useContext} from "react";
import {Ingredient} from "../api/types";
import {RecipesBy} from "../components/RecipesSearch/RecipesBy";
import {useTranslation} from "react-i18next";
import {getRecipesByIngredientsAndTitle} from "../api/recipes/recipeSearch";
import {getPageTitle} from "../utils/routes";
import {Loading} from "../components/Loading";
import {StaticDataContext} from "../contexts/StaticDataContext";

export const RecipesByIngredients = () => {
  const {t} = useTranslation()
  const {ingredients} = useContext(StaticDataContext)
  document.title = getPageTitle(t("common:searchByIngredients"))

  const compareSearchIngredients = (s: string, ingredient: Ingredient): boolean => {
    return ingredient.nameBase.toLowerCase().includes(s)
      || ingredient.nameOne.toLowerCase().includes(s)
      || ingredient.nameFew.toLowerCase().includes(s)
      || ingredient.nameMany.toLowerCase().includes(s)
      || ingredient.nameUnit.toLowerCase().includes(s)
      || ingredient.namePart.toLowerCase().includes(s)
  }

  const getIngredientName = (ingredient: Ingredient): string => {
    return ingredient.nameBase
  }

  if (!ingredients) {
    return <Loading/>
  }

  return (
    <RecipesBy searchComparison={compareSearchIngredients} getName={getIngredientName}
               by={"ingredients"} data={ingredients}
               getRecipesBy={getRecipesByIngredientsAndTitle}
               noResultsText={t("recipeSearch:noIngredients")} backToText={t("recipeSearch:backToIngredients")}
               searchPlaceHolderText={t("recipeSearch:searchIngredient")}
               noRecipesText={t("recipeSearch:noRecipesWithIngredients")}
    />
  )
}