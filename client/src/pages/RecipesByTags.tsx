import React, {useContext} from "react";
import {Tag} from "../api/types";
import {RecipesBy} from "../components/RecipesSearch/RecipesBy";
import {useTranslation} from "react-i18next";
import {getRecipesByTagsAndTitle} from "../api/recipes/recipeSearch";
import {getPageTitle} from "../utils/routes";
import {Loading} from "../components/Loading";
import {StaticDataContext} from "../contexts/StaticDataContext";

export const RecipesByTags = () => {
  const {t} = useTranslation()
  const {tags} = useContext(StaticDataContext)
  document.title = getPageTitle(t("common:searchByTags"))

  const compareSearchIngredients = (s: string, tag: Tag): boolean => {
    return tag.name.toLowerCase().includes(s)
  }

  const getTagName = (tag: Tag): string => {
    return tag.name
  }

  if (!tags) {
    return <Loading/>
  }

  return (
    <RecipesBy searchComparison={compareSearchIngredients} getName={getTagName}
               by={"tags"} data={tags}
               getRecipesBy={getRecipesByTagsAndTitle}
               noResultsText={t("recipeSearch:noTags")} backToText={t("recipeSearch:backToTags")}
               searchPlaceHolderText={t("recipeSearch:searchTag")} noRecipesText={t("recipeSearch:noRecipesWithTag")}
    />
  )
}