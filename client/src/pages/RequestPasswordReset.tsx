import {ResetPasswordRequestForm} from "../components/ResetPassword/ResetPasswordRequestForm";
import {Container} from "@mui/material";
import React from "react";
import {ResetPasswordChangeForm} from "../components/ResetPassword/ResetPasswordChangeForm";
import {getPageTitle} from "../utils/routes";
import {useTranslation} from "react-i18next";

export const ResetPasswordRequest = (props: { match: any }) => {
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:resetPassword"))
  const token: string = props.match.params.token

  return (
    <Container maxWidth='sm'>
      {
        token ?
          <ResetPasswordChangeForm token={token}/>
          :
          <ResetPasswordRequestForm/>
      }
    </Container>
  )
}