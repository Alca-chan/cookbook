import {Container, Paper, Stack, Typography} from "@mui/material";
import React, {useContext} from "react";
import {getPageTitle} from "../utils/routes";
import {useTranslation} from "react-i18next";
import {makeStyles} from "../utils/makeStyles";
import {UserContext} from "../contexts/UserContext";
import {Loading} from "../components/Loading";
import {ChangeEmail} from "../components/Settings/ChangeEmail";
import {ChangePassword} from "../components/Settings/ChangePassword";

const useStyles = makeStyles()(theme => ({
  header: {
    backgroundColor: theme.palette.table.header,
    padding: theme.spacing(1.5, 2, 1.5, 2)
  },
  content: {
    padding: theme.spacing(2)
  }
}))

export const Settings = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  const {user} = useContext(UserContext)
  document.title = getPageTitle(t("common:settings"))

  if (!user) {
    return <Loading/>
  }

  return (
    <Container maxWidth='sm'>
      <Stack direction={"column"} spacing={2}>
        <Paper>
          <Typography align={"left"} variant={"h6"} className={classes.header}>
            {t("settings:changeEmail")}
          </Typography>
          <div className={classes.content}>
            <ChangeEmail/>
          </div>
        </Paper>
        <Paper>
          <Typography align={"left"} variant={"h6"} className={classes.header}>
            {t("settings:changePassword")}
          </Typography>
          <div className={classes.content}>
            <ChangePassword/>
          </div>
        </Paper>
      </Stack>
    </Container>
  )
}