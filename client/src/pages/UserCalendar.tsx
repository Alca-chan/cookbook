import React, {useCallback, useContext, useEffect, useMemo, useRef, useState} from 'react';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
  useTheme
} from "@mui/material";
import {UserContext} from "../contexts/UserContext";
import {RecipeShort} from "../api/types";
import {Loading} from "../components/Loading";
import {Moment} from "moment";
import {CalendarContext} from "../contexts/CalendarContext";
import {CalendarFormDialog} from "../components/Dialogs/CalendarFormDialog";
import {Delete, Edit, MoreVert} from "@mui/icons-material";
import {ConfirmDialog} from "../components/Dialogs/ConfirmDialog";
import {DragDropContext, Draggable, Droppable, DropResult} from "react-beautiful-dnd";
import {CalendarRecipeCard} from "../components/Calendar/CalendarRecipeCard";
import {NoResultsText} from "../components/RecipesGrid/NoResultsText";
import {useTranslation} from "react-i18next";
import {getPageTitle} from "../utils/routes";
import {getCancelToken, getUniqueRecipeShortPromises} from "../api/utils";
import {defaultDateFormat} from "../utils/utils";
import {makeStyles} from "../utils/makeStyles";
import {CalendarDateRangePicker} from "../components/Calendar/CalendarDateRangePicker";
import {MAX_CARD_WIDTH} from "../components/RecipesGrid/RecipeCard";

const moment = require('moment')

const useStyles = makeStyles()(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row'
  },
  cardHeader: {
    flex: 1,
    width: '100%',
    background: theme.palette.primary.light
  },
  dayGridItem: {
    paddingTop: theme.spacing(0.8),
  },
  dayCard: {
    background: theme.palette.appBackground.board,
    height: '100%'
  },
  recipeCardGrid: {
    padding: theme.spacing(0.5),
    paddingTop: theme.spacing(1),
    overflow: "auto"
  },
  menuOptionIcon: {
    marginRight: theme.spacing(1)
  },
  recipeCard: {
    paddingRight: theme.spacing(0.8),
    paddingLeft: theme.spacing(0.8)
  },
  dateRange: {
    marginBottom: theme.spacing(2)
  },
  quickOptions: {
    minWidth: "140px",
    width: "160px"
  }
}))

type CalendarDay = {
  date: Moment,
  recipes: CalendarEvent[]
}

export type CalendarEvent = {
  calendarId: string,
  order: number,
  recipe: RecipeShort
}

const dateRangeOptions = ["7days", "thisWeek", "nextWeek", "thisMonth", "custom"]
type DateRangeOption = typeof dateRangeOptions[number]

export const UserCalendar = () => {
  const {classes} = useStyles()
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:calendar"))
  const {user} = useContext(UserContext)
  const {calendar, moveInCalendar, updateOrder, removeFromCalendar} = useContext(CalendarContext)
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null)
  const [data, setData] = useState<CalendarDay[] | null>(null)
  const [selectedDay, setSelectedDay] = useState<CalendarDay | undefined>(undefined)
  const [openCalendarDialog, setOpenCalendarDialog] = useState(false)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)
  const [dateRange, setDateRange] = useState<[Moment | null, Moment | null]>([moment(), moment().add(6, 'days')])
  const [dateRangeOption, setDateRangeOption] = useState<DateRangeOption>("7days")
  const [denseView, setDenseView] = useState(false)
  const theme = useTheme()
  const spacing = parseInt(theme.spacing(1).replace("px", ""))
  const ref = useRef<HTMLDivElement>(null)

  const gridWidth = useCallback((items: number) => {
    const i = Math.min(3, items)
    const dayCardPadding = 2 * spacing * 2
    const recipeCardGridPadding = 0.5 * spacing * 2
    const recipeCardMargin = 0.8 * spacing * 2
    const result = dayCardPadding + recipeCardGridPadding + recipeCardMargin * i + MAX_CARD_WIDTH * i
    return ref.current ? Math.min(ref.current.offsetWidth, result) : result
  }, [spacing])

  const smallBreakpoint = useMemo(() => {
    // width of 2 day grids with one recipe: 2 grids + spacing + scrollbar
    return gridWidth(1) * 2 + 2 * spacing * 3 + 12
  }, [gridWidth, spacing])

  const smallDisplay = useMediaQuery('(max-width:' + smallBreakpoint + 'px)')

  const events = useMemo(() => {
    if (!data) return null
    if (dateRange[0] && dateRange[1]) {
      return data.filter(d => moment(d.date).isBetween(dateRange[0], dateRange[1], 'days', '[]'))
    } else {
      return data
    }
  }, [data, dateRange])

  const loadData = useCallback(async (token) => {
    try {
      if (!calendar) return
      const days: CalendarDay[] = []
      const uniquePromises = getUniqueRecipeShortPromises(token, calendar)
      const recipes: (RecipeShort | undefined)[] = await Promise.all(uniquePromises.map(u => u.promise))

      calendar.forEach(event => {
        const recipe = recipes.find(r => r && r.id === event.recipeId)
        if (!recipe) return
        const found = days.find(day => moment(event.date).isSame(day.date, 'day'))
        const newEvent = {calendarId: event.id, recipe: recipe, order: event.order}
        if (found) {
          found.recipes.push(newEvent)
        } else {
          const newDay: CalendarDay = {
            date: moment(event.date),
            recipes: [newEvent]
          }
          days.push(newDay)
        }
      })

      days.sort((a, b) => moment(a.date).diff(b.date))
      days.forEach(d => d.recipes.sort((a, b) => a.order - b.order))
      setData(days)
    } catch {
      /* canceled request */
    }
  }, [calendar])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  if (!events || !data) {
    return <Loading/>
  }

  const onChangeDate = () => {
    if (!user || !selectedDay) return
    setOpenCalendarDialog(true)
    setAnchor(null) // close menu
  }

  const onRemoveDate = () => {
    if (!user || !selectedDay) return
    setAnchor(null) // close menu
    setOpenDeleteDialog(true)
  }

  const removeDate = () => {
    if (!user || !selectedDay) return
    setData(data.filter(day => !day.date.isSame(selectedDay.date, 'day')))
    removeFromCalendar(selectedDay.recipes.map(e => e.calendarId))
  }

  const saveMovedDay = (selectedDate: Moment) => {
    if (!user || !selectedDay) return
    selectedDay.date = selectedDate
    moveInCalendar(selectedDay.recipes.map(e => e.calendarId), selectedDate)
  }

  const onMenuClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, day: CalendarDay) => {
    setSelectedDay(day)
    setAnchor(e.currentTarget)
  }

  const onMenuClose = () => {
    setSelectedDay(undefined)
    setAnchor(null)
  }

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return

    const sourceDate = moment(result.source.droppableId)
    const destDate = moment(result.destination.droppableId)

    const sourceIndex = result.source.index
    const destIndex = result.destination.index

    if (sourceIndex === destIndex && sourceDate.isSame(destDate, 'day')) return

    const sourceDay = data.find((day) => day.date.isSame(sourceDate, 'day'))
    if (!sourceDay || sourceDay.recipes.length < sourceIndex) return

    if (sourceDate.isSame(destDate, 'day')) {
      const sourceEvent = sourceDay.recipes.splice(sourceIndex, 1)[0]
      sourceDay.recipes.splice(destIndex, 0, sourceEvent)
      sourceDay.recipes.forEach((e, index) => e.order = index)

      updateOrder(sourceDay.recipes.map((e) => ({id: e.calendarId, order: e.order})))
      return
    }

    const destinationDay = data.find((day) => day.date.isSame(destDate, 'day'))
    if (!destinationDay || destinationDay.recipes.length < destIndex) return
    const sourceEvent = sourceDay.recipes.splice(sourceIndex, 1)[0]
    destinationDay.recipes.splice(destIndex, 0, sourceEvent)

    sourceDay.recipes.forEach((e, index) => e.order = index)
    destinationDay.recipes.forEach((e, index) => e.order = index)

    if (sourceDay.recipes.length < 1) {
      setData([...data.filter((day) => !day.date.isSame(sourceDate, 'day'))])
    }

    const changed = sourceDay.recipes.concat(destinationDay.recipes)
    moveInCalendar([sourceEvent.calendarId], destDate, changed.map((e) => ({id: e.calendarId, order: e.order})))
  }

  const onDateRangeChange = (newRange: [Moment | null, Moment | null]) => {
    if (!newRange[0] || !newRange[1]) return
    setDateRange([moment(newRange[0]), moment(newRange[1])])
    setDateRangeOption("custom")
  }

  const onDateRangeOptionChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const selected = e.target.value
    if (dateRangeOptions.includes(selected)) {
      setDateRangeOption(selected as DateRangeOption)
      if (selected === "7days") {
        setDateRange([moment(), moment().add(6, 'days')])
      } else if (selected === "thisWeek") {
        setDateRange([moment().startOf('week'), moment().endOf('week')])
      } else if (selected === "nextWeek") {
        const nextWeek = moment().add(1, "week").startOf('week')
        setDateRange([nextWeek, nextWeek.clone().endOf('week')])
      } else if (selected === "thisMonth") {
        setDateRange([moment().startOf('month'), moment().endOf('month')])
      }
    }
  }

  return (
    <>
      {
        (openDeleteDialog && selectedDay) && (
          <ConfirmDialog open={openDeleteDialog} setOpen={setOpenDeleteDialog} onConfirm={removeDate}
                         title={t("userCalendar:removeAllEventsTitle")}>
            <Typography variant={"body1"}>
              {t("userCalendar:removeAllEventsText", {date: moment(selectedDay.date).format("DD. MM.")})}
            </Typography>
          </ConfirmDialog>
        )
      }
      {
        (openCalendarDialog && selectedDay) && (
          <CalendarFormDialog open={openCalendarDialog} setOpen={setOpenCalendarDialog} onSave={saveMovedDay}
                              title={t("userCalendar:moveAllEventsTitle")}>
            <Typography variant={"body1"}>
              {t("userCalendar:moveAllEventsText", {date: moment(selectedDay.date).format("DD. MM.")})}
            </Typography>
          </CalendarFormDialog>
        )
      }
      <Stack direction={{xs: "column", md: "row"}} spacing={2} alignItems={{xs: "flex-start", md: "center"}}
             justifyContent={{xs: "center", sm: "space-between"}} className={classes.dateRange}>
        <Stack direction={{xs: "column", md: "row"}} spacing={2} alignItems={{xs: "flex-start", md: "center"}}
               justifyContent={{xs: "center", sm: "flex-start"}}>
          <Stack direction={{xs: "column", sm: "row"}} spacing={2} alignItems={{xs: "flex-start", sm: "center"}}
                 justifyContent={{xs: "center", sm: "flex-start"}}>
            <Typography>{t("userCalendar:displayEventsBetween")}:</Typography>
            <CalendarDateRangePicker startDate={dateRange[0]} endDate={dateRange[1]} onChange={onDateRangeChange}/>
          </Stack>
          <TextField
            className={classes.quickOptions}
            variant={"outlined"}
            value={dateRangeOption}
            label={t("userCalendar:quickOptions")}
            type={"text"}
            select
            onChange={(e) => onDateRangeOptionChange(e)}
          >
            {
              dateRangeOptions.map((o, index) =>
                <MenuItem value={o} key={index}>{t("userCalendar:" + o)}</MenuItem>
              )
            }
          </TextField>
        </Stack>
        {
          !smallDisplay && (
            <Stack direction={"row"} spacing={1} alignItems={"center"} justifyContent={"flex-end"}>
              <Typography>{t("userCalendar:view")}:</Typography>
              <Button variant={"contained"} color={!denseView ? "primary" : "grey"}
                      onClick={() => setDenseView(false)}>
                {t("userCalendar:normal")}
              </Button>
              <Button variant={"contained"} color={denseView ? "primary" : "grey"}
                      onClick={() => setDenseView(true)}>
                {t("userCalendar:dense")}
              </Button>
            </Stack>
          )
        }
      </Stack>
      {
        events.length < 1 && (
          <NoResultsText>{t("userCalendar:emptyCalendar")}</NoResultsText>
        )
      }
      <div ref={ref}>
        <Grid container columnSpacing={{xs: denseView ? 2 : 0, xl: 2}} rowSpacing={2} className={classes.container}>
          <DragDropContext onDragEnd={onDragEnd}>
            {
              events.map((day) =>
                <Grid item key={day.date.valueOf()} xs={(denseView && !smallDisplay) ? undefined : 12}
                      xl={denseView ? undefined : 6} className={classes.dayGridItem}>
                  <Card className={classes.dayCard} key={day.date.valueOf()}
                        style={{width: (denseView && !smallDisplay) ? gridWidth(day.recipes.length) + "px" : undefined}}>
                    <CardHeader className={classes.cardHeader} title={moment(day.date).format("DD. MM.")}
                                action={
                                  <div>
                                    <IconButton size={"medium"} onClick={(e) => onMenuClick(e, day)}>
                                      <MoreVert/>
                                    </IconButton>
                                    <Menu open={anchor !== null} anchorEl={anchor}
                                          anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                                          onClose={onMenuClose}>
                                      <MenuItem onClick={onChangeDate}>
                                        <Edit fontSize={"small"}
                                              className={classes.menuOptionIcon}/>{t("userCalendar:changeDate")}
                                      </MenuItem>
                                      <MenuItem onClick={onRemoveDate}>
                                        <Delete fontSize={"small"}
                                                className={classes.menuOptionIcon}/>{t("userCalendar:removeRecipes")}
                                      </MenuItem>
                                    </Menu>
                                  </div>
                                }/>
                    <CardContent>
                      <Droppable droppableId={day.date.format(defaultDateFormat)} direction={"horizontal"}>
                        {(provided) => (
                          <Grid container direction={"row"} wrap={"nowrap"}
                                className={classes.recipeCardGrid}
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                          >
                            {
                              day.recipes.map((event, index) =>
                                <Grid item key={event.calendarId} className={classes.recipeCard}>
                                  <Draggable draggableId={event.calendarId} index={index} key={event.calendarId}>
                                    {(provided) => (
                                      <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={provided.draggableProps.style}
                                      >
                                        <CalendarRecipeCard event={event} date={day.date}/>
                                      </div>
                                    )}
                                  </Draggable>
                                </Grid>
                              )
                            }
                            {provided.placeholder}
                          </Grid>
                        )}
                      </Droppable>
                    </CardContent>
                  </Card>
                </Grid>
              )
            }
          </DragDropContext>
        </Grid>
      </div>
    </>
  )
}