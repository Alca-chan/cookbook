import React, {useCallback, useContext, useEffect, useState} from "react";
import {UserContext} from "../contexts/UserContext";
import {RecipeShort} from "../api/types";
import {NoResultsText} from "../components/RecipesGrid/NoResultsText";
import {RecipesGrid} from "../components/RecipesGrid/RecipesGrid";
import {Loading} from "../components/Loading";
import {useTranslation} from "react-i18next";
import {getRecipesByUser} from "../api/recipes/recipeSearch";
import {getPageTitle} from "../utils/routes";
import {getCancelToken} from "../api/utils";

export const UserRecipes = () => {
  const {t} = useTranslation()
  document.title = getPageTitle(t("common:myRecipes"))
  const {user} = useContext(UserContext)
  const [recipes, setRecipes] = useState<RecipeShort[]>()

  const loadData = useCallback(async (token) => {
    try {
      if (!user) return
      const data: RecipeShort[] = await getRecipesByUser(token, user.id)
      if (data.length < 1) {
        setRecipes([])
      } else {
        setRecipes(data)
      }
    } catch {
      /* canceled request */
    }
  }, [user])

  useEffect(() => {
    const source = getCancelToken()
    loadData(source.token).then()
    return () => {
      source.cancel()
    }
  }, [loadData])

  if (!recipes) {
    return <Loading/>
  }

  if (recipes.length < 1) {
    return <NoResultsText>{t("userRecipes:noRecipes")}</NoResultsText>
  }

  return (
    <RecipesGrid data={recipes}/>
  )
}