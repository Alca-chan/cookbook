import {Crop} from "react-image-crop";
import imageCompression from "browser-image-compression";

export const MAX_IMAGE_SIZE = 700

export const toBase64 = (file: File): Promise<string> => new Promise((resolve, reject) => {
  const reader = new FileReader()
  reader.readAsDataURL(file)
  // result for readAsDataURL is a string
  reader.onload = () => resolve(reader.result as string)
  reader.onerror = error => reject(error)
})

export const cropImage = (image: HTMLImageElement, crop: Crop): string => {
  const canvas = document.createElement('canvas')

  const scaleX = image.naturalWidth / image.width
  const scaleY = image.naturalHeight / image.height
  const ctx = canvas.getContext('2d')
  const pixelRatio = window.devicePixelRatio

  if (!ctx) return ""

  canvas.width = crop.width * pixelRatio * scaleX
  canvas.height = crop.height * pixelRatio * scaleY

  ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0)
  ctx.imageSmoothingQuality = 'high'

  ctx.drawImage(
    image,
    crop.x * scaleX,
    crop.y * scaleY,
    crop.width * scaleX,
    crop.height * scaleY,
    0,
    0,
    crop.width * scaleX,
    crop.height * scaleY
  )

  return canvas.toDataURL('image/jpeg')
}

export const rotateImage = async (degrees: number, image: HTMLImageElement): Promise<HTMLImageElement> => {
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')
  if (!ctx) return image

  canvas.width = degrees === 180 || 0 ? image.width : image.height
  canvas.height = degrees === 180 || 0 ? image.height : image.width

  ctx.translate(canvas.width / 2, canvas.height / 2)
  ctx.rotate(getRadianAngle(degrees))
  const dx = degrees === 180 || 0 ? -canvas.width / 2 : -canvas.height / 2
  const dy = degrees === 180 || 0 ? -canvas.height / 2 : -canvas.width / 2
  ctx.drawImage(image, dx, dy)

  return await createImage(canvas.toDataURL("image/jpeg"))
}

const createImage = (imageSrc: string): Promise<HTMLImageElement> => {
  return new Promise((resolve, reject) => {
    const image = new Image()
    image.addEventListener('load', () => resolve(image))
    image.addEventListener('error', (error) => reject(error))
    image.setAttribute('crossOrigin', 'anonymous') // needed to avoid cross-origin issues on CodeSandbox
    image.src = imageSrc
  })
}

export const getImageDimensions = async (imageSrc: string): Promise<{ width: number, height: number }> => {
  const image = await createImage(imageSrc)
  return {width: image.width, height: image.height}
}

const getRadianAngle = (degreeValue: number): number => {
  return (degreeValue * Math.PI) / 180
}

export const compressImage = async (image: string): Promise<string> => {
  const options = {
    maxSizeMB: 1,
    maxWidthOrHeight: 1080,
    useWebWorker: true
  }
  const imageFile = await imageCompression.getFilefromDataUrl(image, "")
  const compressedImage = await imageCompression(imageFile, options)
  return await imageCompression.getDataUrlFromFile(compressedImage)
}