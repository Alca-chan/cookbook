import {createMakeAndWithStyles} from "tss-react";
import {createTheme} from "@mui/material/styles";
import {alpha, darken, PaletteColorOptions} from "@mui/material";
import createPalette, {PaletteColor} from "@mui/material/styles/createPalette";
import {grey, red} from "@mui/material/colors";

// needed for mui button override
declare module "@mui/material/Button" {
  interface ButtonPropsColorOverrides {
    grey: true
  }
}

declare module "@mui/material" {
  interface Color {
    main: string,
    dark: string
  }
}

// needed for custom values in theme palette
declare module '@mui/material/styles/createPalette' {
  interface TablePaletteColorOptions {
    border?: string,
    header?: string,
    subHeader?: string,
    highlight?: string
  }

  interface TablePaletteColor {
    border: string,
    header: string,
    subHeader: string,
    highlight: string
  }

  interface BackgroundPaletteColorOptions {
    outer?: string,
    inner?: string,
    image?: string,
    board?: string
  }

  interface BackgroundPaletteColor {
    outer: string,
    inner: string,
    image: string,
    board: string
  }

  interface IconPaletteColorOptions {
    background?: string,
    color?: string,
    backgroundSelected?: string,
    colorSelected?: string,
    backgroundSelectedHighlight?: string,
    colorSelectedHighlight?: string
  }

  interface IconPaletteColor {
    background: string,
    color: string,
    backgroundSelected: string,
    colorSelected: string,
    backgroundSelectedHighlight: string,
    colorSelectedHighlight: string
  }

  interface PaletteOptions {
    table?: TablePaletteColorOptions,
    label?: PaletteColorOptions,
    appBackground?: BackgroundPaletteColorOptions,
    icon?: IconPaletteColorOptions
  }

  interface Palette {
    table: TablePaletteColor,
    label: PaletteColor,
    appBackground: BackgroundPaletteColor,
    icon: IconPaletteColor
  }
}

const palette = createPalette({
  primary: {
    main: '#ffa726',
    light: '#ffd95b',
    dark: '#c77800'
  },
  secondary: {
    main: '#80d8ff',
    light: '#b5ffff',
    dark: '#49a7cc',
  },
  grey: {
    main: grey[300],
    dark: grey[400]
  },
  background: {
    default: '#eeeeee',
    paper: '#ffffff'
  }
})

const defaultTheme = createTheme()
export const muiTheme = createTheme({
  palette: {
    ...palette,
    table: {
      border: alpha(palette.common.black, 0.1),
      header: palette.primary.light,
      subHeader: alpha(palette.primary.light, 0.4),
      highlight: alpha(palette.primary.light, 0.2)
    },
    label: {
      main: palette.primary.light,
      light: alpha(palette.primary.light, 0.4)
    },
    appBackground: {
      inner: '#fafafa',
      outer: '#eeeeee',
      image: darken(palette.background.paper, 0.04),
      board: alpha(palette.primary.light, 0.1)
    },
    icon: {
      background: alpha(palette.common.black, 0.05),
      color: alpha(palette.common.black, 0.55),
      backgroundSelected: alpha(palette.common.black, 0.10),
      colorSelected: alpha(palette.common.black, 0.55),
      backgroundSelectedHighlight: alpha(red[100], 0.80),
      colorSelectedHighlight: red[400]
    }
  },
  components: {
    MuiButton: { // MUI v5 button override to MUI v4 "default" color (color={"default"} -> color={"grey"})
      variants: [
        {
          props: {variant: "contained", color: "grey"},
          style: {
            color: palette.getContrastText(palette.grey[300])
          }
        },
        {
          props: {variant: "outlined", color: "grey"},
          style: {
            color: palette.text.primary,
            borderColor:
              palette.mode === "light"
                ? "rgba(0, 0, 0, 0.23)"
                : "rgba(255, 255, 255, 0.23)",
            "&.Mui-disabled": {
              border: `1px solid ${palette.action.disabledBackground}`
            },
            "&:hover": {
              borderColor:
                palette.mode === "light"
                  ? "rgba(0, 0, 0, 0.23)"
                  : "rgba(255, 255, 255, 0.23)",
              backgroundColor: alpha(
                palette.text.primary,
                palette.action.hoverOpacity
              )
            }
          }
        },
        {
          props: {color: "grey", variant: "text"},
          style: {
            color: palette.text.primary,
            "&:hover": {
              backgroundColor: alpha(
                palette.text.primary,
                palette.action.hoverOpacity
              )
            }
          }
        }
      ]
    },
    MuiCssBaseline: {
      styleOverrides: {
        '*::-webkit-scrollbar': {
          width: '12px',
          height: '12px',
          [defaultTheme.breakpoints.down('xs')]: {
            width: '16px',
            height: '16px'
          }
        },
        '*::-webkit-scrollbar-track': {
          backgroundColor: darken(palette.background.paper, 0.1),
          boxShadow: 'inset 0 0 2px grey',
          webkitBoxShadow: 'inset 0 0 2px grey',
          borderRadius: '10px'
        },
        '*::-webkit-scrollbar-thumb': {
          backgroundColor: darken(palette.background.paper, 0.2),
          boxShadow: 'inset 0 0 2px grey',
          webkitBoxShadow: 'inset 0 0 2px grey',
          borderRadius: '10px',
        }
      }
    }
  }
})

const useTheme = () => ({
  ...muiTheme
})

export const {makeStyles, withStyles} = createMakeAndWithStyles({
  useTheme
  /*
  OR, if you have extended the default mui theme adding your own custom properties:
  Let's assume the myTheme object that you provide to the <ThemeProvider /> is of
  type MyTheme then you'll write:
  */
  //"useTheme": useTheme as (()=> MyTheme)
})