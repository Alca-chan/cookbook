import {TFunction} from "i18next";
import {Recipes} from "../pages/Recipes";
import {RecipesByIngredients} from "../pages/RecipesByIngredients";
import {RecipesByTags} from "../pages/RecipesByTags";
import {RecipesByEquipment} from "../pages/RecipesByEquipment";
import {RecipePage} from "../pages/RecipePage";
import {EditRecipe} from "../pages/EditRecipe";
import {NewRecipe} from "../pages/NewRecipe";
import {UserRecipes} from "../pages/UserRecipes";
import {Favorites} from "../pages/Favorites";
import {Cart} from "../pages/Cart";
import {UserCalendar} from "../pages/UserCalendar";
import {Login} from "../pages/Login";
import {ResetPasswordRequest} from "../pages/RequestPasswordReset";
import {About} from "../pages/About";
import {NotFound} from "../pages/NotFound";
import {User} from "../api/types";
import {APP_NAME} from "../App";
import {Settings} from "../pages/Settings";

export const getHomePagePaths = () => {
  return ["/", "/recipes", getRoute('cs', "/recipes"), "/search", getRoute('cs', "/search")]
}

export const getRoutes = (t: TFunction, user: User | null | undefined) => {
  return [
    {
      path: getHomePagePaths(),
      name: APP_NAME,
      authenticated: true,
      component: Recipes
    },
    {
      path: ["/search/ingredients", getRoute('cs', "/search/ingredients")],
      name: t("common:searchByIngredients"),
      authenticated: true,
      component: RecipesByIngredients
    },
    {
      path: ["/search/tags", getRoute('cs', "/search/tags")],
      name: t("common:searchByTags"),
      authenticated: true,
      component: RecipesByTags
    },
    {
      path: ["/search/equipment", getRoute('cs', "/search/equipment")],
      name: t("common:searchByEquipment"),
      authenticated: true,
      component: RecipesByEquipment
    },
    {
      path: ["/recipe/:id", getRoute('cs', "/recipe/:id")],
      name: t("common:recipe"),
      authenticated: true,
      component: RecipePage
    },
    {
      path: ["/recipe/:id/edit", getRoute('cs', "/recipe/:id/edit")],
      name: t("common:edit"),
      authenticated: Boolean(user),
      component: EditRecipe
    },
    {
      path: ["/new-recipe", getRoute('cs', "/new-recipe")],
      name: t("common:newRecipe"),
      authenticated: Boolean(user),
      component: NewRecipe
    },
    {
      path: ["/my-recipes", getRoute('cs', "/my-recipes")],
      name: t("common:myRecipes"),
      authenticated: Boolean(user),
      component: UserRecipes
    },
    {
      path: ["/my-cookbooks", getRoute('cs', "/my-cookbooks")],
      name: t("common:favorites"),
      authenticated: Boolean(user),
      component: Favorites
    },
    {
      path: ["/shopping-list", getRoute('cs', "/shopping-list")],
      name: t("common:shoppingList"),
      authenticated: Boolean(user),
      component: Cart
    },
    {
      path: ["/calendar", getRoute('cs', "/calendar")],
      name: t("common:calendar"),
      authenticated: Boolean(user),
      component: UserCalendar
    },
    {
      path: ["/login", getRoute('cs', "/login")],
      name: t("common:login"),
      authenticated: true,
      component: Login
    },
    {
      path: ["/password-reset/:token?", getRoute('cs', "/password-reset/:token?")],
      name: t("common:resetPassword"),
      authenticated: !user,
      component: ResetPasswordRequest
    },
    {
      path: ["/about", getRoute('cs', "/about")],
      name: t("common:about"),
      authenticated: true,
      component: About
    },
    {
      path: ["/settings", getRoute('cs', "/settings")],
      name: t("common:settings"),
      authenticated: Boolean(user),
      component: Settings
    },
    {
      path: "*",
      name: t("common:notFound"),
      authenticated: true,
      component: NotFound
    }
  ]
}

export const getRoute = (lang: string, route: string): string => {
  if (lang === 'cs') {
    switch (route) {
      case '/recipes':
        return '/recepty'
      case '/search':
        return '/vyhledavani'
      case '/search/ingredients':
        return '/vyhledavani/ingredience'
      case '/search/tags':
        return '/vyhledavani/stitky'
      case '/search/equipment':
        return '/vyhledavani/nacini'
      case '/recipe/:id':
        return '/recept/:id'
      case '/recipe/:id/edit':
        return '/recept/:id/edit'
      case '/new-recipe':
        return '/novy-recept'
      case '/my-recipes':
        return '/moje-recepty'
      case '/my-cookbooks':
        return '/moje-kucharky'
      case '/shopping-list':
        return '/nakupni-seznam'
      case '/calendar':
        return '/kalendar'
      case '/login':
        return '/prihlaseni'
      case '/password-reset/:token?':
        return '/obnoveni-hesla/:token?'
      case '/about':
        return '/o-strance'
      case '/settings':
        return '/nastaveni'
      default:
        return '*'
    }
  }
  // english is default
  return route
}

export const getPageTitle = (name: string): string => {
  return name + " - " + APP_NAME
}

export const parsePageTitle = (page: string): string => {
  return page.replace(" - " + APP_NAME, "")
}