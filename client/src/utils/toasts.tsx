import toast from "react-hot-toast";
import React from "react";
import {TFunction} from "i18next";

export const uploadToServerToast = async (t: TFunction, promise: Promise<void>) => {
  await toast.promise(
    promise,
    {
      loading: t("toast:uploadingToServer"),
      success: <b>{t("toast:uploadingToServerSuccessful")}</b>,
      error: <b>{t("toast:uploadingToServerError")}</b>
    }
  )
}

export const sentPasswordResetToast = async (t: TFunction, promise: Promise<void>) => {
  await toast.promise(
    promise,
    {
      loading: t("toast:sendingPasswordResetRequest"),
      success: <b>{t("toast:sendingPasswordResetRequestSuccessful")}</b>,
      error: <b>{t("toast:sendingPasswordResetRequestError")}</b>
    }
  )
}

export const changePasswordToast = async (t: TFunction, promise: Promise<void>) => {
  await toast.promise(
    promise,
    {
      loading: t("toast:sendingPasswordChangeRequest"),
      success: <b>{t("toast:sendingPasswordChangeRequestSuccessful")}</b>,
      error: <b>{t("toast:sendingPasswordChangeRequestError")}</b>
    }
  )
}

export const changeEmailToast = async (t: TFunction, promise: Promise<void>) => {
  await toast.promise(
    promise,
    {
      loading: t("toast:sendingEmailChangeRequest"),
      success: <b>{t("toast:sendingEmailChangeRequestSuccessful")}</b>,
      error: <b>{t("toast:sendingEmailChangeRequestError")}</b>
    }
  )
}

export const copyLinkToast = async (t: TFunction, promise: Promise<void>) => {
  await toast.promise(
    promise,
    {
      loading: t("toast:copyLink"),
      success: <b>{t("toast:copyLinkSuccessful")}</b>,
      error: <b>{t("toast:copyLinkError")}</b>
    }
  )
}

export const copyShoppingListToast = async (t: TFunction, promise: Promise<void>) => {
  await toast.promise(
    promise,
    {
      loading: t("toast:copyShoppingList"),
      success: <b>{t("toast:copyShoppingListSuccessful")}</b>,
      error: <b>{t("toast:copyShoppingListError")}</b>
    }
  )
}