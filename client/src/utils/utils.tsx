import {CartItemToAdd, CartList, Ingredient, Unit} from "../api/types";
import {Moment} from "moment";

const moment = require('moment')

export const DEBUG_LANGUAGES = false // set this to true if you want to be able to switch between languages

export const PASSWORD_MIN_LENGTH = 6

export const defaultDateFormat = "YYYY-MM-DD"
export const displayDateFormat = "D. M. Y"
export const displayShortDateFormat = "D. M."

export const checkValidEmailAddress = (email: string): Boolean => {
  const emailRegEx: RegExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return Boolean(email && emailRegEx.test(email.toLowerCase()))
}

export const wait = (ms: number) => new Promise(res => setTimeout(res, ms))

export const timeToMinutes = (time: number, unit: string): number => {
  if (unit === "days") {
    return time * 24 * 60
  } else if (unit === "hours") {
    return time * 60
  } else {
    return time
  }
}

export const minutesToTime = (minutes: number): { days: number, hours: number, minutes: number } => {
  const days = Math.floor(minutes / (24 * 60))
  minutes -= Math.floor(days * 24 * 60) // round down in case there is some precision issue (e.g. 0.005 -> 0)
  const hours = Math.floor(minutes / 60)
  minutes -= Math.floor(hours * 60)
  return {days: days, hours: hours, minutes: minutes}
}

export const minutesToOneTimeUnit = (minutes: number): { value: number, unit: string } => {
  const time = minutesToTime(minutes)
  if (time.minutes !== 0) {
    return {value: minutes, unit: "minutes"}
  } else if (time.hours !== 0) {
    return {value: time.hours + time.days * 24, unit: "hours"}
  } else {
    return {value: time.days, unit: "days"}
  }
}

export const getMeasureIngredientUnitString = (measure: number | undefined, unit: Unit, ingredient: Ingredient): { measureString: string, unitString: string, ingredientString: string } => {
  let measureString: string = measure === undefined ? '' : measure.toLocaleString('cs-CZ', {maximumFractionDigits: 2})
  let unitString: string = unit.symbol
  if (unitString === "ks") {
    unitString = ""
  } else if (!unit.measurable) {
    measureString = ""
    unitString = unit.nameOne
  } else if (unitString === unit.nameOne && measure !== undefined) {
    if (measure % 1 !== 0) {
      unitString = unit.namePart
    } else if (measure < 2) {
      unitString = unit.nameOne
    } else if (measure < 5) {
      unitString = unit.nameFew
    } else {
      unitString = unit.nameMany
    }
  }

  let ingredientString: string = ingredient.nameUnit
  if (unit.symbol === "ks" && measure !== undefined) {
    if (measure % 1 !== 0) {
      ingredientString = ingredient.namePart
    } else if (measure < 2) {
      ingredientString = ingredient.nameOne
    } else if (measure < 5) {
      ingredientString = ingredient.nameFew
    } else {
      ingredientString = ingredient.nameMany
    }
  }
  return {measureString: measureString, unitString: unitString, ingredientString: ingredientString}
}

export const getMeasureIngredientUnitStringForShoppingList = (measure: number | undefined, unit: Unit, ingredient: Ingredient, note?: string): { measure: string, ingredient: string } => {
  const {
    measureString,
    unitString,
    ingredientString
  } = getMeasureIngredientUnitString(measure, unit, ingredient)
  return {
    measure: measureString + (measureString.length > 0 && unitString.length > 0 ? " " : "") + unitString,
    ingredient: ingredientString + ((note && note.replace(/\s/g, "").length > 0) ? (" (" + note + ")") : ""),
  }
}

export const createCartItemToAdd = (recipeIngredientId: string | undefined, recipeId: string | undefined, measure: number | undefined, unit: Unit, ingredient: Ingredient, order: number, note?: string, title?: string): CartItemToAdd => {
  const measureIngredientString = getMeasureIngredientUnitStringForShoppingList(measure, unit, ingredient, note)
  return createNewCartItem(recipeIngredientId, recipeId, measureIngredientString.measure, measureIngredientString.ingredient, order, title)
}

export const createNewCartItem = (recipeIngredientId: string | undefined, recipeId: string | undefined, measure: string, ingredient: string, order: number, title?: string): CartItemToAdd => {
  return {
    recipeIngredientId: recipeIngredientId ?? null,
    recipeId: recipeId ?? null,
    measure: measure,
    ingredient: ingredient,
    order: order,
    title: title ?? null
  }
}

const recipeTitleString = (list: CartList): string => {
  let title = list.title
  if (list.date) {
    const date = moment(list.date).format(displayShortDateFormat)
    title = title.concat(" (", date, ")")
  }
  return title
}

export const recipeString = (list: CartList): string => {
  const items = list.items.map(i => i.done ? '' : ('  ' + i.measure + ' ' + i.ingredient + '\n')).join('')
  return items.length > 0 ? (recipeTitleString(list) + '\n' + items) : ''
}

export const compareDates = (a: Moment | undefined, b: Moment | undefined): boolean => {
  if (!a && !b) return true
  if (!a || !b) return false
  return a.isSame(b, 'day')
}

export const splitArray = <T extends Object>(array: T[], filterFn: (item: T) => boolean) => array.reduce((previous: { found: T[], rest: T[] }, item: T) => {
  if (filterFn(item)) {
    return {found: [...previous.found, item], rest: [...previous.rest]}
  }
  return {found: [...previous.found], rest: [...previous.rest, item]}
}, {found: [], rest: []})