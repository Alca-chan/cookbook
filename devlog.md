# Dev log

### December 2020

#### 9. 12. 2020 - 22. 12. 2020

* created simple application using Firebase and Meal DB API
* user login / logout
* management of shopping list and favorites
* recipe detail
* listing recipes by their category, the first letter, name, or random
* creating new recipes and their management

### April 2021

#### 17. 4. 2021 - 28. 4. 2021

* created a postgres DB and a server API
* connection of the frontend and the server

#### 29. 4. 2021

Time spend: ~ 2.5 hours

* Firebase and Meal DB API changed to my server API
* application is being rewritten to fit this new API
* most of the functionality is still not redone
  * displaying data (recipe detail, ...)
  * user login/logout
  * cart, favorites for user
  * adding new recipes
  * listing recipes by *
  * ...
* client side application is runnable

#### 30. 4. 2021

Time spend: ~ 4.5 hours

* Almost all functionality is now working
  * Adding and updating is not yet done
  * user login is disabled

### May 2021

#### 2. 5. 2021

Time spend: ~ 2 hours

* Started working on login
  * local authentication using Passport.js by a username and a password
  * created user DB

#### 5. 5. 2021 - 6. 5. 2021

Time spend: ~ 7 hours

* Finished user authentication and registration on both server and client side
* User registration: username, email, password
* User login: username, password
* Get current user (using cookies)

#### 12. 5. 2021

Time spend: ~ 3 hours

* User login persistence
* User logout

### July 2021

#### 17. 7. 2021 - 26. 7. 2021

Time spend: ~ 9 hours

* Implemented cart functionality
  * add / add all ingredients to cart / remove from cart
  * simple cart view
  * db
* Implemented favourites
  * add / remove favourites
  * favourites view
  * db
* IDs converted to string
* Small fixes

#### 30. 7. 2021

Time spend: ~ 3 hours

* New / edit recipe form
  * Ingredients form
    * measure and note text input, unit and ingredient dropdown autocomplete
  * Tags and equipment forms
    * choose tags and equipment from dropdown autocomplete menu

### August 2021

#### 1. 8. 2021 - 4. 8. 2021

Time spend: ~ 4.5 hours

* New / edit recipe form
* Save recipe to db / delete from db
* Update recipe
  * delete all data from associated tables and insert new even when there are no changes
* Recipe related tables are now on delete cascade

#### 5. 8. 2021 - 8. 8. 2021

Time spend: ~ 3.5 hours

* Shopping list and favourites are available only after logging in
* New / edit recipe form
  * method and ingredients edit tools
    * move item up/down
    * delete item
    * add new item above current
  * confirm dialogs when deleting recipe or step
* Recipe author saved in db
  * users can edit or delete only their recipes
* Fixed My recipes page

#### 10. 8. 2021 - 12. 8. 2021

Time spend: ~ 10.5 hours

* New / edit recipe form
  * method and ingredients form
    * Drag and drop
    * ingredients are in sections
      * nested drag and drop
      * section titles
  * fixed error when clearing unit or ingredient

#### 13. 8. 2021 - 14. 8. 2021

Time spend: ~ 8 hours

* Only edited items are updated in db
* Ingredient sections and item order are stored in db
* Ingredient table
  * ingredient sections are merged when they have the same title
  * fixed issue with empty section name
  * fixed add all to cart
* New / edit recipe form
  * min ingredient measure value

#### 15. 8. 2021 - 17. 8. 2021

Time spend: ~ 9.5 hours

* Image upload
  * editor for cropping in recipe form
  * stored as base64 string in db
  * display image in recipe form, recipe page and recipe grid

#### 18. 8. 2021 - 24. 8. 2021

Time spend: ~ 3.5 hours

* Added titles and dividers to recipe form
* Recipe ingredient strings in recipe table are in correct form
* Login / registration form
  * check taken usernames
  * check username, email and password constrains
  * display errors
* Devlog update


Total time: ~ 77 hours