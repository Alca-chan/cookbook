import {Pool} from 'pg';

import passport from "./passportStrategy";
import dotenv from 'dotenv';
import express = require('express');
import cors = require('cors');
import path = require('path');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import session = require('cookie-session');

dotenv.config({path: "./variables.env"})

const production = process.env.NODE_ENV === "production"

const CLIENT_URL = production ? process.env.CLIENT_URL : process.env.DEV_CLIENT_URL
export const SERVER_URL = '/api'
export const BASE_URL = production ? process.env.BASE_URL : process.env.DEV_BASE_URL

const app = express()

app.use(express.static(path.join(__dirname, 'build')))
app.use(cors({
  origin: CLIENT_URL,
  credentials: true
}))
app.use(bodyParser.json({limit: '5mb'}))
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: '5mb'
  })
)
app.use(express.static("public"))
app.use(cookieParser("secret"))
app.use(session({
  secret: "secret",
  resave: true,
  saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())

const api = express.Router()

const port = process.env.PORT || 3001

app.listen(port, () => {
  console.log("Server running at port " + port)
})

const poolConfig = production ? {
  user: process.env.PG_USER,
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  password: process.env.PG_PASSWORD,
  port: process.env.PG_PORT,
  ssl: {
    rejectUnauthorized: false
  }
} : {
  user: process.env.DEV_PG_USER,
  host: process.env.DEV_PG_HOST,
  database: process.env.DEV_PG_DATABASE,
  password: process.env.DEV_PG_PASSWORD,
  port: process.env.PG_PORT
}

export const pool = new Pool(poolConfig)

require("./routes/recipe")(api)
require("./routes/ingredient")(api)
require("./routes/equipment")(api)
require("./routes/tag")(api)
require("./routes/unit")(api)
require("./routes/recipe-equipment")(api)
require("./routes/recipe-tag")(api)
require("./routes/recipe-ingredient")(api)
require("./routes/cart")(api)
require("./routes/favorites")(api)
require("./routes/calendar")(api)
require("./routes/user")(api, passport)
require("./routes/password-reset")(api)

app.use('/api', api)

// not found
app.get('/api/*', function (request, response) {
  response.sendStatus(404)
})

// path is not /api
// if in production mode, serve frontend
if (production) {
  app.get('*', function (request, response) {
    response.sendFile(path.join(__dirname, 'build/index.html'))
  })
} else {
  // if in dev mode, return not found
  app.get('*', function (request, response) {
    response.sendStatus(404)
  })
}
