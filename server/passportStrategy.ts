import {Strategy as LocalStrategy} from 'passport-local';
import fetch from 'node-fetch';
import {BASE_URL, pool, SERVER_URL} from './index';
import bcrypt = require('bcryptjs');
import passport = require('passport');

passport.use('local-login', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  (username, password, done) => {
    pool.query(
      "SELECT * FROM \"user\" WHERE username = $1",
      [username],
      (error, results) => {
        if (error) {
          throw error
        }
        if (results.rows.length !== 1) {
          return done(null, false)
        }
        const user = results.rows[0]
        bcrypt.compare(password, user.password, (err, result) => {
          if (err) {
            throw error
          }
          if (result) {
            return done(null, user)
          } else {
            return done(null, false)
          }
        })
      }
    )
  })
)

passport.use('local-register', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
  },
  async (request, username, password, done) => {
    return await fetch(BASE_URL + SERVER_URL + "/users/username/" + username)
      .then(response => response.json())
      .then(async (result) => {
        if (result.length != 0) {
          return done(null, false, {message: "User already exists"})
        }
        const hashedPassword = await bcrypt.hash(password, 10)

        const body = {
          method: "POST",
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: username,
            email: request.body.email,
            password: hashedPassword
          })
        }
        return await fetch(BASE_URL + SERVER_URL + "/users", body)
          .then(response => response.json())
          .then((result) => {
            done(null, result[0])
          })
      })
      .catch((err) => {
        console.log(err)
        return done(null, false, {message: "Could not create the user"})
      })
  })
)

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  pool.query(
    "SELECT * FROM \"user\" WHERE id = $1",
    [id],
    (error, results) => {
      if (error) {
        done(error, null)
      }
      if (results.rows.length !== 1) {
        return done(null, false);
      }
      const user = {
        id: id,
        username: results.rows[0].username,
        email: results.rows[0].email
      }
      return done(null, user)
    })
})

export default passport



