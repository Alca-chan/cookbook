CREATE TABLE public."user"
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    username character varying COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.token
(
    token character varying COLLATE pg_catalog."default" NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    CONSTRAINT token_pkey PRIMARY KEY (token),
    CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE public.equipment
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT equipment_pkey PRIMARY KEY (id)
);

CREATE TABLE public.tag
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tag_pkey PRIMARY KEY (id)
);

CREATE TABLE public.unit
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    symbol character varying COLLATE pg_catalog."default" NOT NULL,
    name_one character varying COLLATE pg_catalog."default",
    name_few character varying COLLATE pg_catalog."default",
    name_many character varying COLLATE pg_catalog."default",
    name_part character varying COLLATE pg_catalog."default",
    measurable boolean NOT NULL,
    CONSTRAINT unit_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ingredient
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name_one character varying COLLATE pg_catalog."default" NOT NULL,
    name_few character varying COLLATE pg_catalog."default",
    name_many character varying COLLATE pg_catalog."default",
    name_unit character varying COLLATE pg_catalog."default",
    name_part character varying COLLATE pg_catalog."default",
    name_base character varying COLLATE pg_catalog."default",
    energy_kj integer,
    energy_kcal integer,
    fat numeric,
    saturated_fatty_acid numeric,
    monounsaturated_fatty_acid numeric,
    polyunsaturated_fatty_acid numeric,
    carbohydrate numeric,
    sugar numeric,
    polyol numeric,
    starch numeric,
    fiber numeric,
    protein numeric,
    salt numeric,
    CONSTRAINT ingredient_pkey PRIMARY KEY (id)
);

CREATE TABLE public.recipe
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    title character varying COLLATE pg_catalog."default" NOT NULL,
    description character varying COLLATE pg_catalog."default",
    method character varying COLLATE pg_catalog."default" NOT NULL,
    "time" integer NOT NULL,
    time_prep integer NOT NULL,
    votes integer,
    user_id bigint NOT NULL,
    image text COLLATE pg_catalog."default",
    date character varying COLLATE pg_catalog."default" NOT NULL,
    portions numeric NOT NULL,
    portions_note character varying COLLATE pg_catalog."default",
    CONSTRAINT recipe_pkey PRIMARY KEY (id),
    CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE public.recipe_equipment
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    recipe_id bigint NOT NULL,
    equipment_id bigint NOT NULL,
    CONSTRAINT recipe_equipment_pkey PRIMARY KEY (id),
    CONSTRAINT equipment_id_fkey FOREIGN KEY (equipment_id)
        REFERENCES public.equipment (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES public.recipe (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE public.recipe_ingredient
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    recipe_id bigint NOT NULL,
    ingredient_id bigint NOT NULL,
    measure real,
    unit_id integer NOT NULL,
    note character varying COLLATE pg_catalog."default",
    section character varying COLLATE pg_catalog."default",
    section_order integer NOT NULL,
    "order" integer NOT NULL,
    CONSTRAINT recipe_ingredient_pkey PRIMARY KEY (id),
    CONSTRAINT ingredient_id_fkey FOREIGN KEY (ingredient_id)
        REFERENCES public.ingredient (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES public.recipe (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT unit_id_fkey FOREIGN KEY (unit_id)
        REFERENCES public.unit (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE public.recipe_tag
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    recipe_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    CONSTRAINT recipe_tag_pkey PRIMARY KEY (id),
    CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES public.recipe (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT tag_id_fkey FOREIGN KEY (tag_id)
        REFERENCES public.tag (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE public.favorites
(
    recipe_id bigint NOT NULL,
    user_id bigint NOT NULL,
    collection character varying COLLATE pg_catalog."default",
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    CONSTRAINT favorites_pkey PRIMARY KEY (id),
    CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES public.recipe (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE public.cart
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    recipe_id bigint,
    user_id bigint NOT NULL,
    measure character varying COLLATE pg_catalog."default" NOT NULL,
    ingredient character varying COLLATE pg_catalog."default" NOT NULL,
    recipe_ingredient_id bigint,
    done boolean NOT NULL DEFAULT false,
    "order" integer NOT NULL,
    date date,
    title character varying COLLATE pg_catalog."default",
    CONSTRAINT cart_pkey PRIMARY KEY (id),
    CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES public.recipe (id) MATCH SIMPLE
        ON UPDATE SET NULL
        ON DELETE SET NULL
        NOT VALID,
    CONSTRAINT recipe_ingredient_id FOREIGN KEY (recipe_ingredient_id)
        REFERENCES public.recipe_ingredient (id) MATCH SIMPLE
        ON UPDATE SET NULL
        ON DELETE SET NULL
        NOT VALID,
    CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE public.calendar
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    recipe_id bigint,
    user_id bigint NOT NULL,
    date date NOT NULL,
    portions numeric,
    "order" numeric NOT NULL,
    title character varying COLLATE pg_catalog."default",
    CONSTRAINT calendar_pkey PRIMARY KEY (id),
    CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES public.recipe (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);