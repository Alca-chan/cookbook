INSERT INTO "user" (username, email, password)
VALUES ('test1', 'test@test.test', '$2a$10$Ac1OWKsHcyjrNsVcK5DMuOepiHwRBnxHTkjCLaRMNGuEOqtpGL8Gy');
-- username: test1, password: 123456

INSERT INTO equipment (name)
VALUES ('Tlakový hrnec'),
       ('Fritéza'),
       ('Mixér'),
       ('Trouba'),
       ('Mikrovlnná trouba'),
       ('Domácí pekárna'),
       ('Tyčový mixér'),
       ('Šlehač'),
       ('Zmrzlinovač');

INSERT INTO ingredient (name_one, name_few, name_many, name_unit, name_part, name_base, energy_kj, energy_kcal, fat,
                        saturated_fatty_acid, monounsaturated_fatty_acid, polyunsaturated_fatty_acid, carbohydrate,
                        sugar, polyol, starch, fiber, protein, salt)
VALUES ('ananas', 'ananasy', 'ananasů', 'ananasu', 'ananasu', 'ananas', 227, 54, 0.19, null, null, null, 13, 10, null,null, 2, 0.49, null),
       ('angrešt','angrešty','angreštů','angreštu','angreštu','angrešt',183,44,0.2,0.02,0.02,0.07,10,7,null,null,3,0.82,null),
       ('arašíd','arašídy','arašídů','arašídů','arašídu','arašídy',2578,616,49,9,23,13,18,6,null,null,8,26,null),
       ('avokádo','avokáda','avokád','avokáda','avokáda','avokádo',1037,248,23,3,7,2,6,null,null,null,5,2,null),
       ('čedar','čedary','čedarů','čedaru','čedaru','čedar',1706,407,32,20,9,1,3,0.36,null,null,null,26,null),
       ('cibule','cibule','cibulí','cibule','cibule','cibule',null,null,null,null,null,null,null,null,null,null,null,null,null),
       ('červená cibule','červené cibule','červených cibulí','červené cibule','červené cibule','červená cibule',190,45,0.1,null,null,null,10,7,null,null,2,2,null),
       ('česnek','česneky','česneků','česneku','česneku','česnek',532,127,0.27,0.06,0.01,0.16,25,3,null,null,2,6,null),
       ('čočka','čočky','čoček','čočky','čočky','čočka',1290,308,0.8,0.1,null,null,37,0.9,null,null,23,26,null),
       ('moučkový cukr','moučkové cukry','moučkových cukrů','moučkového cukru','moučkového cukru','moučkový cukr',1675,400,null,null,null,null,97,97,null,null,null,null,null),
       ('cukr','cukry','cukrů','cukru','cukru','cukr',1680,401,0,0,0,0,100,100,null,null,0,0,null),
       ('třtinový cukr','třtinové cukry','třtinových cukrů','třtinového cukru','třtinového cukru','třtinový cukr',1682,402,0,0,0,0,99,97,null,null,null,0,null),
       ('hladká mouka','hladké mouky','hladkých mouk','hladké mouky','hladké mouky','hladká mouka',1466,350,2,0.23,0.41,0.78,73,2,null,null,3,11,null),
       ('mrkev','mrkve','mrkví','mrkve','mrkve','mrkev',148,35,0.22,0.04,null,0.12,7,6,null,null,4,1,null),
       ('mléko','mléka','mlék','mléka','mléka','mléko',267,64,4,2,0.97,0.12,5,5,null,null,null,3,null),
       ('voda','vody','vod','vody','vody','voda',0,0,0,0,0,0,0,0,0,0,0,0,0),
       ('brambora','brambory','brambor','brambor','brambory','brambora',371,89,0.2,null,null,null,19,1,null,null,2,2,null),
       ('vejce','vejce','vajec','vajec','vejce','vejce',632,151,11,3,4,2,0.94,0.34,null,null,null,12,null),
       ('žloutek','žloutky','žloutků','žloutků','žloutku','žloutek',1483,354,31,10,13,6,1,0.23,null,null,null,17,0.15),
       ('bílek','bílky','bílků','bílků','bílku','bílek',210,50,0.2,null,null,null,0.8,0.4,null,null,null,11,0.44),
       ('bobkový list','bobkové listy','bobkovýchlistů','bobkového listu','bobkového listu','bobkový list',1566,374,5,null,null,null,71,null,null,null,null,9,0.27),
       ('mascarpone','mascarpone','mascarpone','mascarpone','mascarpone','mascarpone',1582,378,40,28,null,null,3,3,null,null,0.1,3,0.1),
       ('smetana ke šlehání','smetany ke šlehání','smetan ke šlehání','smetany ke šlehání','smetany ke šlehání','smetana ke šlehání',1232,294,31,22,10,null,3,3,null,null,null,2,0.1),
       ('smetana na vaření','smetany na vaření','smetan na vaření','smetany na vaření','smetany na vaření','smetana na vaření',563,134,12,7,null,null,4,4,null,null,null,3,0.1),
       ('zakysaná smetana','zakysané smetany','zakysaných smetan','zakysané smetany','zakysané smetany','zakysaná smetana',561,134,12,8,null,null,4,4,null,null,null,3,0.1),
       ('dlouhý piškot','dlouhé piškoty','dlouhých piškotů','dlouhých piškotů','dlouhého piškotu','dlouhé piškoty',1631,390,3,1,null,null,81,40,null,null,2,9,0.25),
       ('káva','kávy','káv','kávy','kávy','káva',8,2,0,0,0,0,0.4,null,null,null,0,0.1,null),
       ('rum','rumy','rumů','rumu','rumu','rum',919,220,0,0,0,0,0,0,null,null,0,0,null),
       ('kakao','kakaa','kakaí','kakaa','kakaa','kakao',1278,305,12,7,null,null,12,0.5,null,null,35,21,2),
       ('pepř','pepře','pepřů','pepře','pepře','pepř',null,null,null,null,null,null,null,null,null,null,null,null,null),
       ('sůl','soli','solí','soli','soli','sůl',null,null,null,null,null,null,null,null,null,null,null,null,null),
       ('borůvka','borůvky','borůvek','borůvek','borůvky','borůvky',null,null,null,null,null,null,null,null,null,null,null,null,null);

INSERT INTO tag (name)
VALUES ('Rychlé'),
       ('Snídaně'),
       ('Těstoviny'),
       ('Dětské'),
       ('Pečené'),
       ('Slavnostní'),
       ('Zákusek'),
       ('Sladké'),
       ('Vegetariánské'),
       ('Bezlepkové'),
       ('Polévka'),
       ('Tradiční');

INSERT INTO unit (symbol, name_one, name_few, name_many, name_part, measurable)
VALUES ('mg', 'miligram', 'miligramy', 'miligramů', 'miligramu', true),
       ('g', 'gram', 'gramy', 'gramů', 'gramu', true),
       ('dkg', 'dekagram', 'dekagramy', 'dekagramů', 'dekagramu', true),
       ('kg', 'kilogram', 'kilogramy', 'kilogramů', 'kilogramu', true),
       ('ml', 'mililitr', 'mililitry', 'mililitrů', 'mililitru', true),
       ('cl', 'centilitr', 'centilitry', 'centilitrů', 'centilitru', true),
       ('dl', 'decilitr', 'decilitry', 'decilitrů', 'decilitru', true),
       ('l', 'litr', 'litry', 'litrů', 'litru', true),
       ('špetka', 'špetka', 'špetky', 'špetek', 'špetky', false),
       ('lžička', 'lžička', 'lžičky', 'lžiček', 'lžičky', true),
       ('lžíce', 'lžíce', 'lžíce', 'lžic', 'lžíce', true),
       ('hrnek', 'hrnek', 'hrnky', 'hrnků', 'hrnku', true),
       ('ks', 'kus', 'kusy', 'kusů', 'kusu', true),
       ('sáček', 'sáček', 'sáčky', 'sáčků', 'šáčku', true),
       ('balení', 'balení', 'balení', 'balení', 'balení', true),
       ('kelímek', 'kelímek', 'kelímky', 'kelímků', 'kelímku', true),
       ('dle chuti', 'dle chuti', 'dle chuti', 'dle chuti', 'dle chuti', false);

INSERT INTO recipe (title, description, method, "time", time_prep, votes, user_id, image, date, portions, portions_note)
VALUES ('Tiramisu', 'Jednoduché tiramisu bez vajíček',
        '["Ze smetany ke šlehání vyšleháme šlehačku.","Ve druhé nádobě vyšleháme do hladka mascarpone, zakysanou smetanu a moučkový cukr.","K vyšlehané směsi přidáme šlehačku a promícháme, aby vznikl hladký krém.","Uvaříme kávu do hrnku, osladíme pár lžičkami cukru a přidáme trochu rumu.","Jako nádobu na tiramisu použijeme hranatou dortovou formu, větší plastovou krabičku nebo skleněnou zapékací mísu. Nádobu vyložíme alobalem.","Dlouhé piškoty namáčíme v kávě s rumem a klademe na dno nádoby těsně vedle sebe.","Až máme hotovou vrstvu piškotů, potřeme ji polovinou krému.","Znovu namáčíme piškoty a klademe je na krém.","Nakonec potřeme piškoty zbytkem krému, uhladíme a posypeme kakaem.","Přikryjeme víčkem, fólií nebo alobalem a dáme do lednice vychladit, nejlépe přes noc.","Před podáváním vytáhneme opatrně z krabičky a nakrájíme."]',
        480, 30, 0, 1, null, '2021-09-23', 1, 'dóza cca 20x30 cm');

INSERT INTO public.recipe_equipment(recipe_id, equipment_id)
VALUES (1, 8);

INSERT INTO public.recipe_tag(recipe_id, tag_id)
VALUES (1, 7),
       (1, 8);

INSERT INTO public.recipe_ingredient(recipe_id, ingredient_id, measure, unit_id, note, section, section_order, "order")
VALUES (1, 29, 2, 11, null, 'Na posypání', 2, 0),
       (1, 26, 1, 15, null, 'Korpus', 0, 0),
       (1, 27, 1, 12, 'silné a oslazené', 'Korpus', 0, 1),
       (1, 28, 2, 11, null, 'Korpus', 0, 2),
       (1, 22, 1, 16, '200 - 250 g', 'Krém', 1, 0),
       (1, 25, 1, 16, null, 'Krém', 1, 1),
       (1, 23, 1, 16, null, 'Krém', 1, 2),
       (1, 10, 3, 11, null, 'Krém', 1, 3);

INSERT INTO public.favorites(recipe_id, user_id, collection)
VALUES (1, 1, null);

INSERT INTO public.calendar(
    recipe_id, user_id, date, portions, "order", title)
VALUES (1, 1, '01-01-2022', 1, 0, null);