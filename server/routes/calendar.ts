import {CalendarDB} from "../types";
import {pool} from "../index";
import format from "pg-format";

module.exports = (api) => {
  /**
   * get calendar by user id
   * params:
   *   id:
   *     description: user id
   */
  api.get("/calendar/:id", (request, response) => {
    const userId = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM calendar WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get calendar for current user
   */
  api.get("/user/calendar", (request, response) => {
    if (!request.user) {
      response.send([])
      return
    }

    const userId = parseInt(request.user.id)

    pool.query(
      "SELECT * FROM calendar WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create calendar event for current user
   * body:
   *   type: {
   *     recipeId:
   *       type: string
   *       description: id of the recipe to add to calendar
   *     date:
   *       type: string
   *       description: event date
   *     portions:
   *       type: number
   *       description: number of portions
   *     order:
   *       type: number
   *       description: event's order in the day
   *     title:
   *       type: string
   *       description: event's title
   *   }
   */
  api.post("/calendar", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const calendarDB: CalendarDB = request.body

    pool.query(
      "INSERT INTO calendar (recipe_id, user_id, date, portions, \"order\", title) VALUES ($1, $2, $3, $4, $5, $6)",
      [
        calendarDB.recipeId,
        userId,
        calendarDB.date,
        calendarDB.portions,
        calendarDB.order,
        calendarDB.title
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * update calendar event for current user
   * body:
   *   type: {
   *     id:
   *       type: string
   *       description: id of the event to update
   *     recipeId:
   *       type: string
   *       description: new recipe id
   *     date:
   *       type: string
   *       description: new date
   *     portions:
   *       type: number
   *       description: new portions value
   *     order:
   *       type: number
   *       description: new order value
   *     title:
   *       type: string
   *       description: new event title
   *   }
   */
  api.put("/calendar", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const calendarDB: CalendarDB = request.body
    const id = parseInt(request.body.id)

    pool.query(
      "UPDATE calendar SET recipe_id = $3, date = $4, portions = $5, \"order\" = $6, title = $7 WHERE id = $1 AND user_id = $2",
      [
        id,
        userId,
        calendarDB.recipeId,
        calendarDB.date,
        calendarDB.portions,
        calendarDB.order,
        calendarDB.title
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * move calendar events to a different date for current user
   * body:
   *   type: {
   *     ids:
   *       type: string[]
   *       description: id of events to move
   *     date:
   *       type: string
   *       description: new date to which the events will be moved
   *   }
   */
  api.put("/calendar/move", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const date = request.body.date
    const ids = request.body.ids

    pool.query(
      "UPDATE calendar SET date = $3 WHERE id = ANY($1::bigint[]) AND user_id = $2",
      [
        ids,
        userId,
        date
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * update events order for current user
   * body:
   *   type: {
   *     changed:
   *       type: {
   *         id:
   *           type: string
   *           description: id of the event to update
   *         order:
   *           type: number
   *           description: new order for the event
   *       }
   *       description: events to move
   *   }
   */
  api.put("/calendar/order", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const changed: { id: string, order: number }[] = request.body.changed

    const values = changed.map(i =>
      [
        i.id,
        i.order
      ]
    )

    pool.query(
      format('UPDATE calendar SET \"order\"=t.order::numeric FROM (VALUES %L) AS t(id, \"order\") WHERE t.id::bigint = calendar.id AND user_id = $1', values),
      [
        userId
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete events from calendar for current user
   * body:
   *   type: {
   *     ids:
   *       type: string[]
   *       description: ids of the events to delete
   *   }
   */
  api.delete("/calendar", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const ids = request.body.ids

    pool.query(
      "DELETE FROM calendar WHERE id = ANY($1::bigint[]) AND user_id = $2",
      [ids, userId],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })
}