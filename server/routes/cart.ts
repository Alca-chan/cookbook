import {pool} from "../index";
import {CartItem, CartItemToAdd} from "../types";
import format from "pg-format";

module.exports = (api) => {
  /**
   * get shopping list by user id
   * params:
   *   id:
   *     description: user id
   */
  api.get("/cart/:id", (request, response) => {
    const userId = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM cart WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get shopping list for current user
   */
  api.get("/user/cart", (request, response) => {
    if (!request.user) {
      response.send([])
      return
    }

    const userId = parseInt(request.user.id)

    pool.query(
      "SELECT * FROM cart WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new shopping list item for current user
   * body:
   *   type: {
   *     recipeId:
   *       type: string | null
   *       description: recipe id
   *     measure:
   *       type: string
   *       description: ingredient measure
   *     ingredient:
   *       type: string
   *       description: ingredient name
   *     recipeIngredientId:
   *       type: string | null
   *       description: recipe_ingredient id
   *     order:
   *       type: number
   *       description: order of the item in the shopping list
   *     date:
   *       type: string | undefined
   *       description: date of the shopping list
   *     title:
   *       type: string | null
   *       description: title of the shopping list
   *   }
   */
  api.post("/cart", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const item: CartItemToAdd = request.body

    pool.query(
      "INSERT INTO cart (recipe_id, user_id, measure, ingredient, recipe_ingredient_id, \"order\", \"date\", title) VALUES ($1, $2, $3, $4, $5, $6, $7)",
      [
        item.recipeId,
        userId,
        item.measure,
        item.ingredient,
        item.recipeIngredientId,
        item.order,
        item.date,
        item.title
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * create new shopping list items for current user
   * body:
   *   type: {
   *     items:
   *       type: {
   *         recipeId:
   *           type: string | null
   *           description: recipe id
   *         measure:
   *           type: string
   *           description: ingredient measure
   *         ingredient:
   *           type: string
   *           description: ingredient name
   *         recipeIngredientId:
   *           type: string | null
   *           description: recipe_ingredient id
   *         order:
   *           type: number
   *           description: order of the item in the shopping list
   *         date:
   *           type: string | undefined
   *           description: date of the shopping list
   *         title:
   *           type: string | null
   *           description: title of the shopping list
   *       }[]
   *       description: items to add to shopping list
   *   }
   */
  api.post("/cart/all", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const cartItems: CartItemToAdd[] = request.body.items
    const date: string = request.body.date

    const query = "INSERT INTO cart (recipe_id, user_id, measure, ingredient, recipe_ingredient_id, \"order\", \"date\", title)  VALUES " + cartItems.map(item =>
      "("
      + item.recipeId + ","
      + userId + ",'"
      + item.measure + "','"
      + item.ingredient + "',"
      + item.recipeIngredientId + ","
      + item.order + ","
      + (date ? ("'" + date + "'") : null) + ",'"
      + item.title +
      "')"
    )

    pool.query(
      query,
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * update done value of selected items in the shopping list for current user
   * body:
   *   type: {
   *     ids:
   *       type: string[]
   *       description: ids of the items to update
   *     done:
   *       type: boolean
   *       description: new done value
   *   }
   */
  api.put("/cart/done", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const ids = request.body.ids
    const done = request.body.done

    pool.query(
      "UPDATE cart SET done = $2 WHERE id = ANY($1::int[])",
      [
        ids,
        done
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * update order of selected items in the shopping list for current user
   * body:
   *   type: {
   *     items:
   *       type: {
   *         recipeId:
   *           type: string | null
   *           description: recipe id
   *         order:
   *           type: number
   *           description: order of the item in the shopping list
   *         date:
   *           type: string | undefined
   *           description: date of the shopping list
   *         title:
   *           type: string | null
   *           description: title of the shopping list
   *       }[]
   *       description: items to update
   *   }
   */
  api.put("/cart/order", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const cartItems: { id: string, recipeId: string, order: number, date: string | undefined, title: string | null }[] = request.body.items

    const values = cartItems.map(i =>
      [
        i.id,
        i.order,
        (i.date ? i.date : null),
        i.title
      ]
    )

    pool.query(
      format("UPDATE cart SET \"order\" = t.order::numeric, \"date\" = t.date::date, title = t.title FROM (VALUES %L) AS t(id, \"order\", \"date\", title) WHERE t.id::bigint = cart.id", values),
      [],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * update items in the shopping list for current user
   * body:
   *   type: {
   *     items:
   *       type: {
   *         id:
   *           type: string
   *           description: id of the item in the shopping list
   *         recipeId:
   *           type: string | null
   *           description: recipe id
   *         measure:
   *           type: string
   *           description: ingredient measure
   *         ingredient:
   *           type: string
   *           description: ingredient name
   *         recipeIngredientId:
   *           type: string | null
   *           description: recipe_ingredient id
   *         order:
   *           type: number
   *           description: order of the item in the shopping list
   *         date:
   *           type: string | undefined
   *           description: date of the shopping list
   *         title:
   *           type: string | null
   *           description: title of the shopping list
   *       }[]
   *       description: items to update
   *   }
   */
  api.put("/cart", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const cartItems: CartItem[] = request.body.items

    const values = cartItems.map(i =>
      [
        i.id,
        i.recipeId,
        i.recipeIngredientId,
        i.measure,
        i.ingredient,
        i.done,
        i.order,
        (i.date ? i.date : null),
        i.title
      ]
    )

    pool.query(
      format("UPDATE cart SET recipe_id = t.recipe_id::bigint, measure = t.measure, ingredient = t.ingredient, recipe_ingredient_id = t.recipe_ingredient_id::bigint, done = t.done::boolean, \"order\" = t.order::int, \"date\" = t.date::date, title=t.title FROM (VALUES %L) AS t(id, recipe_id, recipe_ingredient_id, measure, ingredient, done, \"order\", \"date\", title) WHERE t.id::bigint = cart.id", values),
      [],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete items from shopping list for current user
   * body:
   *   type: {
   *     ids:
   *       type: string[]
   *       description: ids of items to delete
   *   }
   */
  api.delete("/cart", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const ids = request.body.ids
    const userId = parseInt(request.user.id)

    pool.query(
      "DELETE FROM cart WHERE id = ANY($1) AND user_id = $2",
      [ids, userId],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete all shopping list items for recipe and date for current user
   * body:
   *   type: {
   *     recipeId:
   *       type: string
   *       description: id of the recipe
   *     date:
   *       type: string | null
   *       description: shopping list date
   *   }
   */
  api.delete("/cart/all", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const recipeId = request.body.recipeId
    const date = request.body.date
    const userId = parseInt(request.user.id)

    pool.query(
      "DELETE FROM cart WHERE recipe_id = $1 AND user_id = $2 AND date = $3",
      [recipeId, userId, date],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })
}