import {Equipment} from "../types";
import {pool} from "../index";

module.exports = (api) => {
  /**
   * get all equipment
   */
  api.get("/equipment", (request, response) => {
    pool.query(
      "SELECT * FROM equipment LEFT JOIN (SELECT equipment_id, count(recipe_id) FROM equipment INNER JOIN recipe_equipment re ON equipment.id = re.equipment_id GROUP BY equipment_id) AS c ON equipment.id = c.equipment_id ORDER BY c.count DESC NULLS LAST",
      [],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get equipment by ids
   * params:
   *   ids:
   *     description: equipment ids list
   */
  api.get("/equipment/:ids", (request, response) => {
    const ids = request.params.ids.split(",")

    pool.query(
      "SELECT * FROM equipment WHERE id = ANY($1::int[])",
      [ids],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new equipment
   * body:
   *   type: {
   *     name:
   *       type: string
   *       description: equipment name
   *   }
   */
  api.post("/equipment", (request, response) => {
    const equipment: Equipment = request.body

    pool.query(
      "INSERT INTO equipment (name) VALUES ($1)",
      [equipment.name],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * update equipment
   * params:
   *   id:
   *     description: equipment id
   * body:
   *   type: {
   *     name:
   *       type: string
   *       description: equipment name
   *   }
   */
  api.put("/equipment/:id", (request, response) => {
    const equipment: Equipment = request.body
    const id = parseInt(request.params.id)

    pool.query(
      "UPDATE equipment SET name = $2 WHERE id = $1",
      [
        id,
        equipment.name
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete equipment
   * params:
   *   id:
   *     description: equipment id
   */
  api.delete("/equipment/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "DELETE FROM equipment WHERE id = $1",
      [id],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })
}