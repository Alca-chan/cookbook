import {pool} from "../index";

module.exports = (api) => {
  /**
   * get favorites by user id
   * params:
   *   id:
   *     description: user id
   */
  api.get("/favorites/:id", (request, response) => {
    const userId = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM favorites WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get favorites for current user
   */
  api.get("/user/favorites", (request, response) => {
    if (!request.user) {
      response.send([])
      return
    }

    const userId = parseInt(request.user.id)

    pool.query(
      "SELECT * FROM favorites WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new favorite item for current user
   * body:
   *   type: {
   *     recipeId:
   *       type: string
   *       description: recipe id
   *     collection:
   *       type: string | null
   *       description: name of the collection where the recipe is added
   *   }
   */
  api.post("/favorites", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const recipeId = request.body.recipeId
    const collection = request.body.collection

    if (!recipeId) {
      response.sendStatus(400)
      return
    }

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            // release the client back to the pool
            done()
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (error) => {
        if (shouldAbort(error)) return

        client.query(
          "SELECT * FROM favorites WHERE user_id = $1 AND recipe_id = $2",
          [userId, recipeId],
          async (error, results) => {
            if (shouldAbort(error)) return

            if (!results.rows || results.rows.length > 0) { // user already has this recipe in favorites
              done()
              response.sendStatus(200)
              return
            }

            client.query(
              "INSERT INTO favorites (user_id, recipe_id, collection) VALUES ($1, $2, $3)",
              [userId, recipeId, collection],
              async (error) => {
                if (shouldAbort(error)) return

                client.query(
                  "UPDATE recipe SET votes = votes + 1 WHERE id = $1 RETURNING votes",
                  [recipeId],
                  async (error, results) => {
                    if (shouldAbort(error)) return

                    client.query('COMMIT', (error) => {
                      if (error) {
                        console.error('Error committing transaction', error.stack)
                        response.sendStatus(500)
                      }
                      done()
                      response.status(201).send(results.rows[0])
                    })
                  }
                )
              }
            )
          }
        )
      })
    })
  })

  /**
   * update favorite item for current user
   * body:
   *   type: {
   *     recipeId:
   *       type: string
   *       description: recipe id
   *     collection:
   *       type: string | null
   *       description: name of the collection where the recipe is moved
   *   }
   */
  api.put("/favorites", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const recipeId = request.body.recipeId
    const userId = parseInt(request.user.id)
    const collection = request.body.collection

    pool.query(
      "UPDATE favorites SET collection = $3 WHERE recipe_id = $1 AND user_id = $2",
      [recipeId, userId, collection],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * delete item from favorites for current user
   * body:
   *   type: {
   *     recipeId:
   *       type: string
   *       description: id of the item to delete
   *   }
   */
  api.delete("/favorites", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const recipeId = request.body.recipeId
    const userId = parseInt(request.user.id)

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            // release the client back to the pool
            done()
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (error) => {
        if (shouldAbort(error)) return
        client.query(
          "DELETE FROM favorites WHERE recipe_id = $1 AND user_id = $2 RETURNING *",
          [recipeId, userId],
          async (error, results) => {
            if (shouldAbort(error)) return
            if (!results.rows || results.rows.length < 1) { // user did not have this recipe in favorites
              done()
              response.sendStatus(200)
              return
            }

            client.query(
              "UPDATE recipe SET votes = GREATEST(votes - 1, 0) WHERE id = $1 RETURNING votes",
              [recipeId],
              async (error, results) => {
                if (shouldAbort(error)) return

                client.query('COMMIT', (error) => {
                  if (error) {
                    console.error('Error committing transaction', error.stack)
                    response.sendStatus(500)
                  }
                  done()
                  response.status(200).send(results.rows[0])
                })
              }
            )
          }
        )
      })
    })
  })
}