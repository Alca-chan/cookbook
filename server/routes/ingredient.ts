import {pool} from "../index";
import {Ingredient} from "../types";

module.exports = (api) => {
  /**
   * get all ingredients
   */
  api.get("/ingredients", (request, response) => {
    pool.query(
      "SELECT * FROM ingredient LEFT JOIN (SELECT ingredient_id, count(distinct recipe_id) FROM ingredient INNER JOIN recipe_ingredient ri ON ingredient.id = ri.ingredient_id GROUP BY ingredient_id) AS c ON ingredient.id = c.ingredient_id ORDER BY c.count DESC NULLS LAST",
      [],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get ingredients by ids
   * params:
   *   ids:
   *     description: ingredient ids list
   */
  api.get("/ingredients/:ids", (request, response) => {
    const ids = request.params.ids.split(",")

    pool.query(
      "SELECT * FROM ingredient WHERE id = ANY($1::int[])",
      [ids],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new ingredient
   * body:
   *   type: {
   *     nameOne:
   *       type: string | null
   *       description:  name form for 1 piece
   *     nameFew:
   *       type: string | null
   *       description: name form for 2 - 4 pieces
   *     nameMany:
   *       type: string | null
   *       description: name form for 5 or more pieces
   *     namePart:
   *       type: string | null
   *       description: name form for parts
   *     nameBase:
   *       type: string | null
   *       description: commonly used name form
   *     energyKj:
   *       type: number | null
   *       description: kJ on 100 g
   *     energyKcal:
   *       type: number | null
   *       description: kcal on 100 g
   *     fat:
   *       type: number | null
   *     saturatedFattyAcid:
   *       type: number | null
   *     monounsaturatedFattyAcid:
   *       type: number | null
   *     polyunsaturatedFattyAcid:
   *       type: number | null
   *     carbohydrate:
   *       type: number | null
   *     sugar:
   *       type: number | null
   *     polyol:
   *       type: number | null
   *     starch:
   *       type: number | null
   *     fiber:
   *       type: number | null
   *     protein:
   *       type: number | null
   *     salt:
   *       type: number | null
   *   }
   */
  api.post("/ingredients", (request, response) => {
    const ingredient: Ingredient = request.body

    pool.query(
      "INSERT INTO ingredient (name_one, name_few, name_many, name_unit, name_part, energy_kj, energy_kcal, fat, saturated_fatty_acid, monounsaturated_fatty_acid, polyunsaturated_fatty_acid, carbohydrate, sugar, polyol, starch, fiber, protein, salt) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)",
      [
        ingredient.nameOne,
        ingredient.nameFew,
        ingredient.nameMany,
        ingredient.namePart,
        ingredient.nameBase,
        ingredient.energyKj,
        ingredient.energyKcal,
        ingredient.fat,
        ingredient.saturatedFattyAcid,
        ingredient.monounsaturatedFattyAcid,
        ingredient.polyunsaturatedFattyAcid,
        ingredient.carbohydrate,
        ingredient.sugar,
        ingredient.polyol,
        ingredient.starch,
        ingredient.fiber,
        ingredient.protein,
        ingredient.salt
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * update ingredient
   * params:
   *   id:
   *     description: ingredient id
   * body:
   *   type: {
   *     nameOne:
   *       type: string | null
   *       description:  name form for 1 piece
   *     nameFew:
   *       type: string | null
   *       description: name form for 2 - 4 pieces
   *     nameMany:
   *       type: string | null
   *       description: name form for 5 or more pieces
   *     namePart:
   *       type: string | null
   *       description: name form for parts
   *     nameBase:
   *       type: string | null
   *       description: commonly used name form
   *     energyKj:
   *       type: number | null
   *       description: kJ on 100 g
   *     energyKcal:
   *       type: number | null
   *       description: kcal on 100 g
   *     fat:
   *       type: number | null
   *     saturatedFattyAcid:
   *       type: number | null
   *     monounsaturatedFattyAcid:
   *       type: number | null
   *     polyunsaturatedFattyAcid:
   *       type: number | null
   *     carbohydrate:
   *       type: number | null
   *     sugar:
   *       type: number | null
   *     polyol:
   *       type: number | null
   *     starch:
   *       type: number | null
   *     fiber:
   *       type: number | null
   *     protein:
   *       type: number | null
   *     salt:
   *       type: number | null
   *   }
   */
  api.put("/ingredients/:id", (request, response) => {
    const ingredient: Ingredient = request.body
    const id = parseInt(request.params.id)

    pool.query(
      "UPDATE ingredient SET name_one = $1, name_few = $2, name_many = $3, name_unit = $4, name_part = $5, energy_kj = $6, energy_kcal = $7, fat = $8, saturated_fatty_acid = $9, monounsaturated_fatty_acid = $10, polyunsaturated_fatty_acid = $11, carbohydrate = $12, sugar = $13, polyol = $14, starch = $15, fiber = $16, protein = $17, salt = $18 WHERE id = $19",
      [
        ingredient.nameOne,
        ingredient.nameFew,
        ingredient.nameMany,
        ingredient.namePart,
        ingredient.nameBase,
        ingredient.energyKj,
        ingredient.energyKcal,
        ingredient.fat,
        ingredient.saturatedFattyAcid,
        ingredient.monounsaturatedFattyAcid,
        ingredient.polyunsaturatedFattyAcid,
        ingredient.carbohydrate,
        ingredient.sugar,
        ingredient.polyol,
        ingredient.starch,
        ingredient.fiber,
        ingredient.protein,
        ingredient.salt,
        id
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete ingredient
   * params:
   *   id:
   *     description: ingredient id
   */
  api.delete("/ingredients/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "DELETE FROM ingredient WHERE id = $1",
      [id],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })
}