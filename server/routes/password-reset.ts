import dotenv from "dotenv";
import {pool} from "../index";
import {User} from "../types";
import bcrypt = require('bcryptjs');

const crypto = require("crypto")

dotenv.config({path: "../variables.env"})
const mgConfig = {
  apiKey: process.env.EMAIL_APIKEY,
  domain: process.env.EMAIL_DOMAIN,
  host: process.env.EMAIL_HOST
}
const mailgun = require('mailgun-js')(mgConfig)

type EmailMessage = {
  subject: string,
  text: string,
  html: string
}

const TOKEN_EXPIRATION = 1000 * 60 * 60 * 24 * 2 // 2 days

const resetPasswordMessage = (user: User, link: string, baseurl: string): EmailMessage => {
  return {
    subject: "Obnovení hesla",
    text:
      "Ahoj, " + user.username + "!\n" +
      "\n" +
      "Otevři následující odkaz pro obnovení hesla na Dnes uvařím.\n" +
      link + "\n" +
      "Pokud jsi nežádal/a o resetování hesla, nemusíš tomuto mailu dále věnovat pozornost.",
    html:
      "<p>Ahoj, " + user.username + "!</p>" +
      "<br/>" +
      "<p>Klikni na následující <a href='" + link + "'>odkaz</a> pro obnovení hesla na <a href='" + baseurl + "'>Dnes uvařím</a>.</p>" +
      "<p>Pokud jsi nežádal/a o resetování hesla, nemusíš tomuto mailu dále věnovat pozornost.</p>" +
      "<br/>" +
      "<p>Je-li odkaz nefunkční, můžeš ho zkopírovat a otevřít: <a href='" + link + "'>" + link + "</a></p>"
  }
}

const sendEmail = async (recipient: string, message: EmailMessage) => {
  new Promise<void>((resolve, reject) => {
    const data = {
      from: "Dnes uvařím <info@" + mgConfig.domain + ">",
      to: recipient,
      subject: message.subject,
      text: message.text,
      html: message.html
    }

    mailgun.messages().send(data, (error) => {
      if (error) {
        console.log(error, "email not sent")
        return reject(error)
      }
      console.log("email sent successfully")
      return resolve()
    })
  })
}

module.exports = (api) => {
  /**
   * request for password reset, an email with reset link is send to the user
   * body:
   *   type: {
   *     email:
   *       type: string
   *       description: email of the user
   *   }
   */
  api.post("/password-reset", (request, response) => {
    const email: string = request.body.email

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            // release the client back to the pool
            done()
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (error) => {
        if (shouldAbort(error)) return

        client.query(
          "SELECT * FROM \"user\" WHERE email=$1",
          [email],
          async (error, results) => {
            if (shouldAbort(error)) return

            if (!results.rows || results.rows.length < 1) {
              done()
              response.sendStatus(200)
              return
            }

            const user: User = results.rows[0]
            const token: string = await crypto.randomBytes(32).toString("hex")

            client.query(
              "INSERT INTO token (token, user_id) VALUES ($1, $2)",
              [token, user.id],
              async (error) => {
                if (shouldAbort(error)) return

                const link = `${process.env.BASE_URL}/password-reset/${token}`
                await sendEmail(email, resetPasswordMessage(user, link, process.env.BASE_URL))

                client.query('COMMIT', (error) => {
                  if (error) {
                    console.error('Error committing transaction', error.stack)
                    response.sendStatus(500)
                  }
                  done()
                  response.sendStatus(201)
                })
              }
            )
          }
        )
      })
    })
  })

  /**
   * resets password for the user
   * body:
   *   type: {
   *     token:
   *       type: string
   *       description: token that was sent to the user via email
   *     newPassword:
   *       type: string
   *       description: new password for the user
   *   }
   */
  api.post("/password-reset/:token", (request, response) => {
    const token: string = request.params.token
    const newPassword: string = request.body.password

    if (!token || !newPassword) {
      response.sendStatus(400)
    }

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            // release the client back to the pool
            done()
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (error) => {
        if (shouldAbort(error)) return

        client.query(
          "SELECT * FROM token WHERE token=$1",
          [token],
          async (error, results) => {
            if (shouldAbort(error)) return

            if (!results.rows || results.rows.length < 1) {
              done()
              response.status(400).send("Invalid or expired token")
              return
            }

            const token: { token: string, user_id: string, created_at: string } = results.rows[0]

            // if the token is expired, delete it
            if (new Date().valueOf() - (new Date(token.created_at).valueOf() + TOKEN_EXPIRATION) > 0) {
              client.query(
                "DELETE FROM token WHERE token=$1",
                [token.token],
                async (error) => {
                  if (shouldAbort(error)) return

                  client.query('COMMIT', (error) => {
                    if (error) {
                      console.error('Error committing transaction', error.stack)
                      response.sendStatus(500)
                    }
                    done()
                    response.status(400).send("Invalid or expired token")
                    return
                  })
                }
              )
            } else {
              const hashedPassword = await bcrypt.hash(newPassword, 10)

              client.query(
                "UPDATE \"user\" SET password=$1 WHERE id=$2",
                [hashedPassword, token.user_id],
                async (error) => {
                  if (shouldAbort(error)) return

                  client.query(
                    "DELETE FROM token WHERE token=$1",
                    [token.token],
                    async (error) => {
                      if (shouldAbort(error)) return

                      client.query('COMMIT', (error) => {
                        if (error) {
                          console.error('Error committing transaction', error.stack)
                          response.sendStatus(500)
                        }
                        done()
                        response.status(200).send("Password was successfully changed")
                      })
                    }
                  )
                }
              )
            }
          }
        )
      })
    })
  })
}