import {pool} from "../index";
import {getOrderByForRecipe} from "../utils/utils";

module.exports = (api) => {
  /**
   * get ingredients by recipe id
   * params:
   *   id:
   *     description: id of the recipe
   */
  api.get("/recipe/:id/ingredients", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM recipe_ingredient WHERE recipe_id = $1",
      [id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get recipe_ingredient by id
   * params:
   *   id:
   *     description: id of the recipe_ingredient
   */
  api.get("/recipe/ingredient/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM recipe_ingredient WHERE id = $1",
      [id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get recipes with any of the given ingredient ids and recipe title
   * params:
   *   ids:
   *     description: ids of the ingredients the recipe uses
   * query:
   *   string:
   *     description: text to search in recipe title
   *   order:
   *     description: how to order the results
   */
  api.get("/ingredient/any/:ids/recipes", (request, response) => {
    const ids = request.params.ids.split(",")
    const string = request.query.string
    const orderBy = getOrderByForRecipe(request.query.order)

    const query = string ?
      {
        text: "SELECT (recipe_id) FROM recipe INNER JOIN recipe_ingredient ON (recipe_ingredient.recipe_id = recipe.id) WHERE ingredient_id = ANY($1::int[]) AND recipe.title ILIKE $2 ORDER BY $3",
        values: [
          ids,
          '%' + string + '%',
          orderBy
        ]
      }
      :
      {
        text: "SELECT (s.recipe_id) FROM (SELECT recipe_id FROM recipe_ingredient WHERE ingredient_id = ANY($1::int[])) as s INNER JOIN recipe ON s.recipe_id = recipe.id ORDER BY $2",
        values: [ids, orderBy]
      }

    pool.query(
      query,
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get recipes with all of the given ingredient ids and recipe title
   * params:
   *   ids:
   *     description: ids of the ingredients the recipe uses
   * query:
   *   string:
   *     description: text to search in recipe title
   *   order:
   *     description: how to order the results
   */
  api.get("/ingredient/all/:ids/recipes", (request, response) => {
    const ids = request.params.ids.split(",")
    const string = request.query.string
    const orderBy = getOrderByForRecipe(request.query.order)

    const query = string ?
      {
        text: "SELECT (s.recipe_id) FROM (SELECT recipe_id FROM recipe INNER JOIN recipe_ingredient ON (recipe_ingredient.recipe_id = recipe.id) WHERE ingredient_id = ANY($1::int[]) AND recipe.title ILIKE $2 GROUP BY recipe_id HAVING COUNT(distinct ingredient_id) >= $3) as s INNER JOIN recipe ON s.recipe_id = recipe.id ORDER BY $4",
        values: [
          ids,
          '%' + string + '%',
          ids.length,
          orderBy
        ]
      }
      :
      {
        text: "SELECT (s.recipe_id) FROM (SELECT recipe_id FROM recipe_ingredient WHERE ingredient_id = ANY($1::int[]) GROUP BY recipe_id HAVING COUNT(distinct ingredient_id) >= $2) as s INNER JOIN recipe ON s.recipe_id = recipe.id ORDER BY $3",
        values: [
          ids,
          ids.length,
          orderBy
        ]
      }


    pool.query(
      query,
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  // create, update, delete are done only through recipe
}