import {EquipmentStatus, IngredientsStatus, Recipe, TagsStatus} from "../types";
import {pool} from "../index";
import format from "pg-format";
import {getOrderByForRecipe} from "../utils/utils";

module.exports = (api) => {
  /**
   * get all recipes
   */
  api.get("/recipes", (request, response) => {
    pool.query(
      "SELECT * FROM recipe",
      [],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get recipes by title
   * query:
   *   string:
   *     description: text to search in recipe title
   *   order:
   *     description: how to order the results
   */
  api.get("/recipes/title", (request, response) => {
    const string = request.query.string
    const orderBy = getOrderByForRecipe(request.query.order)

    pool.query(
      "SELECT * FROM recipe WHERE title ILIKE $1 ORDER BY $2",
      ['%' + string + '%', orderBy],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get recipe by id
   * params:
   *   id:
   *     description: recipe id
   */
  api.get("/recipes/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM recipe WHERE id = $1",
      [id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get recipe by user
   * params:
   *   id:
   *     description: user id
   */
  api.get("/recipes/user/:id", (request, response) => {
    const userId = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM recipe WHERE user_id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get n random recipes
   * params:
   *   n:
   *     description: number of recipes to return
   */
  api.get("/recipes/random/:n", (request, response) => {
    const limit = request.params.n

    pool.query(
      "SELECT * FROM recipe ORDER BY RANDOM() LIMIT $1",
      [limit],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new recipe for current user
   * body:
   *   type: {
   *     title:
   *       type: string
   *       description: title of the recipe
   *     description:
   *       type: string
   *       description: short recipe description
   *     ingredients:
   *       type: {
   *         ingredients: {
   *           ingredient:
   *             type: {
   *               id:
   *                 type: string
   *                 description: id of the ingredient
   *             }
   *             description: ingredient to add to recipe
   *           measure:
   *             type: number | null
   *             description: measure of the ingredient
   *           unit:
   *             type: {
   *               id:
   *                 type: string
   *                 description: id of the unit
   *             }
   *             description: unit of the measure
   *           note:
   *             type: string
   *             description: note for the ingredient
   *           section:
   *             type: {
   *               title:
   *                 type: string | null
   *                 description: section's title
   *               order:
   *                 type: number
   *                 description: order of the section
   *             }
   *             description: section info
   *           order:
   *             type: number
   *             description: order in section
   *         }[]
   *       }[]
   *       description: ingredients to add with measure and unit
   *     time:
   *       type: number
   *       description: total time in minutes
   *     timePrep:
   *       type: number
   *       description: active preparation time in minutes
   *     image:
   *       type: string | null
   *       description: image in Base64
   *     date:
   *       type: string
   *       description: date in format "YYYY-MM-DD"
   *     portions:
   *       type: number
   *       description: number of portions
   *     portionsNote:
   *       type: string
   *       description: note for portions
   *   }
   */
  api.post("/recipes", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const recipe: Recipe = request.body.recipe
    const userId = parseInt(request.user.id)

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (err) => {
        if (shouldAbort(err)) return

        const methodString = "[" + recipe.method.map(s => "\"" + s.description + "\"") + "]"
        client.query(
          "INSERT INTO recipe (title, description, method, time, time_prep, votes, user_id, image, date, portions, portions_note) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING *",
          [
            recipe.title,
            recipe.description,
            methodString,
            recipe.time,
            recipe.timePrep,
            0,
            userId,
            recipe.image,
            recipe.date,
            recipe.portions,
            recipe.portionsNote
          ],
          (err, res) => {
            if (shouldAbort(err)) return

            const recipeId = res.rows[0].id
            const recipeIngredientsValues = recipe.ingredients.map(s => s.ingredients.map(i =>
              [
                recipeId,
                i.ingredient.id,
                (i.measure ?? null),
                i.unit.id,
                i.note,
                i.section.title,
                i.section.order,
                i.order
              ]
            )).flat()

            client.query(
              format("INSERT INTO recipe_ingredient (recipe_id, ingredient_id, measure, unit_id, note, section, section_order, \"order\") VALUES %L", recipeIngredientsValues),
              [],
              (err) => {
                if (shouldAbort(err)) return
                let error = false

                if (!error && recipe.requiredEquipment.length > 0) {
                  client.query(
                    format("INSERT INTO recipe_equipment (recipe_id, equipment_id) VALUES %L", recipe.requiredEquipment.map(e => [recipeId, e.id])),
                    [],
                    (err) => {
                      if (shouldAbort(err)) error = true
                    })
                }

                if (!error && recipe.tags.length > 0) {
                  client.query(
                    format("INSERT INTO recipe_tag (recipe_id, tag_id) VALUES %L", recipe.tags.map(t => [recipeId, t.id])),
                    [],
                    (err) => {
                      if (shouldAbort(err)) error = true
                    })
                }

                if (!error) {
                  client.query('COMMIT', (err) => {
                    if (err) {
                      console.error('Error committing transaction', err.stack)
                      response.sendStatus(500)
                    }
                    response.status(201).json(recipeId)
                  })
                }
                done()
              })
          })
      })
    })
  })

  /**
   * update recipe (current user has to be its author)
   * params:
   *   id:
   *     description: recipe id
   * body:
   *   type: {
   *     recipe:
   *       type: {
   *         title:
   *           type: string
   *           description: title of the recipe
   *         description:
   *           type: string
   *           description: short recipe description
   *         time:
   *           type: number
   *           description: total time in minutes
   *         timePrep:
   *           type: number
   *           description: active preparation time in minutes
   *         image:
   *           type: string | null
   *           description: image in Base64
   *         date:
   *           type: string
   *           description: date in format "YYYY-MM-DD"
   *         portions:
   *           type: number
   *           description: number of portions
   *         portionsNote:
   *           type: string
   *           description: note for portions
   *       }
   *       description: recipe to update
   *     ingredients:
   *       type: {
   *         toAdd:
   *           type: {
   *             recipeIngredientId:
   *               type: string
   *               description: id of the recipe_ingredient to update
   *             ingredient:
   *               type: {
   *                 id:
   *                   type: string
   *                   description: id of the ingredient
   *               }
   *               description: ingredient to add to recipe
   *             measure:
   *               type: number | null
   *               description: measure of the ingredient
   *             unit:
   *               type: {
   *                 id:
   *                   type: string
   *                   description: id of the unit
   *               }
   *               description: unit of the measure
   *             note:
   *               type: string
   *               description: note for the ingredient
   *             section:
   *               type: {
   *                 title:
   *                   type: string | null
   *                   description: section's title
   *                 order:
   *                   type: number
   *                   description: order of the section
   *               }
   *               description: section info
   *             order:
   *               type: number
   *               description: order in section
   *           }[]
   *           description: ingredients to add
   *         toUpdate:
   *           type: {
   *             recipeIngredientId:
   *               type: string
   *               description: id of the recipe_ingredient to update
   *             ingredient:
   *               type: {
   *                 id:
   *                   type: string
   *                   description: id of the ingredient
   *               }
   *               description: ingredient to add to recipe
   *             measure:
   *               type: number | null
   *               description: measure of the ingredient
   *             unit:
   *               type: {
   *                 id:
   *                   type: string
   *                   description: id of the unit
   *               }
   *               description: unit of the measure
   *             note:
   *               type: string
   *               description: note for the ingredient
   *             section:
   *               type: {
   *                 title:
   *                   type: string | null
   *                   description: section's title
   *                 order:
   *                   type: number
   *                   description: order of the section
   *               }
   *               description: section info
   *             order:
   *               type: number
   *               description: order in section
   *           }[]
   *           description: ingredients to update
   *         toDelete:
   *           type: {
   *             recipeIngredientId:
   *               type: string
   *               description: id of the recipe_ingredient to update
   *           }[]
   *           description: ingredients to delete
   *       }
   *       description: ingredients to add, update and delete
   *     tags:
   *       type: {
   *         toAdd:
   *           type: {
   *             id: id of the tag
   *           }[]
   *           description: tags to add
   *         toDelete:
   *           type: {
   *             id: id of the tag
   *           }[]
   *           description: tags to remove
   *       }
   *       description: tags to add and remove
   *     equipment:
   *       type: {
   *         toAdd:
   *           type: {
   *             id: id of the equipment
   *           }[]
   *           description: equipment to add
   *         toDelete:
   *           type: {
   *             id: id of the equipment
   *           }[]
   *           description: equipment to remove
   *       }
   *       description: equipment to add and remove
   *     date:
   *       type: string
   *       description: new date (can be the creation date or a new one) in format "YYYY-MM-DD"
   *   }
   */
  api.put("/recipes/:id", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const recipe: Recipe = request.body.recipe
    const userId = parseInt(request.user.id)
    const ingredients: IngredientsStatus = request.body.ingredients
    const tags: TagsStatus = request.body.tags
    const equipment: EquipmentStatus = request.body.equipment
    const date: string = request.body.date
    const id = parseInt(request.params.id)

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (err) => {
        if (shouldAbort(err)) return

        client.query("UPDATE recipe SET title = $3, description = $4, method = $5, time = $6, time_prep = $7, user_id = $2, image = $8, date = $9, portions = $10, portions_note = $11 WHERE id = $1 AND user_id = $2",
          [
            id,
            userId,
            recipe.title,
            recipe.description,
            "[" + recipe.method.map(s => "\"" + s.description + "\"") + "]",
            recipe.time,
            recipe.timePrep,
            recipe.image,
            date,
            recipe.portions,
            recipe.portionsNote
          ], (err) => {
            if (shouldAbort(err)) return
            let error = false

            if (!error && ingredients.toDelete.length > 0) {
              client.query(
                "DELETE FROM recipe_ingredient WHERE id = ANY($1::bigint[])",
                [ingredients.toDelete.map(i => i.recipeIngredientId)],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error && tags.toDelete.length > 0) {
              client.query(
                "DELETE FROM recipe_tag WHERE recipe_id = $1 AND tag_id = ANY($2::bigint[])",
                [id, tags.toDelete.map(i => i.id)],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error && equipment.toDelete.length > 0) {
              client.query(
                "DELETE FROM recipe_equipment WHERE recipe_id = $1 AND equipment_id = ANY($2::bigint[])",
                [id, equipment.toDelete.map(i => i.id)],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error && ingredients.toUpdate.length > 0) {
              const valuesToUpdate = ingredients.toUpdate.map(i =>
                [
                  i.recipeIngredientId,
                  i.ingredient.id,
                  (i.measure ?? null),
                  i.unit.id,
                  i.note,
                  i.section.title,
                  i.section.order,
                  i.order
                ]
              )

              client.query(
                format(
                  "UPDATE recipe_ingredient SET ingredient_id = t.ingredient_id::bigint, measure = t.measure::real, unit_id = t.unit_id::bigint, note = t.note, section = t.section, section_order = t.section_order::int, \"order\" = t.order::int FROM (VALUES %L) AS t(id, ingredient_id, measure, unit_id, note, section, section_order, \"order\") WHERE t.id::bigint = recipe_ingredient.id", valuesToUpdate),
                [],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error && ingredients.toAdd.length > 0) {
              const valuesToAdd = ingredients.toAdd.map(i =>
                [
                  id,
                  i.ingredient.id,
                  (i.measure ?? null),
                  i.unit.id,
                  i.note,
                  i.section.title,
                  i.section.order,
                  i.order
                ]
              )

              client.query(
                format("INSERT INTO recipe_ingredient (recipe_id, ingredient_id, measure, unit_id, note, section, section_order, \"order\") VALUES %L", valuesToAdd),
                [],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error && equipment.toAdd.length > 0) {
              client.query(
                format("INSERT INTO recipe_equipment (recipe_id, equipment_id) VALUES %L", equipment.toAdd.map(e => [id, e.id])),
                [],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error && tags.toAdd.length > 0) {
              client.query(
                format("INSERT INTO recipe_tag (recipe_id, tag_id) VALUES %L", tags.toAdd.map(t => [id, t.id])),
                [],
                (err) => {
                  if (shouldAbort(err)) error = true
                })
            }

            if (!error) {
              client.query('COMMIT', (err) => {
                if (err) {
                  console.error('Error committing transaction', err.stack)
                  response.sendStatus(500)
                }
                response.sendStatus(200)
              })
            }
            done()
          })
      })
    })
  })

  /**
   * remove recipe (current user has to be its author)
   * params:
   *   id:
   *     description: recipe id
   */
  api.delete("/recipes/:id", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const id = parseInt(request.params.id)

    pool.connect((err, client, done) => {
      const shouldAbort = (err) => {
        if (err) {
          console.error('Error in transaction', err.stack)
          client.query('ROLLBACK', (err) => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            response.sendStatus(500)
          })
        }
        return !!err
      }

      client.query('BEGIN', (err) => {
        if (shouldAbort(err)) return
        // recipe_ingredient, recipe_tag, recipe_equipment are on delete cascade
        client.query(
          "DELETE FROM recipe WHERE id = $1 AND user_id = $2",
          [id, userId],
          (err) => {
            if (shouldAbort(err)) return

            client.query('COMMIT', (err) => {
              if (err) {
                console.error('Error committing transaction', err.stack)
                response.sendStatus(500)
              }
              done()
              response.sendStatus(200)
            })
          })
      })
    })
  })
}