import {Tag} from "../types";
import {pool} from "../index";

module.exports = (api) => {
  /**
   * get all tags
   */
  api.get("/tag", (request, response) => {
    pool.query(
      "SELECT * FROM tag LEFT JOIN (SELECT tag_id, count(recipe_id) FROM tag INNER JOIN recipe_tag rt ON tag.id = rt.tag_id GROUP BY tag_id) AS c ON tag.id = c.tag_id ORDER BY c.count DESC NULLS LAST",
      [],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get tags by ids
   * params:
   *   ids:
   *     description: tag ids list
   */
  api.get("/tag/:ids", (request, response) => {
    const ids = request.params.ids.split(",")

    pool.query(
      "SELECT * FROM tag WHERE id = ANY($1::int[])",
      [ids],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new tag
   * body:
   *   type: {
   *     name:
   *       type: string
   *       description: tag name
   *   }
   */
  api.post("/tag", (request, response) => {
    const tag: Tag = request.body

    pool.query(
      "INSERT INTO tag (name) VALUES ($1)",
      [tag.name],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * update tag
   * params:
   *   id:
   *     description: tag id
   * body:
   *   type: {
   *     name:
   *       type: string
   *       description: tag name
   *   }
   */
  api.put("/tag/:id", (request, response) => {
    const tag: Tag = request.body
    const id = parseInt(request.params.id)

    pool.query(
      "UPDATE tag SET name = $2 WHERE id = $1",
      [
        id,
        tag.name
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete tag
   * params:
   *   id:
   *     description: tag id
   */
  api.delete("/tag/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "DELETE FROM tag WHERE id = $1",
      [id],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })
}