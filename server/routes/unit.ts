import {Unit} from "../types";
import {pool} from "../index";

module.exports = (api) => {
  /**
   * get all units
   */
  api.get("/unit", (request, response) => {
    pool.query(
      "SELECT * FROM unit",
      [],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get units by ids
   * params:
   *   ids:
   *     description: unit ids list
   */
  api.get("/unit/:ids", (request, response) => {
    const ids = request.params.ids.split(",")

    pool.query(
      "SELECT * FROM unit WHERE id = ANY($1::int[])",
      [ids],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new unit
   * body:
   *   type: {
   *     symbol:
   *       type: string
   *       description: symbol for the unit
   *     nameOne:
   *       type: string | null
   *       description: name form when measure is 1
   *     nameFew:
   *       type: string | null
   *       description: name form when measure is 2 - 4
   *     nameMany:
   *       type: string | null
   *       description: name form when measure is 5 or more
   *     namePart:
   *       type: string | null
   *       description: name form when measure is part
   *   }
   */
  api.post("/unit", (request, response) => {
    const unit: Unit = request.body

    pool.query(
      "INSERT INTO unit (symbol, name_one, name_few, name_many, name_part) VALUES ($1, $2, $3, $4, $5)",
      [
        unit.symbol,
        unit.nameOne,
        unit.nameFew,
        unit.nameMany,
        unit.namePart
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(201)
      }
    )
  })

  /**
   * update unit
   * params:
   *   id:
   *     description: unit id
   * body:
   *   type: {
   *     symbol:
   *       type: string
   *       description: symbol for the unit
   *     nameOne:
   *       type: string | null
   *       description: name form when measure is 1
   *     nameFew:
   *       type: string | null
   *       description: name form when measure is 2 - 4
   *     nameMany:
   *       type: string | null
   *       description: name form when measure is 5 or more
   *     namePart:
   *       type: string | null
   *       description: name form when measure is part
   *   }
   */
  api.put("/unit/:id", (request, response) => {
    const unit: Unit = request.body
    const id = parseInt(request.params.id)

    pool.query(
      "UPDATE unit SET symbol = $2, name_one = $3, name_few = $4, name_many = $5, name_part = $6 WHERE id = $1",
      [
        id,
        unit.symbol,
        unit.nameOne,
        unit.nameFew,
        unit.nameMany,
        unit.namePart
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * delete unit
   * params:
   *   id:
   *     description: unit id
   */
  api.delete("/unit/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "DELETE FROM unit WHERE id = $1",
      [id],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })
}