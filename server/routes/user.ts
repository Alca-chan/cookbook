import {User} from "../types";
import {pool} from "../index";
import bcrypt = require('bcryptjs');

module.exports = (api, passport) => {
  /**
   * get all users
   */
  api.get("/users", (request, response) => {
    pool.query(
      "SELECT * FROM \"user\"",
      [],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get user by id
   * params:
   *   id:
   *     description: user id
   */
  api.get("/users/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "SELECT * FROM \"user\" WHERE id = $1",
      [id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get user by username
   * params:
   *   name:
   *     description: username of the user
   */
  api.get("/users/username/:name", (request, response) => {
    const name = request.params.name

    pool.query(
      "SELECT * FROM \"user\" WHERE username = $1",
      [name],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * get user by email
   * params:
   *   id:
   *     description: user's email
   */
  api.get("/users/email/:email", (request, response) => {
    const email = request.params.email

    pool.query(
      "SELECT * FROM \"user\" WHERE email = $1",
      [email],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      }
    )
  })

  /**
   * create new user
   * body:
   *   type: {
   *     username:
   *       type: string
   *       description: new user's username
   *     email:
   *       type: string
   *       description: new user's email
   *     password:
   *       type: string
   *       description: new user's password
   *   }
   */
  api.post("/users", (request, response) => {
    const user: User = request.body

    pool.query(
      "INSERT INTO \"user\" (username, email, password) VALUES ($1, $2, $3) RETURNING *",
      [user.username, user.email, user.password],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(201).json(results.rows)
      }
    )
  })

  /**
   * update email for current user
   * body:
   *   type: {
   *     email:
   *       type: string
   *       description: user's new email
   *     password:
   *       type: string
   *       description: user's password
   *   }
   */
  api.put("/users/email", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const email = request.body.newEmail
    const password = request.body.password

    pool.query(
      "SELECT * FROM \"user\" WHERE id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }

        if (results.rows.length !== 1) {
          response.send("Could not authenticate")
          return
        }
        const user = results.rows[0]
        bcrypt.compare(password, user.password, (err, result) => {
          if (err) {
            throw error
          }
          if (!result) {
            response.send("Wrong password")
            return
          }

          pool.query(
            "UPDATE \"user\" SET email = $2 WHERE id = $1",
            [
              userId,
              email
            ],
            (error) => {
              if (error) {
                throw error
              }
              response.send("Email was successfully changed")
            }
          )
        })
      }
    )
  })

  /**
   * update password for current user
   * body:
   *   type: {
   *     password:
   *       type: string
   *       description: user's old password
   *     newPassword:
   *       type: string
   *       description: user's new password
   *   }
   */
  api.put("/users/password", (request, response) => {
    if (!request.user) {
      response.sendStatus(401)
      return
    }

    const userId = parseInt(request.user.id)
    const password = request.body.password
    const newPassword = request.body.newPassword

    pool.query(
      "SELECT * FROM \"user\" WHERE id = $1",
      [userId],
      (error, results) => {
        if (error) {
          throw error
        }

        if (results.rows.length !== 1) {
          response.send("Could not authenticate")
          return
        }
        const user = results.rows[0]
        bcrypt.compare(password, user.password, async (err, result) => {
          if (err) {
            throw error
          }
          if (!result) {
            response.send("Wrong password")
            return
          }

          const hashedPassword = await bcrypt.hash(newPassword, 10)

          pool.query(
            "UPDATE \"user\" SET password = $2 WHERE id = $1",
            [
              userId,
              hashedPassword
            ],
            (error) => {
              if (error) {
                throw error
              }
              response.send("Password was successfully changed")
            }
          )
        })
      }
    )
  })

  /**
   * update user
   * params:
   *   id:
   *     description: user's id
   * body:
   *   type: {
   *     username:
   *       type: string
   *       description: user's new username
   *     email:
   *       type: string
   *       description: user's new email
   *     password:
   *       type: string
   *       description: user's new password
   *   }
   */
  api.put("/users/:id", (request, response) => {
    const user: User = request.body
    const id = parseInt(request.params.id)

    pool.query(
      "UPDATE \"user\" SET username = $2, email = $3, password = $4 WHERE id = $1",
      [
        id,
        user.username,
        user.email,
        user.password
      ],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * update user by id
   * params:
   *   id:
   *     description: user's id
   */
  api.delete("/users/:id", (request, response) => {
    const id = parseInt(request.params.id)

    pool.query(
      "DELETE FROM \"user\" WHERE id = $1",
      [id],
      (error) => {
        if (error) {
          throw error
        }
        response.sendStatus(200)
      }
    )
  })

  /**
   * login user with passport.js local strategy
   */
  api.post('/login', (request, response, next) => {
    passport.authenticate('local-login', {session: true}, (error, user) => {
      if (error) {
        throw error
      }
      if (!user) {
        response.send("Wrong username or password")
      } else {
        request.logIn(user, (error) => {
          if (error) {
            throw error
          }
          response.send("Authentication successful")
        })
      }
    })(request, response, next)
  })

  /**
   * register new user with passport.js local strategy
   */
  api.post('/register', (request, response, next) => {
    passport.authenticate('local-register', {session: true}, (error, user, info) => {
      if (error) {
        throw error
      }
      if (!user) {
        response.send(info)
      } else {
        request.logIn(user, (error) => {
          if (error) {
            throw error
          }
          response.send("Registration successful")
        })
      }
    })(request, response, next)
  })

  /**
   * get current user
   */
  api.get('/user', (request, response) => {
    response.send(request.user)
  })

  /**
   * logout current user
   */
  api.get('/logout', (request, response) => {
    request.logout()
    response.send(request.user)
  })
}