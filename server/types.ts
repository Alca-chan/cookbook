import {FilledIngredientMeasure} from "../client/src/api/types";

export type Ingredient = {
  id?: number,
  nameOne: string | null,
  nameFew: string | null,
  nameMany: string | null,
  nameUnit: string | null,
  namePart: string | null,
  nameBase: string | null,
  energyKj: number | null,
  energyKcal: number | null,
  fat: number | null,
  saturatedFattyAcid: number | null,
  monounsaturatedFattyAcid: number | null,
  polyunsaturatedFattyAcid: number | null,
  carbohydrate: number | null,
  sugar: number | null,
  polyol: number | null,
  starch: number | null,
  fiber: number | null,
  protein: number | null,
  salt: number | null
}

export type Recipe = {
  id?: number,
  user: User | undefined,
  title: string,
  description: string,
  ingredients: IngredientsSection[],
  method: MethodStep[],
  time: number,
  timePrep: number,
  tags: Tag[],
  votes: number,
  requiredEquipment: Equipment[],
  image: File | undefined,
  date: string,
  portions: number,
  portionsNote: string
}

export type MethodStep = {
  order: number,
  description: string
}

export type Tag = {
  id?: number,
  name: string
}

export type Equipment = {
  id?: number,
  name: string
}

export type IngredientsSection = {
  title?: string,
  ingredients: IngredientMeasure[]
}

export type IngredientMeasure = {
  order: number,
  recipeIngredientId: string,
  ingredient: Ingredient,
  measure: number | undefined,
  unit: Unit,
  note: string,
  section: { title: string, order: number }
}

export type Unit = {
  id?: number,
  symbol: string,
  nameOne: string,
  nameFew: string,
  nameMany: string,
  namePart: string
}

export type CartItemDB = {
  id?: number,
  recipeId: number,
  userId: number,
  measure: string,
  ingredient: string,
  recipeIngredientId: number | null,
  done: boolean,
  order: number,
  date: string | null,
  title: string | null
}

export type CartItem = {
  id: string,
  recipeId: number | null,
  measure: string,
  ingredient: string,
  recipeIngredientId: string | null,
  done: boolean,
  order: number,
  date: string | undefined,
  title: string | null
}

export type CartItemToAdd = {
  recipeId: string | null,
  measure: string,
  ingredient: string,
  recipeIngredientId: string | null,
  order: number,
  date: string | undefined,
  title: string | null
}

export type User = {
  id: string,
  username: string,
  email: string,
  password: string
}

export type IngredientsStatus = {
  toAdd: FilledIngredientMeasure[],
  toUpdate: IngredientMeasure[]
  toDelete: IngredientMeasure[]
}

export type TagsStatus = {
  toAdd: Tag[],
  toDelete: Tag[]
}

export type EquipmentStatus = {
  toAdd: Equipment[],
  toDelete: Equipment[]
}

export type CalendarDB = {
  recipeId: string,
  date: string,
  portions: number,
  order: number,
  title: string
}