export const getOrderByForRecipe = (order: string): string => {
  let orderBy = "recipe.id"
  if (order === "alphabetic") orderBy = "recipe.title"
  if (order === "date") orderBy = "recipe.date"
  if (order === "time") orderBy = "recipe.time"
  if (order === "timePrep") orderBy = "recipe.time_prep"
  if (order === "votes") orderBy = "recipe.votes DESC"
  return orderBy
}